/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.configmgt.resources;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @description 产品关于信息
 * @date:2021/3/25 /configcenter/product
 */
@Api(value = "产品关于接口")
@Component
@Path("/")
public class ProductInfoResource {
    @Autowired
    private Environment env;

    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public MessageResult<JsonNode> getProductInfo() {
        ObjectNode result = GlobalJacksonObjectMapper.instance().createObjectNode();
        result.put("gaf_version", "3.0.0");
        result.put("gaf_imgr_link", String.format("http://%s:%s",env.getProperty("HOSTIP"), env.getProperty("IMGR_PORT")));
        result.put("gaf_imgr_version", "");
        result.put("gaf_iserver_link", "");
        result.put("gaf_iserver_version", "");
        result.put("support_document_link", "https://www.yuque.com/docs/share/cc17ae90-c470-4471-9b1e-01c0eac35957?# 《GAF V3.0企业版帮助文档》");
        result.put("support_api_link", "/api/docs/api/doc.html#/home");
        result.put("support_qq_group_number", "463398333");
        return MessageResult.successe(JsonNode.class).data(result).build();
    }

}
