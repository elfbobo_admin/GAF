/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.configmgt.resources.root;

import com.supermap.gaf.configmgt.resources.ConfigurationsResource;
import com.supermap.gaf.configmgt.resources.ProductInfoResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;

/**
 * @author dqc
 * @date:2021/3/25
 */
@Path("/")
@Api(value = "配置中心组件")
public class ConfMgtRootResoure {

    @Path("/manager")
    public Class<ConfigurationsResource> configurationsResourceClass() {
        return ConfigurationsResource.class;
    }

    @Path("/product")
    public Class<ProductInfoResource> productInfoResourceClass() {
        return ProductInfoResource.class;
    }


}
