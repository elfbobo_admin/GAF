package com.supermap.gaf.configmgt;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


/**
 * @author dqc
 * @date 2022/4/20
 */
public class ExportBasedataDirectory {
    public static void main(String[] args) throws Exception{
//        String targetPath = "/Users/duke/Downloads/testio/basedata";
        String targetPath = args[0];
        if (StringUtils.isEmpty(targetPath)){
            throw new RuntimeException("无法获取拷贝位置");
        }
        //复制jar出来
        File sourceJarFile = copyJarFileFromClasspath();
        //jar包导出基础数据
        Path path = Paths.get(targetPath);
        File targetDirectory = Files.createDirectories(path).toFile();
        String jarPath = "com/supermap/gaf/base/data";
        exportDirectoryFromJarFile(sourceJarFile, targetDirectory, jarPath);
    }

    /**
     * 将jar复制出来
     * @return
     * @throws Exception
     */
    private static File copyJarFileFromClasspath() throws Exception{
        String basedatJarPath = ResourceUtils.CLASSPATH_URL_PREFIX + "BOOT-INF/lib/gaf-common-base-data-*-SNAPSHOT.jar";
        File targetJarfile = File.createTempFile("gaf-basedata-",".jar");
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource resource = resolver.getResources(basedatJarPath)[0];
        InputStream is = resource.getInputStream(); OutputStream os = new FileOutputStream(targetJarfile);
        copy(is ,os);
        return targetJarfile;
    }


    /**
     * 将jar中的文件夹复制出来
     */
    private static void exportDirectoryFromJarFile(File sourceJarFile, File targetDirectory, String jarPath) throws Exception{
        JarFile jarFile = new JarFile(sourceJarFile);
        for (final Enumeration<JarEntry> e = jarFile.entries(); e.hasMoreElements();) {
            final JarEntry entry = e.nextElement();
            if (entry.getName().startsWith(jarPath)) {
                final String filename = StringUtils.removeStart(entry.getName(), jarPath);
                final File outFile = new File(targetDirectory, filename);
                if (!entry.isDirectory()) {
                    final InputStream entryInputStream = jarFile.getInputStream(entry);
                    if (!outFile.getParentFile().exists()){
                        Files.createDirectories(Paths.get(outFile.getParentFile().toURI()));
                    }
                    copy(entryInputStream ,new FileOutputStream(outFile));
                    entryInputStream.close();
                }
            }
        }
    }


    /**
     * 拷贝
     * @param is
     * @param os
     */
    private static void copy(InputStream is, OutputStream os){
        try {
            int readBytes;
            byte[] buffer = new byte[4096];
            while ((readBytes = is.read(buffer)) > 0) {
                os.write(buffer, 0, readBytes);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            try {
                is.close();
                os.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
