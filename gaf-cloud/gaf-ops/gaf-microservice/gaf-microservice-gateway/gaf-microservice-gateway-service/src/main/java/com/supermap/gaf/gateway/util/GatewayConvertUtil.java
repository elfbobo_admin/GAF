package com.supermap.gaf.gateway.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.supermap.gaf.gateway.GatewayFilterDefinition;
import com.supermap.gaf.gateway.GatewayPredicateDefinition;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import com.supermap.gaf.gateway.commontypes.GatewayRouteEntity;

public class GatewayConvertUtil {

    /**
     * 将前端入参转换为自定义的租户路由实体模型
     *
     * @param gatewayRouteInfoDefinition
     * @return
     */
    public static GatewayRouteEntity assembleGatewayRouteEntity(GatewayRouteDefinition gatewayRouteInfoDefinition) {
        if (null == gatewayRouteInfoDefinition) {
            return null;
        }
        //这里没解析id和时间属性，在resource层里做
        GatewayRouteEntity gatewayRouteEntity = new GatewayRouteEntity();
        gatewayRouteEntity.setId(gatewayRouteInfoDefinition.getId());
        gatewayRouteEntity.setCreateTime(gatewayRouteInfoDefinition.getCreateTime());
        gatewayRouteEntity.setUpdateTime(gatewayRouteInfoDefinition.getUpdateTime());
        gatewayRouteEntity.setRouteId(gatewayRouteInfoDefinition.getRouteId());
        gatewayRouteEntity.setRouteUri(gatewayRouteInfoDefinition.getUri());
        gatewayRouteEntity.setRouteOrder(gatewayRouteInfoDefinition.getOrder());
        gatewayRouteEntity.setEnable(gatewayRouteInfoDefinition.getEnable());
        gatewayRouteEntity.setPredicates(JSON.toJSONString(gatewayRouteInfoDefinition.getPredicates()));
        gatewayRouteEntity.setFilters(JSON.toJSONString(gatewayRouteInfoDefinition.getFilters()));
        return gatewayRouteEntity;
    }

    /**
     * 将数据库中信息转换为自定义的网关路由信息，便于展示
     *
     * @param gatewayRouteEntity
     * @return
     */
    public static GatewayRouteDefinition assembleGatewayRouteDefinition(GatewayRouteEntity gatewayRouteEntity) {
        if (null == gatewayRouteEntity) {
            return null;
        }
        GatewayRouteDefinition gatewayRouteDefinition = new GatewayRouteDefinition();
        gatewayRouteDefinition.setId(gatewayRouteEntity.getId());
        gatewayRouteDefinition.setEnable(gatewayRouteEntity.getEnable());
        gatewayRouteDefinition.setRouteId(gatewayRouteEntity.getRouteId());
        gatewayRouteDefinition.setCreateTime(gatewayRouteEntity.getCreateTime());
        gatewayRouteDefinition.setUpdateTime(gatewayRouteEntity.getUpdateTime());
        gatewayRouteDefinition.setPredicates(JSONArray.parseArray(gatewayRouteEntity.getPredicates(), GatewayPredicateDefinition.class));
        gatewayRouteDefinition.setFilters(JSONArray.parseArray(gatewayRouteEntity.getFilters(), GatewayFilterDefinition.class));
        gatewayRouteDefinition.setUri(gatewayRouteEntity.getRouteUri());
        gatewayRouteDefinition.setOrder(gatewayRouteEntity.getRouteOrder());
        return gatewayRouteDefinition;
    }

}
