/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.gateway.util;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.charset.StandardCharsets;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.BEARER;


/**
 * @author : duke
 * @date:2021/3/25
 * @since 2020/4/18 2:17 PM
 */
public class GafFluxUtils {

    public static final String HTML_CONTENT_FORMAT = "\n" +
            "<html>\n" +
            "<head><title>访问失败</title></head>\n" +
            "<body bgcolor=\"white\">\n" +
            "<center><h1>%s</h1></center>\n" +
            "<hr></body>\n" +
            "</html>";

    public static Mono<Void> error(ServerWebExchange exchange, String content, HttpStatus httpStatus, MediaType contentType) {
        ServerHttpResponse response = exchange.getResponse();
        byte[] bits = content.getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        response.setStatusCode(httpStatus);
        // 指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, contentType.toString());
        return response.writeWith(Mono.just(buffer));
    }
    /**
     * 得到无访问权限响应体
     * 用于未认证返回
     * @param exchange
     * @return
     */
    public static Mono<Void> unAuth(ServerWebExchange exchange, String message){
        MessageResult<String> content = MessageResult.failed(String.class).status(HttpStatus.UNAUTHORIZED.value()).message(message).build();
        return error(exchange,JSON.toJSONString(content),HttpStatus.UNAUTHORIZED,MediaType.APPLICATION_JSON_UTF8);
    }

    /**
     * 得到403 forbidden无访问权限响应体
     *
     * @param exchange
     * @return
     */
    public static Mono<Void> forbidden(ServerWebExchange exchange, String message){
        MessageResult<String> content = MessageResult.failed(String.class).status(HttpStatus.FORBIDDEN.value()).message(message).build();
        return error(exchange,JSON.toJSONString(content),HttpStatus.FORBIDDEN,MediaType.APPLICATION_JSON_UTF8);
    }

    /**
     * 重定向
     *
     * @param exchange
     * @param url
     * @return
     */
    public static Mono<Void> redirectTo(ServerWebExchange exchange, String url) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.FOUND);
        response.getHeaders().setLocation(URI.create(url));
        return response.writeAndFlushWith(Mono.empty());
    }


    ///**
    // * request中获取认证参数
    // *
    // * @param request
    // * @return
    // */
    //public static AuthenticationParam getAuthenticationParamByServerHttpRequest(ServerHttpRequest request) {
    //    AuthenticationParam param = null;
    //
    //    String authorization = request.getHeaders().getFirst(AUTHORIZATION);
    //    HttpCookie cookie = request.getCookies().getFirst(CUSTOM_LOGIN_SESSION_NAME);
    //
    //    String cookieVal = cookie == null ? null : cookie.getValue();
    //
    //    if (!StringUtils.isEmpty(cookieVal) || !StringUtils.isEmpty(authorization)) {
    //        param = new AuthenticationParam();
    //        param.setShortCredential(cookieVal);
    //        param.setJwtToken(authorization);
    //    }
    //    return param;
    //}

    public static String removeTokenBeareHead(String token) {
        if (token != null && token.startsWith(BEARER)) {
            token = token.substring(BEARER.length()).trim();
        }
        return token;
    }



//    /**
//     * WebSession获取SecurityContext
//     * @param session
//     * @return
//     */
//    public static SecurityContext getSecurityContext(WebSession session){
//        return (SecurityContext) session.getAttributes().get(SPRING_SECURITY_CONTEXT_KEY);
//    }
}
