/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.gateway.filters;

import com.supermap.gaf.extend.commontypes.AuthenticationParam;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.extend.commontypes.MessageResult;
import com.supermap.gaf.extend.spi.ValidateAuthentication;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.*;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

/**
 * 注意： 该代码对应gaf-boot中的同名的filter,功能逻辑等应该保持一致
 * <p>
 * 此过滤器提供用户获取认证信息的逻辑
 * 1.获取认证信息
 *     静态资源和公共资源不用获取
 *
 * @author : duke
 * @date:2021/3/25
 * @since 2020/11/23 3:44 PM
 */
@Slf4j
@Component
public class XgatewayAuthenticationQueryFilter implements GlobalFilter, Ordered {


    @Autowired
    private ValidateAuthentication validateAuthentication;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ExchangeAuthenticationAttribute attribute = exchange.getAttribute(EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME);
        if (attribute.getIsPublicUrl()) {
            return chain.filter(exchange);
        }

        AuthenticationParam  param = extra(exchange.getRequest());
        MessageResult<AuthenticationResult> mr = this.validateAuthentication.doValidateAuthentication(param);
        if (mr != null && mr.isSuccessed()) {
            attribute.setAuthenticationResult(mr.getData());
        }
        return chain.filter(exchange);
    }

    /**
     * 从http请求中提取 所需的认证参数
     * 例如将cookie 提取到  AuthenticationParam 的 shortCredential中
     * 或者将 header中的  token  取到  AuthenticationParam 的 shortCredential中
     * 或者如果有jwt,也可以直接提取,用于后续直接验签解析
     *
     * @param request
     * @return
     */
    private AuthenticationParam extra(ServerHttpRequest request) {
        AuthenticationParam param = null;
        String authorization = request.getHeaders().getFirst(AUTHORIZATION);
        HttpCookie cookie = request.getCookies().getFirst(CUSTOM_LOGIN_SESSION_NAME);

        String cookieVal = cookie == null ? null : cookie.getValue();

        if (!StringUtils.isEmpty(cookieVal) || !StringUtils.isEmpty(authorization)) {
            param = new AuthenticationParam();
            param.setShortCredential(cookieVal);
            param.setJwtToken(authorization);
            return  param;
        }
        return null;
    }

    @Override
    public int getOrder() {
        return GATEWAY_AUTHENTICATION_QUERY_FILTER_ORDER;
    }
}
