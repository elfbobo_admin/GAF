package com.supermap.gaf.gateway.client;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignContext;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;


/**
 * webflux下获取能自动注入cookie的client
 * @author  zrc
 */
@Component
public class WebfluxFeignClientFactory {

    @Autowired
    private FeignContext feignContext;

    /**
     * Get client t.
     *
     * @param <T>      the type parameter
     * @param apiType  the api type
     * @param exchange the exchange
     * @return the t
     */
    public <T> T getClient(Class<T> apiType, ServerWebExchange exchange){
        T re = exchange.getAttribute(apiType.getName());
        if(re!=null){
            return re;
        }
        FeignClient feignClient = apiType.getAnnotation(FeignClient.class);
        String context = feignClient.contextId();
        String url = StringUtils.isEmpty(feignClient.url())?"http://"+feignClient.name():feignClient.url();
        re = Feign.builder().encoder(feignContext.getInstance(context, Encoder.class))
                .decoder(feignContext.getInstance(context, Decoder.class))
                .contract(feignContext.getInstance(context, Contract.class))
                .client(feignContext.getInstance(context, Client.class))
                .requestInterceptor(new JWTTokenClientFeignInterceptor(exchange.getRequest()))
                .target(apiType,url);
        exchange.getAttributes().put(apiType.getName(),re);
        return re;
    }
}
