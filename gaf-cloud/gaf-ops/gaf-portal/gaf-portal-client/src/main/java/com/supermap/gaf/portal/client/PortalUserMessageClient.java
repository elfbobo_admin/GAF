package com.supermap.gaf.portal.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.portal.entity.po.UserMessage;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Component
@FeignClient(name = "GAF-PORTAL", contextId = "PortalUserMessageClient")
public interface PortalUserMessageClient {

    @PostMapping("/portal/user/message")
    MessageResult<Void> insertUserMessage(@RequestBody UserMessage userMessage);

}
