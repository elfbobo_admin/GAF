package com.supermap.gaf.portal.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.portal.entity.vo.UserMessageSelectVo;

import java.util.List;

/**
 * 用户消息通知表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface UserMessageService {
	
	/**
    * 根据id查询用户消息通知表
    * @return
    */
    UserMessage getById(String userMessageId);
	
	/**
     * 分页条件查询
     * @param userMessageSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<UserMessage> listByPageCondition(UserMessageSelectVo userMessageSelectVo, int pageNum, int pageSize);

    /**
     * 不分页条件查询
     * @param userMessageSelectVo 查询条件
     * @return 分页对象
     */
    List<UserMessage> listByCondition(UserMessageSelectVo userMessageSelectVo);


    /**
    * 新增用户消息通知表
    * @return 新增的userMessage
    */
    UserMessage insertUserMessage(UserMessage userMessage);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<UserMessage> userMessages);

    /**
    * 删除用户消息通知表
    * 
    */
    void deleteUserMessage(String userMessageId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> userMessageIds);
    /**
    * 更新用户消息通知表
    * @return 更新后的userMessage
    */
    UserMessage updateUserMessage(UserMessage userMessage);
    
}
