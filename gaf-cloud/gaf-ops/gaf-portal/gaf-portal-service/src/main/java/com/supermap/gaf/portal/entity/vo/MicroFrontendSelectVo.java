package com.supermap.gaf.portal.entity.vo;
import com.supermap.gaf.portal.entity.po.MicroFrontend;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;

import javax.ws.rs.QueryParam;

/**
 * 微前端应用 条件查询实体
 * @author dqc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("微前端应用 条件查询实体")
public class MicroFrontendSelectVo {
    private static final long serialVersionUID = -1L;
    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = MicroFrontend.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = MicroFrontend.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @StringRange(entityClass = MicroFrontend.class)
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("microFrontendId")
    @ApiModelProperty("主键")
    private String microFrontendId;
    @QueryParam("name")
    @ApiModelProperty("微应用名称")
    private String name;
    @QueryParam("entry")
    @ApiModelProperty("微应用入口")
    private String entry;
    @QueryParam("container")
    @ApiModelProperty("微应用挂载节点")
    private String container;
    @QueryParam("activeRule")
    @ApiModelProperty("微应用触发的路由规则")
    private String activeRule;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}
