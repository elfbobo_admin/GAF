/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.portal.menu.service.impl;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.extend.commontypes.Role;
import com.supermap.gaf.extend.commontypes.User;
import com.supermap.gaf.extend.spi.UserAuthoritiesService;
import com.supermap.gaf.portal.menu.service.UserProfileService;
import com.supermap.gaf.security.SecurityUtilsExt;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author:yw
 * @date:2021/3/25
 * @Date 2021-3-12
 **/
@Service
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    private UserAuthoritiesService userAuthoritiesService;

    @Override
    public String queryUserProfile() {
        Map<String, Object> res = new HashMap<String, Object>(16);
        Map<String, Object> user = getUser();
        String userName = user.get("userName").toString();
        res.put("success", StringUtils.equalsIgnoreCase(userName, "") ? false : true);
        res.put("user", user);
        return JSON.toJSONString(res);
    }

    @Override
    public Map<String, Object> getLoginUserInfo() {
        return getUser();
    }

    private Map<String, Object> getUser() {
        Map<String, Object> variables = new HashMap<String, Object>(16);
        User user = SecurityUtilsExt.getUser();

        log.info("user profile:{}", user);
        variables.put("userName", StringUtils.isNotEmpty(user.getUserName()) ? user.getUserName() : "");
        variables.put("realName", StringUtils.isNotEmpty(user.getUserAlias()) ? user.getUserAlias() : "");
        List<Role> roles = userAuthoritiesService.listRoles();
        variables.put("roles", roles);
        variables.put("currentTenantId", "system");
        return variables;
    }
}
