package com.supermap.gaf.portal.entity.po;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import java.io.Serializable;

/**
 * 微前端应用
 * @author dqc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("微前端应用")
public class MicroFrontend implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("主键")
    private String microFrontendId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("微应用名称")
    private String name;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("微应用入口")
    private String entry;
    @ApiModelProperty("微应用挂载节点")
    private String container;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("微应用触发的路由规则")
    private String activeRule;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}