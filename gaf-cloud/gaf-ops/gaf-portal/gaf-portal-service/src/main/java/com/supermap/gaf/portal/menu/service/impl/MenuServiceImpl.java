/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.portal.menu.service.impl;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.authority.commontype.AuthResourceMenu;
import com.supermap.gaf.extend.commontypes.Menu;
import com.supermap.gaf.extend.spi.UserResourcesService;
import com.supermap.gaf.portal.menu.commontypes.MenuInfo;
import com.supermap.gaf.portal.menu.dao.MenuDao;
import com.supermap.gaf.portal.menu.service.MenuService;
import com.supermap.gaf.portal.menu.utils.MenuManager;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author:yw
 * @date:2021/3/25
 * @Date 2021-3-12
 **/
@Service
@Qualifier("portal")
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuDao menuDao;

    @Autowired
    private UserResourcesService userResourcesService;

    @Override
    public String addMenu(MenuInfo menu) {
        return null;
    }

    @Override
    public String deleteMenu(String menuId) {
        return null;
    }

    @Override
    public String updateMenu(String menuId, MenuInfo menu) {
        return null;
    }

    @Override
    public String queryMenu(String menuId) {
        return null;
    }

    private List<MenuInfo> getMenuArrPermession() {
        List<MenuInfo> menuArr = menuDao.queryMenus();
        Map<String, String> menuMap = MenuManager.getMenuMapping(menuArr);
        Set<String> permessionMenus = new HashSet<>();
        boolean permessionAll = false;
        // 这个逻辑可以优化，上边直接获取菜单添加，而不是获取id
        List<MenuInfo> menuArrPermession = new ArrayList<>();
        // menu:
        return menuArrPermession;
    }

    @Override
    public String queryMenu() {
        String msg;
        boolean success = false;
        Map<String, Object> res = new HashMap<String, Object>(16);

        String userName = SecurityUtilsExt.getUserName();

        if (StringUtils.isEmpty(userName)) {
            List<MenuInfo> menuArr = menuDao.queryMenus();
            if (menuArr.size() > 0) {
                success = true;
                msg = "查询菜单成功";
            } else {
                success = false;
                msg = "查询不到菜单信息";
            }
            res.put("menus", menuArr);
            res.put("success", success);
            res.put("msg", msg);
            return JSON.toJSONString(res);
        }
        List<MenuInfo> menuArrPermession = getMenuArrPermession();
        if (menuArrPermession.size() > 0) {
            success = true;
            msg = "查询菜单成功";
        } else {
            success = false;
            msg = "查询不到菜单信息";
        }
        res.put("menus", menuArrPermession);
        res.put("success", success);
        res.put("msg", msg);
        return JSON.toJSONString(res);
    }

    @Override
    public List<MenuInfo> queryMenuFromAuthority() {
        List<Menu> menus = userResourcesService.listMenus();
        if (CollectionUtils.isEmpty(menus)){
            return Collections.emptyList();
        }
        return menus.stream().map(this::convertFrom).collect(Collectors.toList());
    }
    private MenuInfo convertFrom(Menu menu) {
        MenuInfo menuInfo = new MenuInfo();
        menuInfo.setPath("");
        menuInfo.setTarget("0");
        menuInfo.setEmbedUrl("");

        menuInfo.setId(menu.getId());
        if (StringUtils.isEmpty(menu.getPid())) {
            menuInfo.setPid("");
        } else {
            menuInfo.setPid(menu.getPid());
        }

        menuInfo.setName(menu.getName());
        if (StringUtils.isEmpty(menu.getIcon())) {
            menuInfo.setIcon("");
        } else {
            menuInfo.setIcon(menu.getIcon());
        }
        if (null == menu.getOrder()) {
            menuInfo.setOrder(0);
        } else {
            menuInfo.setOrder(menu.getOrder());
        }
        if (!StringUtils.isEmpty(menu.getUrl())) {
            menuInfo.setEmbedUrl(menu.getUrl());
        }
        if (!StringUtils.isEmpty(menu.getPath())) {
            menuInfo.setPath(menu.getPath());
        }
        String equalParam = "1";
        if (equalParam.equals(menu.getTarget())) {
            menuInfo.setTarget(equalParam);
        }

        // embed暂时没用,以后可能有用
        menuInfo.setEmbed(true);
        return menuInfo;
    }


    @Deprecated
    private MenuInfo convertMenu(AuthResourceMenu authResourceMenu) {
        MenuInfo menuInfo = new MenuInfo();
        menuInfo.setPath("");
        menuInfo.setTarget("0");
        menuInfo.setEmbedUrl("");

        menuInfo.setId(authResourceMenu.getResourceMenuId());
        if (StringUtils.isEmpty(authResourceMenu.getParentId())) {
            menuInfo.setPid("");
        } else {
            menuInfo.setPid(authResourceMenu.getParentId());
        }

        menuInfo.setName(authResourceMenu.getName());
        if (StringUtils.isEmpty(authResourceMenu.getIcon())) {
            menuInfo.setIcon("");
        } else {
            menuInfo.setIcon(authResourceMenu.getIcon());
        }
        if (null == authResourceMenu.getSortSn()) {
            menuInfo.setOrder(0);
        } else {
            menuInfo.setOrder(authResourceMenu.getSortSn());
        }
        if (!StringUtils.isEmpty(authResourceMenu.getUrl())) {
            menuInfo.setEmbedUrl(authResourceMenu.getUrl());
        }
        if (!StringUtils.isEmpty(authResourceMenu.getPath())) {
            menuInfo.setPath(authResourceMenu.getPath());
        }
        String equalParam = "1";
        if (equalParam.equals(authResourceMenu.getTarget())) {
            menuInfo.setTarget(equalParam);
        }

        // embed暂时没用,以后可能有用
        menuInfo.setEmbed(true);
        return menuInfo;
    }


    @Override
    public String importMenus(String flag, List<MenuInfo> arr) {
        return null;
    }
}
