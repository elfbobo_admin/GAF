package com.supermap.gaf.portal.dao;
import com.supermap.gaf.portal.entity.po.MicroFrontend;
import com.supermap.gaf.portal.entity.vo.MicroFrontendSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 微前端应用数据访问类
 * @author dqc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface MicroFrontendMapper{
	/**
     * 根据主键 microFrontendId 查询
     *
	 */
    MicroFrontend select(@Param("microFrontendId") String microFrontendId);
	
	/**
     * 多条件查询
     * @param microFrontendSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<MicroFrontend> selectList(MicroFrontendSelectVo microFrontendSelectVo);

    /**
     * 新增
     *
	 */
    void insert(MicroFrontend microFrontend);
	
	/**
     * 批量插入
     * 
	 */
	void batchInsert(@Param("list") Collection<MicroFrontend> microFrontends);
	
	/**
     * 批量删除
     * 
	 */
    int batchDelete(@Param("list") Collection<String> microFrontendIds);

	/**
     * 刪除
     *
	 */
    int delete(@Param("microFrontendId") String microFrontendId);

    /**
    * 更新
    * 
    **/
    int update(MicroFrontend microFrontend);
	/**
	 * 选择性更新
	 *
	 **/
	int updateSelective(MicroFrontend microFrontend);
}
