package com.supermap.gaf.portal.dao;
import java.util.*;

import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.portal.entity.vo.UserMessageSelectVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 用户消息通知表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface UserMessageMapper{
	/**
     * 根据主键 userMessageId查询
     * 
	 */
    UserMessage select(@Param("userMessageId") String userMessageId);
	
	/**
     * 多条件查询
     * @param userMessageSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<UserMessage> selectList(UserMessageSelectVo userMessageSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(UserMessage userMessage);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<UserMessage> userMessages);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> userMessageIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("userMessageId") String userMessageId);
    /**
     * 更新
     *
	 */
    int update(UserMessage userMessage);
}
