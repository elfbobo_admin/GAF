package com.supermap.gaf.portal.resources;
import io.swagger.annotations.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.supermap.gaf.portal.service.MicroFrontendService;
import com.supermap.gaf.portal.entity.po.MicroFrontend;
import com.supermap.gaf.portal.entity.vo.MicroFrontendSelectVo;
import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.groups.ConvertGroup;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;


/**
 * 微前端应用接口
 * @author dqc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "微前端应用接口")
public class MicroFrontendResource{
    @Autowired
    private MicroFrontendService microFrontendService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询微前端应用", notes = "根据id查询微前端应用")
	@Path("/{microFrontendId}")
    public MessageResult<MicroFrontend> getById(@PathParam("microFrontendId")String microFrontendId){
        MicroFrontend microFrontend = microFrontendService.getById(microFrontendId);
		return MessageResult.data(microFrontend).message("查询成功").build();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询微前端应用", notes = "分页条件查询微前端应用")
    public MessageResult<Page<MicroFrontend>> pageList(@Valid @BeanParam MicroFrontendSelectVo microFrontendSelectVo,
										@DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
										@DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<MicroFrontend> page = microFrontendService.listByPageCondition(microFrontendSelectVo, pageNum, pageSize);
		return MessageResult.data(page).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增微前端应用", notes = "新增微前端应用")
    public MessageResult<Void> insertMicroFrontend(@Valid @ConvertGroup(to= AddGroup.class)MicroFrontend microFrontend){
        microFrontendService.insertMicroFrontend(microFrontend);
		return MessageResult.successe(Void.class).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除微前端应用", notes = "根据id删除微前端应用")
	@Path("/{microFrontendId}")
    public MessageResult<Integer> deleteMicroFrontend(@PathParam("microFrontendId")String microFrontendId){
        int re = microFrontendService.deleteMicroFrontend(microFrontendId);
		return MessageResult.data(re).message("删除操作成功").build();
    }


	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除微前端应用", notes = "批量删除微前端应用")
    public MessageResult<Integer> batchDelete(@NotEmpty List<String> microFrontendIds){
        int re = microFrontendService.batchDelete(microFrontendIds);
		return MessageResult.data(re).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新微前端应用", notes = "根据id更新微前端应用")
	@Path("/{microFrontendId}")
    public MessageResult<Integer> updateMicroFrontend(@Valid @ConvertGroup(to= UpdateGroup.class)MicroFrontend microFrontend,@PathParam("microFrontendId")String microFrontendId){
        microFrontend.setMicroFrontendId(microFrontendId);
        int re = microFrontendService.updateMicroFrontend(microFrontend);
		return MessageResult.data(re).message("更新操作成功").build();
    }

	
	


}
