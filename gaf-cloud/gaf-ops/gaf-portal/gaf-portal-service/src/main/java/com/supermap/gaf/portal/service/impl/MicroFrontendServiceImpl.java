package com.supermap.gaf.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.GafCommonConstant;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.portal.dao.MicroFrontendMapper;
import com.supermap.gaf.portal.entity.po.MicroFrontend;
import com.supermap.gaf.portal.entity.vo.MicroFrontendSelectVo;
import com.supermap.gaf.portal.service.MicroFrontendService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 微前端应用服务实现类
 * @author dqc 
 * @date yyyy-mm-dd
 */
@Service
public class MicroFrontendServiceImpl implements MicroFrontendService{
    
	private static final Logger  log = LoggerFactory.getLogger(MicroFrontendServiceImpl.class);
	
	@Autowired
    private MicroFrontendMapper microFrontendMapper;
	
	@Override
    public MicroFrontend getById(String microFrontendId){
        if(microFrontendId == null){
            throw new IllegalArgumentException("microFrontendId不能为空");
        }
        return  microFrontendMapper.select(microFrontendId);
    }
	
	@Override
    public Page<MicroFrontend> listByPageCondition(MicroFrontendSelectVo microFrontendSelectVo, int pageNum, int pageSize) {
        PageInfo<MicroFrontend> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            microFrontendMapper.selectList(microFrontendSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
	public 	List<MicroFrontend> selectList(MicroFrontendSelectVo microFrontendSelectVo){
		return microFrontendMapper.selectList(microFrontendSelectVo);
	}

	@Override
    public void insertMicroFrontend(MicroFrontend microFrontend){
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		microFrontend.setMicroFrontendId(UUID.randomUUID().toString());
		microFrontend.setContainer(GafCommonConstant.MICRO_FRONTEND_CONTAINER);
		String userName = SecurityUtilsExt.getUserName();
		microFrontend.setCreatedBy(userName);
		microFrontend.setUpdatedBy(userName);
        microFrontendMapper.insert(microFrontend);
    }
	
	@Override
    public int deleteMicroFrontend(String microFrontendId){
		return batchDelete(Arrays.asList(microFrontendId));
    }

	@Override
    public int batchDelete(List<String> microFrontendIds){
		if(!CollectionUtils.isEmpty(microFrontendIds)){
			return microFrontendMapper.batchDelete(microFrontendIds);
		}
		return 0;
    }
	
	@Override
    public int updateMicroFrontend(MicroFrontend microFrontend){
		microFrontend.setUpdatedBy(SecurityUtilsExt.getUserName());
		microFrontend.setContainer(GafCommonConstant.MICRO_FRONTEND_CONTAINER);
		return microFrontendMapper.update(microFrontend);
    }
    
}
