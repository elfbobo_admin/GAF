package com.supermap.gaf.authentication.entity.enums;

/**
 * 第三方登录模式
 * single 表示 登录方式只为一种  当点击登录按钮时，直接跳到后端配置的第三方登录页面
 * mutiple 表示 登录方式为多种 点击登录 ，默认跳转本地登录页，有多种第三方登录方式选择
 */
public enum ThirdPartyLoginModelEnum {
    SINGLE,
    MUTIPLE

}
