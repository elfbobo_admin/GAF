/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.client;

import com.supermap.gaf.authority.entity.AuthorizationParam;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


/**
 * @author : duke
 * @date:2021/3/25
 * @since 2020/11/23 5:43 PM
 */
@FeignClient(name = "GAF-AUTHORITY",contextId = "AuthorizationClient")
@Component
public interface AuthorizationClient {

    /**
     * 请求认证中心进行鉴权验证
     *
     * @return
     */
    @PostMapping(value = "/authority/authorization", consumes = APPLICATION_JSON_VALUE)
    MessageResult<Boolean> authorization(@RequestBody AuthorizationParam authorizationParam);

}



