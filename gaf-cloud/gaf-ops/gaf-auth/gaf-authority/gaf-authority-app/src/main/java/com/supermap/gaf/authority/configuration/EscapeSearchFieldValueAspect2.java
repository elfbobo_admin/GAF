package com.supermap.gaf.authority.configuration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.ws.rs.QueryParam;
import java.lang.reflect.Parameter;

/**
 * @author wxl
 * @since 2021/11/25
 */
@Aspect
@Component
public class EscapeSearchFieldValueAspect2 {

    @Pointcut("execution(public * com.supermap.gaf.authority.resources.*Resource.pageList(..))")
    public void escapeSearchFieldValuePoint2() {}

    @Around("escapeSearchFieldValuePoint2()")
    public Object escapeSearchFieldValue2(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        Parameter[] parameters = methodSignature.getMethod().getParameters();
        int index = getKeyIndex(parameters, "searchFieldValue");
        Object[] args = pjp.getArgs();
        if (index != -1) {
           String value = (String) args[index];
            args[index] = escape(value);
        }
        return pjp.proceed(args);
    }

    private int getKeyIndex(Parameter[] parameters, String key) {
        for (int i = 0; i < parameters.length; i++) {
            QueryParam annotation = parameters[i].getAnnotation(QueryParam.class);
            if (annotation != null) {
                if (key.equalsIgnoreCase(annotation.value())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String escape(String value) {
        if (value == null || value.isEmpty()) return value;
        return value.replaceAll("\\\\", "\\\\\\\\")
                .replaceAll("_", "\\\\_")
                .replaceAll("%", "\\\\%");
    }

}
