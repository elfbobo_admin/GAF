package com.supermap.gaf.authority.service;

import com.supermap.gaf.authority.entity.AuthorizationParam;

public interface AuthorizationService {
    Boolean authorization(AuthorizationParam authorizationParam);
}
