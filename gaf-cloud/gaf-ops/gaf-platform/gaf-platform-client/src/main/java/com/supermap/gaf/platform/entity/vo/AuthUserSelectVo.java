/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.entity.vo;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Pattern;
import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 用户 分页条件查询实体
 *
 * @author dqc
 * @date:2021/3/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户 分页条件查询实体")
@SuperBuilder
public class AuthUserSelectVo  {

    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = AuthUser.class)
    private String searchFieldName;

    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = AuthUser.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = AuthUser.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;

    @QueryParam("userId")
    @ApiModelProperty("用户id")
    private String userId;
    @QueryParam("name")
    @ApiModelProperty("登录用户名")
    private String name;
    @QueryParam("password")
    @ApiModelProperty("密码")
    private String password;
    @QueryParam("realName")
    @ApiModelProperty("真实姓名")
    private String realName;
    @QueryParam("idNumber")
    @ApiModelProperty("身份证号")
    private String idNumber;
    @QueryParam("mobileNumber")
    @ApiModelProperty("手机号")
    private String mobileNumber;
    @QueryParam("email")
    @Pattern(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message = "不满足邮箱格式")
    @ApiModelProperty("邮箱")
    private String email;
    @QueryParam("address")
    @ApiModelProperty("地址")
    private String address;
    @QueryParam("expirationTime")
    @ApiModelProperty("授权截止时间")
    private Date expirationTime;
    @QueryParam("state")
    @ApiModelProperty("状态,是否启用.true启用,false禁用,默认启用")
    private Boolean state;
    @QueryParam("description")
    @ApiModelProperty("描述")
    private String description;
    @QueryParam("lastLoginTime")
    @ApiModelProperty("上次登录时间")
    private Date lastLoginTime;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("修改人")
    private String updatedBy;




}








