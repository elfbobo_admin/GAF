/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.dao;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户mapper
 *
 * @author dqc
 * @date:2021/3/25
 */
@Mapper
@Component
public interface AuthUserMapper {
    /**
     * 根据id查询用户
     *
     * @param userId 用户id
     * @return 用户 未查询到则返回null
     */
    AuthUser select(@Param("userId") String userId);


    /**
     * 批量查询
     * @param userIdList
     * @return
     */
    List<AuthUser> batchSelect(@Param("userIdList") List<String> userIdList);

    /**
     * 根据用户id查询
     *
     * @param userId 用户id
     * @return 用户 返回对象中包含加密后的密码
     */
    AuthUser selectWithPassword(@Param("userId") String userId);


    /**
     * 新增用户
     *
     * @param authUser 用户
     * @return 新增的数量
     */
    int insert(AuthUser authUser);

    /**
     * 批量新增用户
     *
     * @param authUsers 用户集合
     * @return 新增的数量
     */
    int batchInsert(List<AuthUser> authUsers);

    /**
     * 根据id集合批量删除用户
     *
     * @param userIds 用户id集合
     * @return 删除的数量
     */
    int batchDelete(List<String> userIds);

    /**
     * 根据用户id删除用户
     *
     * @param userId 用户id
     * @return 影响的行数即删除的数量
     */
    int delete(@Param("userId") String userId);

    /**
     * 更新用户
     * 使用时请注意 若authUser 属性中postId为null 则会更新为null
     * 其他属性若未null,则不会更新
     *
     * @param authUser 用户
     * @return 影响的行数即更新的数量
     */
    int update(AuthUser authUser);


    /**
     * 组合条件 等值查询
     *
     * @param queryAuthUser 查询参数 至少有一个属性值不为null
     * @return 若未查询到数据则返回空集合
     */
    List<AuthUser> selectByCombination(AuthUser queryAuthUser);

    /**
     * 组合条件 等值查询数量
     *
     * @param queryAuthUser 查询参数 至少有一个属性值不为null
     * @return 若未查询到数据则返回空集合
     */
    Integer countByCombination(AuthUser queryAuthUser);



    /**
     * 条件查询用户集合
     * @param authUserSelectVo
     * @return
     */
    List<AuthUser> selectList(AuthUserSelectVo authUserSelectVo);


}
