/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.resources.root;

import com.supermap.gaf.platform.resources.*;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;

/**
 * @author dqc
 * @date:2021/3/25
 */
@Path("/")
@Api("平台管理组件")
public class RootResource {

    @Path("/auth-users")
    public Class<AuthUserResource> authUserResourceClass() {
        return AuthUserResource.class;
    }

    @Path("/tenants")
    public Class<TenantResource> tenantResource(){
        return  TenantResource.class;
    }


    @Path("/tenant-users")
    public Class<TenantUserResource> tenantUserResource(){
        return  TenantUserResource.class;
    }

}
