package com.supermap.gaf.storage.enums;

public enum TargetType {
    PLATFORM("P"), TENANT("T"),USER("U");
    private String value;

    TargetType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
