/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.sys.mgt.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


/**
 * 系统管理字典相关接口调用
 *
 * @author wxl
 * @date:2021/3/25
 */
@FeignClient(name = "GAF-SYS-MGT"/*,url = "http://localhost:8081"*/, contextId = "SysDatasourceResourceClient")
public interface SysDatasourceResourceClient {

    @GetMapping("/sys-mgt/sys-resource-datasources/{datasourceId}")
    MessageResult<SysResourceDatasource> getById(@PathVariable("datasourceId") String datasourceId);

}
