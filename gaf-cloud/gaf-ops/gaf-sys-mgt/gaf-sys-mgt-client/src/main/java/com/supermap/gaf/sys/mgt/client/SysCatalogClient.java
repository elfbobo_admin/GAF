/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.sys.mgt.client;

import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.Page;
import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import com.supermap.gaf.validator.StringRange;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * 系统管理相关接口调用
 *
 * @author wxl
 * @date:2021/3/25
 */
@FeignClient(name = "GAF-SYS-MGT"/*,url = "http://localhost:8081"*/, contextId = "sysCatalogClient")
public interface SysCatalogClient {

    @GetMapping("/sys-mgt/sys-catalogs/{catalogId}")
    MessageResult<SysCatalog> getById(@PathVariable("catalogId") String catalogId);

    @GetMapping("/sys-mgt/sys-catalogs/condition")
    MessageResult<List<SysCatalog>> list(@SpringQueryMap SysCatalog queryCatalog);

    @PostMapping("/sys-mgt/sys-catalogs")
    MessageResult<SysCatalog> insertSysCatalog(@RequestBody @NotNull SysCatalog sysCatalog);

    @GetMapping("/sys-mgt/sys-catalogs/nodes/type/{type}")
    MessageResult<List<TreeNode>> getNodesByType(@PathVariable("type") String type);

    @PostMapping(value = "/sys-mgt/sys-catalogs/import",produces = {MediaType.APPLICATION_JSON})
    MessageResult<Void> batchImport(@RequestBody List<SysCatalog> sysCatalogs);


    @DeleteMapping(value = "/sys-mgt/sys-catalogs",produces = {MediaType.APPLICATION_JSON})
    MessageResult<Void> batchDelete(@RequestBody List<String> catalogIds, @DefaultValue("true")@RequestParam("recursionAble") Boolean recursionAble);


    @GetMapping("/sys-mgt/sys-catalogs/resource-root-catalogs")
    MessageResult<Page<SysCatalog>> pageListResourceRootCatalog(@StringRange(entityClass = SysCatalog.class) @RequestParam("searchFieldName") String searchFieldName,
                                                                @RequestParam("searchFieldValue") String searchFieldValue,
                                                                @DefaultValue("6") @StringRange(fromEnum = CatalogTypeEnum.class,enumGetter = "value")@RequestParam("type") String type,
                                                                @StringRange(entityClass = SysCatalog.class) @RequestParam("orderFieldName") String orderFieldName,
                                                                @StringRange(value = {"asc", "desc"},ignoreCase = true) @RequestParam("orderMethod") String orderMethod,
                                                                @DefaultValue("1") @RequestParam("pageNum") Integer pageNum,
                                                                @DefaultValue("10") @RequestParam("pageSize") Integer pageSize);
}
