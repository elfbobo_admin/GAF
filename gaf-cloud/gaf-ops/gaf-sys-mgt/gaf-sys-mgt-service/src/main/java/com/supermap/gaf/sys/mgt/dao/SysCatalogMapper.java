/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.sys.mgt.dao;

import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 目录mapper
 *
 * @author wangxiaolong
 * @date:2021/3/25
 */
@Mapper
@Component
public interface SysCatalogMapper {
    /**
     * 根据id查询目录
     *
     * @param catalogId 目录id
     * @return 目录 未查询到则返回null
     */
    SysCatalog select(@Param("catalogId") String catalogId);


    /**
     * 新增目录
     *
     * @param sysCatalog 目录
     * @return 新增的数量
     */
    int insert(SysCatalog sysCatalog);

    /**
     * 批量新增目录
     *
     * @param sysCatalogs 目录集合
     * @return 新增的数量
     */
    int batchInsert(List<SysCatalog> sysCatalogs);

    /**
     * 根据id集合批量删除目录
     *
     * @param catalogIds 目录id集合
     * @return 删除的数量
     */
    int batchDelete(@Param("list") Collection<String> catalogIds);

    /**
     * 根据目录id删除目录
     *
     * @param catalogId 目录id
     * @return 影响的行数即删除的数量
     */
    int delete(@Param("catalogId") String catalogId);

    /**
     * 更新目录
     *
     * @param sysCatalog 目录
     * @return 影响的行数即更新的数量
     */
    int update(SysCatalog sysCatalog);

    /**
     * 单字段等值条件查询 若字段名为null则查询所有数据
     *
     * @param fieldName  字段名
     * @param fieldValue 字段值
     * @return 目录集合 若未查询到则返回空集合
     */
    List<SysCatalog> selectByOneField(@Param("fieldName") String fieldName, @Param("fieldValue") String fieldValue);

    /**
     * 获取所有parentId
     *
     * @return 所有parentId
     */
    Set<String> selectParentIds();

    /**
     * 根据组合条件 更新范围内的有效子目录 不包含边界
     *
     * @param queryCondition 查询条件 注意queryCondition 至少有一个属性不为null
     * @param addOrSub       序号+1 还是 -1
     * @param startSortSn    开始序号
     * @param endSortSn      结束序号
     */
    void updateSortSnByCombination(@Param("queryCondition") SysCatalog queryCondition, @Param("addOrSub") Boolean addOrSub, @Param("startSortSn") Integer startSortSn, @Param("endSortSn") Integer endSortSn);

    /**
     * 根据父id查询有效子目录数
     *
     * @param parentId 父id
     * @return 目录数
     */
    Integer countByParentId(String parentId);

    /**
     * 统计根据组合条件等值查询结果的数量
     * 注意： 至少有一个属性不为null
     *
     * @param queryCatalog 查询参数
     * @return 查询结果的数量
     */
    Integer countByCombination(SysCatalog queryCatalog);

    /**
     * 根据父id集合 查询有效子目录
     *
     * @param parentIds 父id集合
     * @return 目录
     */
    List<SysCatalog> selectByParentIds(@Param("list") Collection<String> parentIds);

    /**
     * 组合条件等值查询
     * 注意： queryCatalog 至少有一个属性不为null
     *
     * @param queryCatalog 查询参数
     * @return 查询结果
     */
    List<SysCatalog> selectByCombination(SysCatalog queryCatalog);

    /**
     * 查询根目录，条件是单字段模糊匹配和目录字段等值匹配
     * 模糊匹配的字段不能和等值匹配的字段冲突
     *
     * @param searchFieldName  字段名
     * @param searchFieldValue 字段值  模糊匹配
     * @param orderFieldName   排序字段名
     * @param orderMethod      排序方法
     * @param queryCatalog     目录查询条件
     * @return 分页条件查询结果
     */
    List<SysCatalog> selectByCondition(@Param("searchFieldName") String searchFieldName,
                                                                           @Param("searchFieldValue") String searchFieldValue,
                                                                           @Param("orderFieldName") String orderFieldName,
                                                                           @Param("orderMethod") String orderMethod,
                                                                           @Param("queryCatalog") SysCatalog queryCatalog);


    /**
     * 根据某个结点以它为根结点的所有层次的子结点
     *
     * @param rootId 目录id
     * @return 所有子目录
     */
    List<SysCatalog> getCatalogTreeListByRootId(@Param("rootId") String rootId);
}
