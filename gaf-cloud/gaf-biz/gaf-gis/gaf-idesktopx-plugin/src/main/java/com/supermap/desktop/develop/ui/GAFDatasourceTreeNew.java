package com.supermap.desktop.develop.ui;

import cn.hutool.core.util.BooleanUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.Datasource;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.Datasources;
import com.supermap.data.EngineType;
import com.supermap.desktop.core.implement.SmPopupMenu;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.utilties.DatasourceUtilities;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.develop.entity.Catalog;
import com.supermap.desktop.develop.entity.GAFDatasourceConnectionInfo;
import com.supermap.desktop.develop.entity.GAFWorkspaceConnectionInfo;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import org.apache.arrow.vector.util.CallBack;
import org.apache.commons.lang.StringUtils;
import scala.tools.nsc.doc.model.diagram.NormalNode;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class GAFDatasourceTreeNew extends GAFDatasourceTree{

    public static final String isStandard = "1";
    public static final String isNotStandard = "0";

    public GAFDatasourceTreeNew() {
    }

    public GAFDatasourceTreeNew(HistoryManager historyManager, JSONArray list) {
        super(historyManager, list);
    }

    @Override
    protected void buildTree(DefaultMutableTreeNode root, JSONArray list) {
        DefaultMutableTreeNode normalNode = new DefaultMutableTreeNode(new Catalog("普通数据源",isNotStandard));
        DefaultMutableTreeNode standardNode = new DefaultMutableTreeNode(new Catalog("标准数据源",isStandard));
        root.add(normalNode);
        root.add(standardNode);

        DefaultMutableTreeNode normalNodeConn = new DefaultMutableTreeNode(new Catalog("数据库型",isNotStandard));
        DefaultMutableTreeNode normalNodeFile = new DefaultMutableTreeNode(new Catalog("文件型",isNotStandard));
        normalNode.add(normalNodeConn);
        normalNode.add(normalNodeFile);

//        DefaultMutableTreeNode standardNodeConn = new DefaultMutableTreeNode("数据库型");
//        DefaultMutableTreeNode standardNodeFile = new DefaultMutableTreeNode("文件型型");
//        standardNode.add(standardNodeConn);
//        standardNode.add(standardNodeFile);

        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                JSONObject node = list.getJSONObject(i);
                GAFDatasourceConnectionInfo connectionInfo = new GAFDatasourceConnectionInfo();
                connectionInfo.setId(node.getString("datasourceId"));
                connectionInfo.setServer(node.getString("addr"));
                connectionInfo.setAlias(node.getString("dsName"));
                connectionInfo.setDatabase(node.getString("dbName"));
                connectionInfo.setUser(node.getString("userName"));
                connectionInfo.setPassword(node.getString("password"));
                try {
                    connectionInfo.setEngineType((EngineType) EngineType.parse(EngineType.class, node.getString("typeCode")));
                } catch (RuntimeException e) {
                    ApplicationContextUtils.getOutput().output(String.format("%s数据源连接信息解析失败，不存在类型%s", node.getString("dsName"), node.getString("typeCode")));
                }
                if (EngineType.SQLPLUS == connectionInfo.getEngineType()) {
                    connectionInfo.setDriver("SQL SERVER");
                }
                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(connectionInfo);
                if(BooleanUtil.isTrue(node.getBoolean("isStandard"))){
                    standardNode.add(newNode);
                }else{
                    if(CommonUtils.isFileTypeSource(connectionInfo)){
                        normalNodeFile.add(newNode);
                    }else{
                        normalNodeConn.add(newNode);
                    }
                }
            }
        }
    }

    @Override
    SmPopupMenu buildCatalogPopMenu(DefaultMutableTreeNode treeNode) {
        return super.buildCatalogPopMenu(treeNode);
    }
}
