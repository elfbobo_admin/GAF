package com.supermap.desktop.develop.ui;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.controls.ui.controls.*;
import com.supermap.desktop.controls.ui.controls.ProviderLabel.ProviderType;
import com.supermap.desktop.controls.ui.controls.ProviderLabel.WarningOrHelpProvider;
import com.supermap.desktop.controls.ui.controls.button.SmButton;
import com.supermap.desktop.controls.ui.controls.progress.SmDialogProgressTotal;
import com.supermap.desktop.controls.utilities.ComponentFactory;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.FileChooseMode;
import com.supermap.desktop.core.Interface.IAfterWork;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedEvent;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedListener;
import com.supermap.desktop.core.iserver.FunctionProgressEvent;
import com.supermap.desktop.core.iserver.FunctionProgressListener;
import com.supermap.desktop.core.iserver.ServerReleaseMapCache;
import com.supermap.desktop.core.iserver.ServerReleaseWorkspace;
import com.supermap.desktop.core.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginManager;
import com.supermap.desktop.core.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.core.utilties.CursorUtilities;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.core.utilties.StringUtilities;
import com.supermap.desktop.develop.GAFProperties;
import com.supermap.desktop.develop.entity.GAFTileStorageConnection;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.netservices.NetServicesProperties;
import com.supermap.desktop.netservices.iserver.JDialogFolderSelector;
import com.supermap.desktop.netservices.iserver.SelectableFile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CancellationException;

public class DialogGAFReleaseLocalCache extends SmDialog implements ActionListener {
    private static final long serialVersionUID = 1L;
    private String tileId;
//    private JLabel labelIServerLogin;
    private SmComboBoxGAFIServerLogin iServerLogin;
    private JLabel labelSciFileChooser;
    private SmFileChooserControl sciFileChooser;
    private JCheckBox checkBoxIsUpload;
    private WarningOrHelpProvider uploadTip;
    private JCheckBox checkBoxRestMap;
    private JCheckBox checkBoxRestVectorCache;
    private JCheckBox checkBoxWMS111;
    private JCheckBox checkBoxWMS130;
    private JCheckBox checkBoxWMTS100;
    private JCheckBox checkBoxWMTSCHINA;
    private JCheckBox checkBoxAGSRestMap;
    private JCheckBox checkBoxBaiduRest;
    private JCheckBox checkBoxGoogleRest;
    private SmButton buttonRelease;
    private SmButton buttonClose;
    private String sciFilePath;
    private boolean isVector = false;
    private int servicesType = 0;
    private IServerLoginInfoSelectionChangedListener loginInfoSelectionChangedListener = new IServerLoginInfoSelectionChangedListener() {
        public void selectionChanged(IServerLoginInfoSelectionChangedEvent event) {
            DialogGAFReleaseLocalCache.this.setCheckBoxIsUploadState();
            DialogGAFReleaseLocalCache.this.setButtonReleaseEnabled();
        }
    };
    private FileChooserPathChangedListener sciFileChangedListener = new FileChooserPathChangedListener() {
        public void pathChanged() {
            DialogGAFReleaseLocalCache.this.sciFileChanged();
            DialogGAFReleaseLocalCache.this.setButtonReleaseEnabled();
        }
    };

    private void sciFileChanged() {
        if (!StringUtilities.isNullOrEmptyString(this.sciFileChooser.getPath())) {
            this.sciFilePath = this.sciFileChooser.getPath();
            File sciFile = new File(this.sciFilePath);
            if (!sciFile.exists() || sciFile.isDirectory()) {
                this.sciFilePath = "";
                return;
            }

            String[] names = sciFile.getParentFile().list();
            boolean hasJSon = false;
            boolean hasTiles = false;

            for(int i = 0; i < names.length; ++i) {
                if (names[i].equals("styles")) {
                    hasJSon = true;
                }

                if (names[i].equals("tiles")) {
                    hasTiles = true;
                }
            }

            this.isVector = hasJSon && hasTiles;
        } else {
            this.sciFilePath = "";
        }

    }

    public DialogGAFReleaseLocalCache(GAFTileStorageConnection tileStorageConnection) {
        this.iServerLogin = CommonUtils.getGAFIserverLogin();
        this.initializeComponents();
        this.initializeResources();

        this.tileId = tileStorageConnection.getId();
        this.sciFilePath = tileStorageConnection.getLocalPath();
        this.sciFileChooser.setPath(tileStorageConnection.getLocalPath());
        this.sciFileChooser.setEnabled(false);

        this.registerEvents();
        this.setCheckBoxIsUploadState();
        this.setButtonReleaseEnabled();
        this.componentList.add(this.buttonRelease);
        this.componentList.add(this.buttonClose);
        this.setFocusTraversalPolicy(this.policy);
        this.setSize(new Dimension(375, 500));

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.buttonRelease) {
            this.releaseMapCache();
        } else if (e.getSource() == this.buttonClose) {
            this.dispose();
        } else if (e.getSource() == this.checkBoxWMTS100) {
            this.checkBoxWMTS100CheckChange();
        } else if (e.getSource() == this.checkBoxWMTSCHINA) {
            this.checkBoxWMTSCHINACheckChange();
        } else if (e.getSource() == this.checkBoxWMS130) {
            this.checkBoxWMS130CheckChange();
        } else if (e.getSource() == this.checkBoxWMS111) {
            this.checkBoxWMS111CheckChange();
        } else if (e.getSource() == this.checkBoxRestMap) {
            this.checkBoxRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxRestVectorCache) {
            this.checkBoxRestVectorCacheChange();
        } else if (e.getSource() == this.checkBoxAGSRestMap) {
            this.checkBoxAGSRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxBaiduRest) {
            this.checkBoxBaiduRestCheckChange();
        } else if (e.getSource() == this.checkBoxGoogleRest) {
            this.checkBoxGoogleRestCheckChange();
        }

    }

    private void initializeComponents() {
//        JPanel panelService = this.getServicePanel();
        JPanel panelFile = this.getFilePanel();
        JPanel panelRestService = this.getRestServicePanel();
        JPanel panelOGCService = this.getOGCServicePanel();
        JPanel panelOtherService = this.getOtherServicePanel();
        JPanel panelMain = new JPanel();
        this.setContentPane(panelMain);
        this.buttonRelease = new SmButton("Release");
        this.buttonClose = new SmButton("Close");
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
//        panelMain.add(panelService, (new GridBagConstraintsHelper(0, 0, 2, 1)).setAnchor(11).setInsets(10, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelFile, (new GridBagConstraintsHelper(0, 1, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelRestService, (new GridBagConstraintsHelper(0, 2, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelOGCService, (new GridBagConstraintsHelper(0, 3, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelOtherService, (new GridBagConstraintsHelper(0, 4, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 1.0D));
        panelMain.add(ComponentFactory.createButtonPanel(new JComponent[]{this.buttonRelease, this.buttonClose}), (new GridBagConstraintsHelper(0, 5, 2, 1)).setAnchor(14).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
    }

//    private JPanel getServicePanel() {
//        JPanel panelService = new JPanel();
//        panelService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_Services")));
//        this.labelIServerLogin = new JLabel();
//        this.iServerLogin = new SmComboBoxIServerLogin();
//        GridBagLayout gridBagLayout = new GridBagLayout();
//        panelService.setLayout(gridBagLayout);
//        panelService.add(this.labelIServerLogin, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(0.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 10, 0));
//        panelService.add(this.iServerLogin, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(1).setAnchor(17).setInsets(0, 10, 10, 10));
//        return panelService;
//    }

    private JPanel getFilePanel() {
        JPanel panelCache = new JPanel();
        panelCache.setBorder(BorderFactory.createTitledBorder(NetServicesProperties.getString("String_Cache")));
        this.labelSciFileChooser = new JLabel();
        SmFileChoose smFileChoose = (new SmFileChoose("ChooseReleaseSciFile", FileChooseMode.OPEN_ONE)).setTitle(ControlsProperties.getString("String_OpenFile")).addFileFilter(NetServicesProperties.getString("String_MapCache_CacheConfigFile"), new String[]{"sci"});
        this.sciFileChooser = new SmFileChooserControl();
        this.sciFileChooser.setFileChooser(smFileChoose);
        this.checkBoxIsUpload = new JCheckBox("IsUpload");
        this.uploadTip = new WarningOrHelpProvider(ProviderType.WARNING);
        GridBagLayout gridBagLayout = new GridBagLayout();
        panelCache.setLayout(gridBagLayout);
        panelCache.add(this.labelSciFileChooser, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(0.0D, 0.0D).setAnchor(17).setInsets(0, 10, 10, 10));
        panelCache.add(this.sciFileChooser, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setAnchor(10).setFill(2).setInsets(0, 0, 10, 10));
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(this.checkBoxIsUpload);
        panel.add(this.uploadTip);
        panelCache.add(panel, (new GridBagConstraintsHelper(0, 1, 2, 1)).setAnchor(17).setFill(0));
        return panelCache;
    }

    private JPanel getRestServicePanel() {
        JPanel panelRestService = new JPanel();
        panelRestService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_RestServices")));
        this.checkBoxRestMap = new JCheckBox("RestMap");
        this.checkBoxRestVectorCache = new JCheckBox("RestVectorCache");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelRestService.setLayout(gbl_panelRestService);
        panelRestService.add(this.checkBoxRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelRestService.add(this.checkBoxRestVectorCache, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        return panelRestService;
    }

    private void setCheckBoxIsUploadState() {
        this.checkBoxIsUpload.setSelected(true);
        this.checkBoxIsUpload.setEnabled(this.isLocal());
    }

    private boolean isLocal() {
        return this.iServerLogin.getIServerLoginInfo() == null ? false : IServerLoginManager.isLocalAddress(this.iServerLogin.getIServerLoginInfo().getServer());
    }

    private JPanel getOGCServicePanel() {
        JPanel panelOGCService = new JPanel();
        panelOGCService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OGCServices")));
        this.checkBoxWMS111 = new JCheckBox("WMS1.1.1");
        this.checkBoxWMS130 = new JCheckBox("WMS1.3.0");
        this.checkBoxWMTS100 = new JCheckBox("WMTS1.0.0");
        this.checkBoxWMTSCHINA = new JCheckBox("WMTS-CHINA");
        GridBagLayout gbl_panelOGCService = new GridBagLayout();
        panelOGCService.setLayout(gbl_panelOGCService);
        panelOGCService.add(this.checkBoxWMS111, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMS130, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMTS100, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMTSCHINA, (new GridBagConstraintsHelper(1, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        return panelOGCService;
    }

    private JPanel getOtherServicePanel() {
        JPanel panelOtherService = new JPanel();
        panelOtherService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OtherServices")));
        this.checkBoxAGSRestMap = new JCheckBox("AGSRestMap");
        this.checkBoxBaiduRest = new JCheckBox("BaiduRest");
        this.checkBoxGoogleRest = new JCheckBox("GoogleRest");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelOtherService.setLayout(gbl_panelRestService);
        panelOtherService.add(this.checkBoxAGSRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOtherService.add(this.checkBoxBaiduRest, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0).setIpad(25, 0));
        panelOtherService.add(this.checkBoxGoogleRest, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        return panelOtherService;
    }

    private void initializeResources() {
        this.setTitle(GAFProperties.getString("String_Alias")+"-"+NetServicesProperties.getString("String_ReleaseLocalCache"));
//        this.labelIServerLogin.setText(NetServicesProperties.getString("String_LabelChooseIServer"));
        this.labelSciFileChooser.setText(NetServicesProperties.getString("String_SciPath"));
        this.checkBoxIsUpload.setText(NetServicesProperties.getString("String_UploadLocalCacheFiles"));
        this.uploadTip.setTipText(CoreProperties.getString("String_UploadLocalFilesTip"));
        this.checkBoxRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_RestMap"));
        this.checkBoxRestVectorCache.setText(CoreProperties.getString("String_iServer_ServicesType_RestVectorCache"));
        this.checkBoxWMS111.setText(CoreProperties.getString("String_iServer_ServicesType_WMS111"));
        this.checkBoxWMS130.setText(CoreProperties.getString("String_iServer_ServicesType_WMS130"));
        this.checkBoxWMTS100.setText(CoreProperties.getString("String_iServer_ServicesType_WMTS100"));
        this.checkBoxWMTSCHINA.setText(CoreProperties.getString("String_iServer_ServicesType_WMTSCHINA"));
        this.checkBoxAGSRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_AGSRestMap"));
        this.checkBoxBaiduRest.setText(CoreProperties.getString("String_iServer_ServicesType_BaiduRest"));
        this.checkBoxGoogleRest.setText(CoreProperties.getString("String_iServer_ServicesType_GoogleRest"));
        this.buttonRelease.setText(NetServicesProperties.getString("String_Release"));
        this.buttonClose.setText(CoreProperties.getString("String_Close"));
    }

    private void registerEvents() {
        this.removeEvents();
//        this.iServerLogin.addIServerLoginInfoSelectionChangedListener(this.loginInfoSelectionChangedListener);
        this.sciFileChooser.addPathChangedListener(this.sciFileChangedListener);
        this.checkBoxRestMap.addActionListener(this);
        this.checkBoxRestVectorCache.addActionListener(this);
        this.checkBoxWMS111.addActionListener(this);
        this.checkBoxWMS130.addActionListener(this);
        this.checkBoxWMTS100.addActionListener(this);
        this.checkBoxWMTSCHINA.addActionListener(this);
        this.checkBoxAGSRestMap.addActionListener(this);
        this.checkBoxBaiduRest.addActionListener(this);
        this.checkBoxGoogleRest.addActionListener(this);
        this.buttonRelease.addActionListener(this);
        this.buttonClose.addActionListener(this);
    }

    private void removeEvents() {
//        this.iServerLogin.removeIServerLoginInfoSelectionChangedListener(this.loginInfoSelectionChangedListener);
        this.sciFileChooser.removePathChangedListener(this.sciFileChangedListener);
        this.checkBoxRestMap.removeActionListener(this);
        this.checkBoxRestVectorCache.removeActionListener(this);
        this.checkBoxWMS111.removeActionListener(this);
        this.checkBoxWMS130.removeActionListener(this);
        this.checkBoxWMTS100.removeActionListener(this);
        this.checkBoxWMTSCHINA.removeActionListener(this);
        this.checkBoxAGSRestMap.removeActionListener(this);
        this.checkBoxBaiduRest.removeActionListener(this);
        this.checkBoxGoogleRest.removeActionListener(this);
        this.buttonRelease.removeActionListener(this);
        this.buttonClose.removeActionListener(this);
    }

    private void setButtonReleaseEnabled() {
        try {
            if (!StringUtilities.isNullOrEmptyString(this.sciFilePath) && this.servicesType != 0 && this.iServerLogin.getIServerLoginInfo() != null) {
                this.buttonRelease.setEnabled(true);
                this.getRootPane().setDefaultButton(this.buttonRelease);
            } else {
                this.buttonRelease.setEnabled(false);
            }
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMTSCHINACheckChange() {
        try {
            if (this.checkBoxWMTSCHINA.isSelected()) {
                this.servicesType |= 4096;
            } else {
                this.servicesType ^= 4096;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMTS100CheckChange() {
        try {
            if (this.checkBoxWMTS100.isSelected()) {
                this.servicesType |= 2048;
            } else {
                this.servicesType ^= 2048;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS130CheckChange() {
        try {
            if (this.checkBoxWMS130.isSelected()) {
                this.servicesType |= 1024;
            } else {
                this.servicesType ^= 1024;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS111CheckChange() {
        try {
            if (this.checkBoxWMS111.isSelected()) {
                this.servicesType |= 512;
            } else {
                this.servicesType ^= 512;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestMapCheckChange() {
        try {
            if (this.checkBoxRestMap.isSelected()) {
                this.servicesType |= 2;
            } else {
                this.servicesType ^= 2;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestVectorCacheChange() {
        try {
            if (this.checkBoxRestVectorCache.isSelected()) {
                this.servicesType |= 4194304;
            } else {
                this.servicesType ^= 4194304;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxAGSRestMapCheckChange() {
        try {
            if (this.checkBoxAGSRestMap.isSelected()) {
                this.servicesType |= 32768;
            } else {
                this.servicesType ^= 32768;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxBaiduRestCheckChange() {
        try {
            if (this.checkBoxBaiduRest.isSelected()) {
                this.servicesType |= 262144;
            } else {
                this.servicesType ^= 262144;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxGoogleRestCheckChange() {
        try {
            if (this.checkBoxGoogleRest.isSelected()) {
                this.servicesType |= 524288;
            } else {
                this.servicesType ^= 524288;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void releaseMapCache() {
        CursorUtilities.setWaitCursor();

        try {
            ServerReleaseMapCache serverRelease = this.fillServerRelease();
            if (!StringUtilities.isNullOrEmpty(serverRelease.getSciPath()) && (serverRelease.getHostType() == 1 || this.checkBoxIsUpload.isSelected())) {
                ArrayList files = new ArrayList();
                File sciDirectory = new File(serverRelease.getSciDirectory());
                File[] childFiles = sciDirectory.listFiles();

                for(int i = 0; i < childFiles.length; ++i) {
                    if (!childFiles[i].isHidden()) {
                        SelectableFile selectableFile = null;
                        if (serverRelease.getSciPath().equals(childFiles[i].getPath())) {
                            selectableFile = SelectableFile.fromFile(childFiles[i], true);
                        } else {
                            selectableFile = SelectableFile.fromFile(childFiles[i], false);
                        }

                        selectableFile.getSizeString();
                        files.add(selectableFile);
                    }
                }

                JDialogFolderSelector folderSelector = new JDialogFolderSelector((ArrayList)files.clone(), serverRelease.getSciPath());
                if (folderSelector.showDialog() != DialogResult.OK) {
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_Operation_Cancel"));
                    return;
                }

                serverRelease.getFiles().clear();

                for(int i = 0; i < files.size(); ++i) {
                    SelectableFile selectableFile = (SelectableFile)files.get(i);
                    if (selectableFile.isSelected()) {
                        serverRelease.getFiles().add(selectableFile);
                    }
                }
            }

            SmDialogProgressTotal smDialogProgressTotal = new SmDialogProgressTotal(NetServicesProperties.getString("String_ReleaseMapCache"));
            smDialogProgressTotal.doWork(new DialogGAFReleaseLocalCache.ServerReleaseMapCacheCallable(serverRelease), new IAfterWork<Boolean>() {
                public void afterWork(Boolean param) {
                    if (param) {
                        if (SwingUtilities.isEventDispatchThread()) {
                            DialogGAFReleaseLocalCache.this.setVisible(false);
                        } else {
                            SwingUtilities.invokeLater(() -> {
                                DialogGAFReleaseLocalCache.this.setVisible(false);
                            });
                        }
                        ArrayList<String> urls = serverRelease.getResultURLs();
                        DialogGAFReleaseMongoCache.registerServiceFromTile(urls,tileId);
                    }
                }
            });
        } catch (Exception var11) {
            Application.getActiveApplication().getOutput().output(var11);
        } finally {
            CursorUtilities.setDefaultCursor();
        }

    }

    private ServerReleaseMapCache fillServerRelease() {
        IServerLoginInfo iServerLoginInfo = this.iServerLogin.getIServerLoginInfo();
        ServerReleaseMapCache serverRelease = new ServerReleaseMapCache(iServerLoginInfo);
        serverRelease.setUpload(this.checkBoxIsUpload.isSelected());
        serverRelease.setServicesType(this.servicesType);
        serverRelease.setVector(this.isVector);
        serverRelease.setSciFilePath(this.sciFilePath);
        return serverRelease;
    }

    class ServerReleaseMapCacheCallable extends UpdateProgressCallable {
        private ServerReleaseMapCache b;
        private FunctionProgressListener c = new FunctionProgressListener() {
            public void functionProgress(FunctionProgressEvent event) {
                try {
                    DialogGAFReleaseLocalCache.ServerReleaseMapCacheCallable.this.updateProgressTotal(event.getCurrentProgress(), event.getTotalProgress(), event.getCurrentMessage(), event.getTotalMessage(), "", event.getCurrentCount(), event.getCount());
                } catch (CancellationException var3) {
                }

            }
        };

        public ServerReleaseMapCacheCallable(ServerReleaseMapCache serverRelease) {
            this.b = serverRelease;
        }

        public Boolean call() {
            boolean result = true;

            try {
                Application.getActiveApplication().getOutput().output(CoreProperties.getString("String_ReleaseStart"));
                this.b.addFunctionProgressListener(this.c);
                long startTime = System.currentTimeMillis();
                boolean releaseResult = this.b.release();
                if (releaseResult) {
                    long totalTime = System.currentTimeMillis() - startTime;
                    Application.getActiveApplication().getOutput().output(MessageFormat.format(NetServicesProperties.getString("String_iServer_Message_ReleaseSuccess"), String.valueOf(totalTime / 1000L)));
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_ReleaseMapCacheSuccessResult"));
                    Iterator var7 = this.b.getResultURLs().iterator();

                    while(var7.hasNext()) {
                        String resultUrl = (String)var7.next();
                        Application.getActiveApplication().getOutput().output(resultUrl);
                    }
                } else {
                    result = false;
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_iServer_Message_ReleaseFaild"));
                }
            } catch (Exception var12) {
                result = false;
                Application.getActiveApplication().getOutput().output(var12);
            } finally {
                this.b.removeFunctionProgressListener(this.c);
                ServerReleaseWorkspace.clearTmp();
            }

            return result;
        }
    }

    private void registryService(java.util.List<String> services){
        for(String service:services){
            JOptionPaneUtilities.showMessageDialog(service);

        }
    }
}
