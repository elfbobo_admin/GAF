package com.supermap.desktop.develop.histroy;

public interface UniqueHistoryNode {
     boolean isConflict(UniqueHistoryNode another);
}
