package com.supermap.desktop.develop.entity;

import java.io.File;
import java.util.function.Function;

/**
 * @author heykb
 */
public class PresignUploadRequest {
    private String contentMd5;
    private String presignUrl;
    private File file;
    private boolean async;
    private Function<Integer,Void> progressListener;

    public PresignUploadRequest(String presignUrl) {
        this.presignUrl = presignUrl;
    }

    public PresignUploadRequest(String presignUrl, String contentMd5) {
        this.contentMd5 = contentMd5;
        this.presignUrl = presignUrl;
    }

    public PresignUploadRequest(String presignUrl, File file, String contentMd5,  boolean async) {
        this.contentMd5 = contentMd5;
        this.presignUrl = presignUrl;
        this.file = file;
        this.async = async;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public Function<Integer, Void> getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(Function<Integer, Void> progressListener) {
        this.progressListener = progressListener;
    }



    public String getContentMd5() {
        return contentMd5;
    }

    public void setContentMd5(String contentMd5) {
        this.contentMd5 = contentMd5;
    }

    public String getPresignUrl() {
        return presignUrl;
    }

    public void setPresignUrl(String presignUrl) {
        this.presignUrl = presignUrl;
    }
}
