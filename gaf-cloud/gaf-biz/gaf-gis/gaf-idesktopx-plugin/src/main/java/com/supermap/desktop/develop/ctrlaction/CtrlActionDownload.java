/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ctrlaction;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.desktop.controls.ui.controls.SmFileChoose;
import com.supermap.desktop.core.FileChooseMode;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.implement.CtrlAction;
import com.supermap.desktop.core.utilties.FileUtilities;
import com.supermap.desktop.develop.entity.GAFDatasourceConnectionInfo;
import com.supermap.desktop.develop.entity.GAFSource;
import com.supermap.desktop.develop.entity.GAFTileStorageConnection;
import com.supermap.desktop.develop.entity.GAFWorkspaceConnectionInfo;
import com.supermap.desktop.develop.ui.*;
import com.supermap.desktop.develop.utils.CommonUtils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.nio.file.Paths;

/**
 * @author SuperMap
 * @date:2021/3/25
 */
public class CtrlActionDownload extends CtrlAction implements GAFTreeNodeCtrlAction {
    private SmFileChoose smFileChoose;

    private DefaultMutableTreeNode node;


    @Override
    public void setNode(DefaultMutableTreeNode node) {
        this.node = node;
        Object conn = node.getUserObject();

        if (this.smFileChoose == null){
            initFileChoose(null);
        }
        if (conn instanceof DatasourceConnectionInfo) {
            this.smFileChoose.setTitle("下载数据源到");
        } else if (conn instanceof GAFWorkspaceConnectionInfo) {
            this.smFileChoose.setTitle("下载工作空间到");
        }
    }

    @Override
    public boolean visible() {
        Object conn = node.getUserObject();
        if (conn instanceof GAFSource) {
            String localPath = ((GAFSource) conn).getLocalPath();
            if(getCaller().getID().equals("download")){
                return  (localPath==null);
            }else{
                return (localPath!=null);
            }
        }
        return true;
    }

    @Override
    public boolean enable() {
        return CommonUtils.isFileTypeSource(node.getUserObject());
    }

    public CtrlActionDownload(IBaseItem caller) {
        super(caller);
    }

    private void initFileChoose(String path) {
        this.smFileChoose = new SmFileChoose("", FileChooseMode.SAVE_ONE, path);
        Object conn = node.getUserObject();
        if (conn instanceof GAFSource) {
            String localPath = ((GAFSource) conn).getLocalPath();
            if (localPath != null) {
                try{
                    this.smFileChoose.setCurrentDirectory(Paths.get(localPath).getParent().toFile());
                }catch (Exception e){}
            }
        }
        this.smFileChoose.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    @Override
    public void run() {
        Object userObject = node.getUserObject();
        if(userObject instanceof GAFTileStorageConnection){
            GAFTileStorageConnection conn = (GAFTileStorageConnection) userObject;
            GAFTileTree tree = GafTileManager.gafTilesManagerTree;
            tree.preOpAndCheck(conn,true,connection->{
                if (CommonUtils.isFileTypeSource(connection)) {
                    // 记录下载记录
                    conn.setLocalPath(connection.getServer());
                    CommonUtils.appendDownloadHistory(CommonUtils.TILE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                    tree.updateNode(node);
                    FileUtilities.openFileExplorer(conn.getLocalPath());
                }
                return null;
            });
            return;
        }
        if(userObject instanceof GAFDatasourceConnectionInfo){
            GAFDatasourceConnectionInfo conn = (GAFDatasourceConnectionInfo) userObject;
            GAFDatasourceTree tree = GafDatasourceManager.gafDatasourceManagerTree;
            tree.preOpAndCheck(conn,true,connection->{
                if (CommonUtils.isFileTypeSource(connection)) {
                    // 记录下载记录
                    conn.setLocalPath(connection.getServer());
                    CommonUtils.appendDownloadHistory(CommonUtils.DATASOURCE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                    tree.updateNode(node);
                    FileUtilities.openFileExplorer(conn.getLocalPath());
                }
                return null;
            });
            return;
        }
        if(userObject instanceof GAFWorkspaceConnectionInfo){
            GAFWorkspaceConnectionInfo conn = (GAFWorkspaceConnectionInfo) userObject;
            GAFWorkspaceTree tree = GafWorkspaceManager.gafWorkspaceManagerTree;
            tree.preOpAndCheck(conn,true,connection->{
                if (CommonUtils.isFileTypeSource(connection)) {
                    // 记录下载记录
                    conn.setLocalPath(connection.getServer());
                    CommonUtils.appendDownloadHistory(CommonUtils.WORKSPACE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                    tree.updateNode(node);
                    FileUtilities.openFileExplorer(conn.getLocalPath());
                }
                return null;
            });
            return;
        }

    }
}
