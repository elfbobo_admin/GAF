/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ctrlaction;

import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.implement.CtrlAction;
import com.supermap.desktop.core.utilties.FileUtilities;
import com.supermap.desktop.develop.entity.GAFSource;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @author SuperMap
 * @date:2021/3/25
 */
public class CtrlActionOpenInExplorer extends CtrlAction implements GAFTreeNodeCtrlAction {
    private DefaultMutableTreeNode node;


    @Override
    public void setNode(DefaultMutableTreeNode node) {
        this.node = node;
    }

    @Override
    public boolean visible() {
        Object conn = node.getUserObject();
        return conn instanceof GAFSource && ((GAFSource) conn).getLocalPath()!=null;
    }


    public CtrlActionOpenInExplorer(IBaseItem caller) {
        super(caller);
    }

    @Override
    public void run() {
        Object conn = node.getUserObject();
        String localPath = ((GAFSource) conn).getLocalPath();
        FileUtilities.openFileExplorer(localPath);
    }

}
