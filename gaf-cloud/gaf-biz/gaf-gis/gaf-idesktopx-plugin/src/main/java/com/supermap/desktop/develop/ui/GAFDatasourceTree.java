package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.Datasource;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.Datasources;
import com.supermap.data.EngineType;
import com.supermap.desktop.core.implement.SmPopupMenu;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.utilties.DatasourceUtilities;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.develop.entity.Catalog;
import com.supermap.desktop.develop.entity.GAFDatasourceConnectionInfo;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import org.apache.arrow.vector.util.CallBack;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class GAFDatasourceTree extends AbstratGafTree{

    public GAFDatasourceTree() {
    }

    public GAFDatasourceTree(HistoryManager historyManager, JSONArray list) {
        super(historyManager, list);
    }

    @Override
    public List getCurrentOpened() {
        ArrayList<DatasourceConnectionInfo> connectionInfos = new ArrayList<DatasourceConnectionInfo>();
        Datasources datasources = ApplicationContextUtils.getWorkspace().getDatasources();
        for (int i = 0; i < datasources.getCount(); ++i) {
            connectionInfos.add(DatasourceUtilities.copyDatasourceConnectionInfo(datasources.get(i).getConnectionInfo()));
        }
        return connectionInfos;
    }

    @Override
    protected void buildTree(DefaultMutableTreeNode root, JSONArray list) {
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                JSONObject node = list.getJSONObject(i);
                recursionBuildTree(node, root);
            }
        }
    }
    private void recursionBuildTree(JSONObject node, DefaultMutableTreeNode parent) {
        if (isCatalog(node)) {
            Catalog catalog = new Catalog(node.getString("title"), ""/*node.getJSONObject("userObject").getString("value")*/);
            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(catalog);
            parent.add(treeNode);
            JSONArray children = node.getJSONArray("children");
            if (children != null) {
                for (int i = 0; i < children.size(); ++i) {
                    recursionBuildTree(children.getJSONObject(i), treeNode);
                }
            }
            // 默认展开
        } else {
            JSONObject conn = node.getJSONObject("userObject");
            if (conn != null) {
                GAFDatasourceConnectionInfo connectionInfo = new GAFDatasourceConnectionInfo();
                connectionInfo.setId(node.getString("key"));
                connectionInfo.setServer(conn.getString("addr"));
                connectionInfo.setAlias(conn.getString("dsName"));
                connectionInfo.setDatabase(conn.getString("dbName"));
                connectionInfo.setUser(conn.getString("userName"));
                connectionInfo.setPassword(conn.getString("password"));
                try {
                    connectionInfo.setEngineType((EngineType) EngineType.parse(EngineType.class, conn.getString("typeCode")));
                } catch (RuntimeException e) {
                    ApplicationContextUtils.getOutput().output(String.format("%s数据源连接信息解析失败，不存在类型%s", conn.getString("dsName"), conn.getString("typeCode")));
                }
                if (EngineType.SQLPLUS == connectionInfo.getEngineType()) {
                    connectionInfo.setDriver("SQL SERVER");
                }
                parent.add(new DefaultMutableTreeNode(connectionInfo));
            } else {
                parent.add(new DefaultMutableTreeNode(node.getString("title")));
            }
        }
    }
    boolean isCatalog(JSONObject node) {
        if (node.getBoolean("leaf") && node.getJSONObject("userObject") != null && StringUtils.isEmpty(node.getJSONObject("userObject").getString("dictTypeCode"))) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    void doubleClickHandler(DefaultMutableTreeNode node, CallBack cb) {
        GAFDatasourceConnectionInfo conn = (GAFDatasourceConnectionInfo) node.getUserObject();
        preOpAndCheck(conn, false, connectionInfo -> {
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                // 记录下载记录
                conn.setLocalPath(connectionInfo.getServer());
                CommonUtils.appendDownloadHistory(CommonUtils.DATASOURCE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                updateNode(node);
            }
            openDatasource(connectionInfo);
            if (cb != null) cb.doWork();
            return null;
        });
    }
    public static void openDatasource(DatasourceConnectionInfo connectionInfo) {
        Datasources datasources = ApplicationContextUtils.getWorkspace().getDatasources();
        Datasource datasource = datasources.get(connectionInfo.getAlias());
        if (datasource != null) {
            if (CommonUtils.sameDatasource(connectionInfo, datasource.getConnectionInfo())) {
                JOptionPaneUtilities.showMessageDialog("该数据源已经打开");
            } else {
                JOptionPaneUtilities.showErrorMessageDialog("打开失败，存在同名数据源！！");
                throw new RuntimeException("打开失败，存在同名数据源！！");
            }
        } else {
            datasources.open(connectionInfo);
            ApplicationContextUtils.getOutput().output("数据源打开成功！");
        }
    }

    public void preOpAndCheck(GAFDatasourceConnectionInfo connectionInfo, boolean reDownload, Function<DatasourceConnectionInfo, Object> continueOp) {
        try {
            DatasourceConnectionInfo canOpenConn = DatasourceUtilities.copyDatasourceConnectionInfo(connectionInfo);
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                String localPath = CommonUtils.getDownloadedPath(CommonUtils.DATASOURCE_DOWNLOAD_HISTORY, connectionInfo.getId());
                if (localPath != null && !reDownload) {
                    // 有下载记录直接打开
                    if (!Files.exists(Paths.get(localPath))) {
                        JOptionPaneUtilities.showErrorMessageDialog(String.format("%s不存在", localPath));
                        return;
                    }
                    canOpenConn.setServer(localPath);
                    if(continueOp !=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
                Optional<String> fileName = CommonUtils.getFileName(connectionInfo);
                if (!fileName.isPresent()) {
                    JOptionPaneUtilities.showErrorMessageDialog("无法下载该文件");
                    return;
                }
                if (CommonUtils.tipIfNotExist("/datas/" + fileName.get())) {
                    return;
                }
                int re = CommonUtils.dirChoose(smFileChoose, fileName.get().toString());
                if (re == -1) {
                    // 取消
                    return;
                } else if (re == 0) {
                    // 正常
                    Path downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + fileName.get());
                    Path downloadPath2 = downloadPath;
                    if (downloadPath.toString().endsWith(".udb")) {
                        String uddFileName = fileName.get().substring(0, fileName.get().length() - 1) + "d";
                        Path uddDownloadPath = downloadPath2.getParent().resolve(uddFileName);
                        CommonUtils.downloadAsync("/datas/" + uddDownloadPath.getFileName(), uddDownloadPath, evt -> {
                            if (evt.getPropertyName().equals("progress")) {
                                Integer progress = (Integer) evt.getNewValue();
                                if (progress % 10 == 0) {
                                    ApplicationContextUtils.getOutput().output("已下载" + uddDownloadPath.getFileName().toString() + ":" + progress + "%");
                                }
                                if (progress == 100) {
                                    ApplicationContextUtils.getOutput().output(uddDownloadPath.getFileName().toString() + "下载成功");
                                    downloadAndOpen(downloadPath, canOpenConn, continueOp);
                                }
                            }
                        });
                    } else {
                        downloadAndOpen(downloadPath, canOpenConn, continueOp);
                    }
                    return;
                } else {
                    Path downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + fileName.get());
                    // 直接打開
                    canOpenConn.setServer(downloadPath.toString());
                    if(continueOp !=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
            } else {
                if(continueOp !=null){
                    continueOp.apply(canOpenConn);
                }
                return;
            }
        } catch (FileDownloadException e) {
            ApplicationContextUtils.getOutput().output(e.getMessage() + "下载失败");
        } catch (Throwable e) {
            ApplicationContextUtils.getOutput().output(e.getMessage());
        }
    }

    private void downloadAndOpen(Path downloadPath, DatasourceConnectionInfo connectionInfo, Function<DatasourceConnectionInfo, Object> continueOp) {
        try {
            connectionInfo.setServer(downloadPath.toString());
            CommonUtils.downloadAsync("/datas/" + downloadPath.getFileName(), downloadPath, evt -> {
                if (evt.getPropertyName().equals("progress")) {
                    Integer progress = (Integer) evt.getNewValue();
                    if (progress % 10 == 0) {
                        ApplicationContextUtils.getOutput().output("已下载" + downloadPath.getFileName().toString() + ":" + progress + "%");
                    }
                    if (progress == 100) {
                        ApplicationContextUtils.getOutput().output(downloadPath.getFileName().toString() + "下载成功");
                        connectionInfo.setServer(downloadPath.toString());
                        if(continueOp !=null){
                            continueOp.apply(connectionInfo);
                        }
                    }
                }
            });
        } catch (FileDownloadException e) {
            ApplicationContextUtils.getOutput().output(e.getMessage() + "下载失败");
        } catch (Throwable e) {
            ApplicationContextUtils.getOutput().output(e.getMessage());
        }
    }

    @Override
    protected boolean faceLeafAndCheckOpened(TreeCellRender render, List opened, JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        GAFDatasourceConnectionInfo connectionInfo = (GAFDatasourceConnectionInfo) ((DefaultMutableTreeNode) value).getUserObject();

        render.setIcon(CommonUtils.getICon(connectionInfo.getEngineType()));
        if (CommonUtils.isFileTypeSource(connectionInfo)) {
            Optional<String> fileName = CommonUtils.getFileName(connectionInfo);
            if (fileName.isPresent()) {
                render.setText(String.format("%s（%s）", fileName.get(), connectionInfo.getAlias()));
            } else {
                render.setText(String.format("❌%s（%s）", "格式错误", connectionInfo.getAlias()));
            }

        } else {
            render.setText(connectionInfo.getAlias());
        }
        // 已打开状态
        if(opened == null){
            return false;
        }
        for(Object item:opened){
            if (item instanceof DatasourceConnectionInfo) {
                DatasourceConnectionInfo openedConn = (DatasourceConnectionInfo) item;
                if (CommonUtils.sameDatasource(connectionInfo, openedConn)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    SmPopupMenu buildLeafPopMenu(DefaultMutableTreeNode treeNode) {
        return ApplicationContextUtils.getContextMenuManager().get("SuperMap.Desktop.UI.GafDatasourceListManager.ContextMenuDatasource");
    }

    @Override
    SmPopupMenu buildCatalogPopMenu(DefaultMutableTreeNode treeNode) {
        return ApplicationContextUtils.getContextMenuManager().get("SuperMap.Desktop.UI.GafDatasourceListManager.ContextMenuDatasourceParent");    }


}
