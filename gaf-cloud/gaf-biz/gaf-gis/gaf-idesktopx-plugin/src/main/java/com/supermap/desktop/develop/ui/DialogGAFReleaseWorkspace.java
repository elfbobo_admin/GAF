//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.DatasetType;
import com.supermap.data.Datasource;
import com.supermap.data.Datasources;
import com.supermap.data.EngineType;
import com.supermap.desktop.controls.ui.controls.DialogResult;
import com.supermap.desktop.controls.ui.controls.ProviderLabel.ProviderType;
import com.supermap.desktop.controls.ui.controls.ProviderLabel.WarningOrHelpProvider;
import com.supermap.desktop.controls.ui.controls.SmDialog;
import com.supermap.desktop.controls.ui.controls.button.SmButton;
import com.supermap.desktop.controls.ui.controls.progress.SmDialogProgressTotal;
import com.supermap.desktop.controls.utilities.ComponentFactory;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.Interface.IAfterWork;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedEvent;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedListener;
import com.supermap.desktop.core.iserver.*;
import com.supermap.desktop.core.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import com.supermap.desktop.core.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.core.utilties.CursorUtilities;
import com.supermap.desktop.core.utilties.FileUtilities;
import com.supermap.desktop.core.utilties.ListUtilities;
import com.supermap.desktop.core.utilties.StringUtilities;
import com.supermap.desktop.develop.GAFProperties;
import com.supermap.desktop.develop.rewrite.GAFServerReleaseWorkspace;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.netservices.NetServicesProperties;
import com.supermap.desktop.netservices.iserver.JDialogFolderSelector;
import com.supermap.desktop.netservices.iserver.SelectableFile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.CancellationException;

public class DialogGAFReleaseWorkspace extends SmDialog implements ActionListener {
    private static final long serialVersionUID = 1L;
//    private JLabel labelIServerLogin;
    private SmComboBoxGAFIServerLogin iServerLogin;
    private JCheckBox checkBoxRestMap;
    private JCheckBox checkBoxRestData;
    private JCheckBox checkBoxRestRealspace;
    private JCheckBox checkBoxRestSpatialAnalyst;
    private JCheckBox checkBoxRest_NetWorkAnalyst3d;
    private JCheckBox checkBoxRestTransPortationAnalyst;
    private JCheckBox checkBoxRestTrafficTransferAnalyst;
    private JCheckBox checkBoxRestAddressMatch;
    private JCheckBox checkBoxWCS111;
    private JCheckBox checkBoxWMS111;
    private JCheckBox checkBoxWCS112;
    private JCheckBox checkBoxWMS130;
    private JCheckBox checkBoxWFS100;
    private JCheckBox checkBoxWFS200;
    private JCheckBox checkBoxWMTS100;
    private JCheckBox checkBoxWPS100;
    private JCheckBox checkBoxWMTSCHINA;
    private JCheckBox checkBoxIsEditable;
    private JCheckBox checkBoxIsUpload;
    private WarningOrHelpProvider uploadTip;
    private JCheckBox checkBoxAGSRestMap;
    private JCheckBox checkBoxAGSRestData;
    private JCheckBox checkBoxAGSRestNetWorkAnalyst;
    private JCheckBox checkBoxBaiduRest;
    private JCheckBox checkBoxGoogleRest;
    private SmButton buttonRelease;
    private SmButton buttonClose;
    private WorkspaceInfo workspaceInfo;
    private int servicesType;
    private boolean isEditable;
    private boolean canRelease;
    private String workspaceId;
    private IServerLoginInfoSelectionChangedListener loginInfoSelectionChangedListener = new IServerLoginInfoSelectionChangedListener() {
        public void selectionChanged(IServerLoginInfoSelectionChangedEvent event) {
            DialogGAFReleaseWorkspace.this.setCheckBoxIsUploadState();
            DialogGAFReleaseWorkspace.this.setButtonReleaseEnabled();
        }
    };
    public DialogGAFReleaseWorkspace(WorkspaceInfo workspaceInfo,String workspaceId) {
        this.workspaceId = workspaceId;
        this.workspaceInfo = workspaceInfo;
        this.canRelease = true;
        this.iServerLogin = CommonUtils.getGAFIserverLogin();
        this.initializeComponents();
        this.initializeResources();
        this.initializeParameters();
        this.initializeControls();
        this.registerEvents();
        this.setCheckBoxIsUploadState();
        this.setButtonReleaseEnabled();
        this.componentList.add(this.buttonRelease);
        this.componentList.add(this.buttonClose);
        this.checkBoxRestTrafficTransferAnalyst.setVisible(false);
        this.checkBoxRestAddressMatch.setVisible(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.buttonRelease) {
            this.buttonReleaseClicked();
        } else if (e.getSource() == this.buttonClose) {
            this.buttonCloseClicked();
        } else if (e.getSource() == this.checkBoxIsEditable) {
            this.isEditable = this.checkBoxIsEditable.isSelected();
        } else if (e.getSource() == this.checkBoxWPS100) {
            this.checkBoxWPS100CheckChange();
        } else if (e.getSource() == this.checkBoxWMTS100) {
            this.checkBoxWMTS100CheckChange();
        } else if (e.getSource() == this.checkBoxWMTSCHINA) {
            this.checkBoxWMTSCHINACheckChange();
        } else if (e.getSource() == this.checkBoxWMS130) {
            this.checkBoxWMS130CheckChange();
        } else if (e.getSource() == this.checkBoxWMS111) {
            this.checkBoxWMS111CheckChange();
        } else if (e.getSource() == this.checkBoxWFS100) {
            this.checkBoxWFS100CheckChange();
        } else if (e.getSource() == this.checkBoxWFS200) {
            this.checkBoxWFS200CheckChange();
        } else if (e.getSource() == this.checkBoxWCS112) {
            this.checkBoxWCS112CheckChange();
        } else if (e.getSource() == this.checkBoxWCS111) {
            this.checkBoxWCS111CheckChange();
        } else if (e.getSource() == this.checkBoxRestData) {
            this.checkBoxRestDataCheckChange();
        } else if (e.getSource() == this.checkBoxRestMap) {
            this.checkBoxRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxRestRealspace) {
            this.checkBoxRestRealspaceCheckChange();
        } else if (e.getSource() == this.checkBoxRestSpatialAnalyst) {
            this.checkBoxRestSpatialAnalystCheckChange();
        } else if (e.getSource() == this.checkBoxRest_NetWorkAnalyst3d) {
            this.checkBoxRest_NetWorkAnalyst3dCheckChange();
        } else if (e.getSource() == this.checkBoxRestTransPortationAnalyst) {
            this.checkBoxRestTransPortationAnalystCheckChange();
        } else if (e.getSource() == this.checkBoxRestTrafficTransferAnalyst) {
            this.checkBoxRestTrafficTransferAnalystCheckChange();
        } else if (e.getSource() == this.checkBoxRestAddressMatch) {
            this.checkBoxRestAddressMatchCheckChange();
        } else if (e.getSource() == this.checkBoxAGSRestMap) {
            this.checkBoxAGSRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxAGSRestData) {
            this.checkBoxAGSRestDataCheckChange();
        } else if (e.getSource() == this.checkBoxAGSRestNetWorkAnalyst) {
            this.checkBoxAGSRestNetWorkAnalystCheckChange();
        } else if (e.getSource() == this.checkBoxBaiduRest) {
            this.checkBoxBaiduRestCheckChange();
        } else if (e.getSource() == this.checkBoxGoogleRest) {
            this.checkBoxGoogleRestCheckChange();
        }

    }

    private void initializeComponents() {
//        JPanel panelService = this.getServicePanel();
        JPanel panelRestService = this.getRestServicePanel();
        JPanel panelOGCService = this.getOGCServicePanel();
        JPanel panelOtherService = this.getOtherServicePanel();
        JPanel panelAdditionalSetting = new JPanel();
        panelAdditionalSetting.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_GroupBox_ParamSetting")));
        this.checkBoxIsEditable = new JCheckBox("IsEditable");
        this.checkBoxIsUpload = new JCheckBox("IsUpload");
        this.uploadTip = new WarningOrHelpProvider(ProviderType.WARNING);
        this.uploadTip.setTipText(CoreProperties.getString("String_UploadLocalFilesTip"));
        GridBagLayout gbl_panelAdditionalSetting = new GridBagLayout();
        panelAdditionalSetting.setLayout(gbl_panelAdditionalSetting);
        panelAdditionalSetting.add(this.checkBoxIsEditable, (new GridBagConstraintsHelper(0, 0, 1, 1)).setAnchor(17).setInsets(0, 10, 5, 10).setFill(0).setWeight(1.0D, 0.0D));
        panelAdditionalSetting.add(this.checkBoxIsUpload, (new GridBagConstraintsHelper(1, 0, 1, 1)).setAnchor(17).setInsets(0, 50, 5, 0).setFill(0).setWeight(0.0D, 0.0D));
        panelAdditionalSetting.add(this.uploadTip, (new GridBagConstraintsHelper(2, 0, 1, 1)).setAnchor(17).setInsets(0, 0, 5, 10).setFill(0).setWeight(1.0D, 0.0D));
        JPanel panelMain = new JPanel();
        this.setContentPane(panelMain);
        this.buttonRelease = new SmButton("Release");
        this.buttonClose = new SmButton("Close");
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
//        panelMain.add(panelService, (new GridBagConstraintsHelper(0, 0, 2, 1)).setAnchor(11).setInsets(0, 10, 0, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelRestService, (new GridBagConstraintsHelper(0, 1, 2, 1)).setAnchor(11).setInsets(0, 10, 0, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelOGCService, (new GridBagConstraintsHelper(0, 2, 2, 1)).setAnchor(11).setInsets(0, 10, 0, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelOtherService, (new GridBagConstraintsHelper(0, 3, 2, 1)).setAnchor(11).setInsets(0, 10, 0, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelAdditionalSetting, (new GridBagConstraintsHelper(0, 4, 2, 1)).setAnchor(11).setInsets(0, 10, 0, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(ComponentFactory.createButtonPanel(new JComponent[]{this.buttonRelease, this.buttonClose}), (new GridBagConstraintsHelper(0, 5, 2, 1)).setAnchor(14).setInsets(10, 0, 10, 10).setFill(2).setWeight(1.0D, 1.0D));
    }

//    private JPanel getServicePanel() {
//        JPanel panelService = new JPanel();
//        panelService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_Services")));
//        this.labelIServerLogin = new JLabel();
//        this.iServerLogin = new SmComboBoxIServerLogin();
//        GridBagLayout gridBagLayout = new GridBagLayout();
//        panelService.setLayout(gridBagLayout);
//        panelService.add(this.labelIServerLogin, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(0.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 10, 0));
//        panelService.add(this.iServerLogin, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(1).setAnchor(17).setInsets(0, 10, 10, 10));
//        return panelService;
//    }

    private JPanel getRestServicePanel() {
        JPanel panelRestService = new JPanel();
        panelRestService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_RestServices")));
        this.checkBoxRestMap = new JCheckBox("RestMap");
        this.checkBoxRestData = new JCheckBox("RestData");
        this.checkBoxRestRealspace = new JCheckBox("Realspace");
        this.checkBoxRestSpatialAnalyst = new JCheckBox("RestSpatialAnalyst");
        this.checkBoxRest_NetWorkAnalyst3d = new JCheckBox("Rest_NetWorkAnalyst3d");
        this.checkBoxRestTransPortationAnalyst = new JCheckBox("RestTransPortationAnalyst");
        this.checkBoxRestTrafficTransferAnalyst = new JCheckBox("RestTrafficAnalyst");
        this.checkBoxRestAddressMatch = new JCheckBox("RestTrafficAnalyst");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelRestService.setLayout(gbl_panelRestService);
        panelRestService.add(this.checkBoxRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelRestService.add(this.checkBoxRestData, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelRestService.add(this.checkBoxRestSpatialAnalyst, (new GridBagConstraintsHelper(0, 2, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelRestService.add(this.checkBoxRestTrafficTransferAnalyst, (new GridBagConstraintsHelper(0, 3, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 5, 0));
        panelRestService.add(this.checkBoxRestRealspace, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 10));
        panelRestService.add(this.checkBoxRest_NetWorkAnalyst3d, (new GridBagConstraintsHelper(1, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10));
        panelRestService.add(this.checkBoxRestTransPortationAnalyst, (new GridBagConstraintsHelper(1, 2, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10));
        panelRestService.add(this.checkBoxRestAddressMatch, (new GridBagConstraintsHelper(1, 3, 3, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 5, 10));
        return panelRestService;
    }

    private JPanel getOGCServicePanel() {
        JPanel panelOGCService = new JPanel();
        panelOGCService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OGCServices")));
        this.checkBoxWCS111 = new JCheckBox("WCS1.1.1");
        this.checkBoxWMS111 = new JCheckBox("WMS1.1.1");
        this.checkBoxWCS112 = new JCheckBox("WCS1.1.2");
        this.checkBoxWMS130 = new JCheckBox("WMS1.3.0");
        this.checkBoxWFS100 = new JCheckBox("WFS1.0.0");
        this.checkBoxWFS200 = new JCheckBox("WFS2.0.0");
        this.checkBoxWMTS100 = new JCheckBox("WMTS1.0.0");
        this.checkBoxWPS100 = new JCheckBox("WPS1.0.0");
        this.checkBoxWMTSCHINA = new JCheckBox("WMTS-CHINA");
        GridBagLayout gbl_panelOGCService = new GridBagLayout();
        panelOGCService.setLayout(gbl_panelOGCService);
        panelOGCService.add(this.checkBoxWCS111, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOGCService.add(this.checkBoxWCS112, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWFS100, (new GridBagConstraintsHelper(0, 2, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWFS200, (new GridBagConstraintsHelper(0, 3, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWPS100, (new GridBagConstraintsHelper(0, 4, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMS111, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 10));
        panelOGCService.add(this.checkBoxWMS130, (new GridBagConstraintsHelper(1, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10));
        panelOGCService.add(this.checkBoxWMTS100, (new GridBagConstraintsHelper(1, 2, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10));
        panelOGCService.add(this.checkBoxWMTSCHINA, (new GridBagConstraintsHelper(1, 3, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10));
        return panelOGCService;
    }

    private JPanel getOtherServicePanel() {
        JPanel panelOtherService = new JPanel();
        panelOtherService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OtherServices")));
        this.checkBoxAGSRestMap = new JCheckBox("AGSRestMap");
        this.checkBoxAGSRestData = new JCheckBox("AGSRestData");
        this.checkBoxAGSRestNetWorkAnalyst = new JCheckBox("AGSRestNetWorkAnalyst");
        this.checkBoxBaiduRest = new JCheckBox("BaiduRest");
        this.checkBoxGoogleRest = new JCheckBox("GoogleRest");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelOtherService.setLayout(gbl_panelRestService);
        panelOtherService.add(this.checkBoxAGSRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOtherService.add(this.checkBoxAGSRestData, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOtherService.add(this.checkBoxAGSRestNetWorkAnalyst, (new GridBagConstraintsHelper(0, 2, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 5, 0));
        panelOtherService.add(this.checkBoxBaiduRest, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 10).setIpad(45, 0));
        panelOtherService.add(this.checkBoxGoogleRest, (new GridBagConstraintsHelper(1, 1, 1, 1)).setWeight(1.0D, 0.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 10).setIpad(45, 0));
        return panelOtherService;
    }

    private void initializeResources() {
        this.setTitle(GAFProperties.getString("String_Alias")+"-"+NetServicesProperties.getString("String_ReleaseWorkspace"));
//        this.labelIServerLogin.setText(NetServicesProperties.getString("String_LabelChooseIServer"));
        this.checkBoxRestData.setText(CoreProperties.getString("String_iServer_ServicesType_RestData"));
        this.checkBoxRestRealspace.setText(CoreProperties.getString("String_iServer_ServicesType_RestRealspace"));
        this.checkBoxRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_RestMap"));
        this.checkBoxRestSpatialAnalyst.setText(CoreProperties.getString("String_iServer_ServicesType_RestSpatialAnalyst"));
        this.checkBoxRest_NetWorkAnalyst3d.setText(CoreProperties.getString("String_iServer_ServicesType_Rest_NetWorkAnalyst3d"));
        this.checkBoxRestTransPortationAnalyst.setText(CoreProperties.getString("String_iServer_ServicesType_RestTransPortationAnalyst"));
        this.checkBoxRestTrafficTransferAnalyst.setText(CoreProperties.getString("String_iServer_ServicesType_RestTrafficTransferAnalyst"));
        this.checkBoxRestAddressMatch.setText(CoreProperties.getString("String_iServer_ServicesType_RestAddressMatch"));
        this.checkBoxWCS111.setText(CoreProperties.getString("String_iServer_ServicesType_WCS111"));
        this.checkBoxWMS111.setText(CoreProperties.getString("String_iServer_ServicesType_WMS111"));
        this.checkBoxWCS112.setText(CoreProperties.getString("String_iServer_ServicesType_WCS112"));
        this.checkBoxWMS130.setText(CoreProperties.getString("String_iServer_ServicesType_WMS130"));
        this.checkBoxWFS100.setText(CoreProperties.getString("String_iServer_ServicesType_WFS100"));
        this.checkBoxWFS200.setText(CoreProperties.getString("String_iServer_ServicesType_WFS200"));
        this.checkBoxWMTS100.setText(CoreProperties.getString("String_iServer_ServicesType_WMTS100"));
        this.checkBoxWPS100.setText(CoreProperties.getString("String_iServer_ServicesType_WPS100"));
        this.checkBoxWMTSCHINA.setText(CoreProperties.getString("String_iServer_ServicesType_WMTSCHINA"));
        this.checkBoxAGSRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_AGSRestMap"));
        this.checkBoxAGSRestData.setText(CoreProperties.getString("String_iServer_ServicesType_AGSRestData"));
        this.checkBoxAGSRestNetWorkAnalyst.setText(CoreProperties.getString("String_iServer_ServicesType_AGSRestNetWorkAnalyst"));
        this.checkBoxBaiduRest.setText(CoreProperties.getString("String_iServer_ServicesType_BaiduRest"));
        this.checkBoxGoogleRest.setText(CoreProperties.getString("String_iServer_ServicesType_GoogleRest"));
        this.checkBoxIsEditable.setText(CoreProperties.getString("String_IsEditable"));
        this.checkBoxIsUpload.setText(CoreProperties.getString("String_UploadWorkspace"));
        this.buttonRelease.setText(NetServicesProperties.getString("String_Release"));
        this.buttonClose.setText(CoreProperties.getString("String_Close"));
    }

    private void registerEvents() {
//        this.iServerLogin.addIServerLoginInfoSelectionChangedListener(this.loginInfoSelectionChangedListener);
        this.checkBoxRestData.addActionListener(this);
        this.checkBoxRestMap.addActionListener(this);
        this.checkBoxRestRealspace.addActionListener(this);
        this.checkBoxRestSpatialAnalyst.addActionListener(this);
        this.checkBoxRest_NetWorkAnalyst3d.addActionListener(this);
        this.checkBoxRestTransPortationAnalyst.addActionListener(this);
        this.checkBoxRestTrafficTransferAnalyst.addActionListener(this);
        this.checkBoxRestAddressMatch.addActionListener(this);
        this.checkBoxWCS111.addActionListener(this);
        this.checkBoxWMS111.addActionListener(this);
        this.checkBoxWCS112.addActionListener(this);
        this.checkBoxWMS130.addActionListener(this);
        this.checkBoxWFS100.addActionListener(this);
        this.checkBoxWFS200.addActionListener(this);
        this.checkBoxWMTS100.addActionListener(this);
        this.checkBoxWPS100.addActionListener(this);
        this.checkBoxWMTSCHINA.addActionListener(this);
        this.checkBoxAGSRestMap.addActionListener(this);
        this.checkBoxAGSRestData.addActionListener(this);
        this.checkBoxAGSRestNetWorkAnalyst.addActionListener(this);
        this.checkBoxBaiduRest.addActionListener(this);
        this.checkBoxGoogleRest.addActionListener(this);
        this.checkBoxIsEditable.addActionListener(this);
        this.buttonRelease.addActionListener(this);
        this.buttonClose.addActionListener(this);
    }

    private boolean canRemoteRelease() {
        Boolean canRemoteRelease = true;

        try {
            if (!StringUtilities.isNullOrEmpty(this.workspaceInfo.getWorkspacePath())) {
                File workspaceDirectory = (new File(this.workspaceInfo.getWorkspacePath())).getParentFile();
                ArrayList datasourcesPath = this.workspaceInfo.getDatasourcesPath();

                for(int i = 0; i < datasourcesPath.size(); ++i) {
                    File datasourceDirectory = (new File((String)datasourcesPath.get(i))).getParentFile();
                    if (!workspaceDirectory.equals(datasourceDirectory)) {
                        canRemoteRelease = false;
                        break;
                    }
                }
            }
        } catch (Exception var6) {
            Application.getActiveApplication().getOutput().output(var6);
        }

        return canRemoteRelease;
    }

    private void initializeParameters() {
        try {
            this.isEditable = false;
            this.servicesType = 0;
            if (this.workspaceInfo != null) {
                if (this.workspaceInfo.getContainTypes().size() > 0) {
                    this.servicesType |= 1;
                    this.servicesType |= 65536;
                    this.servicesType |= 2097152;
                }

                if (this.workspaceInfo.getMapCounts() > 0) {
                    this.servicesType |= 2;
                    this.servicesType |= 32768;
                    this.servicesType |= 262144;
                    this.servicesType |= 524288;
                }

                if (this.workspaceInfo.getSceneCounts() > 0) {
                    this.servicesType |= 4;
                }

                if (ListUtilities.isListContainAny(this.workspaceInfo.getContainTypes(), new DatasetType[]{DatasetType.CAD, DatasetType.GRID, DatasetType.LINE, DatasetType.LINEM, DatasetType.NETWORK, DatasetType.POINT, DatasetType.REGION, DatasetType.TABULAR})) {
                    this.servicesType |= 8;
                }

                if (this.workspaceInfo.getContainTypes().contains(DatasetType.NETWORK)) {
                    this.servicesType |= 16;
                    this.servicesType |= 131072;
                }

                if (this.workspaceInfo.getContainTypes().contains(DatasetType.NETWORK3D)) {
                    this.servicesType |= 1048576;
                }
            }
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void initializeControls() {
        this.initializeServicesTypes();
    }

    private void initializeServicesTypes() {
        try {
            this.checkBoxIsEditable.setEnabled(false);
            this.checkBoxRestMap.setEnabled(true);
            this.checkBoxRestData.setEnabled(true);
            this.checkBoxRestRealspace.setEnabled(true);
            this.checkBoxRestSpatialAnalyst.setEnabled(true);
            this.checkBoxRest_NetWorkAnalyst3d.setEnabled(true);
            this.checkBoxRestTransPortationAnalyst.setEnabled(true);
            this.checkBoxRestTrafficTransferAnalyst.setEnabled(true);
            this.checkBoxRestAddressMatch.setEnabled(true);
            this.checkBoxAGSRestMap.setEnabled(true);
            this.checkBoxAGSRestData.setEnabled(true);
            this.checkBoxAGSRestNetWorkAnalyst.setEnabled(true);
            this.checkBoxBaiduRest.setEnabled(true);
            this.checkBoxGoogleRest.setEnabled(true);
            if ((this.servicesType & 1) != 1) {
                this.checkBoxRestData.setEnabled(false);
            }

            if ((this.servicesType & 2) != 2) {
                this.checkBoxRestMap.setEnabled(false);
            }

            if ((this.servicesType & 4) != 4) {
                this.checkBoxRestRealspace.setEnabled(false);
            }

            if ((this.servicesType & 8) != 8) {
                this.checkBoxRestSpatialAnalyst.setEnabled(false);
            }

            if ((this.servicesType & 1048576) != 1048576) {
                this.checkBoxRest_NetWorkAnalyst3d.setEnabled(false);
            }

            if ((this.servicesType & 16) != 16) {
                this.checkBoxRestTransPortationAnalyst.setEnabled(false);
            }

            if ((this.servicesType & 32) != 32) {
                this.checkBoxRestTrafficTransferAnalyst.setEnabled(false);
            }

            if ((this.servicesType & 2097152) != 2097152) {
                this.checkBoxRestAddressMatch.setEnabled(false);
            }

            if ((this.servicesType & '耀') != 32768) {
                this.checkBoxAGSRestMap.setEnabled(false);
            }

            if ((this.servicesType & 65536) != 65536) {
                this.checkBoxAGSRestData.setEnabled(false);
            }

            if ((this.servicesType & 131072) != 131072) {
                this.checkBoxAGSRestNetWorkAnalyst.setEnabled(false);
            }

            if ((this.servicesType & 262144) != 262144) {
                this.checkBoxBaiduRest.setEnabled(false);
            }

            if ((this.servicesType & 524288) != 524288) {
                this.checkBoxGoogleRest.setEnabled(false);
            }

            this.servicesType = 0;
            this.setCheckBoxEditableState();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void setCheckBoxEditableState() {
        if (!ServerReleaseWorkspace.isSupportMapEditabled(this.servicesType) && !ServerReleaseWorkspace.isSupportDataEditabled(this.servicesType)) {
            this.checkBoxIsEditable.setEnabled(false);
            this.checkBoxIsEditable.setSelected(false);
        } else {
            this.checkBoxIsEditable.setEnabled(true);
            this.checkBoxIsEditable.setSelected(this.isEditable);
        }

    }

    private void setCheckBoxIsUploadState() {
        this.checkBoxIsUpload.setSelected(true);
        this.checkBoxIsUpload.setEnabled(this.isLocal());
    }

    private void setButtonReleaseEnabled() {
        try {
            if (this.canRelease && this.servicesType != 0 && this.iServerLogin.getIServerLoginInfo() != null) {
                this.buttonRelease.setEnabled(true);
                this.getRootPane().setDefaultButton(this.buttonRelease);
            } else {
                this.buttonRelease.setEnabled(false);
            }
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWPS100CheckChange() {
        try {
            if (this.checkBoxWPS100.isSelected()) {
                this.servicesType |= 8192;
            } else {
                this.servicesType ^= 8192;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMTSCHINACheckChange() {
        try {
            if (this.checkBoxWMTSCHINA.isSelected()) {
                this.servicesType |= 4096;
            } else {
                this.servicesType ^= 4096;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMTS100CheckChange() {
        try {
            if (this.checkBoxWMTS100.isSelected()) {
                this.servicesType |= 2048;
            } else {
                this.servicesType ^= 2048;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS130CheckChange() {
        try {
            if (this.checkBoxWMS130.isSelected()) {
                this.servicesType |= 1024;
            } else {
                this.servicesType ^= 1024;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS111CheckChange() {
        try {
            if (this.checkBoxWMS111.isSelected()) {
                this.servicesType |= 512;
            } else {
                this.servicesType ^= 512;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWFS100CheckChange() {
        try {
            if (this.checkBoxWFS100.isSelected()) {
                this.servicesType |= 256;
            } else {
                this.servicesType ^= 256;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWFS200CheckChange() {
        try {
            if (this.checkBoxWFS200.isSelected()) {
                this.servicesType |= 16384;
            } else {
                this.servicesType ^= 16384;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWCS112CheckChange() {
        try {
            if (this.checkBoxWCS112.isSelected()) {
                this.servicesType |= 128;
            } else {
                this.servicesType ^= 128;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWCS111CheckChange() {
        try {
            if (this.checkBoxWCS111.isSelected()) {
                this.servicesType |= 64;
            } else {
                this.servicesType ^= 64;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestDataCheckChange() {
        try {
            if (this.checkBoxRestData.isSelected()) {
                this.servicesType |= 1;
            } else {
                this.servicesType ^= 1;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestMapCheckChange() {
        try {
            if (this.checkBoxRestMap.isSelected()) {
                this.servicesType |= 2;
            } else {
                this.servicesType ^= 2;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestRealspaceCheckChange() {
        try {
            if (this.checkBoxRestRealspace.isSelected()) {
                this.servicesType |= 4;
            } else {
                this.servicesType ^= 4;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestSpatialAnalystCheckChange() {
        try {
            if (this.checkBoxRestSpatialAnalyst.isSelected()) {
                this.servicesType |= 8;
            } else {
                this.servicesType ^= 8;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRest_NetWorkAnalyst3dCheckChange() {
        try {
            if (this.checkBoxRest_NetWorkAnalyst3d.isSelected()) {
                this.servicesType |= 1048576;
            } else {
                this.servicesType ^= 1048576;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestTransPortationAnalystCheckChange() {
        try {
            if (this.checkBoxRestTransPortationAnalyst.isSelected()) {
                this.servicesType |= 16;
            } else {
                this.servicesType ^= 16;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestTrafficTransferAnalystCheckChange() {
        try {
            if (this.checkBoxRestTrafficTransferAnalyst.isSelected()) {
                this.servicesType |= 32;
            } else {
                this.servicesType ^= 32;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestAddressMatchCheckChange() {
        try {
            if (this.checkBoxRestAddressMatch.isSelected()) {
                this.servicesType |= 2097152;
            } else {
                this.servicesType ^= 2097152;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxAGSRestMapCheckChange() {
        try {
            if (this.checkBoxAGSRestMap.isSelected()) {
                this.servicesType |= 32768;
            } else {
                this.servicesType ^= 32768;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxAGSRestDataCheckChange() {
        try {
            if (this.checkBoxAGSRestData.isSelected()) {
                this.servicesType |= 65536;
            } else {
                this.servicesType ^= 65536;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxAGSRestNetWorkAnalystCheckChange() {
        try {
            if (this.checkBoxAGSRestNetWorkAnalyst.isSelected()) {
                this.servicesType |= 131072;
            } else {
                this.servicesType ^= 131072;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxBaiduRestCheckChange() {
        try {
            if (this.checkBoxBaiduRest.isSelected()) {
                this.servicesType |= 262144;
            } else {
                this.servicesType ^= 262144;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxGoogleRestCheckChange() {
        try {
            if (this.checkBoxGoogleRest.isSelected()) {
                this.servicesType |= 524288;
            } else {
                this.servicesType ^= 524288;
            }

            this.setCheckBoxEditableState();
            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void buttonReleaseClicked() {
        if (this.iServerLogin.getIServerLoginInfo() != null) {
            CursorUtilities.setWaitCursor();

            try {
                ServerReleaseWorkspace serverRelease = this.fillServerRelease();
                if (!StringUtilities.isNullOrEmpty(serverRelease.getWorkspacePath()) && (serverRelease.getHostType() == 1 || this.checkBoxIsUpload.isSelected())) {
                    ArrayList files = new ArrayList<SelectableFile>() {
                        public boolean add(SelectableFile selectableFile) {
                            Iterator var2 = this.iterator();

                            SelectableFile hasFile;
                            do {
                                if (!var2.hasNext()) {
                                    return super.add(selectableFile);
                                }

                                hasFile = (SelectableFile)var2.next();
                            } while(!FileUtilities.isEquals(hasFile.getPath(), selectableFile.getPath()));

                            return false;
                        }
                    };
                    Datasources datasources = Application.getActiveApplication().getWorkspace().getDatasources();

                    String uddPath;
                    for(int j = 0; j < datasources.getCount(); ++j) {
                        Datasource datasource = datasources.get(j);
                        String udbPath;
                        if (EngineType.UDBX == datasource.getEngineType()) {
                            udbPath = datasource.getConnectionInfo().getServer();
                            if (FileUtilities.isFilePath(udbPath)) {
                                files.add(new SelectableFile(udbPath, true));
                            }
                        } else if (EngineType.UDB == datasource.getEngineType()) {
                            udbPath = datasource.getConnectionInfo().getServer();
                            if (FileUtilities.isFilePath(udbPath) && udbPath.endsWith(".udb")) {
                                uddPath = udbPath.replace(".udb", ".udd");
                                if (FileUtilities.isFilePath(uddPath)) {
                                    files.add(new SelectableFile(udbPath, true));
                                    files.add(new SelectableFile(uddPath, true));
                                }
                            }
                        }
                    }

                    File workDirectory = new File(serverRelease.getWorkDirectory());
                    File[] childFiles = workDirectory.listFiles();

                    for(int i = 0; i < childFiles.length; ++i) {
                        if (!childFiles[i].isHidden()) {
                            uddPath = null;
                            SelectableFile selectableFile;
                            if (serverRelease.getWorkspacePath().equals(childFiles[i].getPath())) {
                                selectableFile = SelectableFile.fromFile(childFiles[i], true);
                            } else {
                                selectableFile = SelectableFile.fromFile(childFiles[i], false);
                            }

                            selectableFile.getSizeString();
                            files.add(selectableFile);
                        }
                    }

                    JDialogFolderSelector folderSelector = new JDialogFolderSelector(files, serverRelease.getWorkspacePath());
                    if (folderSelector.showDialog() != DialogResult.OK) {
                        Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_Operation_Cancel"));
                        return;
                    }

                    serverRelease.getFiles().clear();

                    for(int i = 0; i < files.size(); ++i) {
                        SelectableFile selectableFile = (SelectableFile)files.get(i);
                        if (selectableFile.isSelected()) {
                            serverRelease.getFiles().add(selectableFile);
                        }
                    }
                }

                SmDialogProgressTotal smDialogProgressTotal = new SmDialogProgressTotal(NetServicesProperties.getString("String_ReleaseWorkspace"));
                smDialogProgressTotal.doWork(new DialogGAFReleaseWorkspace.ServerReleaseCallable(serverRelease), new IAfterWork<Boolean>() {
                    public void afterWork(Boolean param) {
                        if (param) {
                            if (SwingUtilities.isEventDispatchThread()) {
                                DialogGAFReleaseWorkspace.this.setVisible(false);
                            } else {
                                SwingUtilities.invokeLater(() -> {
                                    DialogGAFReleaseWorkspace.this.setVisible(false);
                                });
                            }
                            CommonUtils.registerService(serverRelease.getResultServers(),workspaceId,1);
                        }

                    }
                });
            } catch (Exception var12) {
                Application.getActiveApplication().getOutput().output(var12);
            } finally {
                CursorUtilities.setDefaultCursor();
            }

        }
    }

    private void buttonCloseClicked() {
        try {
            this.setDialogResult(DialogResult.CANCEL);
            this.setVisible(false);
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private boolean isLocal() {
        IServerLoginInfo iServerLoginInfo = this.iServerLogin.getIServerLoginInfo();
        if (iServerLoginInfo == null) {
            return false;
        } else {
            ServerReleaseWorkspace serverRelease = new ServerReleaseWorkspace(iServerLoginInfo);
            return serverRelease.getHostType() == 0;
        }
    }

    private ServerReleaseWorkspace fillServerRelease() {
        IServerLoginInfo iServerLoginInfo = this.iServerLogin.getIServerLoginInfo();
        if (iServerLoginInfo == null) {
            return null;
        } else {
            ServerReleaseWorkspace serverRelease = new GAFServerReleaseWorkspace(iServerLoginInfo);
            serverRelease.setUpload(this.checkBoxIsUpload.isSelected());
            serverRelease.setServicesType(this.servicesType);
            serverRelease.setEditable(this.isEditable);
            serverRelease.setConnectionInfo(this.workspaceInfo.getWorkspaceConnectionInfo());
            serverRelease.setWorkspacePath(this.workspaceInfo.getWorkspacePath());
            return serverRelease;
        }
    }

    private class ServerReleaseCallable extends UpdateProgressCallable {
        private ServerReleaseWorkspace b;
        private FunctionProgressListener c = new FunctionProgressListener() {
            public void functionProgress(FunctionProgressEvent event) {
                try {
                    ServerReleaseCallable.this.updateProgressTotal(event.getCurrentProgress(), event.getTotalProgress(), event.getCurrentMessage(), event.getTotalMessage(), "", 1, 1);
                } catch (CancellationException var3) {
                }

            }
        };

        public ServerReleaseCallable(ServerReleaseWorkspace serverRelease) {
            this.b = serverRelease;
        }

        public Boolean call() {
            boolean result = true;

            try {
                Application.getActiveApplication().getOutput().output(CoreProperties.getString("String_ReleaseStart"));
                Date startTime = new Date();
                this.b.addFunctionProgressListener(this.c);
                boolean releaseResult = this.b.release();

                if (releaseResult) {
                    long totalTime = (new Date()).getTime() - startTime.getTime();
                    Application.getActiveApplication().getOutput().output(MessageFormat.format(NetServicesProperties.getString("String_iServer_Message_ReleaseSuccess"), String.valueOf(totalTime / 1000L)));
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_ReleaseMapCacheSuccessResult"));
                    ArrayList resultServers = this.a(this.b.getResultServers());

                    for(int i = 0; i < resultServers.size(); ++i) {
                        Application.getActiveApplication().getOutput().output((String)resultServers.get(i));
                    }
                } else {
                    result = false;
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_iServer_Message_ReleaseFaild"));
                }
            } catch (Exception var11) {
                result = false;
                Application.getActiveApplication().getOutput().output(var11);
            } finally {
                this.b.removeFunctionProgressListener(this.c);
                ServerReleaseWorkspace.clearTmp();
            }

            return result;
        }


        private ArrayList<String> a(String links) {
            ArrayList result = new ArrayList();

            try {
                Object obj = JSON.parse(links);
                if (obj instanceof JSONArray) {
                    JSONArray jsonArray = (JSONArray)obj;

                    for(int i = 0; i < jsonArray.size(); ++i) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.containsKey("serviceType") && jsonObject.containsKey("serviceAddress")) {
                            result.add(ServiceType.serviceTypeLocalized(jsonObject.get("serviceType").toString()) + ": " + jsonObject.get("serviceAddress"));
                        }
                    }
                }
            } catch (Exception var7) {
                Application.getActiveApplication().getOutput().output(var7);
            }

            return result;
        }
    }
}
