//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.controls.ui.controls.SmDialog;
import com.supermap.desktop.controls.ui.controls.button.SmButton;
import com.supermap.desktop.controls.ui.controls.componentBorderPanel.SmPanelComponentTitle;
import com.supermap.desktop.controls.ui.controls.panels.SmPanelMongoDBLogin;
import com.supermap.desktop.controls.ui.controls.progress.SmDialogProgressTotal;
import com.supermap.desktop.controls.utilities.ComponentFactory;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.Interface.IAfterWork;
import com.supermap.desktop.core.enums.InfoType;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedEvent;
import com.supermap.desktop.core.event.IServerLoginInfoSelectionChangedListener;
import com.supermap.desktop.core.iserver.FunctionProgressEvent;
import com.supermap.desktop.core.iserver.FunctionProgressListener;
import com.supermap.desktop.core.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import com.supermap.desktop.core.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.core.utilties.CursorUtilities;
import com.supermap.desktop.core.utilties.NetworkUtilties;
import com.supermap.desktop.core.utilties.StringUtilities;
import com.supermap.desktop.develop.entity.GAFTileStorageConnection;
import com.supermap.desktop.develop.rewrite.ServerReleaseGAFMongoDBMapCache;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.netservices.NetServicesProperties;
import com.supermap.iobjects.processes.publish.processes.ServiceType;
import com.supermap.tilestorage.TileDataType;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageInfo;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CancellationException;

public class DialogGAFReleaseMongoCache extends SmDialog implements ActionListener {
    private static final long serialVersionUID = 1L;
    private String tileId;
    private String pluginAlias;
//    private JLabel labelIServerLogin;
    private SmComboBoxGAFIServerLogin iServerLogin;
    private SmPanelGAFMongoDBLogin smPanelMongoDBLogin;
    private JLabel labelReleaseCache;
    private JComboBox comboBoxReleaseMap;
    private JCheckBox checkBoxReleaseMapServer;
    private JCheckBox checkBoxRelease3DServer;
    private JTable tableRelease3DServer;
    private DialogGAFReleaseMongoCache.TableModelReleaseMongoDB tableModel;
    private JCheckBox checkBoxRestMap;
    private JCheckBox checkBoxRestVectorCache;
    private JCheckBox checkBoxWMS111;
    private JCheckBox checkBoxWMS130;
    private JCheckBox checkBoxWMTS100;
    private JCheckBox checkBoxWMTSCHINA;
    private JCheckBox checkBoxAGSRestMap;
    private JCheckBox checkBoxBaiduRest;
    private JCheckBox checkBoxGoogleRest;
    private SmButton buttonRelease;
    private SmButton buttonClose;
    private ArrayList<String> tilesetNames = new ArrayList();
    private int servicesType = 2;

    private IServerLoginInfoSelectionChangedListener loginInfoSelectionChangedListener = new IServerLoginInfoSelectionChangedListener() {
        public void selectionChanged(IServerLoginInfoSelectionChangedEvent event) {
            DialogGAFReleaseMongoCache.this.setButtonReleaseEnabled();
        }
    };
    private ItemListener releaseMapItemListener = new ItemListener() {
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == 1) {
                boolean isGridCache = false;
                if (DialogGAFReleaseMongoCache.this.comboBoxReleaseMap.getSelectedItem() instanceof TileStorageInfo && !((TileStorageInfo)DialogGAFReleaseMongoCache.this.comboBoxReleaseMap.getSelectedItem()).getDataType().equals(TileDataType.NONE)) {
                    isGridCache = true;
                }

                DialogGAFReleaseMongoCache.this.checkBoxRestVectorCache.setEnabled(!isGridCache);
                DialogGAFReleaseMongoCache.this.setButtonReleaseEnabled();
            }

        }
    };
    private TableModelListener tableModelListener = new TableModelListener() {
        public void tableChanged(TableModelEvent e) {
            ArrayList dataBases = DialogGAFReleaseMongoCache.this.tableModel.getData();
            DialogGAFReleaseMongoCache.this.tilesetNames.clear();

            for(int i = 0; i < dataBases.size(); ++i) {
                if (((DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase)dataBases.get(i)).a) {
                    DialogGAFReleaseMongoCache.this.tilesetNames.add(((DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase)dataBases.get(i)).getCacheName());
                }
            }

            DialogGAFReleaseMongoCache.this.setButtonReleaseEnabled();
        }
    };
    private PropertyChangeListener mongoDBCacheChanged = new PropertyChangeListener() {
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(SmPanelMongoDBLogin.DATABASE_NAME_CHANGED)) {
                DialogGAFReleaseMongoCache.this.initComboBoxReleaseMap();
                DialogGAFReleaseMongoCache.this.initTableDatas();
            }

        }
    };

    private void initTableDatas() {
        if (this.checkBoxRelease3DServer.isSelected()) {
            this.getTableModel().setDatas(this.smPanelMongoDBLogin.getTileStorageInfos());
        } else {
            this.getTableModel().setDatas(new ArrayList());
        }

        this.setButtonReleaseEnabled();
    }

    private void initComboBoxReleaseMap() {
        if (null == this.comboBoxReleaseMap) {
            this.comboBoxReleaseMap = new JComboBox();
            this.comboBoxReleaseMap.setRenderer(new SubstanceDefaultListCellRenderer() {
                public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                    super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                    if (value instanceof TileStorageInfo) {
                        this.setText(((TileStorageInfo)value).getName());
                    } else if (value instanceof String) {
                        this.setText((String)value);
                    }

                    return this;
                }
            });
        }

        this.comboBoxReleaseMap.removeAllItems();
        ArrayList mongoDBCacheInfos = this.smPanelMongoDBLogin.getTileStorageInfos();
        if (null != mongoDBCacheInfos && !mongoDBCacheInfos.isEmpty()) {
//            this.comboBoxReleaseMap.addItem(ControlsProperties.getString("String_All"));
            Iterator var2 = mongoDBCacheInfos.iterator();

            while(var2.hasNext()) {
                TileStorageInfo mapCacheInfo = (TileStorageInfo)var2.next();
                this.comboBoxReleaseMap.addItem(mapCacheInfo);
            }

            this.comboBoxReleaseMap.setSelectedIndex(0);
        }
    }

    public DialogGAFReleaseMongoCache(GAFTileStorageConnection tileStorageConnection,String pluginAlias) {
        this.pluginAlias = pluginAlias;
        this.tileId = tileStorageConnection.getId();
        this.iServerLogin = CommonUtils.getGAFIserverLogin();
        this.smPanelMongoDBLogin = new SmPanelGAFMongoDBLogin(tileStorageConnection);
        this.initializeComponents();
        this.initializeResources();
        this.registerEvents();
        this.setButtonReleaseEnabled();
        this.componentList.add(this.buttonRelease);
        this.componentList.add(this.buttonClose);
        this.setFocusTraversalPolicy(this.policy);
        this.setSize(new Dimension(400, 600));
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.buttonRelease) {
            this.releaseMongoDBMapCache();
        } else if (e.getSource() == this.buttonClose) {
            this.dispose();
        } else if (e.getSource() == this.checkBoxWMTS100) {
            this.checkBoxWMTS100CheckChange();
        } else if (e.getSource() == this.checkBoxWMTSCHINA) {
            this.checkBoxWMTSCHINACheckChange();
        } else if (e.getSource() == this.checkBoxWMS130) {
            this.checkBoxWMS130CheckChange();
        } else if (e.getSource() == this.checkBoxWMS111) {
            this.checkBoxWMS111CheckChange();
        } else if (e.getSource() == this.checkBoxRestMap) {
            this.checkBoxRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxRestVectorCache) {
            this.checkBoxRestVectorCacheCheckChange();
        } else if (e.getSource() == this.checkBoxAGSRestMap) {
            this.checkBoxAGSRestMapCheckChange();
        } else if (e.getSource() == this.checkBoxBaiduRest) {
            this.checkBoxBaiduRestCheckChange();
        } else if (e.getSource() == this.checkBoxGoogleRest) {
            this.checkBoxGoogleRestCheckChange();
        } else if (e.getSource() == this.checkBoxRelease3DServer) {
            this.initTableDatas();
        } else if (e.getSource() == this.checkBoxReleaseMapServer) {
            this.setReleaseMapServerEnabled();
        }

    }

    private void setReleaseMapServerEnabled() {
        boolean isEnabled = this.checkBoxReleaseMapServer.isSelected();
        this.comboBoxReleaseMap.setEnabled(isEnabled);
        this.checkBoxRestMap.setEnabled(isEnabled);
        this.checkBoxRestVectorCache.setEnabled(isEnabled);
        this.checkBoxWMS111.setEnabled(isEnabled);
        this.checkBoxWMS130.setEnabled(isEnabled);
        this.checkBoxWMTS100.setEnabled(isEnabled);
        this.checkBoxWMTSCHINA.setEnabled(isEnabled);
        this.checkBoxAGSRestMap.setEnabled(isEnabled);
        this.checkBoxBaiduRest.setEnabled(isEnabled);
        this.checkBoxGoogleRest.setEnabled(isEnabled);
        this.setButtonReleaseEnabled();
    }

    private void initializeComponents() {
//        JPanel panelService = this.getServicePanel();
//        JPanel panelMongoDB = this.getMongoDBPanel();
        JPanel panelRestService = this.getRestServicePanel();
        JPanel panelOGCService = this.getOGCServicePanel();
        JPanel panelOtherService = this.getOtherServicePanel();
        JPanel panelRelease3D = this.getRelease3DPanel();
        this.checkBoxReleaseMapServer = new JCheckBox();
        this.checkBoxReleaseMapServer.setSelected(true);
        this.labelReleaseCache = new JLabel();
        this.initComboBoxReleaseMap();
        JPanel releaseMapPanel = new JPanel();
        GridBagLayout gridBagLayout = new GridBagLayout();
        releaseMapPanel.setLayout(gridBagLayout);
        releaseMapPanel.add(this.labelReleaseCache, (new GridBagConstraintsHelper(0, 0, 1, 1)).setAnchor(11).setInsets(0, 5, 0, 10).setFill(1).setWeight(0.0D, 0.0D));
        releaseMapPanel.add(this.comboBoxReleaseMap, (new GridBagConstraintsHelper(1, 0, 1, 1)).setAnchor(11).setInsets(0, 0, 0, 5).setFill(1).setWeight(1.0D, 0.0D));
        releaseMapPanel.add(panelRestService, (new GridBagConstraintsHelper(0, 1, 2, 1)).setAnchor(11).setInsets(0, 0, 0, 0).setFill(1).setWeight(1.0D, 1.0D));
        releaseMapPanel.add(panelOGCService, (new GridBagConstraintsHelper(0, 2, 2, 1)).setAnchor(11).setInsets(0, 0, 0, 0).setFill(1).setWeight(1.0D, 1.0D));
        releaseMapPanel.add(panelOtherService, (new GridBagConstraintsHelper(0, 3, 2, 1)).setAnchor(11).setInsets(0, 0, 0, 0).setFill(1).setWeight(1.0D, 1.0D));
        SmPanelComponentTitle releaseMapTitledPane = new SmPanelComponentTitle(this.checkBoxReleaseMapServer, releaseMapPanel);
        JPanel panelMain = new JPanel();
        this.setContentPane(panelMain);
        this.buttonRelease = new SmButton("Release");
        this.buttonClose = new SmButton("Close");
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
//        panelMain.add(panelService, (new GridBagConstraintsHelper(0, 0, 2, 1)).setAnchor(11).setInsets(10, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
//        panelMain.add(panelMongoDB, (new GridBagConstraintsHelper(0, 1, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(releaseMapTitledPane, (new GridBagConstraintsHelper(0, 2, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
        panelMain.add(panelRelease3D, (new GridBagConstraintsHelper(0, 3, 2, 1)).setAnchor(11).setInsets(0, 10, 10, 10).setFill(1).setWeight(1.0D, 1.0D));
        panelMain.add(ComponentFactory.createButtonPanel(new JComponent[]{this.buttonRelease, this.buttonClose}), (new GridBagConstraintsHelper(0, 6, 2, 1)).setAnchor(14).setInsets(0, 10, 10, 10).setFill(2).setWeight(1.0D, 0.0D));
    }

//    private JPanel getServicePanel() {
//        JPanel panelService = new JPanel();
//        panelService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_Services")));
//        this.labelIServerLogin = new JLabel();
//        this.iServerLogin = new SmComboBoxIServerLogin();
//        GridBagLayout gridBagLayout = new GridBagLayout();
//        panelService.setLayout(gridBagLayout);
//        panelService.add(this.labelIServerLogin, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(0.0D, 0.0D).setFill(0).setAnchor(17).setInsets(0, 10, 10, 0));
//        panelService.add(this.iServerLogin, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 0.0D).setFill(1).setAnchor(17).setInsets(0, 10, 10, 10));
//        return panelService;
//    }

//    private JPanel getMongoDBPanel() {
//        this.smPanelMongoDBLogin = new SmPanelMongoDBLogin();
//        this.smPanelMongoDBLogin.setBorder(BorderFactory.createTitledBorder(NetServicesProperties.getString("String_LoginMongoDB")));
//        return this.smPanelMongoDBLogin;
//    }

    private JTable getTableRelease3DServer() {
        if (this.tableRelease3DServer == null) {
            this.tableRelease3DServer = new JTable();
            this.tableRelease3DServer.setModel(this.getTableModel());
            this.tableRelease3DServer.getColumn(this.tableRelease3DServer.getModel().getColumnName(0)).setMaxWidth(30);
            this.tableRelease3DServer.getTableHeader().setReorderingAllowed(false);
        }

        return this.tableRelease3DServer;
    }

    private DialogGAFReleaseMongoCache.TableModelReleaseMongoDB getTableModel() {
        if (this.tableModel == null) {
            this.tableModel = new DialogGAFReleaseMongoCache.TableModelReleaseMongoDB();
        }

        return this.tableModel;
    }

    private JPanel getRestServicePanel() {
        JPanel panelRestService = new JPanel();
        panelRestService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_RestServices")));
        this.checkBoxRestMap = new JCheckBox("RestMap");
        this.checkBoxRestMap.setSelected(true);
        this.checkBoxRestVectorCache = new JCheckBox("RestVectorCache");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelRestService.setLayout(gbl_panelRestService);
        panelRestService.add(this.checkBoxRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelRestService.add(this.checkBoxRestVectorCache, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        return panelRestService;
    }

    private JPanel getOGCServicePanel() {
        JPanel panelOGCService = new JPanel();
        panelOGCService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OGCServices")));
        this.checkBoxWMS111 = new JCheckBox("WMS1.1.1");
        this.checkBoxWMS130 = new JCheckBox("WMS1.3.0");
        this.checkBoxWMTS100 = new JCheckBox("WMTS1.0.0");
        this.checkBoxWMTSCHINA = new JCheckBox("WMTS-CHINA");
        GridBagLayout gbl_panelOGCService = new GridBagLayout();
        panelOGCService.setLayout(gbl_panelOGCService);
        panelOGCService.add(this.checkBoxWMS111, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMS130, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMTS100, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        panelOGCService.add(this.checkBoxWMTSCHINA, (new GridBagConstraintsHelper(1, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        return panelOGCService;
    }

    private JPanel getOtherServicePanel() {
        JPanel panelOtherService = new JPanel();
        panelOtherService.setBorder(BorderFactory.createTitledBorder(CoreProperties.getString("String_OtherServices")));
        this.checkBoxAGSRestMap = new JCheckBox("AGSRestMap");
        this.checkBoxBaiduRest = new JCheckBox("BaiduRest");
        this.checkBoxGoogleRest = new JCheckBox("GoogleRest");
        GridBagLayout gbl_panelRestService = new GridBagLayout();
        panelOtherService.setLayout(gbl_panelRestService);
        panelOtherService.add(this.checkBoxAGSRestMap, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0));
        panelOtherService.add(this.checkBoxBaiduRest, (new GridBagConstraintsHelper(1, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(0, 10, 0, 0).setIpad(25, 0));
        panelOtherService.add(this.checkBoxGoogleRest, (new GridBagConstraintsHelper(0, 1, 1, 1)).setWeight(1.0D, 1.0D).setFill(0).setAnchor(17).setInsets(10, 10, 0, 0));
        return panelOtherService;
    }

    private JPanel getRelease3DPanel() {
        this.checkBoxRelease3DServer = new JCheckBox();
        JPanel release3DPanel = new JPanel();
        GridBagLayout gridBagLayout = new GridBagLayout();
        release3DPanel.setLayout(gridBagLayout);
        release3DPanel.add(new JScrollPane(this.getTableRelease3DServer()), (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(1).setAnchor(10));
        return new SmPanelComponentTitle(this.checkBoxRelease3DServer, release3DPanel);
    }

    private void initializeResources() {
        this.setTitle(pluginAlias+"-"+NetServicesProperties.getString("String_ReleaseMongoDBCache"));
//        this.labelIServerLogin.setText(NetServicesProperties.getString("String_LabelChooseIServer"));
        this.checkBoxRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_RestMap"));
        this.checkBoxRestVectorCache.setText(CoreProperties.getString("String_iServer_ServicesType_RestVectorCache"));
        this.checkBoxWMS111.setText(CoreProperties.getString("String_iServer_ServicesType_WMS111"));
        this.checkBoxWMS130.setText(CoreProperties.getString("String_iServer_ServicesType_WMS130"));
        this.checkBoxWMTS100.setText(CoreProperties.getString("String_iServer_ServicesType_WMTS100"));
        this.checkBoxWMTSCHINA.setText(CoreProperties.getString("String_iServer_ServicesType_WMTSCHINA"));
        this.checkBoxAGSRestMap.setText(CoreProperties.getString("String_iServer_ServicesType_AGSRestMap"));
        this.checkBoxBaiduRest.setText(CoreProperties.getString("String_iServer_ServicesType_BaiduRest"));
        this.checkBoxGoogleRest.setText(CoreProperties.getString("String_iServer_ServicesType_GoogleRest"));
        this.buttonRelease.setText(NetServicesProperties.getString("String_Release"));
        this.buttonClose.setText(CoreProperties.getString("String_Close"));
        this.labelReleaseCache.setText(NetServicesProperties.getString("String_Label_Cache"));
        this.checkBoxReleaseMapServer.setText(NetServicesProperties.getString("String_ReleaseMapServer"));
        this.checkBoxRelease3DServer.setText(NetServicesProperties.getString("String_Release3DServer"));
    }

    private void registerEvents() {
        this.removeEvents();
//        this.iServerLogin.addIServerLoginInfoSelectionChangedListener(this.loginInfoSelectionChangedListener);
        this.checkBoxRestMap.addActionListener(this);
        this.checkBoxRestVectorCache.addActionListener(this);
        this.checkBoxWMS111.addActionListener(this);
        this.checkBoxWMS130.addActionListener(this);
        this.checkBoxWMTS100.addActionListener(this);
        this.checkBoxWMTSCHINA.addActionListener(this);
        this.checkBoxAGSRestMap.addActionListener(this);
        this.checkBoxBaiduRest.addActionListener(this);
        this.checkBoxGoogleRest.addActionListener(this);
        this.checkBoxReleaseMapServer.addActionListener(this);
        this.checkBoxRelease3DServer.addActionListener(this);
        this.buttonRelease.addActionListener(this);
        this.buttonClose.addActionListener(this);
//        this.smPanelMongoDBLogin.addPropertyChangeListener(SmPanelMongoDBLogin.DATABASE_NAME_CHANGED, this.mongoDBCacheChanged);
        this.tableModel.addTableModelListener(this.tableModelListener);

        this.comboBoxReleaseMap.addItemListener(this.releaseMapItemListener);
    }

    private void removeEvents() {
//        this.iServerLogin.removeIServerLoginInfoSelectionChangedListener(this.loginInfoSelectionChangedListener);
        this.checkBoxRestMap.removeActionListener(this);
        this.checkBoxRestVectorCache.removeActionListener(this);
        this.checkBoxWMS111.removeActionListener(this);
        this.checkBoxWMS130.removeActionListener(this);
        this.checkBoxWMTS100.removeActionListener(this);
        this.checkBoxWMTSCHINA.removeActionListener(this);
        this.checkBoxAGSRestMap.removeActionListener(this);
        this.checkBoxBaiduRest.removeActionListener(this);
        this.checkBoxGoogleRest.removeActionListener(this);
        this.buttonRelease.removeActionListener(this);
        this.buttonClose.removeActionListener(this);
//        this.smPanelMongoDBLogin.removePropertyChangeListener(SmPanelMongoDBLogin.DATABASE_NAME_CHANGED, this.mongoDBCacheChanged);
        this.tableModel.removeTableModelListener(this.tableModelListener);
        this.comboBoxReleaseMap.removeItemListener(this.releaseMapItemListener);
    }

    private void setButtonReleaseEnabled() {
        try {
            boolean hasLoginInfo = null != this.iServerLogin.getIServerLoginInfo();
            if (!hasLoginInfo) {
                this.buttonRelease.setEnabled(false);
                return;
            }

            boolean hasMongoDB = null != this.smPanelMongoDBLogin.getConnectionInfo();
            if (!hasMongoDB) {
                this.buttonRelease.setEnabled(false);
                return;
            }

            boolean hasReleaseMap = this.checkBoxReleaseMapServer.isSelected();
            boolean hasRelease3DServer = this.checkBoxRelease3DServer.isSelected();
            if (!hasRelease3DServer && !hasReleaseMap) {
                this.buttonRelease.setEnabled(false);
                return;
            }

            if (hasReleaseMap) {
                if (null == this.comboBoxReleaseMap.getSelectedItem() || this.servicesType == 0) {
                    this.buttonRelease.setEnabled(false);
                    return;
                }

                this.buttonRelease.setEnabled(true);
            }

            if (hasRelease3DServer) {
                if (this.tilesetNames.size() > 0) {
                    this.buttonRelease.setEnabled(true);
                } else {
                    this.buttonRelease.setEnabled(false);
                }
            }
        } catch (Exception var5) {
            Application.getActiveApplication().getOutput().output(var5);
        }

    }

    private void checkBoxWMTSCHINACheckChange() {
        try {
            if (this.checkBoxWMTSCHINA.isSelected()) {
                this.servicesType |= 4096;
            } else {
                this.servicesType ^= 4096;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMTS100CheckChange() {
        try {
            if (this.checkBoxWMTS100.isSelected()) {
                this.servicesType |= 2048;
            } else {
                this.servicesType ^= 2048;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS130CheckChange() {
        try {
            if (this.checkBoxWMS130.isSelected()) {
                this.servicesType |= 1024;
            } else {
                this.servicesType ^= 1024;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxWMS111CheckChange() {
        try {
            if (this.checkBoxWMS111.isSelected()) {
                this.servicesType |= 512;
            } else {
                this.servicesType ^= 512;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestMapCheckChange() {
        try {
            if (this.checkBoxRestMap.isSelected()) {
                this.servicesType |= 2;
            } else {
                this.servicesType ^= 2;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxRestVectorCacheCheckChange() {
        try {
            if (this.checkBoxRestVectorCache.isSelected()) {
                this.servicesType |= 4194304;
            } else {
                this.servicesType ^= 4194304;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxAGSRestMapCheckChange() {
        try {
            if (this.checkBoxAGSRestMap.isSelected()) {
                this.servicesType |= 32768;
            } else {
                this.servicesType ^= 32768;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxBaiduRestCheckChange() {
        try {
            if (this.checkBoxBaiduRest.isSelected()) {
                this.servicesType |= 262144;
            } else {
                this.servicesType ^= 262144;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void checkBoxGoogleRestCheckChange() {
        try {
            if (this.checkBoxGoogleRest.isSelected()) {
                this.servicesType |= 524288;
            } else {
                this.servicesType ^= 524288;
            }

            this.setButtonReleaseEnabled();
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    private void releaseMongoDBMapCache() {
        CursorUtilities.setWaitCursor();

        try {
            ServerReleaseGAFMongoDBMapCache serverRelease = this.fillServerRelease();
            SmDialogProgressTotal smDialogProgressTotal = new SmDialogProgressTotal(NetServicesProperties.getString("String_ReleaseMongoDBMapCache"));
            smDialogProgressTotal.doWork(new DialogGAFReleaseMongoCache.ServerReleaseMapCacheCallable(serverRelease), new IAfterWork<Boolean>() {
                public void afterWork(Boolean param) {
                    if (param) {
                        if (SwingUtilities.isEventDispatchThread()) {
                            DialogGAFReleaseMongoCache.this.dispose();
                        } else {
                            SwingUtilities.invokeLater(DialogGAFReleaseMongoCache.this::dispose);
                        }
                        ArrayList<String> urls = serverRelease.getResultURLs();
                        registerServiceFromTile(urls,tileId);
                    }

                }
            });
        } catch (Exception var6) {
            Application.getActiveApplication().getOutput().output(var6);
        } finally {
            CursorUtilities.setDefaultCursor();
        }

    }

    private ServerReleaseGAFMongoDBMapCache fillServerRelease() {
        IServerLoginInfo iServerLoginInfo = this.iServerLogin.getIServerLoginInfo();
        if (null == iServerLoginInfo) {
            return null;
        } else {
            ServerReleaseGAFMongoDBMapCache serverRelease = new ServerReleaseGAFMongoDBMapCache(iServerLoginInfo);
            TileStorageConnection tileStorageConnection = this.smPanelMongoDBLogin.getConnectionInfo();
            String host = tileStorageConnection.getServer();
            if (host.contains("localhost")) {
                String ip = "";

                try {
                    ip = NetworkUtilties.getIpAddress();
                } catch (Exception var8) {
                    var8.printStackTrace();
                }

                if (!StringUtilities.isNullOrEmptyString(ip)) {
                    host = host.replace("localhost", ip);
                }
            }

            serverRelease.setMongoDBHost(host);
            serverRelease.setDatabase(tileStorageConnection.getDatabase());
            serverRelease.setUsername(tileStorageConnection.getUser());
            serverRelease.setPassword(tileStorageConnection.getPassword());
            serverRelease.setServicesType(this.servicesType);
            if (this.comboBoxReleaseMap.getSelectedItem() instanceof String && this.comboBoxReleaseMap.getSelectedItem().equals(ControlsProperties.getString("String_All"))) {
                serverRelease.setMapName("");
                serverRelease.setTilesetName("");
            } else {
                TileStorageInfo storageInfo = (TileStorageInfo)this.comboBoxReleaseMap.getSelectedItem();
                String mapName = storageInfo.getMapName();
                String cacheName = storageInfo.getName();
                serverRelease.setMapName(storageInfo.getDataType().equals(TileDataType.NONE) ? cacheName : mapName);
                serverRelease.setVector(storageInfo.getDataType().equals(TileDataType.NONE));
                serverRelease.setTilesetName(cacheName);
            }

            serverRelease.setTilesetNames(this.tilesetNames);
            return serverRelease;
        }
    }

    class TableModelReleaseMongoDB extends DefaultTableModel {
        private static final int TABLE_COLUMN_ISSELECTED = 0;
        private static final int TABLE_COLUMN_3DLAYERNAME = 1;
        private ArrayList<DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase> datas = new ArrayList();
        private String[] columnNames = new String[]{"", NetServicesProperties.getString("String_Cache")};

        TableModelReleaseMongoDB() {
        }

        public ArrayList<DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase> getData() {
            return this.datas;
        }

        public void setDatas(ArrayList<TileStorageInfo> mapCacheInfos) {
            this.datas.clear();
            if (null != mapCacheInfos && !mapCacheInfos.isEmpty()) {
                Iterator var2 = mapCacheInfos.iterator();

                while(var2.hasNext()) {
                    TileStorageInfo mapCacheInfo = (TileStorageInfo)var2.next();
                    if (!mapCacheInfo.getDataType().equals(TileDataType.NONE)) {
                        this.datas.add(new DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase(false, mapCacheInfo));
                    }
                }
            }

            this.fireTableDataChanged();
        }

        public int getRowCount() {
            return this.datas == null ? 0 : this.datas.size();
        }

        public int getColumnCount() {
            return this.columnNames.length;
        }

        public String getColumnName(int column) {
            return this.columnNames[column];
        }

        public boolean isCellEditable(int row, int column) {
            return column == 0;
        }

        public Object getValueAt(int row, int column) {
            switch(column) {
                case 0:
                    return ((DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase)this.datas.get(row)).isSelected();
                case 1:
                    return ((DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase)this.datas.get(row)).getCacheName();
                default:
                    return super.getValueAt(row, column);
            }
        }

        public Class<?> getColumnClass(int columnIndex) {
            switch(columnIndex) {
                case 0:
                    return Boolean.class;
                case 1:
                    return String.class;
                default:
                    return super.getColumnClass(columnIndex);
            }
        }

        public void setValueAt(Object aValue, int row, int column) {
            if (column == 0) {
                ((DialogGAFReleaseMongoCache.TableModelReleaseMongoDB.DataBase)this.datas.get(row)).setSelected((Boolean)aValue);
            }

            this.fireTableDataChanged();
        }

        class DataBase {
            boolean a;
            TileStorageInfo b;

            public DataBase(boolean isSelected, TileStorageInfo mapCacheInfo) {
                this.a = isSelected;
                this.b = mapCacheInfo;
            }

            public boolean isSelected() {
                return this.a;
            }

            public String getCacheName() {
                return this.b.getName();
            }

            public void setSelected(boolean selected) {
                this.a = selected;
            }
        }
    }

    private class ServerReleaseMapCacheCallable extends UpdateProgressCallable {
        private ServerReleaseGAFMongoDBMapCache b;
        private FunctionProgressListener c = new FunctionProgressListener() {
            public void functionProgress(FunctionProgressEvent event) {
                try {
                    DialogGAFReleaseMongoCache.ServerReleaseMapCacheCallable.this.updateProgressTotal(event.getCurrentProgress(), event.getTotalProgress(), event.getCurrentMessage(), event.getTotalMessage(), "", event.getCurrentCount(), event.getCount());
                } catch (CancellationException var3) {
                }

            }
        };

        public ServerReleaseMapCacheCallable(ServerReleaseGAFMongoDBMapCache serverRelease) {
            this.b = serverRelease;
        }

        public Boolean call() {
            boolean result = true;

            try {
                Application.getActiveApplication().getOutput().output(CoreProperties.getString("String_ReleaseStart"));
                this.b.addFunctionProgressListener(this.c);
                long startTime = System.currentTimeMillis();
                boolean releaseResult = this.b.release();
                if (releaseResult) {
                    long totalTime = System.currentTimeMillis() - startTime;
                    Application.getActiveApplication().getOutput().output(MessageFormat.format(NetServicesProperties.getString("String_iServer_Message_ReleaseSuccess"), String.valueOf(totalTime / 1000L)));
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_ReleaseMapCacheSuccessResult"));

                    for(int i = 0; i < this.b.getResultURLs().size(); ++i) {
                        Application.getActiveApplication().getOutput().output((String)this.b.getResultURLs().get(i), InfoType.Hyperlink);
                    }
                } else {
                    result = false;
                    Application.getActiveApplication().getOutput().output(NetServicesProperties.getString("String_iServer_Message_ReleaseFaild"));
                }
            } catch (Exception var11) {
                result = false;
                Application.getActiveApplication().getOutput().output(var11);
            } finally {
                this.b.removeFunctionProgressListener(this.c);
            }

            return result;
        }
    }
    public static void registerServiceFromTile(ArrayList<String> urls,String sourceId){
        ArrayList<JSONObject> links = new ArrayList<>();
        for(String url:urls){
            JSONObject object = new JSONObject();
            String lastPath = url.substring(url.lastIndexOf("/")+1);
            String serviceType = "OTHER";
            switch (lastPath){
                case "rest":{
                    serviceType = ServiceType.RESTMAP.name();
                    break;
                }
                case "wms111":{
                    serviceType = ServiceType.WMS111.name();
                    break;
                }
                case "wms130":{
                    serviceType = ServiceType.WMS130.name();
                    break;
                }
                case "wmts100":{
                    serviceType = ServiceType.WMTS100.name();
                    break;
                }
                case "wmts-china":{
                    serviceType = ServiceType.WMTSCHINA.name();
                    break;
                }
                case "arcgisrest":{
                    serviceType = ServiceType.AGSRESTMAP.name();
                    break;
                }
                case "baidurest":{
                    serviceType = ServiceType.BAIDUREST.name();
                    break;
                }
                case "googlerest":{
                    serviceType = ServiceType.GOOGLEREST.name();
                    break;
                }
                case "realspace":{
                    if(url.endsWith("/realspace")){
                        url = url.substring(0,url.length()-"/realspace".length());
                    }
                    serviceType = ServiceType.RESTREALSPACE.name();
                    break;
                }
            }
            object.put("serviceAddress",url);
            object.put("serviceType",serviceType);
            links.add(object);
        }
        CommonUtils.registerService(JSON.toJSONString(links),sourceId,2);
    }
}
