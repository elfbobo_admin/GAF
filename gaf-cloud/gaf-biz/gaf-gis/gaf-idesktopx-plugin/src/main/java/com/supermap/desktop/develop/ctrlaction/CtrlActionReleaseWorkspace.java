/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ctrlaction;

import com.supermap.ProductType;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.WorkspaceType;
import com.supermap.desktop.controls.ui.UICommonToolkit;
import com.supermap.desktop.controls.ui.controls.DialogResult;
import com.supermap.desktop.controls.ui.controls.JDialogWorkspaceSaveAs;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.implement.CtrlAction;
import com.supermap.desktop.core.iserver.WorkspaceInfo;
import com.supermap.desktop.core.license.LicenseManager;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.utilties.DatasourceUtilities;
import com.supermap.desktop.core.utilties.WorkspaceUtilities;
import com.supermap.desktop.develop.entity.GAFWorkspaceConnectionInfo;
import com.supermap.desktop.develop.ui.DialogGAFReleaseWorkspace;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.netservices.NetServicesProperties;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.text.MessageFormat;

/**
 * @author SuperMap
 * @date:2021/3/25
 */
public class CtrlActionReleaseWorkspace extends CtrlAction implements GAFTreeNodeCtrlAction {
    private DefaultMutableTreeNode node;

    @Override
    public void setNode(DefaultMutableTreeNode node) {
        this.node = node;
    }

    public CtrlActionReleaseWorkspace(IBaseItem caller) {
        super(caller);
    }



    public void excute(String workspaceId){
        try {
            boolean prepared = this.a();
            if (prepared) {
                if (Application.getActiveApplication().getWorkspace().getConnectionInfo().getType() != WorkspaceType.DEFAULT) {
                    DialogGAFReleaseWorkspace dialog = new DialogGAFReleaseWorkspace(WorkspaceInfo.getCurrentWorkspaceInfo(),workspaceId);
                    dialog.showDialog();
                } else {
                    UICommonToolkit.showMessageDialog(NetServicesProperties.getString("String_iServer_Message_PleaseSave"));
                }
            } else {
                UICommonToolkit.showMessageDialog(NetServicesProperties.getString("String_iServer_Message_PreparedFaild"));
            }
        } catch (Exception var3) {
            Application.getActiveApplication().getOutput().output(var3);
        }

    }
    @Override
    public void run() {
        GAFWorkspaceConnectionInfo conn = (GAFWorkspaceConnectionInfo) node.getUserObject();
        excute(conn.getId());
    }

    private boolean a() {
        boolean prepared = true;
        boolean closed = Application.getActiveApplication().getMainFrame().getFormManager().closeAll(true);
        boolean isContinue = true;
        if (closed) {
            if (DatasourceUtilities.isContainMemoryDatasource(Application.getActiveApplication().getWorkspace())) {
                String[] datasources = DatasourceUtilities.getMemoryDatasources(Application.getActiveApplication().getWorkspace());
                String datasourcesName = "";

                for(int i = 0; i < datasources.length - 1; ++i) {
                    datasourcesName = datasourcesName + datasources[i] + "、";
                }

                datasourcesName = datasourcesName + datasources[datasources.length - 1];
                String message = MessageFormat.format(NetServicesProperties.getString("String_Message_SaveMemoryDatasource"), datasourcesName);
                int result = UICommonToolkit.showConfirmDialog(message, CoreProperties.getString("String_SaveWorkspace"));
                if (result == 1) {
                    isContinue = false;
                } else {
                    DatasourceUtilities.closeMemoryDatasource();
                }
            }

            if (isContinue) {
                if (WorkspaceUtilities.isWorkspaceModified()) {
                    int result = UICommonToolkit.showConfirmDialog(CoreProperties.getString("String_SaveWorkspacePrompt"), CoreProperties.getString("String_SaveWorkspace"));
                    if (result == 0) {
                        if (Application.getActiveApplication().getWorkspace().getType() == WorkspaceType.DEFAULT) {
                            prepared = this.b();
                        } else {
                            prepared = Application.getActiveApplication().getWorkspace().save();
                        }
                    } else if (result == 1 && Application.getActiveApplication().getWorkspace().getType() == WorkspaceType.DEFAULT) {
                        if (UICommonToolkit.showConfirmDialog(NetServicesProperties.getString("String_iServer_Message_NoSavingIsCertain")) == 1) {
                            prepared = this.b();
                        } else {
                            prepared = false;
                        }
                    }
                } else if (Application.getActiveApplication().getWorkspace().getType() == WorkspaceType.DEFAULT) {
                    prepared = this.b();
                }
            }
        } else {
            prepared = false;
        }

        return prepared;
    }

    public boolean enable() {
        GAFWorkspaceConnectionInfo conn = (GAFWorkspaceConnectionInfo) node.getUserObject();
        WorkspaceConnectionInfo currentOpen = ApplicationContextUtils.getWorkspace().getConnectionInfo();
        return CommonUtils.sameWorkspace(currentOpen,conn) && (LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_ENTERPRISE) || LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_STANDARD) && LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_ONLINESHARE));
    }

    private boolean b() {
        JFrame parent = (JFrame) Application.getActiveApplication().getMainFrame();
        JDialogWorkspaceSaveAs dialog = new JDialogWorkspaceSaveAs(parent, true, (String)null);
        boolean prepare = dialog.showDialog() == DialogResult.APPLY;
        return prepare;
    }
}
