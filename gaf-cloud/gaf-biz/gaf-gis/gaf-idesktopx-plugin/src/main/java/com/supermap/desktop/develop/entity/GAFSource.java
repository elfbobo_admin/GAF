package com.supermap.desktop.develop.entity;

public interface GAFSource {
    String getId();
    String getLocalPath();
    void setId(String id);
    void setLocalPath(String localPath);
}
