package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataCodeContent;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeContentSelectVo;
import com.supermap.gaf.data.mgt.mapper.DataCodeContentMapper;
import com.supermap.gaf.data.mgt.service.CommonValidateService;
import com.supermap.gaf.data.mgt.service.DataCodeContentService;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 数据代码编码服务实现类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Service
public class DataCodeContentServiceImpl implements DataCodeContentService{
    
	private static final Logger  log = LoggerFactory.getLogger(DataCodeContentServiceImpl.class);
	
	@Autowired
    private DataCodeContentMapper dataCodeContentMapper;

	@Autowired
	private CommonValidateService commonValidateService;
	
	@Override
    public DataCodeContent getById(String dataCodeContentId){
        if(dataCodeContentId == null){
            throw new IllegalArgumentException("dataCodeContentId不能为空");
        }
		DataCodeContent re = dataCodeContentMapper.select(dataCodeContentId);
		if(re == null){
			throw new GafException("指定编码不存在");
		}
		return re;
    }
	
	@Override
    public Page<DataCodeContent> listByPageCondition(DataCodeContentSelectVo dataCodeContentSelectVo, int pageNum, int pageSize) {
        PageInfo<DataCodeContent> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataCodeContentMapper.selectList(dataCodeContentSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
	public 	List<DataCodeContent> selectList(DataCodeContentSelectVo dataCodeContentSelectVo){
		return dataCodeContentMapper.selectList(dataCodeContentSelectVo);
	}

	@Override
    public void insertDataCodeContent(DataCodeContent dataCodeContent){
		commonValidateService.validateRefCode(dataCodeContent.getDataCodeId());
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		dataCodeContent.setDataCodeContentId(UUID.randomUUID().toString());
		String userName = SecurityUtilsExt.getUserName();
		dataCodeContent.setCreatedBy(userName);
		dataCodeContent.setUpdatedBy(userName);
        dataCodeContentMapper.insert(dataCodeContent);
    }
	
	@Override
    public int deleteDataCodeContent(String dataCodeContentId){
		return batchDelete(Arrays.asList(dataCodeContentId));
    }

	@Override
    public int batchDelete(List<String> dataCodeContentIds){
		if(!CollectionUtils.isEmpty(dataCodeContentIds)){
			return dataCodeContentMapper.batchDelete(dataCodeContentIds);
		}
		return 0;
    }

	@Override
	public int batchDeleteByDataCodeId(List<String> dataCodeIds) {
		if(!CollectionUtils.isEmpty(dataCodeIds)){
			return dataCodeContentMapper.batchDeleteByDataCodeId(dataCodeIds);
		}
		return 0;
	}

	@Override
    public int updateDataCodeContent(DataCodeContent dataCodeContent){
		commonValidateService.validateRefCode(dataCodeContent.getDataCodeId());
		dataCodeContent.setUpdatedBy(SecurityUtilsExt.getUserName());
		return dataCodeContentMapper.update(dataCodeContent);
    }
    
}
