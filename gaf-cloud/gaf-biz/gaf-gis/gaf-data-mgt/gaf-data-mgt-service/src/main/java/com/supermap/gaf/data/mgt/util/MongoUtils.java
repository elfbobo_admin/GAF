package com.supermap.gaf.data.mgt.util;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.ListCollectionsIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import org.bson.Document;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ClassName:MongoUtils
 * Package:com.supermap.cim.data.util
 *
 * @date:2022/1/24 13:58
 * @author:Yw
 */
public class MongoUtils {
    private static final Logger logger = LogUtil.getLocLogger(MongoUtils.class);
    private static MongoClientOptions options;

    static {
        MongoClientOptions.Builder build = new MongoClientOptions.Builder();
        build.connectionsPerHost(Integer.valueOf(50));
        build.threadsAllowedToBlockForConnectionMultiplier(Integer.valueOf(50));
        build.connectTimeout(Integer.valueOf(69) * 1000);
        build.maxWaitTime(Integer.valueOf(60) * 1000);
        options = build.build();
    }
    public void checkMongoConn(String host,int port,String databaseName,String username,String password) {
        MongoClient mongoClient = null;
        try {
            // 不需要用户名密码
            mongoClient = getMongoClient(host, port, databaseName, username, password);
            getAllCollections(databaseName,mongoClient);
            close(mongoClient);
        } catch (Exception e) {
            logger.error("mongoDB连接失败，原因:" + e.getMessage());
            throw new GafException("mongoDB连接失败，原因"+e.getMessage());
        }
    }
    public MongoClient getMongoClient(String host,int port,String databaseName,String username,String password) {
        MongoClient mongoClient = null;
        try {
            // 不需要用户名密码
            if (null == username && username.equals("")) {
                mongoClient = new MongoClient( host , port );
            } else {
                List<ServerAddress> addrs = Arrays.asList(new ServerAddress(host, port));
                //MongoCredential.createScramSha1Credential()三个参数分别为 用户名 数据库名称 密码
                MongoCredential credential = MongoCredential.createScramSha1Credential(username, databaseName, password.toCharArray());
                List<MongoCredential> credentials = new ArrayList<MongoCredential>();
                credentials.add(credential);
                //通过连接认证获取MongoDB连接
                mongoClient = new MongoClient(addrs,credentials,options);
            }
            return mongoClient;
        } catch (Exception e) {
            logger.error("mongoDB连接失败，原因:" + e.getMessage());
            throw new GafException("mongoDB连接失败，原因"+e.getMessage());
        }
    }

    /**
     * 查询DB下的瓦片列表
     */
    public List<String> getAllCollections(String dbName,MongoClient mongoClient) {
        MongoIterable<String> colls = getDB(dbName,mongoClient).listCollectionNames();
        ListCollectionsIterable<Document> documents = getDB(dbName, mongoClient).listCollections();
        List<String> _list = new ArrayList<String>();
        for (String s : colls) {
            _list.add(s);
        }
        return _list;
    }
    /**
     * 获取DB实例 - 指定DB
     *
     * @param dbName
     * @return
     */
    public MongoDatabase getDB(String dbName,MongoClient mongoClient) {
        if (dbName != null && !"".equals(dbName)) {
            MongoDatabase database = mongoClient.getDatabase(dbName);
            return database;
        }
        return null;
    }
    /**
     * 关闭Mongodb
     */
    public void close(MongoClient mongoClient) {
        if (mongoClient != null) {
            mongoClient.close();
            mongoClient = null;
        }
    }
}
