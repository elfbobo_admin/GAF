package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.dao.DataSyncLogMapper;
import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncLogSelectVo;
import com.supermap.gaf.data.mgt.service.DataSyncLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据同步日志表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DataSyncLogServiceImpl implements DataSyncLogService{
    
	private static final Logger  log = LoggerFactory.getLogger(DataSyncLogServiceImpl.class);
	
	@Autowired
    private DataSyncLogMapper dataSyncLogMapper;
	

	@Override
    public DataSyncLog getById(String dataSyncLogId){
        if(dataSyncLogId == null){
            throw new IllegalArgumentException("dataSyncLogId不能为空");
        }
        return dataSyncLogMapper.select(dataSyncLogId);
    }
	
	@Override
    public Page<DataSyncLog> listByPageCondition(DataSyncLogSelectVo dataSyncLogSelectVo, int pageNum, int pageSize) {
        PageInfo<DataSyncLog> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataSyncLogMapper.selectList(dataSyncLogSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
    public void deleteDataSyncLog(String dataSyncLogId){
        dataSyncLogMapper.delete(dataSyncLogId);
    }

	@Override
    public void batchDelete(List<String> dataSyncLogIds){
        dataSyncLogMapper.batchDelete(dataSyncLogIds);
    }

    
}
