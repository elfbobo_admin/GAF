package com.supermap.gaf.data.mgt.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.supermap.gaf.data.mgt.jackson.CustomDurationSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据质量管理-执行日志表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-执行日志表")
public class DqExecuteLog implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String dqExecuteLogId;
    @NotNull
    @ApiModelProperty("执行日志记录")
    private String content;
    /**
    * 默认值1：1
    */
    @ApiModelProperty("排序")
    private Integer sortSn;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
    @NotNull
    @ApiModelProperty("数据总量")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long totalAmount;
    @NotNull
    @ApiModelProperty("失败数据总量")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long failedAmount;
    @NotNull
    @ApiModelProperty("耗费时间-秒")
    @JsonSerialize(using= CustomDurationSerializer.class)
    private Long costTime;
    @NotNull
    @ApiModelProperty("规则id")
    private String dataQualityRuleId;
    @ApiModelProperty("数据质量管理规则名称")
    private String dataQualityRuleName;
    @NotNull
    @ApiModelProperty("执行批次。")
    private String dqExecuteResultId;
    @NotNull
    @ApiModelProperty("方案id。")
    private String dataQualityProgramId;
    @NotNull
    @ApiModelProperty("执行状态。0代表成功，1代表失败。")
    private Integer resultStatus;
}