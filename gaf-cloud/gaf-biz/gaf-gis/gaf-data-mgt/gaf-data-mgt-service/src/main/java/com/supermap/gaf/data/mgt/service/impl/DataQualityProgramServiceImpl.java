package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.dq.DqCheckJob;
import com.supermap.gaf.data.mgt.dq.DqRuleRunner;
import com.supermap.gaf.data.mgt.dq.DqRuleRunnerFactory;
import com.supermap.gaf.data.mgt.dq.DqRuleType;
import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.data.mgt.entity.DataQualityProgram;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityProgramSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.log.DqExecuteLogger;
import com.supermap.gaf.data.mgt.mapper.DataQualityModelMapper;
import com.supermap.gaf.data.mgt.mapper.DataQualityProgramMapper;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import com.supermap.gaf.data.mgt.mapper.DqProgramRuleMapper;
import com.supermap.gaf.data.mgt.service.DataQualityProgramService;
import com.supermap.gaf.data.mgt.service.DqRuleTypeService;
import com.supermap.gaf.data.mgt.service.SysResourceDatasourceService;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.quartz.service.QuartzService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


/**
 * 数据质量管理-方案表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DataQualityProgramServiceImpl implements DataQualityProgramService {

    public static final String JOB_GROUP = "DQ-CHECK";
	private static final Logger  log = LoggerFactory.getLogger(DataQualityProgramServiceImpl.class);
	
	@Autowired
    private DataQualityProgramMapper dataQualityProgramMapper;

    @Autowired
    private DataQualityModelMapper dataQualityModelMapper;

    @Autowired
    private DqProgramRuleMapper dqProgramRuleMapper;

    @Autowired
    private ConvertHelper convertHelper;

    @Autowired
    private DqExecuteLogMapper dqExecuteLogMapper;

    @Autowired
    private SysResourceDatasourceService sysResourceDatasourceService;
    @Autowired
    private DqRuleRunnerFactory dqRuleRunnerFactory;

    @Autowired
    private QuartzService quartzService;
    @Autowired
    private DqRuleTypeService dqRuleTypeService;



	@Override
    public DataQualityProgram getById(String dataQualityProgramId){
        if(dataQualityProgramId == null){
            throw new IllegalArgumentException("dataQualityProgramId不能为空");
        }
        return dataQualityProgramMapper.select(dataQualityProgramId);
    }
	
	@Override
    public Page<DataQualityProgram> listByPageCondition(DataQualityProgramSelectVo dataQualityProgramSelectVo, int pageNum, int pageSize) {
        PageInfo<DataQualityProgram> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataQualityProgramMapper.selectList(dataQualityProgramSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Override
    @Transactional(rollbackFor = Exception.class)
    public DataQualityProgram insertDataQualityProgram(DataQualityProgram dataQualityProgram){
        dataQualityProgram.setDataQualityProgramId(UUID.randomUUID().toString());
        String userName = SecurityUtilsExt.getUserName();
        dataQualityProgram.setCreatedBy(userName);
        dataQualityProgram.setUpdatedBy(userName);
        dataQualityProgramMapper.insert(dataQualityProgram);
        addJob(dataQualityProgram);
        return dataQualityProgram;
    }

    void addJob(DataQualityProgram dataQualityProgram){
        try{
            JobDataMap data = new JobDataMap();
            data.put("dataQualityProgramId",dataQualityProgram.getDataQualityProgramId());
            quartzService.scheduleCronJob(DqCheckJob.class,dataQualityProgram.getDataQualityProgramId(),JOB_GROUP,dataQualityProgram.getDataQualityCron(),data);
        }catch (Exception e){
            throw new GafException("添加定时任务失败");
        }
    }


	@Override
    public void deleteDataQualityProgram(String dataQualityProgramId){
        batchDelete(Arrays.asList(dataQualityProgramId));
    }

    @Transactional(rollbackFor = Exception.class)
	@Override
    public void batchDelete(List<String> dataQualityProgramIds){
        int re = dataQualityProgramMapper.batchDelete(dataQualityProgramIds);
        dqProgramRuleMapper.deleteByProgramId(dataQualityProgramIds);
        dqExecuteLogMapper.deleteByProgramIds(dataQualityProgramIds);
        if(re>0){
            for(String item:dataQualityProgramIds){
                quartzService.deleteScheduleJob(item,JOB_GROUP);
            }
        }
    }
	

	@Override
    @Transactional(rollbackFor = Exception.class)
    public DataQualityProgram updateDataQualityProgram(DataQualityProgram dataQualityProgram){
        try {
            CronExpression.validateExpression(dataQualityProgram.getDataQualityCron());
        } catch (ParseException e) {
            throw new GafException("cron表达式错误："+e.getMessage());
        }
        dataQualityProgramMapper.update(dataQualityProgram);
        quartzService.deleteScheduleJob(dataQualityProgram.getDataQualityProgramId(),JOB_GROUP);
        addJob(dataQualityProgram);
        return dataQualityProgram;
    }

    @Override
    public void startProgramJon(String dataQualityProgramId){
        DataQualityProgram dataQualityProgram = getById(dataQualityProgramId);
        if(dataQualityProgram == null){
            throw new GafException("检查方案不存在");
        }
        JobDataMap data = new JobDataMap();
        data.put("dataQualityProgramId",dataQualityProgram.getDataQualityProgramId());
        String jobName = "now_"+UUID.randomUUID().toString();
        try{
            //创建立即执行任务触发器
            Trigger trigger =  TriggerBuilder.newTrigger()
                    .withIdentity(jobName, JOB_GROUP)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withMisfireHandlingInstructionFireNow())
                    .startNow()
                    .build();
            //创建任务属性
            JobDetail jobDetail = JobBuilder.newJob(DqCheckJob.class)
                    .withIdentity(jobName, JOB_GROUP)
                    .usingJobData(data)
                    .build();
            quartzService.scheduleJob(jobDetail,trigger);
        }catch (ObjectAlreadyExistsException e){
            throw new GafException("已经存在相同的一个立即执行任务");
        }catch (Exception e){
            e.printStackTrace();
            throw new GafException("创建立即执行任务失败:"+e.getMessage());
        }


    }


    /**
     * 执行方案
     * @param dataQualityProgramId
     */
    @Override
    public void executeProgram(String dataQualityProgramId){
        DatasourceConnectionInfo datasourceConnectionInfo = null;
        List<DqProgramRuleVo> rules = null;
        String dqExecuteResultId = UUID.randomUUID().toString();
        DqExecuteLogger logger = new DqExecuteLogger(dqExecuteLogMapper,dataQualityProgramId,"","方案配置信息检查失败", dqExecuteResultId);
        DataQualityProgram dataQualityProgram = getById(dataQualityProgramId);
        if(dataQualityProgram == null){
            quartzService.deleteScheduleJob(dataQualityProgramId,JOB_GROUP);
            throw new GafException("检查方案不存在");
        }
        try{
            DataQualityModel model = dataQualityModelMapper.select(dataQualityProgram.getDataQualityModelId());
            if(model == null){
                throw new GafException("关联检查模型不存在");
            }
            rules = dqProgramRuleMapper.selectVoList(DqProgramRuleSelectVo.builder().dataQualityProgramId(dataQualityProgram.getDataQualityProgramId()).build());
            if(CollectionUtils.isEmpty(rules)){
                throw new GafException("未找到可执行的规则");
            }
            SysResourceDatasource sysResourceDatasource = sysResourceDatasourceService.getById(model.getDatasourceId());
            if(sysResourceDatasource == null){
                throw new GafException("关联数据源不存在或者被删除");
            }
            datasourceConnectionInfo = convertHelper.conver2DatasourceConnectionInfo(sysResourceDatasource);
        }catch (Exception e){
            logger.error(e.getMessage());
            logger.setFailedAmount(1);
            logger.setTotalAmount(1);
            logger.close();
            return;
        }
        if(rules == null || datasourceConnectionInfo==null){
            return;
        }
        for(DqProgramRuleVo rule:rules){
            DqRuleType dqRuleType = dqRuleTypeService.selectByCode(rule.getTypeCode());
            DqRuleRunner dqRuleCheckDriver = dqRuleRunnerFactory.create(dqRuleType);
            dqRuleCheckDriver.start(datasourceConnectionInfo,rule,dqExecuteResultId);
            logger.info("");
            logger.info("");
            logger.info("");
        }
	}
    
}
