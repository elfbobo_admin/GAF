package com.supermap.gaf.data.mgt.util;

import com.supermap.data.Scenes;
import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import com.supermap.realspace.*;
import org.slf4j.Logger;

/**
 * @description
 * @date:2022/2/21 10:08
 * @author:yw
 */
public class ScenesUtils {
    private static final Logger logger = LogUtil.getLocLogger(ScenesUtils.class);

    public synchronized static void addGridLayer(String cacheFileName, WorkspaceConnectionInfo workspaceConnectionInfo, String datasetPath) {
        Workspace workspace = null;
        Scene scene = null;
        try {
            workspace = new Workspace();
            boolean open = workspace.open(workspaceConnectionInfo);
            if (!open) {
                throw new GafException("打开工作空间 "+workspaceConnectionInfo.getName()+ " 失败");
            }
            scene = new Scene(workspace);
            TerrainLayers terrainLayers = scene.getTerrainLayers();
            TerrainLayer terrainLayer = terrainLayers.add(datasetPath, false, cacheFileName);
            String scenceName = cacheFileName.substring(0,cacheFileName.length() - 5);
            saveScene2(workspace, scene, scenceName);
        } finally {
            if (scene != null) {
                scene.close();
                scene.dispose();
            }
            if (workspace != null) {
                workspace.close();
                workspace.dispose();
            }
        }

    }

    public void addGridLayer(String cacheFileName, Workspace workspace, String datasetPath) {
        Scene scene = new Scene(workspace);
        TerrainLayers terrainLayers = scene.getTerrainLayers();
        TerrainLayer terrainLayer = terrainLayers.add(datasetPath, false, cacheFileName);
        //通过相机定位
        String scenceName = cacheFileName.substring(0,cacheFileName.length() - 5);
        saveScene(workspace, scene, scenceName);
    }
    /**
     * 添加场景图层
     * @param cacheFileName 场景下图层名称
     * @param workspace 工作空间
     * @param datasetPath sci scp等切片缓存文件地址
     */
    public void addLayer(String cacheFileName, Workspace workspace, String datasetPath,Layer3DType layer3DType) {
        // 暂时修改为一个图层保存在一个三维场景中
        Scene scene = new Scene(workspace);
        Layer3Ds layer3Ds = scene.getLayers();
        Layer3D layer3D = layer3Ds.add(datasetPath, layer3DType, true, cacheFileName);
        scene.ensureVisible(layer3D);
        scene.refresh();
        String scenceName = cacheFileName.substring(0,cacheFileName.length() - 5);
        saveScene(workspace, scene, scenceName);
    }

    /**
     * 添加场景图层
     * @param cacheFileName 场景下图层名称
     * @param workspaceConnectionInfo 工作空间连接信息
     * @param datasetPath sci scp等切片缓存文件地址
     */
    public synchronized static void addLayer(String cacheFileName, WorkspaceConnectionInfo workspaceConnectionInfo, String datasetPath, Layer3DType layer3DType) {
        // 暂时修改为一个图层保存在一个三维场景中
        Workspace workspace = null;
        Scene scene = null;
        try {
            workspace = new Workspace();
            boolean open = workspace.open(workspaceConnectionInfo);
            if (!open) {
                throw new GafException("打开工作空间 "+workspaceConnectionInfo.getName()+ " 失败");
            }
            scene = new Scene(workspace);
            Layer3Ds layer3Ds = scene.getLayers();
            Layer3D layer3D = layer3Ds.add(datasetPath, layer3DType, true, cacheFileName);
            scene.ensureVisible(layer3D);
            scene.refresh();
            String scenceName = cacheFileName.substring(0,cacheFileName.length() - 5);
            saveScene2(workspace, scene, scenceName);
        } finally {
            if (scene != null) {
                scene.close();
                scene.dispose();
            }
            if (workspace != null) {
                workspace.close();
                workspace.dispose();
            }
        }
    }

    /**
     * 保存场景
     *
     * @param workspace 工作空间
     * @param scene     场景信息
     * @param sceneName
     * @return
     */
    private static void saveScene2(Workspace workspace, Scene scene, String sceneName) {
        try {
            String sceneXml = scene.toXML();
            Scenes scenes = workspace.getScenes();
            int index = scenes.indexOf(sceneName);
            if (index > -1) {
                // 更新场景
                boolean b = scenes.setSceneXML(sceneName, sceneXml);
                logger.info("更新场景至工作空间中，场景名为："+sceneName+"结果为:" + b);
            } else {
                // 添加场景
                int count = scenes.add(sceneName, sceneXml);
                logger.info("新增场景至工作空间中，场景名为："+ sceneName +"，结果为:" + count);
            }
            boolean b = workspace.save();
            if (!b) {
                throw new GafException("保存工作空间失败");
            }
        }catch (Exception e) {
            throw new GafException("保存场景失败，原因:"+e.getMessage());
        }
    }

    /**
     * 保存场景
     *
     * @param workspace 工作空间
     * @param scene     场景信息
     * @param sceneName
     * @return
     */
    private void saveScene(Workspace workspace, Scene scene, String sceneName) {
        try {
            String sceneXml = scene.toXML();
            Scenes scenes = workspace.getScenes();
            int index = scenes.indexOf(sceneName);
            if (index > -1) {
                // 更新场景
                boolean b = scenes.setSceneXML(sceneName, sceneXml);
                logger.info("更新场景至工作空间中，场景名为："+sceneName+"结果为:" + b);
            } else {
                // 添加场景
                int count = scenes.add(sceneName, sceneXml);
                logger.info("新增场景至工作空间中，场景名为："+ sceneName +"，结果为:" + count);
            }
        }catch (Exception e) {
            throw new GafException("保存场景失败，原因:"+e.getMessage());
        }finally {
            scene.close();
            scene.dispose();
            workspace.save();
        }
    }
}
