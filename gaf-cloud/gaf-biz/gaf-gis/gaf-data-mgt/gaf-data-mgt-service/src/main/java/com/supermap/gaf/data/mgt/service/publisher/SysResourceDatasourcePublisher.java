package com.supermap.gaf.data.mgt.service.publisher;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.event.SysResourceDatasourceDeleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * @author : duke
 * @since 2021/11/16 3:05 PM
 */
@Service
public class SysResourceDatasourcePublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishDelete(SysResourceDatasource sysResourceDatasource){
        SysResourceDatasourceDeleteEvent event = new SysResourceDatasourceDeleteEvent(JSON.toJSONString(sysResourceDatasource));
        applicationEventPublisher.publishEvent(event);
    }
}
