package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.service.CommonValidateService;
import com.supermap.gaf.data.mgt.service.DataCodeService;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.sys.mgt.client.SysCatalogClient;
import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Common validate service.
 */
@Service
public class CommonValidateServiceImpl implements CommonValidateService {

    @Autowired
    private SysCatalogClient sysCatalogClient;

    @Autowired
    private DataCodeService dataCodeService;

    @Override
    public void validateCatalogId(String catalogId, CatalogTypeEnum catalogTypeEnum){
        if(!StringUtils.isEmpty(catalogId)){
            MessageResult<SysCatalog> result = sysCatalogClient.getById(catalogId);
            if(!result.IsSuccessed()){
                throw new GafException("catalogId校验失败"+(result.getMessage()==null?"":(":"+result.getMessage())));
            }
            if(result.getData() == null){
                throw new GafException("catalogId校验失败:目录不存在");
            }
            if(!catalogTypeEnum.getValue().equals(result.getData().getType())){
                throw new GafException("catalogId校验失败:目录类型必须是"+catalogTypeEnum.getValue());
            }
        }
    }

    @Override
    public void validateRefCode(String refCode){
        if(!StringUtils.isEmpty(refCode)){
            dataCodeService.getById(refCode);
        }
    }

}
