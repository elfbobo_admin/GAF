package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityProgram;
import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.data.mgt.entity.DqExecuteResult;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityProgramSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqExecuteLogSelectVo;
import com.supermap.gaf.data.mgt.service.DataQualityProgramService;
import com.supermap.gaf.data.mgt.service.DqExecuteLogService;
import com.supermap.gaf.exception.GafException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.apache.commons.lang3.time.TimeZones.GMT_ID;

/**
 * 数据质量管理-方案表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据质量管理-方案表接口")
public class DataQualityProgramResource{
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private DataQualityProgramService dataQualityProgramService;
    @Autowired
    private DqExecuteLogService dqExecuteLogService;

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据质量管理-方案表", notes = "通过id查询数据质量管理-方案表")
	@Path("/{dataQualityProgramId}")
    public MessageResult<DataQualityProgram> getById(@PathParam("dataQualityProgramId")String dataQualityProgramId){
        DataQualityProgram dataQualityProgram = dataQualityProgramService.getById(dataQualityProgramId);
		return MessageResult.successe(DataQualityProgram.class).data(dataQualityProgram).status(200).message("查询成功").build();
    }
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据质量管理-方案表", notes = "分页条件查询数据质量管理-方案表")
    public MessageResult<Page> pageList(@BeanParam DataQualityProgramSelectVo dataQualityProgramSelectVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataQualityProgram> page = dataQualityProgramService.listByPageCondition(dataQualityProgramSelectVo, pageNum, pageSize);
		return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据质量管理-方案表", notes = "新增数据质量管理-方案表")
    public MessageResult<Void> insertDataQualityProgram(DataQualityProgram dataQualityProgram){
        dataQualityProgramService.insertDataQualityProgram(dataQualityProgram);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据质量管理-方案表", notes = "根据id删除数据质量管理-方案表")
	@Path("/{dataQualityProgramId}")
    public MessageResult<Void> deleteDataQualityProgram(@PathParam("dataQualityProgramId")String dataQualityProgramId){
        dataQualityProgramService.deleteDataQualityProgram(dataQualityProgramId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据质量管理-方案表", notes = "批量删除数据质量管理-方案表")
    public MessageResult<Void> batchDelete(List<String> dataQualityProgramIds){
        dataQualityProgramService.batchDelete(dataQualityProgramIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据质量管理-方案表", notes = "根据id更新数据质量管理-方案表")
	@Path("/{dataQualityProgramId}")
    public MessageResult<Void> updateDataQualityProgram(DataQualityProgram dataQualityProgram, @PathParam("dataQualityProgramId")String dataQualityProgramId){
        dataQualityProgram.setDataQualityProgramId(dataQualityProgramId);
        dataQualityProgramService.updateDataQualityProgram(dataQualityProgram);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

    @POST
    @Path("/{dataQualityProgramId}/run")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "执行规则", notes = "执行规则")
    public MessageResult<Void> executeProgram(@PathParam("dataQualityProgramId") String dataQualityProgramId){
        dataQualityProgramService.startProgramJon(dataQualityProgramId);
        return MessageResult.successe(Void.class).status(200).message("提交完成").build();
    }


    @ApiOperation(value = "查询方案执行统计结果,默认只查询7天内的日志", notes = "查询方案执行统计结果，默认只查询7天内的日志")
    @Path("/{dataQualityProgramId}/executeResults")
    @GET
    public MessageResult<List<DqExecuteResult>> executeResults(@PathParam("dataQualityProgramId")String dataQualityProgramId,@QueryParam("startTime") String startTime,@QueryParam("endTime")String endTime){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(GMT_ID+"+8"));
        Date sTime = null;
        Date eTime = null;
        try {
            if(startTime != null){
                sTime = simpleDateFormat.parse(startTime);
            }
            if(endTime != null){
                eTime = simpleDateFormat.parse(endTime);
            }
        } catch (ParseException e) {
            throw new GafException("日期格式错误，要求'yyyy-MM-dd HH:mm:ss'，时区：GMT+8");
        }
        List<DqExecuteResult> re = dqExecuteLogService.statisticsExecuteResultByProgramId(dataQualityProgramId,sTime,eTime);
        return MessageResult.data(re).message("查询成功").build();
	}

    @ApiOperation(value = "查询具体方案执行统计结果日志详情", notes = "查询具体方案执行统计结果日志详情")
    @Path("/{dataQualityProgramId}/executeResults/{dqExecuteResultId}/logs")
    @GET
    public MessageResult<List<DqExecuteLog>> executeResultLogs(@PathParam("dataQualityProgramId")String dataQualityProgramId,@PathParam("dqExecuteResultId")String dqExecuteResultId){
        List<DqExecuteLog> re = dqExecuteLogService.selectList(DqExecuteLogSelectVo.builder().dataQualityProgramId(dataQualityProgramId)
                .dqExecuteResultId(dqExecuteResultId).orderFieldName("created_by").orderMethod("desc").build());
        return MessageResult.data(re).message("查询成功").build();
    }



}
