package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelVo;
import com.supermap.gaf.data.mgt.service.DataQualityModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据质量管理-模型表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据质量管理-模型表接口")
public class DataQualityModelResource{
    @Autowired
    private DataQualityModelService dataQualityModelService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据质量管理-模型表", notes = "通过id查询数据质量管理-模型表")
	@Path("/{dataQualityModelId}")
    public MessageResult<DataQualityModelVo> getById(@PathParam("dataQualityModelId")String dataQualityModelId){
        DataQualityModelVo re = dataQualityModelService.getById(dataQualityModelId);
		return MessageResult.successe(DataQualityModelVo.class).data(re).status(200).message("查询成功").build();
    }
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据质量管理-模型表", notes = "通过id查询数据质量管理-模型表")
    @Path("/{dataQualityModelId}/rules")
    public MessageResult<List<DataQualityRule>> rules(@PathParam("dataQualityModelId")String dataQualityModelId){
        List<DataQualityRule> rules = dataQualityModelService.rules(dataQualityModelId);
        return MessageResult.data(rules).status(200).message("查询成功").build();
    }


	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据质量管理-模型表", notes = "分页条件查询数据质量管理-模型表")
    public MessageResult<Page> pageList(@BeanParam DataQualityModelSelectVo dataQualityModelSelectVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataQualityModelVo> page = dataQualityModelService.listByPageCondition(dataQualityModelSelectVo, pageNum, pageSize);
		return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据质量管理-模型表", notes = "新增数据质量管理-模型表")
    public MessageResult<Void> insertDataQualityModel(DataQualityModel dataQualityModel){
        dataQualityModelService.insertDataQualityModel(dataQualityModel);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增数据质量管理-模型表", notes = "批量新增数据质量管理-模型表")
    public MessageResult<Void> batchInsert(List<DataQualityModel> DataQualityModels){
        dataQualityModelService.batchInsert(DataQualityModels);
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据质量管理-模型表", notes = "根据id删除数据质量管理-模型表")
	@Path("/{dataQualityModelId}")
    public MessageResult<Void> deleteDataQualityModel(@PathParam("dataQualityModelId")String dataQualityModelId){
        dataQualityModelService.deleteDataQualityModel(dataQualityModelId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据质量管理-模型表", notes = "批量删除数据质量管理-模型表")
    public MessageResult<Void> batchDelete(List<String> dataQualityModelIds){
        dataQualityModelService.batchDelete(dataQualityModelIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据质量管理-模型表", notes = "根据id更新数据质量管理-模型表")
	@Path("/{dataQualityModelId}")
    public MessageResult<Void> updateDataQualityModel(DataQualityModel dataQualityModel, @PathParam("dataQualityModelId")String dataQualityModelId){
        dataQualityModel.setDataQualityModelId(dataQualityModelId);
        dataQualityModelService.updateDataQualityModel(dataQualityModel);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

	
	


}
