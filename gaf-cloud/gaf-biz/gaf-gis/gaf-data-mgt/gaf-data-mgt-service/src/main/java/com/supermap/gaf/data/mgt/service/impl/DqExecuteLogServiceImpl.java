package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.data.mgt.entity.DqExecuteResult;
import com.supermap.gaf.data.mgt.entity.vo.DqExecuteLogSelectVo;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import com.supermap.gaf.data.mgt.service.DqExecuteLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 数据质量管理-执行日志表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DqExecuteLogServiceImpl implements DqExecuteLogService{

	private static final Logger  log = LoggerFactory.getLogger(DqExecuteLogServiceImpl.class);
	
	@Autowired
    private DqExecuteLogMapper dqExecuteLogMapper;
	

	@Override
    public DqExecuteLog getById(String dqExecuteLogId){
        if(dqExecuteLogId == null){
            throw new IllegalArgumentException("dqExecuteLogId不能为空");
        }
        return dqExecuteLogMapper.select(dqExecuteLogId);
    }
	
	@Override
    public List<DqExecuteLog> selectList(DqExecuteLogSelectVo dqExecuteLogSelectVo) {
        return dqExecuteLogMapper.selectList(dqExecuteLogSelectVo);
    }


	@Override
    public DqExecuteLog insertDqExecuteLog(DqExecuteLog dqExecuteLog){
        dqExecuteLogMapper.insert(dqExecuteLog);
        return dqExecuteLog;
    }
	
	@Override
    public void batchInsert(List<DqExecuteLog> dqExecuteLogs){
		if (dqExecuteLogs != null && dqExecuteLogs.size() > 0) {
	        dqExecuteLogs.forEach(dqExecuteLog -> {
				dqExecuteLog.setDqExecuteLogId(UUID.randomUUID().toString());
            });
            dqExecuteLogMapper.batchInsert(dqExecuteLogs);
        }
        
    }
	

	@Override
    public void deleteDqExecuteLog(String dqExecuteLogId){
        dqExecuteLogMapper.delete(dqExecuteLogId);
    }

	@Override
    public void batchDelete(List<String> dqExecuteLogIds){
        dqExecuteLogMapper.batchDelete(dqExecuteLogIds);
    }
	

	@Override
    public DqExecuteLog updateDqExecuteLog(DqExecuteLog dqExecuteLog){
        dqExecuteLogMapper.update(dqExecuteLog);
        return dqExecuteLog;
    }

    @Override
    public List<DqExecuteResult> statisticsExecuteResultByProgramId(String dataQualityProgramId,Date startTime,Date endTime){
        if(startTime == null && endTime == null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.add(Calendar.DATE,1);
            endTime = calendar.getTime();
            calendar.add(Calendar.DATE,-6);
            startTime = calendar.getTime();
        }
        return dqExecuteLogMapper.statisticsExecuteResultByProgramId(dataQualityProgramId,startTime,endTime);

    }

}
