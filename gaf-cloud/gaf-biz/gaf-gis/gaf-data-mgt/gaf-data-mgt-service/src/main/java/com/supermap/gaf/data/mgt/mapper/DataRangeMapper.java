package com.supermap.gaf.data.mgt.mapper;
import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.data.mgt.entity.vo.DataRangeSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据标准值域范围数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataRangeMapper{
	/**
     * 根据主键 dataRangeId 查询
     *
	 */
    DataRange select(@Param("dataRangeId")String dataRangeId);
	
	/**
     * 多条件查询
     * @param dataRangeSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataRange> selectList(DataRangeSelectVo dataRangeSelectVo);

    /**
     * 新增
     *
	 */
    void insert(DataRange dataRange);
	
	/**
     * 批量插入
     * 
	 */
	void batchInsert(@Param("list") Collection<DataRange> dataRanges);



	/**
	 * 批量删除
	 *
	 */
	int batchDeleteByDataStandardIds(@Param("list") Collection<String> dataStandardIds);


	/**
     * 批量删除
     * 
	 */
    int batchDelete(@Param("list") Collection<String> dataRangeIds);

	/**
     * 刪除
     *
	 */
    int delete(@Param("dataRangeId")String dataRangeId);

    /**
    * 更新
    * 
    **/
    int update(DataRange dataRange);
	/**
	 * 选择性更新
	 *
	 **/
	int updateSelective(DataRange dataRange);
}
