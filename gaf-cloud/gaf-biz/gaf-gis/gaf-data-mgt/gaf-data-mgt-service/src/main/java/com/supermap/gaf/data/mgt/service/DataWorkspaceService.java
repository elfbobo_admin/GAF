/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.data.mgt.service;

import com.supermap.data.Workspace;
import com.supermap.gaf.commontypes.ExceptionFunction;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataWorkspace;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.vo.DataWorkspaceSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.WorkspaceIdServiceType;
import com.supermap.gaf.data.mgt.model.MapSimpleInfo;
import com.supermap.gaf.data.mgt.model.SceneSimpleInfo;
import com.supermap.gaf.data.mgt.model.SimpleInfo;
import com.supermap.services.rest.management.ServiceType;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 工作空间服务类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
public interface DataWorkspaceService {

    /**
     * id查询工作空间
     *
     * @param workspaceId the workspace id
     * @return by id
     */
    DataWorkspace getById(String workspaceId);


    /**
     * Select by ids list.
     *
     * @param workspaceIds the workspace ids
     * @return the list
     */
    List<DataWorkspace> selectByIds(Collection<String> workspaceIds);

    /**
     * 多条件查询
     *
     * @param dataWorkspaceSelectVo 查询条件
     * @return 若未查询到则返回空集合 list
     */
    List<DataWorkspace> selectList(DataWorkspaceSelectVo dataWorkspaceSelectVo);


    /**
     * 分页条件查询
     *
     * @param dataWorkspaceSelectVo 查询条件
     * @param pageNum               当前页数
     * @param pageSize              页面大小
     * @return 分页对象 page
     */
    Page<DataWorkspace> listByPageCondition(DataWorkspaceSelectVo dataWorkspaceSelectVo, int pageNum, int pageSize);

    /**
     * 新增工作空间
     *
     * @param dataWorkspace the data workspace
     * @return data workspace
     */
    DataWorkspace insertDataWorkspace(DataWorkspace dataWorkspace);

    /**
     * 创建空的smwu工作空间到服务器指定目录，不注册
     *
     * @param path the path
     * @throws AuthenticationException the authentication exception
     */
    void createWorkspace(String path) throws AuthenticationException;

    /**
     * 批量插入
     *
     * @param dataWorkspaces the data workspaces
     */
    void batchInsert(List<DataWorkspace> dataWorkspaces);

    /**
     * 删除工作空间
     *
     * @param workspaceId the workspace id
     * @throws AuthenticationException the authentication exception
     */
    void deleteDataWorkspace(String workspaceId) throws AuthenticationException;

    /**
     * 批量删除
     *
     * @param workspaceIds the workspace ids
     * @throws AuthenticationException the authentication exception
     */
    void batchDelete(List<String> workspaceIds) throws AuthenticationException;

    /**
     * 更新工作空间
     *
     * @param dataWorkspace the data workspace
     * @return data workspace
     */
    DataWorkspace updateDataWorkspace(DataWorkspace dataWorkspace);

    /**
     * 根据工作空间id和服务类型集合 发布服务
     *
     * @param workspaceIdServiceTypes 工作空间id和服务类型集合
     * @return 发布结果 list
     * @throws IOException the io exception
     */
    List<MessageResult<Object>> publishService(List<WorkspaceIdServiceType> workspaceIdServiceTypes) throws IOException;

    /**
     * 根据工作空间id查询其下的数据源信息
     *
     * @param workspaceId the workspace id
     * @return the list
     */
    List<SimpleInfo>  listDatasourceInfo(String workspaceId);

    /**
     * 根据工作空间id查询其下的地图属性简略信息
     *
     * @param workspaceId the workspace id
     * @return list
     */
    List<MapSimpleInfo> listMapSimpleInfos(String workspaceId);

    /**
     * 根据工作空间id查询场景的简略信息
     *
     * @param workspaceId the workspace id
     * @return list
     */
    List<SceneSimpleInfo> listSceneSimpleInfos(String workspaceId);


    /**
     * Workspace processor t.
     *
     * @param <T>           the type parameter
     * @param dataWorkspace the data workspace
     * @param process       the process
     * @return the t
     */
    <T> T workspaceProcessor(DataWorkspace dataWorkspace, ExceptionFunction<Workspace, T > process);

    /**
     * Publish list.
     *
     * @param publishParam the publish param
     * @return the list
     */
    List<PublishResult> publish(PublishParam publishParam);

    /**
     * Publish list.
     *
     * @param workspaceId  the workspace id
     * @param publishParam the publish param
     * @return the list
     */
    List<PublishResult> publish(String workspaceId, PublishParam publishParam);

    /**
     * Gets publish optional type.
     *
     * @param workspaceId the workspace id
     * @return the publish optional type
     */
    Set<ServiceType> getPublishOptionalType(String workspaceId);
}
