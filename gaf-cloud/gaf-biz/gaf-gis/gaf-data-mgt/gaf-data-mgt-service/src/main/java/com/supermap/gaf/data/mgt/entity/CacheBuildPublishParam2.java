package com.supermap.gaf.data.mgt.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("缓存构建发布参数2")
public class CacheBuildPublishParam2 {

    @ApiModelProperty("发布历史id")
    private String publishHistoryId;

    @ApiModelProperty("数据集切片设置集合")
    List<DatasetCacheBuildSetting> datasetCacheBuildSettings;

    /**
     * 是否切片成功后立即发布
     */
    @ApiModelProperty("是否切片成功后立即发布")
    private Boolean isPublishAfterSlide;

    @ApiModelProperty("是否发布数据服务")
    private Boolean isPublishRestData;

}
