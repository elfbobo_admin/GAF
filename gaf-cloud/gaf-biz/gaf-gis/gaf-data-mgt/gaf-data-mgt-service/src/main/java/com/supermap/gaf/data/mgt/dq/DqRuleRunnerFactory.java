package com.supermap.gaf.data.mgt.dq;


import com.supermap.gaf.data.mgt.dq.plugin.DqRuleHttpPluginRunner;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import com.supermap.gaf.exception.GafException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

@Component
public class DqRuleRunnerFactory {
    @Autowired
    private DqExecuteLogMapper dqExecuteLogMapper;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("balancedRestTemplate")
    private RestTemplate balancedRestTemplate;

    public DqRuleRunner create(@Validated DqRuleType checkType){
        // 如果配置了httpUrl 创建DqRuleCheckHttpPluginRunner
        if(!StringUtils.isEmpty(checkType.getHttpUrl())){
            return createHttpPluginRunner(checkType);
        }
        // 内置runner
        String typeCode = checkType.getTypeCode();
        if("NULL_CHECK".equals(typeCode)){
            return new DqNullCheckRuleRunner(dqExecuteLogMapper);
        }
        throw new GafException(String.format("没有找到编码为%s的规则实现",checkType.getTypeCode()));
    }

    DqRuleHttpPluginRunner createHttpPluginRunner(DqRuleType checkType){
        return new DqRuleHttpPluginRunner(dqExecuteLogMapper,restTemplate,balancedRestTemplate,checkType);
    }


}
