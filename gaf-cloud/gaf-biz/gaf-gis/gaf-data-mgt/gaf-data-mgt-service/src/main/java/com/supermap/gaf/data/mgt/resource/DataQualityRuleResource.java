package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityRuleSelectVo;
import com.supermap.gaf.data.mgt.service.DataQualityRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据质量管理-规则表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据质量管理-规则表接口")
public class DataQualityRuleResource{
    @Autowired
    private DataQualityRuleService dataQualityRuleService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据质量管理-规则表", notes = "通过id查询数据质量管理-规则表")
	@Path("/{dataQualityRuleId}")
    public MessageResult<DataQualityRule> getById(@PathParam("dataQualityRuleId")String dataQualityRuleId){
        DataQualityRule dataQualityRule = dataQualityRuleService.getById(dataQualityRuleId);
		return MessageResult.successe(DataQualityRule.class).data(dataQualityRule).status(200).message("查询成功").build();
    }
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据质量管理-规则表", notes = "分页条件查询数据质量管理-规则表")
    public MessageResult<Page> pageList(@BeanParam DataQualityRuleSelectVo dataQualityRuleSelectVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataQualityRule> page = dataQualityRuleService.listByPageCondition(dataQualityRuleSelectVo, pageNum, pageSize);
		return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据质量管理-规则表", notes = "新增数据质量管理-规则表")
    public MessageResult<Void> insertDataQualityRule(DataQualityRule dataQualityRule){
        dataQualityRuleService.insertDataQualityRule(dataQualityRule);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增数据质量管理-规则表", notes = "批量新增数据质量管理-规则表")
    public MessageResult<Void> batchInsert(List<DataQualityRule> DataQualityRules){
        dataQualityRuleService.batchInsert(DataQualityRules);
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据质量管理-规则表", notes = "根据id删除数据质量管理-规则表")
	@Path("/{dataQualityRuleId}")
    public MessageResult<Void> deleteDataQualityRule(@PathParam("dataQualityRuleId")String dataQualityRuleId){
        dataQualityRuleService.deleteDataQualityRule(dataQualityRuleId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据质量管理-规则表", notes = "批量删除数据质量管理-规则表")
    public MessageResult<Void> batchDelete(List<String> dataQualityRuleIds){
        dataQualityRuleService.batchDelete(dataQualityRuleIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据质量管理-规则表", notes = "根据id更新数据质量管理-规则表")
	@Path("/{dataQualityRuleId}")
    public MessageResult<Void> updateDataQualityRule(DataQualityRule dataQualityRule, @PathParam("dataQualityRuleId")String dataQualityRuleId){
        dataQualityRule.setDataQualityRuleId(dataQualityRuleId);
        dataQualityRuleService.updateDataQualityRule(dataQualityRule);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

	
	


}
