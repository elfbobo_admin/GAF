package com.supermap.gaf.data.mgt.datasync;

import com.supermap.gaf.commontypes.MailSendDetail;
import com.supermap.gaf.data.mgt.publisher.MailSendPublisher;

public class MailDataSyncNotifier extends DataSyncNotifier<String> {

    private MailSendPublisher mailSendPublisher;

    private String email;


    public MailDataSyncNotifier(String[] types, MailSendPublisher mailSendPublisher, String email) {
        super(types);
        this.mailSendPublisher = mailSendPublisher;
        this.email = email;
    }

    @Override
    protected void sendMessage(String type, String message) {
        MailSendDetail mail = new MailSendDetail();
        mail.setSend(email);
        mail.setSubject("数据同步任务");
        mail.setContent(message);
        mailSendPublisher.publish(mail);
    }
}
