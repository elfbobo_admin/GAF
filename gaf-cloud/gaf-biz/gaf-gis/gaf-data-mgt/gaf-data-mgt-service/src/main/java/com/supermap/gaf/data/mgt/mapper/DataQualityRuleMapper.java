package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityRuleSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据质量管理-规则表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataQualityRuleMapper{
	/**
     * 根据主键 dataQualityRuleId查询
     * 
	 */
    DataQualityRule select(@Param("dataQualityRuleId") String dataQualityRuleId);
	
	/**
     * 多条件查询
     * @param dataQualityRuleSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataQualityRule> selectList(DataQualityRuleSelectVo dataQualityRuleSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(DataQualityRule dataQualityRule);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DataQualityRule> dataQualityRules);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dataQualityRuleIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dataQualityRuleId") String dataQualityRuleId);

	/**
	 * 刪除
	 *
	 */
	int deleteByModelIds(List<String> dataQualityModelIds);
    /**
     * 更新
     *
	 */
    int update(DataQualityRule dataQualityRule);
}
