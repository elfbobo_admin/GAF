package com.supermap.gaf.data.mgt.dq.plugin;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.gaf.data.mgt.dq.DqRuleRunner;
import com.supermap.gaf.data.mgt.dq.DqRuleType;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.log.SimpleLogger;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

public class DqRuleHttpPluginRunner extends DqRuleRunner {
    private RestTemplate restTemplate;
    private RestTemplate balancedRestTemplate;
    private DqRuleType checkType;


    public DqRuleHttpPluginRunner(DqExecuteLogMapper dqExecuteLogMapper, RestTemplate restTemplate, RestTemplate balancedRestTemplate, DqRuleType checkType) {
        super(dqExecuteLogMapper);
        this.restTemplate = restTemplate;
        this.balancedRestTemplate = balancedRestTemplate;
        this.checkType = checkType;
    }



    @Override
    public DqResult run(DatasourceConnectionInfo datasourceConnectionInfo, DqProgramRuleVo rule, SimpleLogger logger) {
        DqResult re = new DqResult();
        try{
            logger.info("开始执行【{}】规则。",checkType.getName());
            logger.info("参数【{}】", rule.getParams());
            RequestParam requestParam = new RequestParam();
            RequestParam.ConnectionInfo connectionInfo = new RequestParam.ConnectionInfo();
            connectionInfo.setAlias(datasourceConnectionInfo.getAlias());
            connectionInfo.setEngineType(datasourceConnectionInfo.getEngineType().name());
            connectionInfo.setServer(datasourceConnectionInfo.getServer());
            connectionInfo.setPassword(datasourceConnectionInfo.getPassword());
            connectionInfo.setUser(datasourceConnectionInfo.getUser());
            connectionInfo.setDatabase(datasourceConnectionInfo.getDatabase());
            requestParam.setConnectionInfo(connectionInfo);
            requestParam.setRule(rule);
            RestTemplate curRestTemplate = restTemplate;
            if(checkType.isMicroServiceRequest()){
                curRestTemplate = balancedRestTemplate;
                logger.info("委托微服务请求{}执行规则检查...",checkType.getHttpUrl());
            }else{
                logger.info("委托请求{}执行规则检查...",checkType.getHttpUrl());
            }

            RequestResult requestResult = curRestTemplate.postForObject(checkType.getHttpUrl(),requestParam, RequestResult.class);
            if(!CollectionUtils.isEmpty(requestResult.getLogs())){
                for(RequestResult.Log item: requestResult.getLogs()){
                    if("info".equalsIgnoreCase(item.getLevel())){
                        logger.info(item.getBody());
                    }
                    if("error".equalsIgnoreCase(item.getLevel())){
                        logger.error(item.getBody());
                    }
                    if("warn".equalsIgnoreCase(item.getLevel())){
                        logger.warn(item.getBody());
                    }
                }
            }
            BeanUtils.copyProperties(requestResult,re);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return re;
    }
}
