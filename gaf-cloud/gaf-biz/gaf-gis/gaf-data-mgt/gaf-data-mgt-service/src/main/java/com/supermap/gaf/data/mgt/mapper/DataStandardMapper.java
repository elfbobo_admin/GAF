package com.supermap.gaf.data.mgt.mapper;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据标准数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataStandardMapper{
	/**
     * 根据主键 dataStandardId 查询
     *
	 */
    DataStandard select(@Param("dataStandardId")String dataStandardId);
	
	/**
     * 多条件查询
     * @param dataStandardSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataStandard> selectList(DataStandardSelectVo dataStandardSelectVo);

    /**
     * 新增
     *
	 */
    void insert(DataStandard dataStandard);
	
	/**
     * 批量插入
     * 
	 */
	void batchInsert(@Param("list") Collection<DataStandard> dataStandards);
	
	/**
     * 批量删除
     * 
	 */
    int batchDelete(@Param("list") Collection<String> dataStandardIds);

	List<DataStandard> selectByRefCodes(@Param("list") Collection<String> dataStandardIds);

	/**
     * 刪除
     *
	 */
    int delete(@Param("dataStandardId")String dataStandardId);

    /**
    * 更新
    * 
    **/
    int update(DataStandard dataStandard);
	/**
	 * 选择性更新
	 *
	 **/
	int updateSelective(DataStandard dataStandard);
}
