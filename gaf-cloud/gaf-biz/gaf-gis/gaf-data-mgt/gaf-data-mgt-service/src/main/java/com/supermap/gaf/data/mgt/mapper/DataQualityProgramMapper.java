package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.DataQualityProgram;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityProgramSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据质量管理-方案表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataQualityProgramMapper{
	/**
     * 根据主键 dataQualityProgramId查询
     * 
	 */
    DataQualityProgram select(@Param("dataQualityProgramId") String dataQualityProgramId);
	
	/**
     * 多条件查询
     * @param dataQualityProgramSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataQualityProgram> selectList(DataQualityProgramSelectVo dataQualityProgramSelectVo);


	/**
	 * 根据模型id查询
	 * @param dataQualityModelIds
	 * @return
	 */
	List<DataQualityProgram> selectByModelIds(List<String> dataQualityModelIds);
    /**
	 * 新增
	 * 
	 */
    int insert(DataQualityProgram dataQualityProgram);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DataQualityProgram> dataQualityPrograms);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dataQualityProgramIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dataQualityProgramId") String dataQualityProgramId);
    /**
     * 更新
     *
	 */
    int update(DataQualityProgram dataQualityProgram);
}
