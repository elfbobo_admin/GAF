package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据标准 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据标准 条件查询实体")
public class DataStandardSelectVo {
    private static final long serialVersionUID = -1L;
    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = DataStandard.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataStandard.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataStandard.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    @ApiModelProperty("排序方法")
    private String orderMethod;
    @QueryParam("chineseName")
    @ApiModelProperty("中文名。 ")
    private String chineseName;
    @QueryParam("code")
    @ApiModelProperty("编码。")
    private String code;
    @QueryParam("enName")
    @ApiModelProperty("英文名。")
    private String enName;
    @QueryParam("description")
    @ApiModelProperty("描述。")
    private String description;
    @QueryParam("dataStandardId")
    @ApiModelProperty("主键。")
    private String dataStandardId;
    @QueryParam("catalogId")
    @ApiModelProperty("目录id。")
    private String catalogId;
    @QueryParam("refCode")
    @ApiModelProperty("引用代码id。（当值域是枚举型的时候）")
    private String refCode;
    @QueryParam("domainType")
    @ApiModelProperty("值域类型。（E:枚举型，R:范围型）")
    private String domainType;
    @QueryParam("fieldType")
    @ApiModelProperty("字段类型")
    private String fieldType;
    @QueryParam("fieldLength")
    @ApiModelProperty("字段长度")
    private Integer fieldLength;
    @QueryParam("fieldPrecision")
    @ApiModelProperty("字段精度")
    private Integer fieldPrecision;
    @QueryParam("fieldDefault")
    @ApiModelProperty("字段默认值")
    private String fieldDefault;
    @QueryParam("fieldNotNull")
    @ApiModelProperty("字段是否非空")
    private Boolean fieldNotNull;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}
