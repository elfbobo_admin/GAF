package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.data.mgt.entity.DqExecuteResult;
import com.supermap.gaf.data.mgt.entity.vo.DqExecuteLogSelectVo;

import java.util.Date;
import java.util.List;

/**
 * 数据质量管理-执行日志表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DqExecuteLogService {
	
	/**
    * 根据id查询数据质量管理-执行日志表
    * @return
    */
    DqExecuteLog getById(String dqExecuteLogId);
	
	/**
     * 条件查询
     * @param dqExecuteLogSelectVo 查询条件
     * @return 分页对象
     */
    List<DqExecuteLog> selectList(DqExecuteLogSelectVo dqExecuteLogSelectVo);
	
	
    /**
    * 新增数据质量管理-执行日志表
    * @return 新增的dqExecuteLog
    */
    DqExecuteLog insertDqExecuteLog(DqExecuteLog dqExecuteLog);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<DqExecuteLog> dqExecuteLogs);

    /**
    * 删除数据质量管理-执行日志表
    * 
    */
    void deleteDqExecuteLog(String dqExecuteLogId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dqExecuteLogIds);
    /**
    * 更新数据质量管理-执行日志表
    * @return 更新后的dqExecuteLog
    */
    DqExecuteLog updateDqExecuteLog(DqExecuteLog dqExecuteLog);


    /**
     * 统计方案执行结果
     * @param dataQualityProgramId
     * @return
     */
    List<DqExecuteResult> statisticsExecuteResultByProgramId(String dataQualityProgramId, Date startTime, Date endTime);
    
}
