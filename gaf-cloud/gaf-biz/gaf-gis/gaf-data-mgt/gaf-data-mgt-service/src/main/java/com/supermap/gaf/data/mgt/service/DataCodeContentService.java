package com.supermap.gaf.data.mgt.service;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataCodeContent;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeContentSelectVo;
import java.util.*;

/**
 * 数据代码编码服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataCodeContentService {
	
	/**
    * id查询数据代码编码
    * @return
    */
	DataCodeContent getById(String dataCodeContentId);
	
	/**
     * 分页条件查询
     * @param dataCodeContentSelectVo 查询条件
     * @param pageNum 当前页数
     * @param pageSize 页面大小
     * @return 分页对象
     */
	Page<DataCodeContent> listByPageCondition(DataCodeContentSelectVo dataCodeContentSelectVo, int pageNum, int pageSize);

	/**
	 * 多条件查询
	 * @param dataCodeContentSelectVo 查询条件
	 * @return 若未查询到则返回空集合
	 */
	List<DataCodeContent> selectList(DataCodeContentSelectVo dataCodeContentSelectVo);


		/**
         * 新增数据代码编码
         * @return
         */
    void insertDataCodeContent(DataCodeContent dataCodeContent);

    /**
     * 删除数据代码编码
     * 
     */
    int deleteDataCodeContent(String dataCodeContentId);

    /**
     * 批量删除
     * 
	 */
    int batchDelete(List<String> dataCodeContentIds);

	/**
	 * 根据dataCodeId批量删除
	 *
	 */
	int batchDeleteByDataCodeId(List<String> dataCodeIds);

    /**
     * 更新数据代码编码
     * @return 
     */
    int updateDataCodeContent(DataCodeContent dataCodeContent);
    
}
