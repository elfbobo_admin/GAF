package com.supermap.gaf.data.mgt.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 */
@Data
@ApiModel("缓存构建发布参数")
public class CacheBuildPublishParam extends CacheBuildParam {

    /**
     * 是否切片成功后立即发布
     */
    @ApiModelProperty("是否切片成功后立即发布")
    private Boolean isPublishAfterSlide;

    @ApiModelProperty("是否发布数据服务")
    private Boolean isPublishRestData;


}
