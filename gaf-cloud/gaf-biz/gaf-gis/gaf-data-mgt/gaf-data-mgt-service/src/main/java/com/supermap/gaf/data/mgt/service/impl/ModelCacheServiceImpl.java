package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.data.*;
import com.supermap.data.processing.OSGBCacheBuilder;
import com.supermap.data.processing.StorageType;
import com.supermap.gaf.data.mgt.service.DatasetTypeCacheService;
import com.supermap.gaf.data.mgt.util.ScenesUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import com.supermap.realspace.CacheFileType;
import com.supermap.realspace.Layer3DType;
import com.supermap.realspace.threeddesigner.ModelBuilder3D;
import com.supermap.tilestorage.TileStorageConnection;
import org.slf4j.Logger;

import java.util.Map;
import java.util.function.Consumer;

/**
 * ClassName:ModelCacheServiceImpl
 * Package:com.supermap.cim.data.service.impl
 * @description 模型数据集
 * @date:2022/1/24 9:14
 * @author:Yw
 */
public class ModelCacheServiceImpl implements DatasetTypeCacheService {
    private static final Logger logger = LogUtil.getLocLogger(ModelCacheServiceImpl.class);
    private String fileSuffix = ".scp";
    @Override
    public boolean executeS3MBuilder(Dataset dataset, String resultOutputFolder, String fileName, Map<String,Object> otherParam,Consumer<Integer> stepPercentHandler) {
        Dataset datasetModel = dataset;
        boolean isBuild = false;
        DatasetVector datasetVector =  null;
        OSGBCacheBuilder cacheBuilder = null;
        SteppedListener steppedListener = steppedEvent -> {
            logger.info(String.format("切片完成百分比： %d%%", steppedEvent.getPercent()));
            try {
                stepPercentHandler.accept(steppedEvent.getPercent());
            } catch (Exception e) {
                logger.error("切片完成百分比后置处理失败",e);
            }
        };
        try {
            boolean isSimp =(boolean)otherParam.get("isSimp");
            if (isSimp) {
                //简化率
                Double simplifyingRate = (Double)otherParam.get("simplifyingRate");
                //模型轻量化
                datasetModel = modelSimplify(dataset,simplifyingRate);
            }
            datasetVector = (DatasetVector)datasetModel;
            cacheBuilder = new OSGBCacheBuilder(datasetVector, resultOutputFolder, fileName);
            cacheBuilder.setFileType(CacheFileType.S3MB);
            cacheBuilder.setStorageType(StorageType.Compact);
            cacheBuilder.setLODSize(4);
            cacheBuilder.setTextureSharing(true);
            //设置生成缓存线程数
            cacheBuilder.setProcessThreadsCount(4);
            cacheBuilder.addSteppedListener(steppedListener);
            isBuild = cacheBuilder.build();
            Object obj = otherParam.get("workspaceConnectionInfo");
            if (null != obj) {
                String datasetPath = new StringBuilder()
                        .append(resultOutputFolder)
                        .append("/")
                        .append(fileName)
                        .append("/")
                        .append(fileName)
                        .append(fileSuffix)
                        .toString();
                WorkspaceConnectionInfo  workspaceConnectionInfo = (WorkspaceConnectionInfo)obj;
                ScenesUtils.addLayer(fileName,workspaceConnectionInfo,datasetPath,Layer3DType.OSGB);
            }
            return isBuild;
        } catch (Exception e) {
            logger.error("生成s3m缓存异常，原因：" + e.getMessage());
            throw new GafException(e.getMessage());
        } finally {
            if (null != cacheBuilder) {
                cacheBuilder.removeSteppedListener(steppedListener);
                cacheBuilder.dispose();
            }
        }
    }

    @Override
    public boolean writeToMongo(String configFile, TileStorageConnection ts, String datasetName) {
        String realConfigFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        String configMongoFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append("new_")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        return OSGBCacheBuilder.osgbFile2MongoDB(realConfigFile, ts, configMongoFile);
    }

    private DatasetVector modelSimplify(Dataset dataset0,Double simplifyingRate) {
        Recordset recordset = null;
        Recordset recordsetsimplify = null;
        try {

            PrjCoordSys prjCoordSys = dataset0.getPrjCoordSys();
            // 设置矢量数据集的信息
            DatasetVectorInfo datasetVectorInfo = new DatasetVectorInfo();
            datasetVectorInfo.setType(DatasetType.MODEL);
            datasetVectorInfo.setEncodeType(EncodeType.NONE);
            datasetVectorInfo.setFileCache(true);
            datasetVectorInfo.setName(String.format("%s_%s", dataset0.getName(), "simplify"));
            DatasetVector dataset = (DatasetVector) dataset0;
            if (dataset0.getDatasource().getDatasets().contains(String.format("%s_%s", dataset.getName(), "simplify"))) {
                dataset0.getDatasource().getDatasets().delete(String.format("%s_%s", dataset.getName(), "simplify"));
            }
            DatasetVector datasetsimplify = (DatasetVector) dataset0.getDatasource().getDatasets().create(datasetVectorInfo, prjCoordSys);
            System.out.println("轻量化数据模型创建成功");
            recordsetsimplify = datasetsimplify.getRecordset(false, CursorType.DYNAMIC);
            recordset = dataset.getRecordset(false, CursorType.DYNAMIC);
            Recordset.BatchEditor editor = recordsetsimplify.getBatch();
            editor.begin();
            if (recordset.getRecordCount() > 0) {
                recordset.moveFirst();
                while (!recordset.isEOF()) {
                    GeoModel3D geoModel3D = (GeoModel3D) recordset.getGeometry();
                    System.out.println(recordset.getID());
                    Model model = geoModel3D.getModel();
                    Model model0 = new Model();
                    ModelBuilder3D.simplify(model, model0, simplifyingRate, true);
                    geoModel3D.setModel(model0);
                    recordsetsimplify.addNew(geoModel3D);
                    geoModel3D.dispose();
                    model.dispose();
                    model0.dispose();
                    recordset.moveNext();
                }
            }
            editor.update();
            return datasetsimplify;
        } catch (Exception e) {
            throw new GafException("模型简化失败");
        } finally {
            recordsetsimplify.close();
            recordsetsimplify.dispose();
            dataset0.close();
            recordset.close();
            recordset.dispose();
        }

    }
}
