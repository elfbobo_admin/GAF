package com.supermap.gaf.data.mgt.service;

import com.supermap.data.Dataset;
import com.supermap.tilestorage.TileStorageConnection;

import java.util.Map;
import java.util.function.Consumer;

/**
 * ClassName:DatasetTypeCacheService
 * Package:com.supermap.cim.data.service
 * 数据集类型接口
 * @date:2022/1/24 9:01
 * @author:Yw
 */
public interface DatasetTypeCacheService {
    /**
     *
     * @param dataset 数据集
     * @param resultOutputFolder s3m存放路径
     * @param fileName s3m文件名称
     * @param otherParam 其他参数 可为null
     * @return
     */
    boolean executeS3MBuilder(Dataset dataset, String resultOutputFolder, String fileName, Map<String,Object> otherParam, Consumer<Integer> stepPercentHandler);

    /**
     *
     * @param configFile scp，sci等类型文件，绝对路径
     * @param ts mongo连接信息
     * @param configMongoFile 目标路径，新生成scp文件所放位置
     * @return
     */
    boolean writeToMongo(String configFile, TileStorageConnection ts, String configMongoFile);

}
