package com.supermap.gaf.data.mgt.entity;

import com.supermap.data.FieldType;
import com.supermap.gaf.data.mgt.enums.SdxFieldTypeEnum;
import com.supermap.gaf.data.mgt.model.FieldTypeInfo;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据标准
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据标准")
public class DataStandard implements Serializable{
    private static final long serialVersionUID = -1L;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("中文名。 ")
    private String chineseName;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("编码。")
    private String code;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("英文名。")
    private String enName;
    @ApiModelProperty("描述。")
    private String description;
    @ApiModelProperty("主键。")
    private String dataStandardId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("目录id。")
    private String catalogId;
    @ApiModelProperty("引用代码id。（当值域是枚举型的时候）")
    private String refCode;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @StringRange(fromEnum = DomainType.class,enumGetter = "code",groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("值域类型。（E:枚举型，R:范围型）")
    private String domainType;
    /**
    * 默认值1：false
    */
    @ApiModelProperty("字段类型")
    @NotNull
    @StringRange(fromEnum = SdxFieldTypeEnum.class,enumGetter = "code",groups={AddGroup.class,UpdateGroup.class})
    private String fieldType;
    @ApiModelProperty("字段长度")
    private Integer fieldLength;
    @ApiModelProperty("字段精度")
    private Integer fieldPrecision;
    @ApiModelProperty("字段默认值")
    private String fieldDefault;
    /**
    * 默认值1：false
    */
    @ApiModelProperty("字段是否非空")
    private Boolean fieldNotNull;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;

    public  enum DomainType{
        RANGE("R"),ENUM("E");

        private String code;

        DomainType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
}