package com.supermap.gaf.data.mgt.mapper;
import com.supermap.gaf.data.mgt.entity.DataCodeContent;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeContentSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据代码编码数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataCodeContentMapper{
	/**
     * 根据主键 dataCodeContentId 查询
     *
	 */
    DataCodeContent select(@Param("dataCodeContentId")String dataCodeContentId);
	
	/**
     * 多条件查询
     * @param dataCodeContentSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataCodeContent> selectList(DataCodeContentSelectVo dataCodeContentSelectVo);

    /**
     * 新增
     *
	 */
    void insert(DataCodeContent dataCodeContent);
	
	/**
     * 批量插入
     * 
	 */
	void batchInsert(@Param("list") Collection<DataCodeContent> dataCodeContents);
	
	/**
     * 批量删除
     * 
	 */
    int batchDelete(@Param("list") Collection<String> dataCodeContentIds);


	int batchDeleteByDataCodeId(@Param("list") Collection<String> dataCodeIds);


	/**
     * 刪除
     *
	 */
    int delete(@Param("dataCodeContentId")String dataCodeContentId);

    /**
    * 更新
    * 
    **/
    int update(DataCodeContent dataCodeContent);
	/**
	 * 选择性更新
	 *
	 **/
	int updateSelective(DataCodeContent dataCodeContent);
}
