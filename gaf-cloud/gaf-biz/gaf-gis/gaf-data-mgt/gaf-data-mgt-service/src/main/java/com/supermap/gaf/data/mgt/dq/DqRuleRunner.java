package com.supermap.gaf.data.mgt.dq;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.log.DqExecuteLogger;
import com.supermap.gaf.data.mgt.log.SimpleLogger;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import lombok.Data;

public abstract class DqRuleRunner {

    protected DqExecuteLogMapper dqExecuteLogMapper;

    public DqRuleRunner(DqExecuteLogMapper dqExecuteLogMapper) {
        this.dqExecuteLogMapper = dqExecuteLogMapper;
    }


    public void start(DatasourceConnectionInfo datasourceConnectionInfo, DqProgramRuleVo rule, String dqExecuteResultId){
        try(DqExecuteLogger logger = new DqExecuteLogger(dqExecuteLogMapper,rule.getDataQualityProgramId(),rule.getDataQualityRuleId(),rule.getDataQualityRuleName(), dqExecuteResultId)){
            DqResult re = run(IobjectUtils.copyDatasourceConnectionInfo(datasourceConnectionInfo),rule,logger);
            logger.setFailedAmount(re.getFailedAmount());
            logger.setTotalAmount(re.getTotalAmount());
        }
    }


    @Data
    public static class DqResult{
        private long totalAmount;
        private long failedAmount;
    }

    abstract public DqResult run(DatasourceConnectionInfo datasourceConnectionInfo, DqProgramRuleVo rule, SimpleLogger logger);


}
