package com.supermap.gaf.data.mgt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 瓦片简略信息  包括ugcv5文件类型的瓦片 和 mongodb类型的瓦片信息
 * @author wxl
 * @since 2021/9/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("瓦片简略信息")
public class TileDetailInfo {
    @ApiModelProperty("存储类型")
    private String storageType;
    @ApiModelProperty("图片类型")
    private String imageType;
    @ApiModelProperty("块大小")
    private int blockSize;
    @ApiModelProperty("背景是否透明")
    private Boolean transparent;
    @ApiModelProperty("坐标系")
    private String coordSysName;
    @ApiModelProperty("源点坐标。大小固定为2的数组，表示坐标点(x,y)")
    private double[] originLocation;
    @ApiModelProperty("比例尺")
    private List<Double> scales;
}
