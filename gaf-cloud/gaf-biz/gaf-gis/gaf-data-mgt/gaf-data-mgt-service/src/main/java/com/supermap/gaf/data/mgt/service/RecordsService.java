package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.data.mgt.entity.vo.TablePage;

public interface RecordsService {
    TablePage selectRecordsByPage(String datasourceId, String datasetName, Integer pageNum, Integer pageSize,boolean returnGeo);
}
