package com.supermap.gaf.data.mgt.enums;

/**
 * @description 切片类型枚举
 * @date:2022/2/14 16:04
 * @author:yw
 */
public enum CacheTypeEnum {
    /**
     * 增量切片
     */
    INCREMENT("increment","增量"),
    /**
     * 全量切片
     */
    FULLVOLUME("fullVolume","全量");


    private String code;


    private String description;

    CacheTypeEnum(String code,String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return description;
    }


    public static String getDescriptionByCode(String code) {
        CacheTypeEnum[] values = values();
        for (CacheTypeEnum value : values) {
            if (value.getCode().equals(code)) {
                return value.getDescription();
            }
        }
        throw new IllegalArgumentException("不支持的切片类型:"+code);
    }


}
