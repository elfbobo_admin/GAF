package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.Tile;
import com.supermap.gaf.data.mgt.entity.iserver.MongoDBMVTTileProviderConfig;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.TileSelectVo;
import com.supermap.gaf.data.mgt.model.MongoDBTileSourceInfo;
import com.supermap.gaf.data.mgt.model.TileDetailInfo;
import com.supermap.gaf.data.mgt.service.TileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 瓦片接口
 * @author wxl 
 * @since  yyyy-mm-dd
 */
@Component
@Api(value = "瓦片接口")
public class TileResource{
    @Autowired
    private TileService tileService;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询MongoDB瓦片列表", notes = "查询mongo瓦片列表")
    @Path("/names/mongodb")
    public MessageResult<List<String>> listMongoTilesetNames(MongoDBTileSourceInfo info){
        List<String> tileNames = tileService.listMongoTilesetNames(info);
        MessageResult<List<String>> mr = new MessageResult<>();
        mr.setSuccessed(true);
        mr.setData(tileNames);
        mr.setMessage("查询成功");
        return mr;
    }



    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询UGCV5瓦片列表", notes = "查询UGCV5瓦片列表")
    @Path("/names/ugcv5")
    public MessageResult<List<String>> listUgcv5TilesetNames(@QueryParam("path") String path){
        List<String> tileNames = tileService.listUgcv5TilesetNames(path);
        MessageResult<List<String>> mr = new MessageResult<>();
        mr.setSuccessed(true);
        mr.setData(tileNames);
        mr.setMessage("查询成功");
        return mr;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询瓦片详情", notes = "查询瓦片详情")
    @Path("/{tileId}/detail")
    public MessageResult<TileDetailInfo> detail(@PathParam("tileId") String tileId){
        TileDetailInfo detail = tileService.getDetail(tileId);
        return MessageResult.data(detail).message("查询成功").build();
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询瓦片", notes = "通过id查询瓦片")
	@Path("/{tileId}")
    public MessageResult<Tile> getById(@PathParam("tileId")String tileId){
        Tile tile = tileService.getById(tileId);
		return MessageResult.successe(Tile.class).data(tile).status(200).message("查询成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "发布地图服务", notes = "发布地图服务")
    @Path("/{tileId}/publish-map")
    public MessageResult<List<PublishResult>> publishMap(@PathParam("tileId")String tileId, PublishTileParam param){
        List<PublishResult> results = tileService.publishMap(tileId,param);
        return MessageResult.data(results).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "发布三维服务", notes = "发布三维服务")
    @Path("/{tileId}/publish-3D")
    public MessageResult<List<PublishResult>> publish3D(@PathParam("tileId")String tileId){
        List<PublishResult> results = tileService.publish3D(tileId);
        return MessageResult.data(results).build();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询瓦片", notes = "分页条件查询瓦片")
    public MessageResult<Page> pageList(@BeanParam TileSelectVo tileSelectVo,
										@DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
										@DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<Tile> page = tileService.listByPageCondition(tileSelectVo, pageNum, pageSize);
		return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增瓦片", notes = "新增瓦片")
    public MessageResult<Tile> insertTile(Tile tile){
        Tile result = tileService.insertTile(tile);
        return MessageResult.data(result).status(200).message("新增操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增瓦片", notes = "批量新增瓦片")
    public MessageResult<Void> batchInsert(List<Tile> Tiles){
        tileService.batchInsert(Tiles);
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除瓦片", notes = "根据id删除瓦片")
	@Path("/{tileId}")
    public MessageResult<Tile> deleteTile(@PathParam("tileId")String tileId){
        return MessageResult.data(tileService.deleteTile(tileId)).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除瓦片", notes = "批量删除瓦片")
    public MessageResult<Void> batchDelete(List<String> tileIds){
        tileService.batchDelete(tileIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新瓦片", notes = "根据id更新瓦片")
	@Path("/{tileId}")
    public MessageResult<Tile> updateTile(Tile tile,@PathParam("tileId")String tileId){
        tile.setTileId(tileId);
		return MessageResult.data(tileService.updateTile(tile)).status(200).message("更新操作成功").build();
    }

}
