package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.enums.NodeTypeEnum;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.data.mgt.entity.MmField;
import com.supermap.gaf.data.mgt.entity.vo.DataRangeSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardVo;
import com.supermap.gaf.data.mgt.mapper.DataStandardMapper;
import com.supermap.gaf.data.mgt.service.*;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.sys.mgt.client.SysCatalogClient;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 数据标准服务实现类 (充分校验)
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Service
public class DataStandardServiceImpl implements DataStandardService{
    
	private static final Logger  log = LoggerFactory.getLogger(DataStandardServiceImpl.class);
	
	@Autowired
    private DataStandardMapper dataStandardMapper;

	@Autowired
	private DataRangeService dataRangeService;


	@Autowired
	private SysCatalogClient sysCatalogClient;
	
	@Autowired
	private CommonValidateService commonValidateService;

	@Autowired
	private DataCodeService dataCodeService;

	@Autowired
	private MmFieldService mmFieldService;
	
	
	@Override
    public DataStandard getById(String dataStandardId){
        if(dataStandardId == null){
            throw new IllegalArgumentException("dataStandardId不能为空");
        }
        DataStandard re =  dataStandardMapper.select(dataStandardId);
        if(re == null){
            throw new GafException("资源不存在");
        }
        return re;
    }

    @Override
    public DataStandardVo getVoById(String dataStandardId){
        DataStandard dataStandard = getById(dataStandardId);
        DataStandardVo re = new DataStandardVo();
        BeanUtils.copyProperties(dataStandard,re);
        if(DataStandard.DomainType.RANGE.getCode().equals(dataStandard.getDomainType())){
            List<DataRange> dataRanges = dataRangeService.selectList(DataRangeSelectVo.builder().dataStandardId(dataStandardId).build());
            re.setDataRanges(dataRanges);
        }else if(dataStandard.getRefCode()!=null){
            try{
                re.setDataCodeName(dataCodeService.getById(dataStandard.getRefCode()).getName());;
            }catch (Exception e){
            }
        }
        return re;
    }
	
	@Override
    public Page<DataStandard> listByPageCondition(DataStandardSelectVo dataStandardSelectVo, int pageNum, int pageSize) {
        PageInfo<DataStandard> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataStandardMapper.selectList(dataStandardSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
    @Transactional(rollbackFor = Exception.class)
    public void insertDataStandard(DataStandardVo dataStandardVo){
        commonValidateService.validateCatalogId(dataStandardVo.getCatalogId(),CatalogTypeEnum.DATA_STANDARD_GROUP_TYPE);
        String dataStandardId = UUID.randomUUID().toString();
        // RANGE范围类型
        if(DataStandard.DomainType.RANGE.getCode().equals(dataStandardVo.getDomainType())){
            validateDataRange(dataStandardVo.getDataRanges());
            dataRangeService.batchDeleteByDataStandardIds(Arrays.asList(dataStandardId));
            batchInsertDataRange(dataStandardId,dataStandardVo.getDataRanges());
        }else{
            // 代码类型
            commonValidateService.validateRefCode(dataStandardVo.getRefCode());
        }
		dataStandardVo.setDataStandardId(dataStandardId);
        String userName = SecurityUtilsExt.getUserName();
        dataStandardVo.setCreatedBy(userName);
        dataStandardVo.setUpdatedBy(userName);
        dataStandardMapper.insert(dataStandardVo);
    }

	
	@Override
    public int deleteDataStandard(String dataStandardId){
		return batchDelete(Arrays.asList(dataStandardId));
    }

	@Override
    @Transactional(rollbackFor = Exception.class)
    public int batchDelete(List<String> dataStandardIds){
        if(!CollectionUtils.isEmpty(dataStandardIds)){
            List<MmField> mmFields = mmFieldService.getByDataStandardIds(dataStandardIds);
            if(!CollectionUtils.isEmpty(mmFields)){
                throw new GafException(String.format("【%s】字段正在使用。",mmFields.stream().map(MmField::getFieldName).collect(Collectors.joining(","))));
            }
            dataRangeService.batchDeleteByDataStandardIds(dataStandardIds);
            return dataStandardMapper.batchDelete(dataStandardIds);
        }
        return 0;
    }

	
	@Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDataStandard(DataStandardVo dataStandardVo){
        commonValidateService.validateCatalogId(dataStandardVo.getCatalogId(),CatalogTypeEnum.DATA_STANDARD_GROUP_TYPE);
        // RANGE范围类型
        if(DataStandard.DomainType.RANGE.getCode().equals(dataStandardVo.getDomainType())){
            validateDataRange(dataStandardVo.getDataRanges());
            dataRangeService.batchDeleteByDataStandardIds(Arrays.asList(dataStandardVo.getDataStandardId()));
            batchInsertDataRange(dataStandardVo.getDataStandardId(),dataStandardVo.getDataRanges());
        }else{
            // 代码类型
            commonValidateService.validateRefCode(dataStandardVo.getRefCode());
            DataStandard origin = getById(dataStandardVo.getDataStandardId());
            // 范围更新成代码，删除旧范围
            if(DataStandard.DomainType.RANGE.getCode().equals(origin.getRefCode())){
                dataRangeService.batchDeleteByDataStandardIds(Arrays.asList(dataStandardVo.getDataStandardId()));
            }
        }
        dataStandardVo.setUpdatedBy(SecurityUtilsExt.getUserName());
		return dataStandardMapper.update(dataStandardVo);
    }

    @Override
    public List<TreeNode> allNodes() {
        List<TreeNode> result = new LinkedList<>();
        MessageResult<List<TreeNode>> messageResult = sysCatalogClient.getNodesByType(CatalogTypeEnum.DATA_STANDARD_GROUP_TYPE.getValue());
        if(!messageResult.isSuccessed()){
            log.error(messageResult.getMessage());
            throw new GafException("获取目录失败");
        }
        List<TreeNode> catalogNodes = messageResult.getData();
        List<DataStandard> dataStandards = dataStandardMapper.selectList(new DataStandardSelectVo());
        List<TreeNode> dataStandardNodes = dataStandards.stream().map(dataStandard -> {
            TreeNode node = new TreeNode();
            node.setTitle(dataStandard.getChineseName());
            node.setParentId(dataStandard.getCatalogId());
            node.setType(NodeTypeEnum.DATA_STANDARD.getValue());
            node.setKey(dataStandard.getDataStandardId());
            return node;
        }).collect(Collectors.toList());
        result.addAll(catalogNodes);
        result.addAll(dataStandardNodes);
        return result;
    }

    @Override
    public List<DataStandard> getByRefCodes(List<String> refCodes) {
        if(!CollectionUtils.isEmpty(refCodes)){
            return dataStandardMapper.selectByRefCodes(refCodes);
        }
        return null;
    }


    void batchInsertDataRange(String dataStandardId, List<DataRange> dataRanges){
        if(dataRanges!=null){
            for(DataRange dataRange:dataRanges){
                dataRange.setDataStandardId(dataStandardId);
            }
            dataRangeService.batchInsert(dataRanges);
        }
    }

    

    void validateDataRange(List<DataRange> dataRanges){
        if(dataRanges!=null){
            for(DataRange dataRange:dataRanges){
                if(dataRange.getMax().compareTo(dataRange.getMin()) == -1){
                    throw new GafException("dataRange(min,max)校验失败:max必须大于min");
                }
            }
        }
    }

}
