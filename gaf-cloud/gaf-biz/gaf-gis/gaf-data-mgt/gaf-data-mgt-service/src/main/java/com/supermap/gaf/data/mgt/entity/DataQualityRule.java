package com.supermap.gaf.data.mgt.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据质量管理-规则表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-规则表")
public class DataQualityRule implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String dataQualityRuleId;
    @NotNull
    @ApiModelProperty("数据质量管理规则名称")
    private String dataQualityRuleName;
    @NotNull
    @ApiModelProperty("所属模型id")
    private String dataQualityModelId;
    @NotNull
    @ApiModelProperty("规则种类code")
    private String typeCode;
    @NotNull
    @ApiModelProperty("严重级别")
    private String importance;
    @NotNull
    @ApiModelProperty("参数")
    private String params;
    /**
    * 默认值1：1
    */
    @ApiModelProperty("排序")
    private Integer sortSn;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}