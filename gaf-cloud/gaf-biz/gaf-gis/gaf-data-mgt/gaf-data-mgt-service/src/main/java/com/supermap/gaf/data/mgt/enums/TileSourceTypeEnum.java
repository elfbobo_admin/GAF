package com.supermap.gaf.data.mgt.enums;

/**
 * 瓦片来源类型
 * @author wxl
 * @since 2021/9/22
 */
public enum TileSourceTypeEnum {

    UGCV5("ugcv5","UGCV5瓦片"),
    MONGODB("mongodb","MongoDB瓦片");

    private String code;
    private String name;

    TileSourceTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
