package com.supermap.gaf.data.mgt.datasync;

import com.google.common.collect.Sets;

import java.util.Set;

public abstract class DataSyncNotifier<T> {
    public static final String START = "1";
    public static final String FAIL = "3";
    public static final String SUCCESS = "2";
    private Set<String> types;
    private DataSyncNotifier<T> nextNotifier;

    public DataSyncNotifier(String[] types) {
        this.types = Sets.newHashSet(types);
    }
    public void setNextNotifier(DataSyncNotifier<T> nextNotifier) {
        this.nextNotifier = nextNotifier;
    }
    public void send(String type, T message){
        if(this.types.contains(type)){
            sendMessage(type,message);
        }
        if(nextNotifier!=null){
            nextNotifier.send(type,message);
        }
    }
    abstract protected void sendMessage(String type,T message);

}
