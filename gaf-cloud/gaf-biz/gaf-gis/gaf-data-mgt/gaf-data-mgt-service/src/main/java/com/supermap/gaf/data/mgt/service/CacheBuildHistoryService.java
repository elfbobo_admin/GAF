package com.supermap.gaf.data.mgt.service;


import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryLatestVO;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryVo;

import java.util.List;

/**
 * @description
 * @date:2022/2/11 9:24
 * @author:yw
 */
public interface CacheBuildHistoryService {

    void insert(CacheBuildHistory cacheBuildHistory);

    CacheBuildHistory findById(String id);

    Page<CacheBuildHistory> pageList(int pageNum, int pageSize, CacheBuildHistoryVo cacheBuildHistoryVo);

    void deleteById(String id);
    void update(String id,CacheBuildHistory cacheBuildHistory);


    List<CacheBuildHistory> findByIds(List<String> cacheBuildHistoryIds);

    /**
     *
     * @param publishHistoryId
     * @return
     */
    List<CacheBuildHistory> listByPublishHistoryId(String publishHistoryId);

    /**
     * 根据发布历史id查询其下所有最新缓存构建历史
     * @param publishHistoryId
     * @return
     */
    List<CacheBuildHistory> listLastests(String publishHistoryId);
    /**
     * 根据发布历史id查询其下所有最新缓存构建历史
     * @param publishHistoryId
     * @return
     */
    List<CacheBuildHistoryLatestVO> listLastest(String publishHistoryId);

    /**
     * 删除缓存构建历史记录,包括缓存切片产物
     * @param id 构建历史记录id
     * @param isDeleteCacheTiles 是否删除缓存切片产物
     */
    void deleteWithResource(String id, Boolean isDeleteCacheTiles);

    /**
     * 批量删除缓存构建历史记录,包括缓存切片产物
     * @param ids 构建历史记录id集合
     * @param isDeleteCacheTiles 是否删除缓存切片产物
     */
    void batchDeleteWithResource(List<String> ids, Boolean isDeleteCacheTiles);

    /**
     * 根据数据源id和数据集名批量删除缓存构建历史记录,包括缓存切片产物
     * @param publishHistoryId 发布历史id
     * @param datasourceId 数据源id
     * @param datasetName 数据集名
     * @param isDeleteCacheTiles 是否删除缓存切片产物
     */
    void deleteWithResourceByCondition(String publishHistoryId, String datasourceId, String datasetName, Boolean isDeleteCacheTiles);
}
