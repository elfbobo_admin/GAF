package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncLogSelectVo;

import java.util.List;

/**
 * 数据同步日志表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataSyncLogService {
	
	/**
    * 根据id查询数据同步日志表
    * @return
    */
    DataSyncLog getById(String dataSyncLogId);
	
	/**
     * 分页条件查询
     * @param dataSyncLogSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<DataSyncLog> listByPageCondition(DataSyncLogSelectVo dataSyncLogSelectVo, int pageNum, int pageSize);


    /**
    * 删除数据同步日志表
    * 
    */
    void deleteDataSyncLog(String dataSyncLogId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dataSyncLogIds);

    
}
