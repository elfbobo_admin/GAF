package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据同步任务表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据同步任务表条件查询实体")
public class DataSyncTaskSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = DataSyncTask.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataSyncTask.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataSyncTask.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("dataSyncTaskId")
    @ApiModelProperty("主键")
    private String dataSyncTaskId;
    @QueryParam("dataSyncTaskName")
    @ApiModelProperty("同步任务名称")
    private String dataSyncTaskName;
    @QueryParam("dataSyncStatus")
    @ApiModelProperty("同步任务状态")
    private Integer dataSyncStatus;
    @QueryParam("spatialDatasourceId")
    @ApiModelProperty("空间数据源id")
    private String spatialDatasourceId;
    @QueryParam("standardDatasourceId")
    @ApiModelProperty("标准数据源id")
    private String standardDatasourceId;
    @QueryParam("mappingInfo")
    @ApiModelProperty("映射信息 格式如：[{standardTableName: A表 ,spatialTableName: B表 ,fieldsMapping:[{standardTableFieldName: X1字段 ,spatialTableFieldName: X1字段 },{standardTableFieldName: X2字段 ,spatialTableFieldName: X2字段 }]},{}]")
    private String mappingInfo;
    @QueryParam("alertEmail")
    @ApiModelProperty("通知邮箱")
    private String alertEmail;
    @QueryParam("alertType")
    @ApiModelProperty("通知方式 多选 1开始时通知、2结束时通知、3报错时通知；多选时用,作为分隔符")
    private String alertType;
    @QueryParam("dataSyncCron")
    @ApiModelProperty("定时任务cron")
    private String dataSyncCron;
    @QueryParam("description")
    @ApiModelProperty("描述")
    private String description;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}