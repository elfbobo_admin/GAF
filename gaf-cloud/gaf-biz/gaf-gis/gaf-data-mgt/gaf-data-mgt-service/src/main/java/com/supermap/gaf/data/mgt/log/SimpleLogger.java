package com.supermap.gaf.data.mgt.log;

public interface SimpleLogger {
    void info(String format, Object... arguments);
    void error(String format, Object... arguments);
    void warn(String format, Object... arguments);
}
