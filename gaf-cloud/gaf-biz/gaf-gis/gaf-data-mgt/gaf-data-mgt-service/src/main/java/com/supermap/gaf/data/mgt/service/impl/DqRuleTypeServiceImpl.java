package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.gaf.data.mgt.dq.DqRuleType;
import com.supermap.gaf.data.mgt.service.DqRuleTypeService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DqRuleTypeServiceImpl implements DqRuleTypeService {
    public static final Map<String,DqRuleType> memory = new ConcurrentHashMap<>();
    static {
        memory.put("NULL_CHECK",DqRuleType.builder().typeCode("NULL_CHECK")
            .name("空值检查").description("检查字段是否为NULL").build());
    }
    @Override
    public List<DqRuleType> selectAll() {
        return new ArrayList<>(memory.values());
    }

    @Override
    public void add(DqRuleType type) {
        memory.putIfAbsent(type.getTypeCode(),type);
    }

    @Override
    public DqRuleType selectByCode(String code) {
        return memory.get(code);
    }
}
