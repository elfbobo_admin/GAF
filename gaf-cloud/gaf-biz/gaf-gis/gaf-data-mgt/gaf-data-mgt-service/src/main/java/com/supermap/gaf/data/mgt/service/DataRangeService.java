package com.supermap.gaf.data.mgt.service;
import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.data.mgt.entity.vo.DataRangeSelectVo;
import com.supermap.gaf.commontypes.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.*;

/**
 * 数据标准值域范围服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataRangeService {
	
	/**
    * id查询数据标准值域范围
    * @return
    */
	DataRange getById(String dataRangeId);
	
	/**
     * 分页条件查询
     * @param dataRangeSelectVo 查询条件
     * @param pageNum 当前页数
     * @param pageSize 页面大小
     * @return 分页对象
     */
	Page<DataRange> listByPageCondition(DataRangeSelectVo dataRangeSelectVo, int pageNum, int pageSize);

	/**
	 * 多条件查询
	 * @param dataRangeSelectVo 查询条件
	 * @return 若未查询到则返回空集合
	 */
	List<DataRange> selectList(DataRangeSelectVo dataRangeSelectVo);


		/**
         * 新增数据标准值域范围
         * @return
         */
    void insertDataRange(DataRange dataRange);

    /**
     * 删除数据标准值域范围
     * 
     */
    int deleteDataRange(String dataRangeId);

    /**
     * 批量删除
     * 
	 */
    int batchDelete(Collection<String> dataRangeIds);

	int batchDeleteByDataStandardIds(@Param("list") Collection<String> dataStandardIds);

    /**
     * 更新数据标准值域范围
     * @return 
     */
    int updateDataRange(DataRange dataRange);

	int batchInsert(List<DataRange> dataRanges);
}
