package com.supermap.gaf.data.mgt.util;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.supermap.gaf.data.mgt.entity.iserver.*;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import com.supermap.server.config.ProviderSetting;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

import static com.supermap.gaf.data.mgt.util.IserverUtils.COMPONENTS_ENDPOINT;
import static com.supermap.gaf.data.mgt.util.IserverUtils.PROVIDERS_ENDPOINT;
import static com.supermap.services.rest.management.ServiceType.RESTREALSPACE;


/**
 * The type Iserver Utils2.
 */
public class IserverUtils2 {

    private static final Logger log = LoggerFactory.getLogger(IserverUtils2.class);

    public static void updateMongoSerice(String iserverHostServerUrl,String serviceName,String[]datasetNames,String token){
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler() {
                @Override
                public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                    return false;
                }

                @Override
                public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                }
            });
            String providerUrl = iserverHostServerUrl+"/manager/providers/"+ serviceName +".rjson?token=" + token;
            System.out.println(providerUrl);
            String provider = restTemplate.getForObject(providerUrl, String.class);
            ProviderSetting providerSetting = JSON.parseObject(provider, ProviderSetting.class);
            Map config = (Map) providerSetting.config;
            config.put("tilesetNames",datasetNames);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
            String s1 = JSON.toJSONString(providerSetting);
            HttpEntity httpEntity = new HttpEntity(s1,httpHeaders);
            System.out.println(s1);
            ResponseEntity<String> exchange = restTemplate.exchange(providerUrl, HttpMethod.PUT, httpEntity, String.class);
            System.out.println(exchange);
        }catch (Exception e) {
            throw new GafException("更新服务实例失败，"+e.getMessage());
        }
    }


    /**
     * 发布MongoDB 3D瓦片
     *
     * @param param                 the param
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<List<PublishResult>> publish3DTile(MongoDBMVTTileProviderConfig param, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {

        String name = param.getName();
        // 添加provider
        BaseResponse<AddProviderResult> providerResultBaseResponse = addMongoDBRealspaceProvider(param, iserverHost, authInHeader, authInfoNameAndValues);
        if (!providerResultBaseResponse.getSucceed()) {
            return BaseResponse.error(providerResultBaseResponse.getError());
        }
        String providerName = providerResultBaseResponse.getData().getName();
        // 添加对应component
        ComponentParam componentParam = ComponentParam.builder()
                .providers(providerName)
                .interfaceNames("rest")
                .type("com.supermap.services.components.impl.RealspaceImpl")
                .name(name)
                .build();
        BaseResponse<ChildResourceResult> childResourceResultBaseResponse = addComponent(componentParam, iserverHost, authInHeader, authInfoNameAndValues);
        if (!childResourceResultBaseResponse.getSucceed()) {
            log.error("删除服务提供者{}", componentParam.getProviders());
            IserverUtils.deleteProvider(Arrays.asList(componentParam.getProviders().split(",")), iserverHost, authInHeader, authInfoNameAndValues);
            return BaseResponse.error(childResourceResultBaseResponse.getError());
        }
        // 解析成服务url返回
        ChildResourceResult childResourceResult = childResourceResultBaseResponse.getData();
        String componentsUrl = childResourceResult.getNewResourceLocation();
        String serviceUrl = IserverUtils.convert2ServiceUrl(componentsUrl);
        List<PublishResult> publishResults = new ArrayList<>();
        PublishResult publishResult = new PublishResult();
        publishResult.setServiceAddress(serviceUrl + "/rest");
        log.info("发布成功：{}",publishResult.getServiceAddress());
        publishResult.setServiceType(RESTREALSPACE);
        publishResults.add(publishResult);
        BaseResponse<List<PublishResult>> re = new BaseResponse<>();
        re.setSucceed(true);
        re.setData(publishResults);
        return re;
    }


    /**
     * Add component base response.
     *
     * @param componentParam        the component param
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    private static BaseResponse<ChildResourceResult> addComponent(ComponentParam componentParam, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        if (StringUtils.isEmpty(componentParam.getName())) {
            componentParam.setName(componentParam.getProviders() + "-" + System.currentTimeMillis());
        }
        try {
            ResponseEntity<String> response = doAddComponent(componentParam, iserverHost, authInHeader, authInfoNameAndValues);

            BaseResponse re = new BaseResponse();
            String bodyString = response.getBody();
            if (response.getStatusCode().is2xxSuccessful()) {
                log.info("向{}添加服务组件成功", iserverHost);
                re.setSucceed(true);
                re.setData(GlobalJacksonObjectMapper.instance().readValue(bodyString, new TypeReference<ChildResourceResult>() {
                }));
            } else {
                log.error("向"+iserverHost+"添加服务组件失败");
                re = GlobalJacksonObjectMapper.instance().readValue(bodyString, BaseResponse.class);
            }
            return re;
        }catch (Exception e) {
            log.error("向"+iserverHost+"添加服务组件失败", e);
            ErrorState errorState = new ErrorState();
            errorState.setCode(500);
            errorState.setErrorMsg(e.getMessage());
            return BaseResponse.error(errorState);
        }

    }

    private static ResponseEntity<String> doAddComponent(ComponentParam componentParam, String iserverHost, boolean authInHeader, String[] authInfoNameAndValues) throws URISyntaxException {
        log.info("向{}添加服务组件", iserverHost);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        String url = String.format(COMPONENTS_ENDPOINT, iserverHost);
        url = modiryUrl(url,authInHeader, authInfoNameAndValues);
        modifyHeaders(headers, authInHeader, authInfoNameAndValues);
        HttpEntity<ComponentParam> httpEntity = new HttpEntity<>(componentParam, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response= restTemplate.postForEntity(url, httpEntity, String.class);
        return response;
    }

    /**
     * Add mongo db realspace provider base response.
     * @return the base response
     */
    private static BaseResponse<AddProviderResult> addMongoDBRealspaceProvider(MongoDBMVTTileProviderConfig config, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = config.getName();
        if (StringUtils.isEmpty(name)) {
            name = "3D-mongodb" + UUID.randomUUID().toString().replace("-", "");
        }
        BaseResponse<AddProviderResult> re = new BaseResponse<>();
        try {
            log.info("向{}添加服务提供者",iserverHost);
            ResponseEntity<String> response = doAddMongoDBRealspaceProvider(config, iserverHost, authInHeader, name, authInfoNameAndValues);
            if (response.getStatusCode().is2xxSuccessful()) {
                log.info("向{}添加服务提供者成功",iserverHost);
                re.setSucceed(true);
            } else {
                String body = response.getBody();
                re = GlobalJacksonObjectMapper.instance().readValue(body, BaseResponse.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorState errorState = new ErrorState();
            errorState.setCode(500);
            errorState.setErrorMsg(e.getMessage());
            re = BaseResponse.error(errorState);
        }
        re.setData(new AddProviderResult(name));
        return re;
    }

    private static ResponseEntity<String> doAddMongoDBRealspaceProvider(MongoDBMVTTileProviderConfig config, String iserverHost, boolean authInHeader, String name, String[] authInfoNameAndValues) throws URISyntaxException {
        Map<String, Object> param = new HashMap<>(4);
        param.put("config", config);
        param.put("name", name);
        param.put("type", "com.supermap.services.providers.MongoDBRealspaceProvider");
        param.put("enable", true);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        String url = String.format(PROVIDERS_ENDPOINT, iserverHost);
        url = modiryUrl(url,authInHeader, authInfoNameAndValues);
        modifyHeaders(headers, authInHeader, authInfoNameAndValues);
        HttpEntity<Map<String,Object>> httpEntity = new HttpEntity<>(param, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response= restTemplate.postForEntity(url, httpEntity, String.class);
        return response;
    }

    private static String modiryUrl(String url,boolean authInHeader, String... authInfoNameAndValues) throws URISyntaxException {
        if (authInfoNameAndValues != null && !authInHeader) {
            URIBuilder uriBuilder= new URIBuilder(url);
            for (int i = 0; i < authInfoNameAndValues.length; i += 2) {
                uriBuilder.addParameter(authInfoNameAndValues[i], authInfoNameAndValues[i + 1]);
            }
            return uriBuilder.build().toString();
        } else {
            return url;
        }

    }

    private static void modifyHeaders(HttpHeaders headers ,boolean authInHeader, String... authInfoNameAndValues) throws URISyntaxException {
        if (authInfoNameAndValues != null && authInHeader) {
            MultiValueMap<String, String> values = new LinkedMultiValueMap<>();
            for (int i = 0; i < authInfoNameAndValues.length; i += 2) {
                values.add(authInfoNameAndValues[i], authInfoNameAndValues[i + 1]);
            }
            headers.addAll(values);
        }
    }

}