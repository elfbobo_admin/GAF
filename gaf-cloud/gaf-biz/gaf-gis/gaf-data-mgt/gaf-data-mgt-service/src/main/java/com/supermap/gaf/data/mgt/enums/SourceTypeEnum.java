package com.supermap.gaf.data.mgt.enums;

/**
 * 发布历史 的 来源类型
 *
 *
 */
public enum SourceTypeEnum {

    // WORKSPACE_FOR_STORAGE_TILE
    FILE_WORKSPACE_FOR_STORAGE_TILE("FILE_WORKSPACE_FOR_STORAGE_TILE","用于存储瓦片地址的文件型工作空间"),
    // MONGODB_TILE
    MONGODB("MONGODB","MongoDB"),
    MANAGED_WORKSPACE("MANAGED_WORKSPACE","管理的工作空间"),
    MANAGED_TILE("MANAGED_TILE","管理的瓦片,包括ugcv5 和 mongodb瓦片");

    /**
     * 值
     */
    private final String value;

    /**
     * 描述
     */
    private final String description;

    SourceTypeEnum(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String value() {
        return this.value;
    }

    public String getDescription() {
        return description;
    }

}
