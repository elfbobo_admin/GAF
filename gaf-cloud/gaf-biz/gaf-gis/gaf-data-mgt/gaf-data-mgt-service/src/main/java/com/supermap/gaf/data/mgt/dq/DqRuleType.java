package com.supermap.gaf.data.mgt.dq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DqRuleType {
    /**
     * 规则类型的编码
     */
    @NotBlank
    private String typeCode;
    /**
     * 规则类型的名称
     */
    private String name;
    private String description;
    private String httpUrl;
    private boolean isMicroServiceRequest;

}
