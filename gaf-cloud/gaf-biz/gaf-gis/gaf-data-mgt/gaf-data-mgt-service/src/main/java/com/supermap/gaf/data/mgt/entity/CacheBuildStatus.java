package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description
 * @date:2022/2/21 11:08
 * @author:yw
 */
@Data
public class CacheBuildStatus {

    private String cacheBuildHistroyId;

    @ApiModelProperty("缓存构建状态")
    private Integer code;

    @ApiModelProperty("缓存构建状态说明")
    private String description;

    @ApiModelProperty("进度条.当缓存构建状态为RUNNING(切片中)时使用")
    private Integer progress;

}
