package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskVo;
import com.supermap.gaf.data.mgt.service.DataSyncTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据同步任务表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据同步任务表接口")
public class DataSyncTaskResource{
    @Autowired
    private DataSyncTaskService dataSyncTaskService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据同步任务表", notes = "通过id查询数据同步任务表")
	@Path("/{dataSyncTaskId}")
    @ApiImplicitParam(name = "dataSyncTaskId",value =  "dataSyncTaskId",paramType = "path",dataType = "string",required = true )
    public MessageResult<DataSyncTask> getById(@PathParam("dataSyncTaskId")String dataSyncTaskId){
        DataSyncTask dataSyncTask = dataSyncTaskService.getById(dataSyncTaskId);
		return MessageResult.data(dataSyncTask).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据同步任务视图详情", notes = "通过id查询数据同步任务视图详情")
    @Path("/vos/{dataSyncTaskId}")
    @ApiImplicitParam(name = "dataSyncTaskId",value =  "dataSyncTaskId",paramType = "path",dataType = "string",required = true )
    public MessageResult<DataSyncTaskVo> getVoById(@PathParam("dataSyncTaskId")String dataSyncTaskId){
        DataSyncTaskVo re = dataSyncTaskService.getVoById(dataSyncTaskId);
        return MessageResult.successe(DataSyncTaskVo.class).data(re).message("查询成功").build();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据同步任务表", notes = "分页条件查询数据同步任务表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[0,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range[0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<Page<DataSyncTask>> pageList(@BeanParam DataSyncTaskSelectVo dataSyncTaskSelectVo,
										@DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
										@DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataSyncTask> page = dataSyncTaskService.listByPageCondition(dataSyncTaskSelectVo, pageNum, pageSize);
		return MessageResult.data(page).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据同步任务表", notes = "新增数据同步任务表")
    @ApiImplicitParam(name ="body",value = "dataSyncTask",paramType = "body",dataTypeClass = DataSyncTask.class,required = true )
    public MessageResult<Void> insertDataSyncTask(DataSyncTask dataSyncTask){
        dataSyncTaskService.insertDataSyncTask(dataSyncTask);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据同步任务表", notes = "根据id删除数据同步任务表")
	@Path("/{dataSyncTaskId}")
    @ApiImplicitParam(name = "dataSyncTaskId",value =  "dataSyncTaskId",paramType = "path",dataType = "string",required = true )
    public MessageResult<Void> deleteDataSyncTask(@PathParam("dataSyncTaskId")String dataSyncTaskId){
        dataSyncTaskService.deleteDataSyncTask(dataSyncTaskId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据同步任务表", notes = "批量删除数据同步任务表")
    @ApiImplicitParam(name = "dataSyncTaskIds",value = "dataSyncTaskIds",paramType = "body",allowMultiple = true,required = true )
    public MessageResult<Void> batchDelete(List<String> dataSyncTaskIds){
        dataSyncTaskService.batchDelete(dataSyncTaskIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据同步任务表", notes = "根据id更新数据同步任务表")
	@Path("/{dataSyncTaskId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name ="body",value = "dataSyncTask",paramType = "body",dataTypeClass = DataSyncTask.class,required = true ),
            @ApiImplicitParam(name = "dataSyncTaskId",value =  "dataSyncTaskId",paramType = "path",dataType = "string",required = true )
    })
    public MessageResult<Void> updateDataSyncTask(DataSyncTask dataSyncTask,@PathParam("dataSyncTaskId")String dataSyncTaskId){
        dataSyncTask.setDataSyncTaskId(dataSyncTaskId);
        dataSyncTaskService.updateDataSyncTask(dataSyncTask);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }


    @POST
    @Path("/{dataSyncTaskId}/run")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "执行规则", notes = "执行规则")
    @ApiImplicitParam(name = "dataSyncTaskId",value =  "dataSyncTaskId",paramType = "path",dataType = "string",required = true )
    public MessageResult<Void> execute(@PathParam("dataSyncTaskId") String dataSyncTaskId){
        dataSyncTaskService.startSyncJob(dataSyncTaskId);
        return MessageResult.successe(Void.class).status(200).message("提交完成").build();
    }





}
