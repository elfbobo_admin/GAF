package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataCode;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeSelectVo;
import com.supermap.gaf.data.mgt.service.DataCodeService;
import com.supermap.gaf.utils.TreeUtil;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.groups.ConvertGroup;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Comparator;
import java.util.List;


/**
 * 数据代码接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据代码接口")
public class DataCodeResource{

    @Autowired
    private DataCodeService dataCodeService;

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询数据代码", notes = "根据id查询数据代码")
	@Path("/{dataCodeId}")
    public MessageResult<DataCode> getById(@PathParam("dataCodeId")String dataCodeId){
        DataCode dataCode = dataCodeService.getById(dataCodeId);
		return MessageResult.data(dataCode).message("查询成功").build();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据代码", notes = "分页条件查询数据代码")
    public MessageResult<Page<DataCode>> pageList(@Valid @BeanParam DataCodeSelectVo dataCodeSelectVo,
                                                  @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                                  @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataCode> page = dataCodeService.listByPageCondition(dataCodeSelectVo, pageNum, pageSize);
		return MessageResult.data(page).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询数据代码+目录树", notes = "查询数据代码+目录树")
    @Path("/tree")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "catalogSortField", value = "目录的排序依据：字段名", paramType = "query", dataType = "string", example = "title"),
            @ApiImplicitParam(name = "catalogSortField", value = "叶子的排序依据：字段名", paramType = "query", dataType = "string", example = "title")
    })
    public MessageResult<List<TreeNode>> tree(@StringRange(entityClass = TreeNode.class,isCamelCaseToUnderscore = false) @QueryParam("catalogSortField")@DefaultValue("title") String catalogSortField,
                                              @StringRange(entityClass = TreeNode.class,isCamelCaseToUnderscore = false) @QueryParam("leafSortField")@DefaultValue("title")String leafSortField){
        List<TreeNode> nodes = dataCodeService.allNodes();
        List<TreeNode> re = TreeUtil.build(nodes, Comparator.comparing(n -> DataStandardResource.getCatalogFirstSortValue(n, catalogSortField, leafSortField)));
        return MessageResult.data(re).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据代码", notes = "新增数据代码")
    public MessageResult<Void> insertDataCode(@Valid @ConvertGroup(to= AddGroup.class)DataCode dataCode){
        dataCodeService.insertDataCode(dataCode);
		return MessageResult.successe(Void.class).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据代码", notes = "根据id删除数据代码")
	@Path("/{dataCodeId}")
    public MessageResult<Integer> deleteDataCode(@PathParam("dataCodeId")String dataCodeId){
        int re = dataCodeService.deleteDataCode(dataCodeId);
		return MessageResult.data(re).message("删除操作成功").build();
    }


	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据代码", notes = "批量删除数据代码")
    public MessageResult<Integer> batchDelete(@NotEmpty List<String> dataCodeIds){
        int re = dataCodeService.batchDelete(dataCodeIds);
		return MessageResult.data(re).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据代码", notes = "根据id更新数据代码")
	@Path("/{dataCodeId}")
    public MessageResult<Integer> updateDataCode(@Valid @ConvertGroup(to= UpdateGroup.class)DataCode dataCode,@PathParam("dataCodeId")String dataCodeId){
        dataCode.setDataCodeId(dataCodeId);
        int re = dataCodeService.updateDataCode(dataCode);
		return MessageResult.data(re).message("更新操作成功").build();
    }

}
