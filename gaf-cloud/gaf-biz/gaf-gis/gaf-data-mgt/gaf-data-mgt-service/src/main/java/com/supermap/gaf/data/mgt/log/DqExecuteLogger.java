package com.supermap.gaf.data.mgt.log;

import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class DqExecuteLogger implements AutoCloseable, SimpleLogger {
    private static final Logger log = LoggerFactory.getLogger(DqExecuteLogger.class);
    private static final String SHANGHAI_TIME_ZONE = "Asia/Shanghai";
    private static final String DATE_FORMAT_PATERN = "yyyy-MM-dd HH:mm:ss";

    private DqExecuteLogMapper dqExecuteLogMapper;

    private DqExecuteLog dqExecuteLog;
    private StringBuffer logBuffer = new StringBuffer();
    public DqExecuteLogger(DqExecuteLogMapper dqExecuteLogMapper, String programId, String ruleId, String ruleName, String dqExecuteResultId) {
        this.dqExecuteLogMapper = dqExecuteLogMapper;
        dqExecuteLog = DqExecuteLog.builder()
                .dqExecuteLogId(UUID.randomUUID().toString())
                .resultStatus(0)
                .dataQualityRuleName(ruleName)
                .dataQualityProgramId(programId)
                .dqExecuteResultId(dqExecuteResultId)
                .createdTime(new Date())
                .dataQualityRuleId(ruleId).build();
    }

    public void setTotalAmount(long amount){
        dqExecuteLog.setTotalAmount(amount);
    }

    public void setFailedAmount(long amount){
        dqExecuteLog.setFailedAmount(amount);
    }

    String getDateFormatString(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_PATERN);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(SHANGHAI_TIME_ZONE));
        return simpleDateFormat.format(new Date());
    }
    @Override
    public void info(String format, Object... arguments){
        log.info(format,arguments);
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logBuffer.append(getDateFormatString()).append(" INFO ").append(ft.getMessage()).append("\n");
    }
    @Override
    public void error(String format, Object... arguments){
        log.error(format,arguments);
        dqExecuteLog.setResultStatus(1);
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logBuffer.append(getDateFormatString()).append(" ERROR ").append(ft.getMessage()).append("\n");
    }
    @Override
    public void warn(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        log.warn(format,arguments);
        logBuffer.append(getDateFormatString()).append(" WARN ").append(ft.getMessage()).append("\n");
    }


    @Override
    public void close() {
        dqExecuteLog.setUpdatedTime(new Date());
        long castTime = (dqExecuteLog.getUpdatedTime().getTime() - dqExecuteLog.getCreatedTime().getTime());
        dqExecuteLog.setCostTime(castTime);
        dqExecuteLog.setContent(logBuffer.toString());
        dqExecuteLogMapper.insert(dqExecuteLog);
    }
}
