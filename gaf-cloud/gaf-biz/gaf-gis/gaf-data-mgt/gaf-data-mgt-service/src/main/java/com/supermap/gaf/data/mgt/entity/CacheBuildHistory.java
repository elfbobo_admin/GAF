package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("缓存切片历史")
public class CacheBuildHistory {
    @ApiModelProperty("id")
    public String id;
    @ApiModelProperty("数据源id")
    public String datasourceId;
    @ApiModelProperty("数据集名")
    public String datasetName;
    @ApiModelProperty("存储名")
    public String storageName;
    @ApiModelProperty(value = "切片类型" , allowableValues = "increment(增量),fullVolume(全量)")
    public String cacheType;
    @ApiModelProperty("瓦片类型(数据集类型).暂时支持GRID,IMAGE,MODEL,POINT,LINE,REGION")
    public String tileType;
    @ApiModelProperty("颜色。发布时的参数设置，例如{\"r\":249,\"g\":31,\"b\":31,\"a\":100}")
    public String color;
    @ApiModelProperty("增量切片查询条件")
    public String queryParameter;
    // 简化和简化率 专用于模型
    @ApiModelProperty("是否轻量化")
    private Boolean isSimp;
    @ApiModelProperty("简化率(轻量化专属)")
    private Double simplifyingRate;

    @ApiModelProperty("状态 0新建 1进行中 2 失败 3成功 ")
    public Integer status;
    @ApiModelProperty("错误信息")
    public String errorMessage;
    @ApiModelProperty("创建时间")
    public Date createdTime;
    @ApiModelProperty("发布历史id。当未进行服务发布时，发布历史可用作连接信息，可用于存储缓存产物")
    public String publishHistoryId;

}
