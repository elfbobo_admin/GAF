package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.entity.*;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;
import com.supermap.gaf.data.mgt.service.PublishService;
import com.supermap.services.rest.management.PublishServiceParameter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 服务发布接口
 *
 * @author wsw
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "服务发布接口")
public class PublishResource {

    @Autowired
    private PublishService publishService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据发布历史id查询其下的最新的缓存构建任务的状态")
    @ApiImplicitParam(name = "id", value = "发布历史id", paramType = "path", dataType = "String")
    @Path("/{publishHistoryId}/lastest-cache-status")
    public MessageResult<List<CacheBuildStatus>> getLatestCacheStatus(@PathParam("publishHistoryId")String publishHistoryId){
        List<CacheBuildStatus> results = publishService.getLatestCacheStatus(publishHistoryId);
        return MessageResult.data(results).status(200).message("查询成功").build();
    }


    @ApiOperation(value = "新的根据工作空间发布服务,并记录发布历史", notes = "新的根据工作空间发布服务,并记录发布历史", response = MessageResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "workspaceId",value =  "workspaceId",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name ="body",value = "publishParam",paramType = "body",dataTypeClass = PublishServiceParameter.class,required = true )
    })
    @Path("/workspace/{workspaceId}")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<PublishHistory> publishDataWorkspace(@PathParam("workspaceId") String workspaceId, PublishParam publishParam){
        PublishHistory publishHistory = publishService.publishDataWorkspace(workspaceId, publishParam);
        return MessageResult.data(publishHistory).message("服务发布完成").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "将来源类型为MONGODB和FILE_WORKSPACE_FOR_STORAGE的发布历史发布为三维服务", notes = "同时可以将数据集发布为数据服务")
    @Path("/do")
    public MessageResult<Void> publishMongodbOrWorkspace(PublishParamemter publishParamemter) {
        publishService.publishMongodbOrWorkspace(publishParamemter);
        return MessageResult.successe(Void.class).build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "在某发布历史下新增数据集切片并发布为三维服务", notes = "同时可以将数据集发布为数据服务")
    @Path("/new-cache")
    public MessageResult<PublishCacheBuildHistorVo> publishScenceWithDataInPublishHistory(CacheBuildPublishParam2 cacheBuildPublishParam2) {
        PublishCacheBuildHistorVo vo = publishService.publishNewCacheBuildInPublishHistory(cacheBuildPublishParam2);
        return MessageResult.data(vo).build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "将数据集切片并发布为三维服务", notes = "同时可以将数据集发布为数据服务")
    @Path("/cache")
    public MessageResult<PublishCacheBuildHistorVo> publishCacheBuild(CacheBuildPublishParam cacheBuildPublishParam) {
        PublishCacheBuildHistorVo vo = publishService.publishCacheBuild(cacheBuildPublishParam);
        return MessageResult.data(vo).build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "将管理的瓦片发布为地图服务", notes = "将管理的瓦片发布为地图服务")
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/tile-map/{tileId}")
    public MessageResult<PublishHistory> publishTileMap(PublishTileParam param,@PathParam("tileId")String tileId){
        PublishHistory publishHistory = publishService.publishTileMap(tileId,param);
        return MessageResult.data(publishHistory).build();
    }

}
