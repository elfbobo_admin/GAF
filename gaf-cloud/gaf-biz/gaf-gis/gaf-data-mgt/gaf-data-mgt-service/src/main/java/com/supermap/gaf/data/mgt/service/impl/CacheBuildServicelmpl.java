package com.supermap.gaf.data.mgt.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.*;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.entity.*;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorTaskVo;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;
import com.supermap.gaf.data.mgt.enums.*;
import com.supermap.gaf.data.mgt.service.*;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.data.mgt.support.TtlContextHolder;
import com.supermap.gaf.data.mgt.util.MongoUtils;
import com.supermap.gaf.data.mgt.wrapper.MyHttpServletRequestWrapper;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageType;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * @author : wsw
 * @since : 2021-3-12
 */
@Service
public class CacheBuildServicelmpl implements CacheBuildService {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CacheBuildServicelmpl.class);
    private  static final Map<DatasetType, DatasetTypeCacheService> map = new HashMap<>();
    static {
        map.put(DatasetType.GRID,new GridCacheServiceImpl());
        map.put(DatasetType.IMAGE,new ImageCacheServiceImpl());
        map.put(DatasetType.MODEL,new ModelCacheServiceImpl());
        map.put(DatasetType.POINT,new VectorCacheServiceImpl());
        map.put(DatasetType.LINE,new VectorCacheServiceImpl());
        map.put(DatasetType.REGION,new VectorCacheServiceImpl());
    }
    @Value("${supermap.workspace-storage.file-path-prefix:data/workspace}")
    private String filePathPrefix;


    @Autowired
    @Qualifier("taskExecutor")
    private Executor taskExecutor;

    @Autowired
    @Qualifier("DatamgtStorageClient")
    private StorageClient storageClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;



    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @Autowired
    private PublishHistoryService publishHistoryService;

    @Autowired
    private DataWorkspaceService dataWorkspaceService;

    @Autowired
    private SysResourceDatasourceService sysResourceDatasourceService;

    @Autowired
    private ConvertHelper convertHelper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public PublishCacheBuildHistorTaskVo cacheBuildInPublishHistory(CacheBuildPublishParam2 cacheBuildPublishParam2) {
        String publishHistoryId = cacheBuildPublishParam2.getPublishHistoryId();
        PublishHistory publishHistory = publishHistoryService.selectById(publishHistoryId);
        List<DatasetCacheBuildSetting> datasetCacheBuildSettings = cacheBuildPublishParam2.getDatasetCacheBuildSettings();
        List<CacheBuildHistory> cacheBuildHistories = recordCacheBuildHistory(datasetCacheBuildSettings,publishHistoryId);
        PublishCacheBuildHistorTaskVo publishCacheBuildHistorTaskVo = runCacheBuild(cacheBuildHistories);
        PublishCacheBuildHistorVo vo = new PublishCacheBuildHistorVo();
        vo.setPublishHistory(publishHistory);
        vo.setCacheBuildHistorys(cacheBuildHistories);
        publishCacheBuildHistorTaskVo.setPublishCacheBuildHistorVo(vo);
        return publishCacheBuildHistorTaskVo;
    }

    @Override
    public List<CacheBuildStatus> getCacheSatus(List<String> cacheBuildHistoryIds) {
        List<CacheBuildStatus> results = new ArrayList<>();
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryService.findByIds(cacheBuildHistoryIds);
        for (CacheBuildHistory cacheBuildHistory : cacheBuildHistories) {
            CacheBuildStatus cacheBuildStatus = new CacheBuildStatus();
            String id = cacheBuildHistory.getId();
            cacheBuildStatus.setCacheBuildHistroyId(id);
            cacheBuildStatus.setCode(cacheBuildHistory.getStatus());
            cacheBuildStatus.setDescription(CacheBuildStatusEnum.getDescriptionByCode(cacheBuildHistory.getStatus()));
            // 从redis中查询是否有进度条
            String percent = stringRedisTemplate.opsForValue().get("cache_build_step_percent:" + id);
            if (percent != null) {
                cacheBuildStatus.setProgress(Integer.parseInt(percent));
            }
            results.add(cacheBuildStatus);
        }
        return results;
    }

    @Transactional
    @Override
    public PublishCacheBuildHistorTaskVo cacheBuild(CacheBuildParam cacheBuildParam) {
        PublishCacheBuildHistorVo vo = record(cacheBuildParam);
        List<CacheBuildHistory> cacheBuildHistorys = vo.getCacheBuildHistorys();

        PublishCacheBuildHistorTaskVo publishCacheBuildHistorTaskVo = runCacheBuild(cacheBuildHistorys);

        publishCacheBuildHistorTaskVo.setPublishCacheBuildHistorVo(vo);
        return publishCacheBuildHistorTaskVo;
    }

    private PublishCacheBuildHistorVo record(CacheBuildParam cacheBuildParam) {
        PublishCacheBuildHistorVo vo = new PublishCacheBuildHistorVo();
        // 根据参数创建工作空间 和 缓存构建历史 发布历史
        String storageType = cacheBuildParam.getStorageType();
        String storageInfo = cacheBuildParam.getStorageInfo();
        // 如果是FILE_WORKSPACE，
        PublishHistory  publishHistory = null;
        if (StorageTypeEnum.WORKSPACE.value().equals(storageType)) {
            // 根据json的格式判断 是 新增的还是已有的工作空间
            JSONObject jsonObject = JSON.parseObject(storageInfo);
            String workspaceName = jsonObject.getString("workspaceName");
            if (workspaceName != null) {
                // 新建文件型工作空间并注册
                String serverPath =  createNewFileWorkspace(workspaceName);
                DataWorkspace dataWorkspace = new DataWorkspace();
                dataWorkspace.setStatus(true);
                dataWorkspace.setPublished(false);
                dataWorkspace.setServer(serverPath);
                dataWorkspace.setTypeCode(WorkspaceType.SMWU.name());
                dataWorkspace.setWsName(workspaceName);
                dataWorkspaceService.insertDataWorkspace(dataWorkspace);
                String workspaceId = dataWorkspace.getWorkspaceId();

                // 新增发布历史
                publishHistory = new PublishHistory();
                publishHistory.setSourceType(SourceTypeEnum.FILE_WORKSPACE_FOR_STORAGE_TILE.value());
                publishHistory.setSourceInfo("{\"workspaceId\": \""+ workspaceId +"\"}");
                publishHistory.setSourceName(workspaceName);
                publishHistory.setState(PublishHistoryStateEnum.NEW.ordinal());


            }
            String workspaceId = jsonObject.getString("workspaceId");
            if (workspaceId != null) {
                DataWorkspace dataWorkspace = dataWorkspaceService.getById(workspaceId);
                if (dataWorkspace == null) throw new GafException("未找到工作空间信息");

                publishHistory = new PublishHistory();
                publishHistory.setSourceType(SourceTypeEnum.MANAGED_WORKSPACE.value());
                publishHistory.setSourceInfo("{\"workspaceId\": \""+ workspaceId +"\"}");
                String wsName = dataWorkspace.getWsName();
                publishHistory.setSourceName(wsName);
                publishHistory.setState(PublishHistoryStateEnum.NEW.ordinal());
            }
            // 新增多个缓存构建历史
        } else  if (StorageTypeEnum.MONGODB.value().equals(storageType)) {
            // 根据json的格式判断 是 新增的还是已有的工作空间
            JSONObject jsonObject = JSON.parseObject(storageInfo);
            String connectionName = jsonObject.getString("connectionName");
            if (connectionName == null) throw new GafException("未找到MongoDB连接名");
            publishHistory = new PublishHistory();
            publishHistory.setSourceType(SourceTypeEnum.MONGODB.value());
            publishHistory.setSourceInfo(storageInfo);
            String s = RandomStringUtils.random(4, true, true);
            publishHistory.setSourceName(connectionName+"_"+s);
            publishHistory.setState(PublishHistoryStateEnum.NEW.ordinal());

        } else {
            throw new GafException("不支持的缓存存储类型:" + storageType);
        }
        if (publishHistory == null) throw new GafException("缓存构建存储信息错误");
        publishHistoryService.insert(publishHistory);
        vo.setPublishHistory(publishHistory);

        String publishHistoryId = publishHistory.getId();
        List<DatasetCacheBuildSetting> datasetCacheBuildSettings = cacheBuildParam.getDatasetCacheBuildSettings();
        List<CacheBuildHistory> cacheBuildHistories = recordCacheBuildHistory(datasetCacheBuildSettings, publishHistoryId);
        vo.setCacheBuildHistorys(cacheBuildHistories);
        return vo;
    }


    private PublishCacheBuildHistorTaskVo runCacheBuild(List<CacheBuildHistory> cacheBuildHistorys) {
        List<CompletableFuture<CacheBuildResult>> futures = new ArrayList<>();
        List<CacheBuildResult> cacheBuildResults = new ArrayList<>();
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes copy = new ServletRequestAttributes(new MyHttpServletRequestWrapper(requestAttributes.getRequest()));
        TtlContextHolder.context.set(copy);
        for (CacheBuildHistory cacheBuildHistory : cacheBuildHistorys) {
            // 更新切片状态
            String id = cacheBuildHistory.getId();
            setStatus(id,CacheBuildStatusEnum.RUNNING);
            CompletableFuture<CacheBuildResult> future = CompletableFuture.supplyAsync(() -> {
                return doCacheBuild(cacheBuildHistory);
            }, taskExecutor).whenComplete((cacheBuildResult, throwable) -> {
                cacheBuildResults.add(cacheBuildResult);
                // 更新切片状态 并清除缓存
                if (cacheBuildResult.getSuccess()) {
                    setStatus(id, CacheBuildStatusEnum.SUCCESSED);
                } else {
                    setError(id, cacheBuildResult.getErrorMessage());
                }
                try {
                    stringRedisTemplate.delete("cache_build_step_percent:" + id);
                } catch (Exception e) {
                    logger.error("清除切片构建状态缓存失败,原因:"+ e.getMessage(),e);
                }


            }).exceptionally(throwable -> {
                String message = throwable.getMessage();
                setError(id, message);
                CacheBuildResult r  = new CacheBuildResult();
                r.setSuccess(false);
                r.setErrorMessage(message);
                cacheBuildResults.add(r);
                return null;
            });
            futures.add(future);
        }
        PublishCacheBuildHistorTaskVo result = new PublishCacheBuildHistorTaskVo();
        result.setAllFutures(futures);
        result.setCacheBuildResults(cacheBuildResults);
        return result;
    }

    // 切片任务
    private CacheBuildResult doCacheBuild(CacheBuildHistory cacheBuildHistory) {
        RequestAttributes requestAttributes = TtlContextHolder.context.get();
        RequestContextHolder.setRequestAttributes(requestAttributes);
        try {

            CacheBuildResult cacheBuildResult = new CacheBuildResult();
            String id = cacheBuildHistory.getId();
            cacheBuildResult.setCacheBuildHistroyId(id);
            cacheBuildResult.setSuccess(true);

            Workspace workspace = null;
            Workspace scenesWorkspace = null;
            Datasource datasource = null;
            try {

                String datasourceId = cacheBuildHistory.getDatasourceId();
                SysResourceDatasource sysResourceDatasource = sysResourceDatasourceService.getById(datasourceId);
                DatasourceConnectionInfo connectionInfo = convertHelper.conver2DatasourceConnectionInfo(sysResourceDatasource);

                workspace = new Workspace();
                datasource = workspace.getDatasources().open(connectionInfo);
                String datasetName = cacheBuildHistory.getDatasetName();
                Dataset dataset = datasource.getDatasets().get(datasetName);
                // 判断是增量还是全量
                String cacheType = cacheBuildHistory.getCacheType();
                if (cacheType.equals(CacheTypeEnum.INCREMENT.getCode())){
                    String  attributeFilter = cacheBuildHistory.getQueryParameter();
                    dataset = increment(dataset,attributeFilter,CursorType.DYNAMIC);
                    logger.info("当前增量后数据集名称：" + dataset.getName());
                }

                DatasetType datasetType = dataset.getType();
                DatasetTypeCacheService datasetTypeCacheService = map.get(datasetType);
                if (datasetTypeCacheService == null) throw new GafException(String.format("未识别数据集类型【%s】需要确认切片支持类型", datasetType));

                // 获取存储信息
                String publishHistoryId = cacheBuildHistory.getPublishHistoryId();
                PublishHistory publishHistory = publishHistoryService.selectById(publishHistoryId);
                String sourceType = publishHistory.getSourceType();
                String sourceInfo = publishHistory.getSourceInfo();

                // 其他参数
                Map<String,Object> otherParams = new HashMap<String,Object>(8);
                otherParams.put("isSimp",cacheBuildHistory.getIsSimp());
                if(cacheBuildHistory.getSimplifyingRate() != null) {
                    otherParams.put("simplifyingRate",cacheBuildHistory.getSimplifyingRate() / 100.0);
                }
                otherParams.put("color",cacheBuildHistory.getColor());

                if (SourceTypeEnum.FILE_WORKSPACE_FOR_STORAGE_TILE.value().equals(sourceType)) {
                    JSONObject jsonObject = JSON.parseObject(sourceInfo);
                    String workspaceId = jsonObject.getString("workspaceId");
                    DataWorkspace dataWorkspace = dataWorkspaceService.getById(workspaceId);
                    if (dataWorkspace == null) throw  new GafException("未找到工作空间信息");
                    WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
                    otherParams.put("workspaceConnectionInfo",workspaceConnectionInfo);
                    //scenesWorkspace = new Workspace();
                    //boolean open = scenesWorkspace.open(workspaceConnectionInfo);
                    //otherParams.put("scenesWorkspace",scenesWorkspace);
                }

                String configFile = "data/cache_to_mongodb";
                if (SourceTypeEnum.FILE_WORKSPACE_FOR_STORAGE_TILE.value().equals(sourceType)) {
                    configFile = "data/cache_to_file";
                }

                String tenantId = SecurityUtilsExt.getTenantId();
                Path serverPath = Paths.get(storageClient.getVolumePath(configFile, tenantId, false).getPath()); // Paths.get("F:/tmpfortest/" + configFile);
                String path = serverPath.toString();
                logger.info("处理后地址:" + path);

                String storageName = cacheBuildHistory.getStorageName();
                boolean isSuccessed = datasetTypeCacheService.executeS3MBuilder(dataset, path, storageName, otherParams,percent -> stringRedisTemplate.opsForValue().set("cache_build_step_percent:"+id,percent.toString()));
                if (!isSuccessed) throw new GafException("切片缓存失败");

                //第二部,判断是否将缓存存入mongo
                if (SourceTypeEnum.MONGODB.value().equals(sourceType)) {
                    writeToMongoDB(datasetType, sourceInfo, configFile, tenantId, path, storageName);
                }
                return cacheBuildResult;
            } catch (Exception e)  {
                logger.error("切片失败,原因:" + e.getMessage(),e);
                cacheBuildResult.setSuccess(false);
                cacheBuildResult.setErrorMessage(e.getMessage());
                return cacheBuildResult;
            } finally {
                if (datasource != null) {
                    datasource.close();
                }

                if (scenesWorkspace != null) {
                    scenesWorkspace.close();
                    scenesWorkspace.dispose();
                }
                if (workspace != null) {
                    workspace.close();
                    workspace.dispose();
                }

            }

        } finally {
            RequestContextHolder.resetRequestAttributes();
        }

    }


    private List<CacheBuildHistory> recordCacheBuildHistory(List<DatasetCacheBuildSetting> datasetCacheBuildSettings, String publishHistoryId) {
        List<CacheBuildHistory> cacheBuildHistories = new ArrayList<>();
        for (DatasetCacheBuildSetting datasetCacheBuildSetting : datasetCacheBuildSettings) {
            CacheBuildHistory cacheBuildHistory = new CacheBuildHistory();
            cacheBuildHistory.setCacheType(datasetCacheBuildSetting.getCacheType());
            cacheBuildHistory.setTileType(datasetCacheBuildSetting.getTileType());
            cacheBuildHistory.setQueryParameter(datasetCacheBuildSetting.getQueryParameter());
            cacheBuildHistory.setDatasetName(datasetCacheBuildSetting.getDatasetName());
            cacheBuildHistory.setDatasourceId(datasetCacheBuildSetting.getDatasourceId());
            cacheBuildHistory.setColor(datasetCacheBuildSetting.getColor());
            cacheBuildHistory.setIsSimp(datasetCacheBuildSetting.getIsSimp());
            cacheBuildHistory.setSimplifyingRate(datasetCacheBuildSetting.getSimplifyingRate());
            cacheBuildHistory.setStatus(CacheBuildStatusEnum.NEW.getCode());
            cacheBuildHistory.setPublishHistoryId(publishHistoryId);
            String s = RandomStringUtils.random(4, true, true);
            if (CacheTypeEnum.INCREMENT.getCode().equals(datasetCacheBuildSetting.getCacheType())) {
                cacheBuildHistory.setStorageName(datasetCacheBuildSetting.getDatasetName() + "_i_" + s);
            } else {
                cacheBuildHistory.setStorageName(datasetCacheBuildSetting.getDatasetName() + "_" + s);
            }
            cacheBuildHistoryService.insert(cacheBuildHistory);
            cacheBuildHistories.add(cacheBuildHistory);
        }

        return cacheBuildHistories;
    }


    private void  setError(String cacheBuildHistoryId,String errorMessage) {
        CacheBuildHistory forUpdate = new CacheBuildHistory();
        forUpdate.setStatus(CacheBuildStatusEnum.FAILED.getCode());
        forUpdate.setErrorMessage(errorMessage);
        cacheBuildHistoryService.update(cacheBuildHistoryId,forUpdate);
    }

    private void  setStatus(String cacheBuildHistoryId,CacheBuildStatusEnum status) {
        CacheBuildHistory forUpdate = new CacheBuildHistory();
        forUpdate.setStatus(status.getCode());
        cacheBuildHistoryService.update(cacheBuildHistoryId,forUpdate);
    }


    private String createNewFileWorkspace(String workspaceName) {
        String path = filePathPrefix + File.separator+ workspaceName + ".smwu";
        Path serverPath = Paths.get(storageClient.getVolumePath(path, SecurityUtilsExt.getTenantId(), false).getPath()); // Paths.get("F:/tmpfortest/"+path);
        File descFile = serverPath.toFile();
        File availableFile = getAvailableFile(descFile, workspaceName, ".smwu");
        Workspace workspace = new Workspace();
        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setServer(availableFile.toPath().toString());
        connectionInfo.setType(WorkspaceType.SMWU);
        connectionInfo.setName(workspaceName);
        workspace.create(connectionInfo);
        workspace.close();
        workspace.dispose();
        return filePathPrefix + File.separator + availableFile.getName();
    }

    private File getAvailableFile(File file,String fileName,String suffix) {
        File tmp = file;
        int i = 1;
        //若文件存在重命名
        while(tmp.exists()) {
            String newFilename = fileName+"("+i+")" + suffix;
            String parentPath = tmp.getParent();
            tmp = new File(parentPath+ File.separator+newFilename);
            i++;
            if (i > 100000000) {
                throw new GafException("找不到合适的文件名");
            }
        }
        return tmp;
    }

    /**
     * 增量切片
     * @param dataset
     * @param attributeFilter where后面的sql语句
     * @param cursorType 游标类型常量
     * @return
     */
    private Dataset increment(Dataset dataset, String attributeFilter, CursorType cursorType) {
        PrjCoordSys prjCoordSys = dataset.getPrjCoordSys();
        // 设置矢量数据集的信息
        DatasetVectorInfo datasetVectorInfo = new DatasetVectorInfo();
        datasetVectorInfo.setType(DatasetType.MODEL);
        datasetVectorInfo.setEncodeType(EncodeType.NONE);
        datasetVectorInfo.setFileCache(true);
        String format = String.format("%s_%s_%s", "increment", dataset.getName(), "simplify");
        Datasets datasets = dataset.getDatasource().getDatasets();
        String availableDatasetName = datasets.getAvailableDatasetName(format);
        datasetVectorInfo.setName(availableDatasetName);
        DatasetVector datasetsimplify = datasets.create(datasetVectorInfo, prjCoordSys);
        Recordset sourceRecordset=((DatasetVector)dataset).query(attributeFilter,cursorType);
        Recordset recordset1 =datasetsimplify.getRecordset(false,CursorType.DYNAMIC);
        Recordset.BatchEditor editor = recordset1.getBatch();
        editor.begin();
        //5、批量将项目数据追加到目标数据集
        sourceRecordset.moveFirst();
        while (!sourceRecordset.isEOF()) {
            Geometry sourceGeometry = sourceRecordset.getGeometry();
            recordset1.addNew(sourceGeometry);
            sourceGeometry.dispose();
            sourceRecordset.moveNext();
        }
        editor.update();
        recordset1.close();
        recordset1.dispose();
        sourceRecordset.close();
        sourceRecordset.dispose();
        return  datasetsimplify;
    }

    private void writeToMongoDB(DatasetType datasetType, String sourceInfo, String configFile, String tenantId, String path, String storageName) {
        ServerInfo serverInfo = JSON.parseObject(sourceInfo, ServerInfo.class);
        String host = serverInfo.getHost();
        Integer port = serverInfo.getPort();
        String database = serverInfo.getDatabase();
        String username = serverInfo.getUsername();
        String password = serverInfo.getPassword();
        new MongoUtils().checkMongoConn(host,port,database,username,password);
        TileStorageConnection ts = new TileStorageConnection();
        ts.setStorageType(TileStorageType.MONGO);
        ts.setServer(host+":"+port);
        ts.setUser(username);
        ts.setPassword(password);
        ts.setDatabase(database);
        ts.setName(storageName);
        String realConfigFile = new StringBuilder().append(path).append(File.separator).append(storageName).toString();
        logger.info("打印mongo读取文件路径"+ realConfigFile);
        boolean b = writeCacheToMongo(datasetType,realConfigFile, ts, storageName);
        if (!b) {
            throw new GafException("缓存存入Mongo失败");
        }
        ts.dispose();
        logger.info("切片缓存成功，缓存存入Mongo成功,准备记录历史");
        String removePath = configFile + "/"+ storageName +"/";
        Integer count = storageClient.delete(removePath, tenantId);
        logger.info("删除缓存文件地址" +removePath+"，删除文件总数："+ count);
    }

    private boolean writeCacheToMongo(DatasetType type, String configFile,TileStorageConnection ts, String datasetName) {
        DatasetTypeCacheService datasetTypeCacheService = map.get(type);
        if (datasetTypeCacheService == null) {
            throw new GafException("暂未支持该数据集类型");
        }
        return datasetTypeCacheService.writeToMongo(configFile, ts, datasetName);
    }

    private WorkspaceConnectionInfo getWorkspaceConnectionInfo(DataWorkspace workspace) {
        WorkspaceConnectionInfo workspaceConnectionInfo = new WorkspaceConnectionInfo();
        workspaceConnectionInfo.setName(workspace.getWsName());
        WorkspaceType type = (WorkspaceType) WorkspaceType.parse(WorkspaceType.class, workspace.getTypeCode());
        workspaceConnectionInfo.setType(type);
        workspaceConnectionInfo.setUser(workspace.getUserName());
        workspaceConnectionInfo.setPassword(workspace.getPassword());
        if (WorkspaceType.SXWU.equals(type) || WorkspaceType.SMWU.equals(type)) {
            Path serverPath = Paths.get(storageClient.getVolumePath(workspace.getServer(), SecurityUtilsExt.getTenantId(), false).getPath()); // Paths.get("F:/tmpfortest/" + workspace.getServer());
            workspaceConnectionInfo.setServer(serverPath.toString());
        } else {
            workspaceConnectionInfo.setServer(workspace.getServer());
            workspaceConnectionInfo.setDatabase(workspace.getDatabase());
            workspaceConnectionInfo.setVersion(WorkspaceVersion.UGC70);
            if (WorkspaceType.SQL.equals(type)) {
                workspaceConnectionInfo.setDriver("SQL Server");
            }
        }
        return workspaceConnectionInfo;
    }

}
