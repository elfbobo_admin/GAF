package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 发布历史
 * 当未进行服务发布时，可以充当连接信息类
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("发布历史")
public class PublishHistory {
    @ApiModelProperty("发布历史id。主键，uuid")
    private String id;

    // 服务的来源信息 ------------------------------------------
    /**
     * @see com.supermap.gaf.data.mgt.enums.SourceTypeEnum
     */
    @ApiModelProperty("来源类型，包括FILE_WORKSPACE_FOR_STORAGE_TILE(用于存储瓦片地址的文件型工作空间)、MONGODB(MongoDB)、MANAGED_WORKSPACE(管理的工作空间)、MANAGED_TILE(管理的瓦片,包括ugcv5 和 mongodb瓦片)")
    private String sourceType; // 原  type 类型（工作空间或者mongo连接信息）

    // 可为null
    @ApiModelProperty("来源名称")
    private String sourceName; //原 name

    /**
     * 服务来源信息，根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制
     */
    @ApiModelProperty("来源信息.json格式,根据来源类型 约定有不同的格式和内容")
    private String sourceInfo; // 原 工作空间或瓦片连接信息 serverInfo

    // ------------------------------------------
    // 发布服务设置信息，包括 发布服务的类型 ...
    /**
     * 根据来源类型和具体的数据集类型 来判断 可选择的服务类型
     * @see com.supermap.services.rest.management.ServiceType
     */
    @ApiModelProperty(value = "发布服务类型.多个使用,分割",allowableValues = "RESTDATA,RESTMAP,RESTREALSPACE,RESTSPATIALANALYST,ARCGISMAP,WMS,WMTS,WPS,WCS,WPS,GRID_DEM,TIN_DEM,MAPWORLD,YINGXIANG,BAIDUREST,GOOGLEREST,OTHER")
    private String serviceType;

    /**
     *  json格式不同的来源类型 可以有额外的特有的发布设置 根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制
     *  例如,由数据集切片得到缓存存储在的文件型工作空间或者是mongodb 来源 是否发布缓存切片关联的数据服务  切片完成是否立即发布服务
     */
    @ApiModelProperty("发布设置. json格式,不同的来源类型 可以有额外的特有的发布设置 根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制")
    private String additionalSetting;

    @ApiModelProperty("状态. 0 新建  4 已发布  5 发布失败")
    private Integer state;

    @ApiModelProperty("发布结果.若state是发布成功,则发布结果为 json格式 例如[\"serviceType\": \"RESTDATA\",\"serviceAddress\": \"http://xxx.xxx.xxx/xxx/xx\"].若失败则为失败消息")
    private String result;

    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;

}
