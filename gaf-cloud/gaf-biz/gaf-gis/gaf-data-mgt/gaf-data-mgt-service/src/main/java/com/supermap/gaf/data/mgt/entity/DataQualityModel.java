package com.supermap.gaf.data.mgt.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据质量管理-模型表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-模型表")
public class DataQualityModel implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String dataQualityModelId;
    @NotNull
    @ApiModelProperty("数据质量管理模型名称")
    private String dataQualityModelName;
    @NotNull
    @ApiModelProperty("数据源id")
    private String datasourceId;

    @ApiModelProperty("数据源类型")
    private String datasourceType;
    /**
    * 默认值1：1
    */
    @ApiModelProperty("排序")
    private Integer sortSn;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}