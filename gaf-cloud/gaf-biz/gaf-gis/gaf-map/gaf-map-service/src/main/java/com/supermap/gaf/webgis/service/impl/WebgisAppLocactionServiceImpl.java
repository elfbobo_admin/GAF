//package com.supermap.gaf.webgis.service.impl;
//
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.security.SecurityUtilsExt;
//import com.supermap.gaf.webgis.dao.WebgisAppLocactionMapper;
//import com.supermap.gaf.webgis.entity.WebgisAppLocaction;
//import com.supermap.gaf.webgis.service.WebgisAppLocactionService;
//import com.supermap.gaf.webgis.service.WebgisAppService;
//import com.supermap.gaf.webgis.vo.WebgisAppLocactionSelectVo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.UUID;
//
///**
// * 初始定位服务实现类
// * @author zhurongcheng
// * @date yyyy-mm-dd
// */
//@Service
//public class WebgisAppLocactionServiceImpl implements WebgisAppLocactionService{
//    @Autowired
//    private WebgisAppLocactionMapper webgisAppLocactionMapper;
//
//    @Autowired
//    private WebgisAppService webgisAppService;
//
//	@Override
//    public WebgisAppLocaction getById(String gisAppLocationId){
//        if(gisAppLocationId == null){
//            throw new IllegalArgumentException("gisAppLocationId不能为空");
//        }
//        return  webgisAppLocactionMapper.select(gisAppLocationId);
//    }
//
//	@Override
//    public Page<WebgisAppLocaction> listByPageCondition(WebgisAppLocactionSelectVo webgisAppLocactionSelectVo, int pageNum, int pageSize) {
//        PageInfo<WebgisAppLocaction> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            webgisAppLocactionMapper.selectList(webgisAppLocactionSelectVo);
//        });
//        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
//    }
//
//	@Override
//    public WebgisAppLocaction insertWebgisAppLocaction(WebgisAppLocaction webgisAppLocaction){
//        //TODO: 主键非GeneratedKey，此处添加自定义主键生成策略
//		webgisAppLocaction.setGisAppLocationId(UUID.randomUUID().toString());
//
//        String userName = SecurityUtilsExt.getUserName();
//        webgisAppLocaction.setCreatedBy(userName);
//		webgisAppLocaction.setUpdatedBy(userName);
//        webgisAppLocactionMapper.insert(webgisAppLocaction);
//        return webgisAppLocaction;
//    }
//
//	@Override
//    public void batchInsert(List<WebgisAppLocaction> webgisAppLocactions){
//		if (webgisAppLocactions != null && webgisAppLocactions.size() > 0) {
//            String userName = SecurityUtilsExt.getUserName();
//            webgisAppLocactions.forEach(webgisAppLocaction -> {
//                webgisAppLocaction.setGisAppLocationId(UUID.randomUUID().toString());
//                webgisAppLocaction.setCreatedBy(userName);
//				webgisAppLocaction.setUpdatedBy(userName);
//            });
//            webgisAppLocactionMapper.batchInsert(webgisAppLocactions);
//        }
//
//    }
//
//	@Override
//    public void deleteWebgisAppLocaction(String gisAppLocationId){
//        webgisAppLocactionMapper.delete(gisAppLocationId);
//    }
//
//	@Override
//    public void batchDelete(List<String> gisAppLocationIds){
//        webgisAppLocactionMapper.batchDelete(gisAppLocationIds);
//    }
//
//	@Override
//    public WebgisAppLocaction updateWebgisAppLocaction(WebgisAppLocaction webgisAppLocaction){
//		webgisAppLocaction.setUpdatedBy(SecurityUtilsExt.getUserName());
//		webgisAppLocactionMapper.update(webgisAppLocaction);
//        return webgisAppLocaction;
//    }
//
//    @Override
//    @Transactional
//    public void updateByWebgisAppId(String gisAppId, WebgisAppLocaction appLocation) {
//        webgisAppService.getAndCheckById(gisAppId);
//        webgisAppLocactionMapper.deleteByAppId(gisAppId);
//        appLocation.setGisAppId(gisAppId);
//        insertWebgisAppLocaction(appLocation);
//    }
//
//    @Override
//    public WebgisAppLocaction selectByGisAppId(String gisAppId) {
//        webgisAppService.getAndCheckById(gisAppId);
//        return webgisAppLocactionMapper.selectBygisAppId(gisAppId);
//    }
//
//}
