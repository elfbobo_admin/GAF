package com.supermap.gaf.webgis.entity;

import com.supermap.gaf.annotation.ConfigName;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 地图工具条模板
 * @author zrc
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("地图工具条模板")
public class WebgisAppToolbar implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("工具条id。主键,uuid")
    @ConfigName("id")
    private String appToolbarId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("名称。")
    private String name;
    @ApiModelProperty("位置。1：顶平，2：左纵上，3：左纵上,4：底平，5：右纵上，6.右纵下（字典表？）")
    @StringRange({"1","2","3","4","5","6"})
    @ConfigName("position")
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    private String toolbarLocation;
    @ApiModelProperty("描述。")
    private String description;
    @ApiModelProperty("创建时间。生成时间不可变更")
    private Date createdTime;
    @ApiModelProperty("创建人。创建人user_id")
    private String createdBy;
    @ApiModelProperty("修改时间。修改时更新")
    private Date updatedTime;
    @ApiModelProperty("修改人。修改人user_id")
    private String updatedBy;
    @ApiModelProperty("类别。1:基础类,2:业务类(工具条只能选用同种类型按钮)")
    @StringRange({"1","2"})
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    private String type;
    @ApiModelProperty("地图应用id。主键,uuid")
    private String gisAppId;
}