package com.supermap.gaf.webgis.entity;

import com.supermap.gaf.annotation.ConfigName;
import com.supermap.gaf.validator.MustBeJson;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * 地图工具条模板按钮
 * @author zrc
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("地图工具条模板按钮")
public class WebgisToolbarButton implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("工具条按钮id。主键,uuid")
    @ConfigName("id")
    private String toolbarButtonId;
    @ApiModelProperty("工具条。主键,uuid")
    private String appToolbarId;
    @ApiModelProperty("按钮id，或者目录名称")
    private String buttonId;
    @ApiModelProperty("图标。若不为空则覆盖按钮的icon属性")
    @ConfigName
    private String icon;
    @ApiModelProperty("参数。params: [ 参数1 ,  参数2 ], 为方法时可以传递的参数")
    @ConfigName(toJson = true)
    @MustBeJson
    private String params;
    @ApiModelProperty("前置触发方法。preActions: [ func1 ,  func2 ],当加载组件或者执行方法前要执行的其他工具类方法")
    @ConfigName(value="actions",toJson = true)
    @MustBeJson
    private String preActions;
    @ApiModelProperty("二次单击关闭。toggle: true false 启用则单次激活功能并高亮图标，二次单击关闭功能并取消高亮")
    @ConfigName
    private Boolean toggle;
    @ApiModelProperty("关闭其他面板。closePanel: true false 打开时是否关闭其它打开的面板")
    @ConfigName("closePanel")
    private Boolean closeOtherPanel;
    @ApiModelProperty("更多属性。自定义属性，json")
    @ConfigName(value = "moreProperties", expand = true)
    @MustBeJson
    private String moreProperties;
    @ApiModelProperty("排序序号。")
    private Integer sortSn;
    @ApiModelProperty("描述。")
    private String description;
    @ApiModelProperty("创建时间。生成时间不可变更")
    private Date createdTime;
    @ApiModelProperty("创建人。创建人user_id")
    private String createdBy;
    @ApiModelProperty("修改时间。修改时更新")
    private Date updatedTime;
    @ApiModelProperty("修改人。修改人user_id")
    private String updatedBy;
    @ApiModelProperty("父id")
    private String pid;
    @ApiModelProperty("是否目录。")
    private Boolean isCatalog = false;
}