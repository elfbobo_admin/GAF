//package com.supermap.gaf.webgis.dao;
//import com.supermap.gaf.webgis.entity.WebgisAppThemeService;
//import com.supermap.gaf.webgis.vo.WebgisAppThemeServiceSelectVo;
//import java.util.*;
//import org.apache.ibatis.annotations.*;
//import org.springframework.stereotype.Component;
//
///**
// * 地图应用专题分析服务配置数据访问类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisAppThemeServiceMapper{
//	/**
//     * 根据主键 appThemeServiceId 查询
//     *
//	 */
//    WebgisAppThemeService select(@Param("appThemeServiceId") String appThemeServiceId);
//
//	/**
//     * 多条件查询
//     * @param webgisAppThemeServiceSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisAppThemeService> selectList(WebgisAppThemeServiceSelectVo webgisAppThemeServiceSelectVo);
//
//    /**
//     * 新增
//     *
//	 */
//    void insert(WebgisAppThemeService webgisAppThemeService);
//
//	/**
//     * 批量插入
//     *
//	 */
//	int batchInsert(@Param("list") Collection<WebgisAppThemeService> webgisAppThemeServices);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(@Param("list") Collection<String> appThemeServiceIds);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("appThemeServiceId") String appThemeServiceId);
//
//    /**
//    * 更新
//    *
//    **/
//    int update(WebgisAppThemeService webgisAppThemeService);
//	/**
//	 * 选择性更新
//	 *
//	 **/
//	int updateSelective(WebgisAppThemeService webgisAppThemeService);
//
//	int deleteByGisAppId(String gisAppId);
//}
