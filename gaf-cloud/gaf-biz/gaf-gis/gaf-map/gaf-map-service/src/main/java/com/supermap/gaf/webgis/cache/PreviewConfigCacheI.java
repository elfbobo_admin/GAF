//package com.supermap.gaf.webgis.cache;
//
//import com.supermap.gaf.webgis.domain.WebgisConfigData;
//
///**
// * @author heykb
// */
//public interface PreviewConfigCacheI {
//    String generateKey(String appAliasEn);
//    void add(String previewCode, WebgisConfigData appConfigData);
//    WebgisConfigData get(String previewCode);
//}
