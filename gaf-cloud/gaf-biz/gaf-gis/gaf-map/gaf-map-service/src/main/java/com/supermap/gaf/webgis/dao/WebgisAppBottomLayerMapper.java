//package com.supermap.gaf.webgis.dao;
//import com.supermap.gaf.webgis.entity.WebgisAppBottomLayer;
//import com.supermap.gaf.webgis.vo.WebgisAppBottomLayerSelectVo;
//import java.util.*;
//import org.apache.ibatis.annotations.*;
//import org.springframework.stereotype.Component;
//
///**
// * 地图应用底图数据访问类
// * @author zhurongcheng
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisAppBottomLayerMapper{
//	/**
//     * 根据主键 gisAppBottomLayerId 查询
//     *
//	 */
//    WebgisAppBottomLayer select(@Param("gisAppBottomLayerId") String gisAppBottomLayerId);
//
//	/**
//     * 单字段条件模糊查询
//     * @param webgisAppBottomLayerSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisAppBottomLayer> selectList(WebgisAppBottomLayerSelectVo webgisAppBottomLayerSelectVo);
//
//    /**
//     * 新增
//     *
//	 */
//    int insert(WebgisAppBottomLayer webgisAppBottomLayer);
//
//	/**
//     * 批量插入
//     *
//	 */
//    int batchInsert(List<WebgisAppBottomLayer> webgisAppBottomLayers);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(List<String> gisAppBottomLayerIds);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("gisAppBottomLayerId") String gisAppBottomLayerId);
//
//    /**
//    * 更新
//    *
//    **/
//    int update(WebgisAppBottomLayer webgisAppBottomLayer);
//
//	int updateSelective(WebgisAppBottomLayer webgisAppBottomLayer);
//
//
//	int updateSelectiveByGisAppId(WebgisAppBottomLayer webgisAppBottomLayer);
//
//	int realDeleteByGisAppId(@Param("gisAppId")String gisAppId);
//
//	int batchDeleteByGisAppIds(List<String> gisAppIds);
//}
