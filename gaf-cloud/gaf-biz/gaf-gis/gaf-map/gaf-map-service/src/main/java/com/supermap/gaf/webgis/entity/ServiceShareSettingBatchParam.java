package com.supermap.gaf.webgis.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceShareSettingBatchParam {
    @ApiModelProperty("批量授权对象")
    private List<ServiceShareSetting> serviceShareSettings;
    @ApiModelProperty("字段设置")
    private String fieldsSetting;
    @ApiModelProperty("空间设置")
    private String spatialSetting;
}
