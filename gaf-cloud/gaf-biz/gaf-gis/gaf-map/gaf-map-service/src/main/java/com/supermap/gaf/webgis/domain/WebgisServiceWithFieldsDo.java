package com.supermap.gaf.webgis.domain;

import com.supermap.gaf.webgis.entity.WebgisDataServiceField;
import com.supermap.gaf.webgis.entity.WebgisServiceDo;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 带数据服务配置的字段信息
 * @author wxl
 * @since 2022/1/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("GIS服务 带关联表的id")
public class WebgisServiceWithFieldsDo extends WebgisServiceDo {
    List<WebgisDataServiceField> fields;
}
