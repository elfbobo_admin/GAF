//package com.supermap.gaf.webgis.cache;
//
//import com.supermap.gaf.webgis.domain.WebgisConfigData;
//
//import java.util.LinkedHashMap;
//import java.util.UUID;
//
///**
// * @author heykb
// */
////@Service
//public class NativePreviewConfigCache implements PreviewConfigCacheI {
//    private static final long EXPIRE_TIME = 1000L*60*5;
//    private static final LinkedHashMap<String, WebgisConfigData> previewParamCache = new LinkedHashMap<>(100, 0.75f, false);
//    @Override
//    public String generateKey(String appAliasEn){
////        UserDetail shiroUser = SecurityUtilsExt.getUser();
////        String previewCode = appAliasEn+"::"+shiroUser.getAuthUser().getName()+"::"+ UUID.randomUUID().toString();
//        return UUID.randomUUID().toString().replaceAll("-","");
//    }
//    @Override
//    public void add(String previewCode, WebgisConfigData appConfigData){
//        long currentTime = System.currentTimeMillis();
//        appConfigData.setTimestamp(currentTime);
//        previewParamCache.put(previewCode,appConfigData);
//    }
//    @Override
//    public WebgisConfigData get(String previewCode){
//        long currentTime = System.currentTimeMillis();
//        WebgisConfigData appConfigData = previewParamCache.get(previewCode);
//        if(appConfigData!=null){
//            // 5分钟超时
//            if(appConfigData.getTimestamp()-currentTime>EXPIRE_TIME){
//                previewParamCache.remove(previewCode);
//                appConfigData = null;
//            }
//        }
//        return appConfigData;
//    }
//}
