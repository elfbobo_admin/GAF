package com.supermap.gaf.webgis.vo;

import com.supermap.gaf.webgis.entity.WebgisService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wxl
 * @since 2021/11/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("GIS服务VO")
public class WebgisServiceVO extends WebgisService {

    @ApiModelProperty("当前用户是否是服务拥有者")
    private boolean isOwner;

    @ApiModelProperty("服务是否共享给当前用户")
    private boolean isShareToMe;
}
