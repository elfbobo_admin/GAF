package com.supermap.gaf.webgis.dao;

import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.entity.ServiceShareSettingBatchUpdateParam;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 服务共享设置表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface ServiceShareSettingMapper{
	/**
     * 根据主键 serviceShareSettingId查询
     * 
	 */
    ServiceShareSetting select(@Param("serviceShareSettingId") String serviceShareSettingId);

	/**
	 * 根据主键 serviceShareSettingId查询
	 *
	 */
	List<ServiceShareSetting> selectByIds(List<String> serviceShareSettingIds);


	/**
     * 多条件查询
     * @param serviceShareSettingSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<ServiceShareSetting> selectList(ServiceShareSettingSelectVo serviceShareSettingSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(ServiceShareSetting serviceShareSetting);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<ServiceShareSetting> serviceShareSettings);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> serviceShareSettingIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("serviceShareSettingId") String serviceShareSettingId);
    /**
     * 更新
     *
	 */
    int update(ServiceShareSetting serviceShareSetting);

	/**
	 * 根据ids批量更新空间范围或字段
	 * @param serviceShareSettingBatchUpdateParam
	 */
	void batchUpdateByIds(ServiceShareSettingBatchUpdateParam serviceShareSettingBatchUpdateParam);

	/**
	 * 根据用户id和服务id集合查询 服务共享设置
	 * @param userId 用户id
	 * @param gisServiceIds 服务id集合
	 * @return 服务共享设置列表
	 */
    List<ServiceShareSetting> selectByUserIdAndServiceIds(@Param("userId") String userId, @Param("gisServiceIds") List<String> gisServiceIds);
}
