package com.supermap.gaf.webgis.vo;

import com.supermap.gaf.annotation.ConfigName;
import com.supermap.gaf.webgis.entity.WebgisToolbarButton;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * 地图工具条模板按钮
 * @author zrc
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("地图工具条模板按钮")
public class WebgisToolbarButtonVo extends WebgisToolbarButton implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("名称")
    @ConfigName("title")
    private String name;
    @ApiModelProperty("方法")
    @ConfigName("name")
    private String method;

    @ApiModelProperty("子按钮")
    @Valid
    @ConfigName("children")
    private List<WebgisToolbarButtonVo> children;
}