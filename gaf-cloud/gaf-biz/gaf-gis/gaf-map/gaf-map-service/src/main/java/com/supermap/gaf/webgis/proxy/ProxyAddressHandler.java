package com.supermap.gaf.webgis.proxy;


import com.supermap.gaf.webgis.config.WebgisConfig;
import org.apache.ibatis.type.StringTypeHandler;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ProxyAddressHandler extends StringTypeHandler {


    private String handle(String address){
        if(address == null){
            return address;
        }
        return ProxyHelper.getProxyAddress(WebgisConfig.getHost(),address,true);
    }

    @Override
    public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return handle(super.getNullableResult(rs, columnIndex));
    }

    @Override
    public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return handle(super.getNullableResult(rs, columnName));
    }

    @Override
    public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return handle(super.getNullableResult(cs, columnIndex));
    }
}
