package com.supermap.gaf.webgis.service.impl;

import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.oauthmock.IportalOauthMocker;
import com.supermap.gaf.platform.client.AuthUserClient;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.gaf.webgis.enums.ServiceTypeEnum;
import com.supermap.gaf.webgis.iportal.IportalClient;
import com.supermap.gaf.webgis.iportal.RegistrationServiceParam;
import com.supermap.gaf.webgis.iportal.RegistrationServiceResponse;
import com.supermap.gaf.webgis.service.IportalService;
import com.supermap.gaf.webgis.service.WebgisServiceService;
import okhttp3.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class IportalServiceImpl implements IportalService {
    @Autowired
    private WebgisServiceService webgisServiceService;
    
    @Autowired
    private IportalOauthMocker iportalOauthMocker;

    @Autowired
    private AuthUserClient authUserClient;
    
    @Autowired
    private IportalClient iportalClient;
    @Override
    public int importService(String serviceId,  String... autoInfoHeaderNameAndValues) {
        serviceId = webgisServiceService.filterByOwn(serviceId);
        if(serviceId == null){
            return 0;
        }
        WebgisService service = webgisServiceService.getById(serviceId);
        Headers iportalAuthInfoHeaders = null;
        String username = SecurityUtilsExt.getUserName();
        String nickname = authUserClient.getByUsername(username).checkAndGetData().getRealName();
        try {
            // 登录iportal
            iportalAuthInfoHeaders = iportalOauthMocker.login(Headers.of(autoInfoHeaderNameAndValues),iportalOauthMocker.new InitIportalOauthUser(username,nickname));
        } catch (Exception e) {
            throw new GafException(e.getMessage());
        }
        String[] headersArray = new String[iportalAuthInfoHeaders.size()*2];
        int i = 0;
        for(String name:iportalAuthInfoHeaders.names()){
            headersArray[i++] = name;
            headersArray[i++] = iportalAuthInfoHeaders.get(name);
        }

        // 注册服务到iportal
        ServiceTypeEnum typeEnum = ServiceTypeEnum.valueOf(service.getTypeCode());
        RegistrationServiceParam param = RegistrationServiceParam.builder().type(typeEnum.getTypeInIportal()).tags(Arrays.asList("用户服务"))
                .linkage(service.getRealAddress()).resTitle(service.getName()).build();

        RegistrationServiceResponse registrationServiceResponse = iportalClient.registrationService(param,headersArray);

        if(!registrationServiceResponse.isSucceed()){
            throw new GafException(registrationServiceResponse.getError().getCode()+":"+ registrationServiceResponse.getError().getErrorMsg());
        }
        return 1;
    }
}
