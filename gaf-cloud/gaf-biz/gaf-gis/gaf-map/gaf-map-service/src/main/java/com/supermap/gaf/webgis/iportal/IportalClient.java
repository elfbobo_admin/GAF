package com.supermap.gaf.webgis.iportal;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Slf4j
public class IportalClient {

    private static RestTemplate restTemplate;
    static {
        restTemplate = new RestTemplate();
        ResponseErrorHandler responseErrorHandler = new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                HttpStatus code = clientHttpResponse.getStatusCode();
                return code.is2xxSuccessful() || code.is4xxClientError();
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
            }
        };
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setOutputStreaming(false);
        restTemplate.setRequestFactory(requestFactory);
        restTemplate.setErrorHandler(responseErrorHandler);
    }
    public static final String SERVICE_REGISTE_ENDPOINT = "%s/web/services.json";

    private String iportalServerPath;

    public IportalClient(String iportalServerPath) {
        this.iportalServerPath = iportalServerPath;
    }


    public RegistrationServiceResponse registrationService(RegistrationServiceParam serviceParam, String...authHeaders){
        JSONObject metadata = JSONObject.parseObject("{\n" +
                "        \"dataIdInfo\": {\n" +
                "            \"dataIdent\": {\n" +
                "                \"idCitation\": {\n" +
                "                    \"resTitle\": \"{title}\"\n" +
                "                }\n" +
                "            }\n" +
                "        },\n" +
                "        \"distInfo\": {\n" +
                "            \"onLineSrc\": {\n" +
                "                \"linkage\": \"{url}\"\n" +
                "            }\n" +
                "        }\n" +
                "    }");
        metadata.getJSONObject("dataIdInfo")
                .getJSONObject("dataIdent")
                .getJSONObject("idCitation")
                .put("resTitle",serviceParam.getResTitle());
        metadata.getJSONObject("distInfo")
                .getJSONObject("onLineSrc")
                .put("linkage",serviceParam.getLinkage());
        JSONObject param = new JSONObject();
        param.put("type",serviceParam.getType());
        param.put("tags",serviceParam.getTags());
        param.put("authorizeSetting",serviceParam.getAuthorizeSetting());
        param.put("metadata",metadata);
        HttpHeaders headers = new HttpHeaders();
        if(authHeaders!=null && authHeaders.length>=2){
            for(int i = 0;i<authHeaders.length;i+=2){
                headers.add(authHeaders[i],authHeaders[i+1]);
            }
        }
        HttpEntity httpEntity = new HttpEntity<>(param,headers);
        RegistrationServiceResponse re = restTemplate.postForObject(String.format(SERVICE_REGISTE_ENDPOINT,iportalServerPath), httpEntity, RegistrationServiceResponse.class);
        return re;
    }


   /* public static BaseResponse deleteService(List<Long> ids){
        Map<String,String> param = new HashMap<>();
        param.put("ids",JSON.toJSONString(ids));
        return restTemplate.exchange("web/services.json",HttpMethod.DELETE,null,BaseResponse.class,param).getBody();
    }*/

}
