//package com.supermap.gaf.webgis.service.impl;
//
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.security.SecurityUtilsExt;
//import com.supermap.gaf.webgis.dao.WebgisAppThemeServiceMapper;
//import com.supermap.gaf.webgis.entity.WebgisAppThemeService;
//import com.supermap.gaf.webgis.service.WebgisAppService;
//import com.supermap.gaf.webgis.service.WebgisAppThemeServiceService;
//import com.supermap.gaf.webgis.theme.dao.ThemeServiceMapper;
//import com.supermap.gaf.webgis.theme.entity.ThemeService;
//import com.supermap.gaf.webgis.vo.WebgisAppThemeServiceSelectVo;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.UUID;
//import java.util.stream.Collectors;
//
///**
// * 地图应用专题分析服务配置服务实现类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Service
//public class WebgisAppThemeServiceServiceImpl implements WebgisAppThemeServiceService {
//
//	private static final Logger  log = LoggerFactory.getLogger(WebgisAppThemeServiceServiceImpl.class);
//
//	@Autowired
//    private WebgisAppThemeServiceMapper webgisAppThemeServiceMapper;
//
//	@Autowired
//	private ThemeServiceMapper themeServiceMapper;
//
//	@Autowired
//	private WebgisAppService webgisAppService;
//
//	@Override
//    public WebgisAppThemeService getById(String appThemeServiceId){
//        if(appThemeServiceId == null){
//            throw new IllegalArgumentException("appThemeServiceId不能为空");
//        }
//        return  webgisAppThemeServiceMapper.select(appThemeServiceId);
//    }
//
//	@Override
//    public Page<WebgisAppThemeService> listByPageCondition(WebgisAppThemeServiceSelectVo webgisAppThemeServiceSelectVo, int pageNum, int pageSize) {
//        PageInfo<WebgisAppThemeService> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            webgisAppThemeServiceMapper.selectList(webgisAppThemeServiceSelectVo);
//        });
//        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
//    }
//
//	@Override
//	public 	List<WebgisAppThemeService> selectList(WebgisAppThemeServiceSelectVo webgisAppThemeServiceSelectVo){
//		return webgisAppThemeServiceMapper.selectList(webgisAppThemeServiceSelectVo);
//	}
//
//	@Override
//    public void insertWebgisAppThemeService(WebgisAppThemeService webgisAppThemeService){
//        // 主键非GeneratedKey，此处添加自定义主键生成策略
//		webgisAppThemeService.setAppThemeServiceId(UUID.randomUUID().toString());
//
//		String userName = SecurityUtilsExt.getUserName();
//		webgisAppThemeService.setCreatedBy(userName);
//		webgisAppThemeService.setUpdatedBy(userName);
//        webgisAppThemeServiceMapper.insert(webgisAppThemeService);
//    }
//
//	@Override
//    public int deleteWebgisAppThemeService(String appThemeServiceId){
//		return batchDelete(Arrays.asList(appThemeServiceId));
//    }
//
//	@Override
//    public int batchDelete(List<String> appThemeServiceIds){
//		if(!CollectionUtils.isEmpty(appThemeServiceIds)){
//			return webgisAppThemeServiceMapper.batchDelete(appThemeServiceIds);
//		}
//		return 0;
//    }
//
//	@Override
//    public int updateWebgisAppThemeService(WebgisAppThemeService webgisAppThemeService){
//		webgisAppThemeService.setUpdatedBy(SecurityUtilsExt.getUserName());
//		return webgisAppThemeServiceMapper.update(webgisAppThemeService);
//    }
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public int updateByWebgisAppId(String gisAppId, List<String> themeServiceCodes) {
//		webgisAppService.getAndCheckById(gisAppId);
//		webgisAppThemeServiceMapper.deleteByGisAppId(gisAppId);
//		if(!CollectionUtils.isEmpty(themeServiceCodes)){
//			String userName = SecurityUtilsExt.getUserName();
//			List<WebgisAppThemeService> webgisAppThemeServices =themeServiceCodes.stream().map(item-> {
//				return WebgisAppThemeService.builder().gisAppId(gisAppId).themeServiceCode(item)
//						.appThemeServiceId(UUID.randomUUID().toString())
//						.createdBy(userName)
//						.updatedBy(userName).build();
//			}).collect(Collectors.toList());
//			return webgisAppThemeServiceMapper.batchInsert(webgisAppThemeServices);
//		}
//		return 0;
//	}
//
//
//	@Override
//	public List<ThemeService> selectThemeServiceByGisAppId(String gisAppId) {
//		webgisAppService.getAndCheckById(gisAppId);
//		List<ThemeService> re = new ArrayList<>();
//		List<WebgisAppThemeService> appThemeServices = selectList(WebgisAppThemeServiceSelectVo.builder().gisAppId(gisAppId).build());
//		List<String> invalidAppThemeServiceIds = new ArrayList<>();
//		for(WebgisAppThemeService item:appThemeServices){
//			// 由于themeServiceMapper实现在lmap，缺少自定义的sql和关联删除逻辑,所以将themeServiceCode的存在一致性放到了查询
//			ThemeService themeService = themeServiceMapper.selectByCode(item.getThemeServiceCode());
//			if(re==null){
//				// 删除无效themeServiceCode
//				invalidAppThemeServiceIds.add(item.getAppThemeServiceId());
//			}else{
//				re.add(themeService);
//			}
//		}
//		try{
//			if(!invalidAppThemeServiceIds.isEmpty()){
//				batchDelete(invalidAppThemeServiceIds);
//			}
//		}catch (Exception e){
//			e.printStackTrace();
//			log.error("删除WebgisAppThemeService无效themeServiceCode失败：{}",e.getMessage());
//		}
//		return re;
//	}
//}
