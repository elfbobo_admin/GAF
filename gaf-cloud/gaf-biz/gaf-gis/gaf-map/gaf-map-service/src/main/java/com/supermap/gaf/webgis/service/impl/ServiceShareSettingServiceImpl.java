package com.supermap.gaf.webgis.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Sets;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.webgis.dao.ServiceShareSettingMapper;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.entity.ServiceShareSettingBatchUpdateParam;
import com.supermap.gaf.webgis.service.ServiceShareSettingService;
import com.supermap.gaf.webgis.service.WebgisServiceService;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 服务共享设置表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class ServiceShareSettingServiceImpl implements ServiceShareSettingService{
    
	private static final Logger  log = LoggerFactory.getLogger(ServiceShareSettingServiceImpl.class);
	
	@Autowired
    private ServiceShareSettingMapper serviceShareSettingMapper;

	@Autowired
	private WebgisServiceService webgisServiceService;
	

	@Override
    public ServiceShareSetting getById(String serviceShareSettingId){
        if(serviceShareSettingId == null){
            throw new IllegalArgumentException("serviceShareSettingId不能为空");
        }
        return serviceShareSettingMapper.select(serviceShareSettingId);
    }
	
	@Override
    public Page<ServiceShareSetting> listByPageCondition(ServiceShareSettingSelectVo serviceShareSettingSelectVo, int pageNum, int pageSize) {
        PageInfo<ServiceShareSetting> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            serviceShareSettingMapper.selectList(serviceShareSettingSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Override
    public ServiceShareSetting insertServiceShareSetting(ServiceShareSetting serviceShareSetting){

        String serviceId = webgisServiceService.filterByOwn(serviceShareSetting.getGisServiceId());
        if(serviceId == null){
            throw new GafException("仅服务创建者能编辑授权");
        }
	    if(!StringUtils.isEmpty(serviceShareSetting.getSpatialSetting())){
	        try{
                JSONArray array = JSON.parseArray(serviceShareSetting.getSpatialSetting());
                serviceShareSetting.setSpatialSetting(JSON.toJSONString(array));
            }catch (Exception e){
	            throw new GafException("spatialSetting 要求json格式的geometry对象数组");
            }
        }
        ServiceShareSettingSelectVo serviceShareSettingVo = new ServiceShareSettingSelectVo();
        serviceShareSettingVo.setGisServiceId(serviceShareSetting.getGisServiceId());
        serviceShareSettingVo.setUserId(serviceShareSetting.getUserId());
        Page<ServiceShareSetting> serviceShareSettingPage = this.listByPageCondition(serviceShareSettingVo, 0, 0);
        List<ServiceShareSetting> list = serviceShareSettingPage.getPageList();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        serviceShareSetting.setServiceShareSettingId(UUID.randomUUID().toString());
        String userName = SecurityUtilsExt.getUserName();
        serviceShareSetting.setCreatedBy(userName);
        serviceShareSetting.setUpdatedBy(userName);
        serviceShareSettingMapper.insert(serviceShareSetting);
        return serviceShareSetting;
    }
	
	@Override
    public void batchInsert(List<ServiceShareSetting> serviceShareSettings){
        if (serviceShareSettings != null && serviceShareSettings.size() > 0) {
            Set<String> serviceIds = serviceShareSettings.stream().map(ServiceShareSetting::getGisServiceId).collect(Collectors.toSet());
            Set<String> authedIds = Sets.newHashSet(webgisServiceService.filterByOwn(serviceIds));
            if(CollectionUtils.isEmpty(authedIds)){
                throw new GafException("仅服务创建者能编辑授权");
            }
            serviceShareSettings = serviceShareSettings.stream().filter(item->authedIds.contains(item.getGisServiceId())).collect(Collectors.toList());
            String userName = SecurityUtilsExt.getUserName();
            serviceShareSettings.forEach(serviceShareSetting -> {
                serviceShareSetting.setCreatedBy(userName);
                serviceShareSetting.setUpdatedBy(userName);
				serviceShareSetting.setServiceShareSettingId(UUID.randomUUID().toString());
            });
            serviceShareSettingMapper.batchInsert(serviceShareSettings);
        }
        
    }

	@Override
    public void deleteServiceShareSetting(String serviceShareSettingId){
        ServiceShareSetting serviceShareSetting = getById(serviceShareSettingId);
        if(serviceShareSetting == null){
            return;
        }
        String serviceId = webgisServiceService.filterByOwn(serviceShareSetting.getGisServiceId());
        if(serviceId == null){
            throw new GafException("仅服务创建者能编辑授权");
        }
        serviceShareSettingMapper.delete(serviceShareSettingId);
    }

	@Override
    public void batchDelete(List<String> serviceShareSettingIds){
	    if(!CollectionUtils.isEmpty(serviceShareSettingIds)){
            List<ServiceShareSetting> serviceShareSettings = serviceShareSettingMapper.selectByIds(serviceShareSettingIds);
            if(CollectionUtils.isEmpty(serviceShareSettings)){
                return ;
            }
            Set<String> serviceIds = serviceShareSettings.stream().map(ServiceShareSetting::getGisServiceId).collect(Collectors.toSet());
            Set<String> authedIds = Sets.newHashSet(webgisServiceService.filterByOwn(serviceIds));
            if(CollectionUtils.isEmpty(authedIds)){
                throw new GafException("仅服务创建者能编辑授权");
            }
            serviceShareSettingMapper.batchDelete(serviceShareSettingIds);
        }
    }
	

	@Override
    public ServiceShareSetting updateServiceShareSetting(ServiceShareSetting serviceShareSetting){
        String serviceId = webgisServiceService.filterByOwn(serviceShareSetting.getGisServiceId());
        if(serviceId == null){
            throw new GafException("仅服务创建者能编辑授权");
        }
	    serviceShareSetting.setUpdatedBy(SecurityUtilsExt.getUserName());
        serviceShareSettingMapper.update(serviceShareSetting);
        return serviceShareSetting;
    }

    @Override
    public void batchUpdateByIds(ServiceShareSettingBatchUpdateParam serviceShareSettingBatchUpdateParam) {
        serviceShareSettingMapper.batchUpdateByIds(serviceShareSettingBatchUpdateParam);
    }

    @Override
    public List<ServiceShareSetting> listByUserIdAndServiceIds(String userId, List<String> gisServiceIds) {
	    return serviceShareSettingMapper.selectByUserIdAndServiceIds(userId,gisServiceIds);
    }

}
