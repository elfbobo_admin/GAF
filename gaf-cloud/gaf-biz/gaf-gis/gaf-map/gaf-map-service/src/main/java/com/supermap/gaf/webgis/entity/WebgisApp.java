//package com.supermap.gaf.webgis.entity;
//
//import com.supermap.gaf.validGroup.AddGroup;
//import com.supermap.gaf.validGroup.UpdateGroup;
//import com.supermap.gaf.validator.StringRange;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.validation.constraints.NotNull;
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * 地图应用
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@ApiModel("地图应用")
//public class WebgisApp implements Serializable{
//    private static final long serialVersionUID = -1L;
//    @ApiModelProperty("地图应用id。主键,uuid")
//    private String gisAppId;
//    @NotNull(groups={AddGroup.class,UpdateGroup.class})
//    @ApiModelProperty("名称。中文名称")
//    private String name;
//    @NotNull(groups={AddGroup.class,UpdateGroup.class})
//    @ApiModelProperty("应用英文别称。全局唯一，英文或数字，发布路径中使用")
//    private String appAliasEn;
//    @ApiModelProperty("应用模板。应用基于该模板生成（与类别对应）")
//    private String templateGisAppId;
//    @ApiModelProperty("底图类型。0:天地图，1:自定义")
//    @StringRange(fromEnum = AppBottomServiceType.class,enumGetter = "code")
//    private String bottomServiceType;
//    @ApiModelProperty("资源目录id。")
//    private String layerRootCatalogId;
//    @ApiModelProperty("代码片断。json:{ htmlContent : html script ， styleContent : css script ， jsContent : js script }")
//    private String codeSnippet;
//    @ApiModelProperty("描述。")
//    private String description;
//    @ApiModelProperty("创建时间。生成时间不可变更")
//    private Date createdTime;
//    @ApiModelProperty("创建人。创建人user_id")
//    private String createdBy;
//    @ApiModelProperty("修改时间。修改时更新")
//    private Date updatedTime;
//    @ApiModelProperty("修改人。修改人user_id")
//    private String updatedBy;
//    @ApiModelProperty("是否为模板应用。true:是，模板应用,false:否，普通应用")
//    private Boolean isTemplateApp;
//    /**
//    * 默认值1：false
//    */
//    @ApiModelProperty("发布状态。是否已发布，发布了的才可用。true:已发布，false:未发布。")
//    private Boolean published;
//    @ApiModelProperty("第三方令牌。天地图服务等访问的令牌")
//    private String token3p;
//    @ApiModelProperty("缩略图。")
//    private String thumbnailUrl;
//
//    public static enum AppBottomServiceType{
//        TIANDITU("0"),CUSTOM("1");
//        private String code;
//
//        AppBottomServiceType(String code) {
//            this.code = code;
//        }
//
//        public String getCode() {
//            return code;
//        }
//    }
//}