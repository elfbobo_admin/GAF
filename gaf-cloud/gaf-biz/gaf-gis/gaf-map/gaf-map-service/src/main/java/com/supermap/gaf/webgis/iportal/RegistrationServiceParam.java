package com.supermap.gaf.webgis.iportal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationServiceParam {
    private String type;
    @Builder.Default
    private List<String> tags = Arrays.asList("用户服务");
    private List<String> authorizeSetting;
    private String resTitle;
    private String linkage;
}
