package com.supermap.gaf.webgis.service;

public interface IportalService {
    int importService(String serviceId, String... autoInfoHeaderNameAndValues);
}
