//package com.supermap.gaf.webgis.service.impl;
//
//import com.amazonaws.services.s3.Headers;
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
//import com.supermap.gaf.sys.mgt.dao.SysCatalogQueryMapper;
//import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
//import com.supermap.gaf.webgis.dao.WebgisAppBottomLayerMapper;
//import com.supermap.gaf.webgis.dao.WebgisAppMapper;
//import com.supermap.gaf.webgis.domain.WebgisConfigData;
//import com.supermap.gaf.webgis.domain.WebgisTemplateAppParam;
//import com.supermap.gaf.webgis.entity.WebgisApp;
//import com.supermap.gaf.webgis.entity.WebgisAppBottomLayer;
//import com.supermap.gaf.webgis.service.*;
//import com.supermap.gaf.webgis.vo.WebgisAppSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisAppToolbarVo;
//import com.supermap.gaf.webgis.vo.WebgisCatalogTreeVo;
//import lombok.SneakyThrows;
//import org.apache.commons.lang3.BooleanUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//import org.springframework.util.StreamUtils;
//
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.StreamingOutput;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.util.*;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipInputStream;
//import java.util.zip.ZipOutputStream;
//
///**
// * 地图应用服务实现类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Service
//public class WebgisAppServiceImpl implements WebgisAppService {
//
//	private static final Logger  log = LoggerFactory.getLogger(WebgisAppServiceImpl.class);
//
//	@Autowired
//    private WebgisAppMapper webgisAppMapper;
//    @Autowired
//    private WebgisAppToolbarService webgisAppToolbarService;
//    @Autowired
//    private WebgisAppBottomLayerMapper webgisAppBottomLayerMapper;
//    @Autowired
//    private WebgisAppBottomLayerService webgisAppBottomLayerService;
//
//    @Autowired
//    private WebgisCatalogService webgisCatalogService;
//    @Autowired
//    private WebgisConfigService webgisConfigService;
//    @Autowired
//    private SysCatalogQueryMapper sysCatalogQueryMapper;
//
//
//    @Override
//    public WebgisApp getAndCheckByAliasEn(String aliasEn){
//        List<WebgisApp> apps =selectList(WebgisAppSelectVo.builder().appAliasEn(aliasEn).build());
//        if(CollectionUtils.isEmpty(apps)){
//            throw new GafException("指定应用不存在");
//        }
//        return apps.get(0);
//    }
//
//    @Override
//    public WebgisApp getAndCheckById(String gisAppId){
//        WebgisApp re = getById(gisAppId);
//        if(re==null){
//            throw new GafException("指定应用不存在");
//        }
//        return re;
//    }
//
//	@Override
//    public WebgisApp getById(String gisAppId){
//        if(gisAppId == null){
//            throw new IllegalArgumentException("gisAppId不能为空");
//        }
//        return webgisAppMapper.select(gisAppId);
//    }
//
//	@Override
//    public Page<WebgisApp> listByPageCondition(WebgisAppSelectVo webgisAppSelectVo, int pageNum, int pageSize) {
//        PageInfo<WebgisApp> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            webgisAppMapper.selectList(webgisAppSelectVo);
//        });
//        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
//    }
//
//	@Override
//	public 	List<WebgisApp> selectList(WebgisAppSelectVo webgisAppSelectVo){
//		return webgisAppMapper.selectList(webgisAppSelectVo);
//	}
//
//	@Override
//    @Transactional(rollbackFor = Exception.class)
//    public WebgisApp insertWebgisApp(WebgisApp webgisApp){
//        String gisAppId = UUID.randomUUID().toString();
//		webgisApp.setGisAppId(gisAppId);
//		webgisApp.setIsTemplateApp(false);
//        String templateGisAppId = webgisApp.getTemplateGisAppId();
//        webgisAppMapper.insert(webgisApp);
//        if(templateGisAppId!=null){
//            webgisConfigService.cloneAppConfig(templateGisAppId,gisAppId);
//        }
//        return getById(gisAppId);
//    }
//
//	@Override
//    public int deleteWebgisApp(String gisAppId){
//		return batchDelete(Arrays.asList(gisAppId));
//    }
//
//	@Override
//    public int batchDelete(List<String> gisAppIds){
//		if(!CollectionUtils.isEmpty(gisAppIds)){
//            webgisAppToolbarService.deleteByGisAppIds(gisAppIds);
//            webgisAppBottomLayerMapper.batchDeleteByGisAppIds(gisAppIds);
//			return webgisAppMapper.batchDelete(gisAppIds);
//		}
//		return 0;
//    }
//
//	@Override
//    public int updateWebgisApp(WebgisApp webgisApp){
//		return webgisAppMapper.update(webgisApp);
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void updateAppToolbars(String gisAppId, List<WebgisAppToolbarVo> appToolbarVos) {
//        getAndCheckById(gisAppId);
//        webgisAppToolbarService.deleteByGisAppIds(Arrays.asList(gisAppId));
//        if(appToolbarVos!=null){
//            appToolbarVos.forEach(item->{
//                item.setGisAppId(gisAppId);
//            });
//            webgisAppToolbarService.batchInsert(appToolbarVos);
//        }
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void updateAppBottomLayers(String gisAppId, List<WebgisAppBottomLayer> webgisAppBottomLayers) {
//        webgisAppMapper.updateSelective(WebgisApp.builder().gisAppId(gisAppId).bottomServiceType(WebgisApp.AppBottomServiceType.CUSTOM.getCode()).token3p("").build());
//        for(WebgisAppBottomLayer webgisAppBottomLayer:webgisAppBottomLayers){
//            webgisAppBottomLayer.setGisAppId(gisAppId);
//        }
//        webgisAppBottomLayerMapper.realDeleteByGisAppId(gisAppId);
//        webgisAppBottomLayerService.batchInsert(webgisAppBottomLayers);
//    }
//
//    @Override
//    public void reverseStatus(String gisAppId) {
//        WebgisApp webgisApp = getAndCheckById(gisAppId);
//        WebgisApp.WebgisAppBuilder builder = WebgisApp.builder().gisAppId(gisAppId);
//        if(BooleanUtils.isTrue(webgisApp.getPublished())) {
//            builder.published(false);
//        }else{
//            builder.published(true);
//        }
//        webgisAppMapper.updateSelective(builder.build());
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void updateResourceCatalogTree(String gisAppId, WebgisCatalogTreeVo treeVo) {
//        WebgisApp webgisApp = getAndCheckById(gisAppId);
//        if(webgisApp.getLayerRootCatalogId()==null || CollectionUtils.isEmpty(sysCatalogQueryMapper.selectByCombination(SysCatalog.builder().catalogId(webgisApp.getLayerRootCatalogId()).build()))){
//            String rootId = webgisCatalogService.createTree(treeVo,false);
//            webgisAppMapper.updateSelective(WebgisApp.builder().gisAppId(gisAppId).layerRootCatalogId(rootId).build());
//        }else{
//            webgisCatalogService.updateTree(webgisApp.getLayerRootCatalogId(),treeVo,false);
//        }
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void insertWebgisAppTp(WebgisTemplateAppParam templateAppParam) {
//        WebgisApp webgisApp = getAndCheckById(templateAppParam.getSourceGisAppId());
//        webgisApp.setIsTemplateApp(true);
//        webgisApp.setAppAliasEn(templateAppParam.getAppAliasEn());
//        webgisApp.setName(templateAppParam.getName());
//        String gisAppId = UUID.randomUUID().toString();
//        webgisApp.setGisAppId(gisAppId);
//        webgisAppMapper.insert(webgisApp);
//        if(templateAppParam.getSourceGisAppId()!=null){
//            webgisConfigService.cloneAppConfig(templateAppParam.getSourceGisAppId(),gisAppId);
//        }
//    }
//
//    @Override
//    public int updateSelective(WebgisApp webgisApp) {
//        return webgisAppMapper.updateSelective(webgisApp);
//    }
//    @Override
//    @SneakyThrows
//    public Map<String,String> getCode(String gisAppId) {
//        Map<String,String> re = new HashMap<>();
//        WebgisApp webgisApp = getAndCheckById(gisAppId);
//        WebgisConfigData configData = webgisConfigService.convert2AppConfig(webgisApp, null, true);
//        final String json = GlobalJacksonObjectMapper.instance().writerWithDefaultPrettyPrinter().writeValueAsString(configData);
//        re.put("config",json);
//        try (ZipInputStream zis = new ZipInputStream(WebgisAppServiceImpl.class.getResourceAsStream("/webgis/webgisVue.zip"))) {
//            ZipEntry zipEntry = zis.getNextEntry();
//            while (zipEntry != null){
//                if(zipEntry.getName().endsWith("App.vue")){
//                    String code = StreamUtils.copyToString(zis,StandardCharsets.UTF_8);
//                    re.put("code",code);
//                    zis.closeEntry();
//                    break;
//                }
//                zis.closeEntry();
//                zipEntry = zis.getNextEntry();
//            }
//        } catch (IOException e) {
//            throw new GafException(e);
//        }
//        return re;
//    }
//
//    @SneakyThrows
//    @Override
//    public Response project(String gisAppId) {
//        WebgisApp webgisApp = getAndCheckById(gisAppId);
//        WebgisConfigData configData = webgisConfigService.convert2AppConfig(webgisApp,null,true);
//        final String json = GlobalJacksonObjectMapper.instance().writerWithDefaultPrettyPrinter().writeValueAsString(configData);
//        StreamingOutput streamingOutput = outputStream -> {
//            try(ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
//                ZipInputStream zis = new ZipInputStream(WebgisAppServiceImpl.class.getResourceAsStream("/webgis/webgisVue.zip"))){
//                ZipEntry zipEntry = zis.getNextEntry();
//                while (zipEntry != null) {
//                    zipOutputStream.putNextEntry(new ZipEntry(zipEntry.getName()));
//                    if (!zipEntry.isDirectory()) {
//                        if ("src/json/webgisConfig.json".equals(zipEntry.getName())) {
//                            Map<String, String> s = new HashMap<>();
//                            s.put("test", "test");
//                            OutputStreamWriter writer = new OutputStreamWriter(zipOutputStream, StandardCharsets.UTF_8);
//                            writer.write(json);
//                            writer.flush();
//                        } else {
//                            StreamUtils.copy(zis, zipOutputStream);
//                        }
//                    }
//                    zipOutputStream.closeEntry();
//                    zis.closeEntry();
//                    zipEntry = zis.getNextEntry();
//                }
//                zis.closeEntry();
//            }
//        };
//        String filenameEncoded = URLEncoder.encode(webgisApp.getAppAliasEn()+".zip","utf-8");
//        return Response.ok(streamingOutput)
//                .header("Content-Disposition",String.format("attachment;filename=\"%s\";filename*=utf-8 %s",filenameEncoded,filenameEncoded))
//                .header(Headers.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM)
//                .build();
//    }
//}
