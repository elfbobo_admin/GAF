//package com.supermap.gaf.webgis.dao;
//
//import com.supermap.gaf.webgis.entity.WebgisApp;
//import com.supermap.gaf.webgis.vo.WebgisAppSelectVo;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//import java.util.List;
//
///**
// * 地图应用数据访问类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisAppMapper {
//	/**
//     * 根据主键 gisAppId 查询
//     *
//	 */
//    WebgisApp select(@Param("gisAppId") String gisAppId);
//
//	/**
//     * 多条件查询
//     * @param webgisAppSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisApp> selectList(WebgisAppSelectVo webgisAppSelectVo);
//
//    /**
//     * 新增
//     *
//	 */
//    void insert(WebgisApp webgisApp);
//
//	/**
//     * 批量插入
//     *
//	 */
//	void batchInsert(@Param("list") Collection<WebgisApp> webgisApps);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(@Param("list") Collection<String> gisAppIds);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("gisAppId") String gisAppId);
//
//    /**
//    * 更新
//    *
//    **/
//    int update(WebgisApp webgisApp);
//	/**
//	 * 选择性更新
//	 *
//	 **/
//	int updateSelective(WebgisApp webgisApp);
//}
