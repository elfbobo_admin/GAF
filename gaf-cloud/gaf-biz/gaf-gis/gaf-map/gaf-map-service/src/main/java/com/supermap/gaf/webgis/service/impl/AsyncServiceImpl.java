/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.gaf.docker.util.DockerClientUtil;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.portal.client.PortalUserMessageClient;
import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.webgis.cache.RegistryResultCacheI;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.gaf.webgis.service.AsyncService;
import com.supermap.gaf.webgis.service.WebgisServiceService;
import com.supermap.gaf.webgis.util.WebgisCommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.supermap.gaf.webgis.service.impl.WebgisServiceServiceImpl.REGISTRY_TYPE_SERVER;

/**
 * @author heykb
 * @date:2021/3/25
 */
@Component
public class AsyncServiceImpl implements AsyncService {

    @Autowired
    private WebgisServiceService webgisServiceService;
    @Autowired
    public RegistryResultCacheI registryResultCacheI;
    @Autowired
    private PortalUserMessageClient portalUserMessageClient;


    @Async("taskExecutor")
    @Override
    public CompletableFuture<Void> batchRegistryWebgis(WebgisService webgisService, String registryType, String resultCode, @Nullable String sourceId, @Nullable Integer sourceType) {
        String url = webgisService.getAddress();
        RestTemplate restTemplate = new RestTemplate();

        List<String[]> parentService = new ArrayList<>();
        try{
            if (REGISTRY_TYPE_SERVER.equals(registryType)){
                JSONArray list = restTemplate.getForObject(url + "/services.json", JSONArray.class);
                for (int i = 0; i < list.size(); ++i) {
                    JSONObject item = list.getJSONObject(i);
                    String typeCode = WebgisCommonUtils.parseServiceType(item.getString("interfaceType"), item.getString("componentType"));
                    if (typeCode == null) {
                        continue;
                    }
                    parentService.add(new String[]{typeCode,item.getString("url")});
                }
            }else if (WebgisServiceServiceImpl.REGISTRY_TYPE_BATCH.equals(registryType)) {
                parentService.add(new String[]{webgisService.getTypeCode(),url});
            }
            for(String[] item:parentService){
                try {
                    WebgisCommonUtils.listService(item[0],item[1], service -> {
                        try {
                            service.setDescription(webgisService.getDescription());
                            service.setTimeAttribute(webgisService.getTimeAttribute());
                            service.setMoreProperties(webgisService.getMoreProperties());
                            service.setCreatedBy(webgisService.getCreatedBy());
                            service.setUpdatedBy(webgisService.getUpdatedBy());
                            webgisServiceService.registryWebgis(service,sourceId,sourceType);
                            registryResultCacheI.success(resultCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (e instanceof GafException && ((GafException) e).getCode() == GafException.CONFLICT) {
                                registryResultCacheI.exist(resultCode);
                            } else {
                                registryResultCacheI.fail(resultCode);
                            }
                        }
                        return null;
                    });
                } catch (Exception e) {
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            registryResultCacheI.error(resultCode,e.getMessage());
        }
        return CompletableFuture.completedFuture(null);
    }

    @Async
    @Override
    public void buildAndPush(String dockerFilePath, String tagFullName, String registryAddress, String registryUsername, String registryPassword, String username) {
        DockerClientUtil dockerClientUtil = new DockerClientUtil();
        boolean tag = dockerClientUtil.buildAndPush(dockerFilePath, tagFullName, registryAddress, registryUsername, registryPassword);
        //通知系统租户
        if (username != null){
            UserMessage userMessage = new UserMessage();
            userMessage.setUserName(username);
            userMessage.setType("系统通知");
            userMessage.setTypeLink("/sysMgt/StorageManagement");
            userMessage.setUserMessageContent(String.format("镜像构建推送状态：【%b】", tag));
            portalUserMessageClient.insertUserMessage(userMessage);
        }
    }

    @Async
    @Override
    public void buildAndSave(String dockerFilePath, String tagFullName, String username) {
        DockerClientUtil dockerClientUtil = new DockerClientUtil();
        String path = dockerClientUtil.buildAndSave(dockerFilePath, tagFullName);
        //通知系统租户
        if (username != null){
            UserMessage userMessage = new UserMessage();
            userMessage.setUserName(username);
            userMessage.setType("系统通知");
            userMessage.setTypeLink("/sysMgt/StorageManagement");
            userMessage.setUserMessageContent(String.format("镜像构建保存状态：【%s】", path));
            portalUserMessageClient.insertUserMessage(userMessage);
        }
    }

}
