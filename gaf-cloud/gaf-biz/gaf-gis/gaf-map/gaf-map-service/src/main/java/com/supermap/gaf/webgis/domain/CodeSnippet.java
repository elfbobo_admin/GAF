//package com.supermap.gaf.webgis.domain;
//
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
///**
// * @author heykb
// */
//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@ApiModel("webgis应用代码片段")
//public class CodeSnippet {
//    @ApiModelProperty(value = "vue组件html代码片段",example = "")
//    private String htmlContent;
//    @ApiModelProperty(value = "css代码片段",example = "")
//    private String styleContent;
//    @ApiModelProperty(value = "js代码片段",example = "")
//    private String jsContent;
//
//}
