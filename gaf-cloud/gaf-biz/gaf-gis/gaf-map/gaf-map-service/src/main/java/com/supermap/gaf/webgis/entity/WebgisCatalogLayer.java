/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.supermap.gaf.annotation.ConfigName;
import com.supermap.gaf.annotation.ParentIdField;
import com.supermap.gaf.annotation.SortSnField;
import com.supermap.gaf.annotation.UpdatedTimeField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 图层
 *
 * @author wangxiaolong
 * @date 2020-12-05
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("图层")
public class WebgisCatalogLayer implements Serializable {
    @Id
    @NotNull
    @ApiModelProperty("图层目录id")
    private String catalogLayerId;
    @NotNull
    @ConfigName("resourceName")
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("所属目录")
    @ParentIdField
    @ConfigName("pid")
    private String layerCatalogId;
    @ConfigName("resourceId")
    @ApiModelProperty("GIS服务")
    private String gisServiceId;
    @SortSnField
    @ConfigName("sortSn")
    @ApiModelProperty("排序序号")
    private Integer sortSn;
    @ApiModelProperty("初始加载")
    @ConfigName("initLoad")
    @JSONField(name = "isInitLoad")
    private Boolean initLoad;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @UpdatedTimeField
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;
    @ApiModelProperty("资源目录树根节点")
    private String rootCatalogId;

}
