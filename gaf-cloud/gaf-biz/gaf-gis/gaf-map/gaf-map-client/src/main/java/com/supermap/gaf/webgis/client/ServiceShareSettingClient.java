package com.supermap.gaf.webgis.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * ClassName:WebgisServiceClient
 * Package:com.supermap.gaf.webgis.client
 *
 * @date:2021/7/15 10:06
 * @author:Yw
 */
@FeignClient(name = "GAF-MAP", contextId = "ServiceShareSettingClient")
public interface ServiceShareSettingClient {
    @GetMapping("map/service-share-setting")
    MessageResult<Page<ServiceShareSetting>> pageList(@SpringQueryMap ServiceShareSettingSelectVo serviceShareSettingSelectVo,
                                                      @RequestParam("pageNum")Integer pageNum,
                                                      @RequestParam("pageSize")Integer pageSize);

    @PostMapping(value = "/map/service-share-setting",consumes = MediaType.APPLICATION_JSON_VALUE)
    MessageResult<Void> insertServiceShareSetting(@RequestBody ServiceShareSetting serviceShareSetting);

}
