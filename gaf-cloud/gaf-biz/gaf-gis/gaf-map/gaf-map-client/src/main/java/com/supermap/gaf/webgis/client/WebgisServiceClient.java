package com.supermap.gaf.webgis.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.webgis.entity.WebgisService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * ClassName:WebgisServiceClient
 * Package:com.supermap.gaf.webgis.client
 *
 * @date:2021/7/15 10:06
 * @author:Yw
 */
@FeignClient(name = "GAF-MAP", contextId = "webgisServiceClient")
public interface WebgisServiceClient {
    /**
     * 根据id集合批量查询webgis服务
     *
     * @param webgisIds
     * @return
     */
    @PostMapping("/map/webgis-services/get-by-webgis-ids")
    MessageResult<List<WebgisService>> getByWebgisIds(List<String> webgisIds);

    @PostMapping(value = "/map/webgis-services/no-real-address",consumes = MediaType.APPLICATION_JSON_VALUE)
    MessageResult<List<WebgisService>> noRealAddress(@RequestBody List<String> proxyAddress, @RequestParam("typeCode") String typeCode);


    @GetMapping("/map/webgis-services/rest-datas/{datasourceName}/{datasetName}")
    MessageResult<List<WebgisService>> selectRestDataByDatasourceNameAndDatasetName(@PathVariable("datasourceName") String datasourceName, @PathVariable("datasetName") String datasetName);

    @PostMapping(value = "/map/webgis-services",produces = MediaType.APPLICATION_JSON_VALUE)
    MessageResult<String> insertWebgisService(@Valid @RequestBody WebgisService webgisService,
                                              @RequestParam("registryType") String registryType,
                                              @RequestParam("sourceType") Integer sourceType,
                                              @RequestParam("sourceId") String sourceId);
}
