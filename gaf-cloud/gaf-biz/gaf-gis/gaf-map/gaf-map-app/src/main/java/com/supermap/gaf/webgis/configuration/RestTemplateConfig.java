/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.configuration;

import com.supermap.gaf.security.spring.RestTemplateInterceptor;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;


/**
 * @author:yj
 * @date:2021/3/25
 */
@Configuration
public class RestTemplateConfig {

//    @Bean
//    @LoadBalanced
//    RestTemplate restTemplate() {
//        RestTemplate restTemplate = new RestTemplate();
////        restTemplate.setInterceptors(Collections.singletonList(new JWTTokenClientHttpRequestInterceptor()));
//        return restTemplate;
//    }


    @Bean("storageRestTemplate")
    @Primary
    @LoadBalanced
    public RestTemplate storageRestTemplate() {
        return new RestTemplate();
    }


    @Bean("workflowRestTemplate")
    @LoadBalanced
    public RestTemplate workflowRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(Collections.singletonList(new RestTemplateInterceptor()));
        return restTemplate;
    }


}
