package com.supermap.gaf.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamEntity;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamGrpEntity;
import com.supermap.gaf.project.commontype.utils.Constant;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.utils.Query;
import com.supermap.gaf.project.commontype.vo.GroupWithParamsVo;
import com.supermap.gaf.project.commontype.vo.ParamVo;
import com.supermap.gaf.project.dao.ProjCodeTemplateParamGrpDao;
import com.supermap.gaf.project.service.ProjCodeTemplateParamGrpService;
import com.supermap.gaf.project.service.ProjCodeTemplateParamService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Service("projCodeTemplateParamGrpService")
public class ProjCodeTemplateParamGrpServiceImpl extends ServiceImpl<ProjCodeTemplateParamGrpDao, ProjCodeTemplateParamGrpEntity> implements ProjCodeTemplateParamGrpService {
    /**
     * 数据库参数分组表中的字段名，该字段名表示模板id
     */
    public static final String TEMPLATE_ID = "code_template_id";
    /**
     * 数据库参数表中的字段名，该字段名表示参数分组id
     */
    public static final String PARAM_GROUP_ID = "param_group_id";
    @Autowired
    private ProjCodeTemplateParamService projCodeTemplateParamService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProjCodeTemplateParamGrpEntity> page = this.page(
                new Query<ProjCodeTemplateParamGrpEntity>().getPage(params),
                new QueryWrapper<ProjCodeTemplateParamGrpEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void updateProjCodeTemplateParamGrp(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity) {
        projCodeTemplateParamGrpEntity.setUpdatedBy(SecurityUtilsExt.getUserName());
        projCodeTemplateParamGrpEntity.setUpdatedTime(new Date());
        this.updateById(projCodeTemplateParamGrpEntity);
    }

    @Override
    public PageUtils queryPageCondition(String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Long pageNum, Long pageSize) {
        QueryWrapper<ProjCodeTemplateParamGrpEntity> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(searchFieldName)) {
            queryWrapper.like(searchFieldName,searchFieldValue);
        }
        if (!StringUtils.isEmpty(orderFieldName)) {
            if (Constant.ASC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByAsc(orderFieldName);
            }else if (Constant.DESC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByDesc(orderFieldName);
            }
        }
        IPage<ProjCodeTemplateParamGrpEntity> page = this.page(
                new Query<ProjCodeTemplateParamGrpEntity>().getPage(orderFieldName, orderMethod, pageNum, pageSize),
                queryWrapper
        );
        return new PageUtils(page);
    }

    @Override
    public List<GroupWithParamsVo> getParamGroupAndParams(String projCodeTemplateId) {
        QueryWrapper<ProjCodeTemplateParamGrpEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TEMPLATE_ID, projCodeTemplateId);
        List<ProjCodeTemplateParamGrpEntity> groups = this.list(queryWrapper);
        if (groups != null && groups.size()>0) {
            List<GroupWithParamsVo> collect = groups.stream().map(projCodeTemplateParamGrpEntity -> {
                GroupWithParamsVo groupWithParamsVo = new GroupWithParamsVo();
                BeanUtils.copyProperties(projCodeTemplateParamGrpEntity, groupWithParamsVo);
                List<ProjCodeTemplateParamEntity> params = projCodeTemplateParamService.getByGroupId(projCodeTemplateParamGrpEntity.getCodeTemplateParamGrpId());
                if (params != null && params.size() > 0) {
                    List<ParamVo> paramVos = params.stream().map(param -> {
                        ParamVo paramVo = new ParamVo();
                        BeanUtils.copyProperties(param, paramVo);
                        return paramVo;
                    }).collect(Collectors.toList());
                    groupWithParamsVo.setParamVos(paramVos);
                }
                return groupWithParamsVo;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    @Override
    public void insertGroup(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity) {
        String userName = SecurityUtilsExt.getUserName();
        Date date = new Date();
        projCodeTemplateParamGrpEntity.setCodeTemplateParamGrpId(null);
        projCodeTemplateParamGrpEntity.setCreatedTime(date);
        projCodeTemplateParamGrpEntity.setUpdatedTime(date);
        projCodeTemplateParamGrpEntity.setCreatedBy(userName);
        projCodeTemplateParamGrpEntity.setUpdatedBy(userName);
        this.save(projCodeTemplateParamGrpEntity);
    }

    @Override
    public List<ProjCodeTemplateParamGrpEntity> listGroupsByTemplateId(String projCodeTemplateId) {
        QueryWrapper<ProjCodeTemplateParamGrpEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(TEMPLATE_ID,projCodeTemplateId);
        return this.list(wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeGrpAndParams(String codeTemplateParamGrpId) {
        this.removeById(codeTemplateParamGrpId);
        projCodeTemplateParamService.remove(new QueryWrapper<ProjCodeTemplateParamEntity>().eq(PARAM_GROUP_ID,codeTemplateParamGrpId));
    }

}