package com.supermap.gaf.project.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.vo.ProjCodeTemplateVo;
import com.supermap.gaf.project.commontype.vo.TemplateTypeVo;
import com.supermap.gaf.project.service.ProjCodeTemplateService;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author wangxiaolong
 */
@Component
@Api(value = "模板表 rest接口")
public class ProjCodeTemplateResource {
    @Autowired
    private ProjCodeTemplateService projCodeTemplateService;

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除模板和对应参数分组和参数", notes = "根据id删除模板和对应参数分组和参数")
    @Path("/{projCodeTemplateId}")
    public MessageResult<Void> deleteProjCodeTemplateAndParam(
            @ApiParam(name = "projCodeTemplateId", required = true, value = "模板id") @NotNull @PathParam("projCodeTemplateId") String projCodeTemplateId) {
        projCodeTemplateService.deleteTemplateAndGroupsAndParams(projCodeTemplateId);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }


    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新模板信息", notes = "更新模板")
    @Path("/{projCodeTemplateId}")
    public MessageResult<Void> updateProjCodeTemplate(@ApiParam(name = "projCodeTemplateEntity",value = "模板实体")ProjCodeTemplateEntity projCodeTemplateEntity,@ApiParam(name = "projCodeTemplateId",value = "模板id",required = true) @PathParam("projCodeTemplateId") String projCodeTemplateId) {
        projCodeTemplateEntity.setProjCodeTemplateId(projCodeTemplateId);
        projCodeTemplateService.updateProjCodeTemplate(projCodeTemplateEntity);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询模板", notes = "id查询模板")
    @Path("/{projCodeTemplateId}")
    public MessageResult<ProjCodeTemplateEntity> selectById(@PathParam("projCodeTemplateId") String projCodeTemplateId) {
        ProjCodeTemplateEntity projCodeTemplateEntity = projCodeTemplateService.getById(projCodeTemplateId);
        return MessageResult.successe(ProjCodeTemplateEntity.class).data(projCodeTemplateEntity).status(200).message("查询成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除模板", notes = "批量删除模板")
    public MessageResult<Void> batchDelete(List<String> projCodeTemplateIds) {
        projCodeTemplateService.delatebath(projCodeTemplateIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增模板", notes = "新增模板")
    public MessageResult<Void> insertProjCodeTemplate(ProjCodeTemplateEntity projCodeTemplateEntity) {
        projCodeTemplateService.insertProjCodeTemplate(projCodeTemplateEntity);
        return MessageResult.successe(Void.class).status(200).message("新增模板操作成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "条件查询模板", notes = "条件查询模板")
    public MessageResult<PageUtils> pageList(
                                        @QueryParam("templateType") Integer templateType,
                                        @StringRange(entityClass = ProjCodeTemplateEntity.class) @QueryParam("searchFieldName") String searchFieldName,
                                       @QueryParam("searchFieldValue") String searchFieldValue,
                                       @StringRange(entityClass = ProjCodeTemplateEntity.class) @QueryParam("orderFieldName") String orderFieldName,
                                       @StringRange(value = {"asc", "desc"},ignoreCase = true) @QueryParam("orderMethod") String orderMethod,
                                       @QueryParam("pageNum") Long pageNum,
                                       @DefaultValue("50") @QueryParam("pageSize") Long pageSize) {
        PageUtils pageUtils = projCodeTemplateService.queryPageCondition(templateType, searchFieldName, searchFieldValue, orderFieldName, orderMethod, pageNum, pageSize);
        return MessageResult.successe(PageUtils.class).data(pageUtils).status(200).message("查询成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增模板", notes = "批量新增模板")
    public MessageResult<Void> batchInsert(@NotNull List<ProjCodeTemplateEntity> projCodeTemplateEntities) {
        projCodeTemplateService.inserBath(projCodeTemplateEntities);
        return MessageResult.successe(Void.class).status(200).message("批量新增模板操作成功").build();

    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/templatetype")
    @ApiOperation(value = "查询模板类型", notes = "查询模板类型")
    public MessageResult<List> getAllTemplateType() {
        List<TemplateTypeVo> allTemplateType = projCodeTemplateService.getAllTemplateType();
        return MessageResult.successe(List.class).data(allTemplateType).status(200).build();
    }
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/all")
    @ApiOperation(value = "查询所有模板", notes = "查询所有模板")
    public MessageResult<List> getAllTemplates() {
        List<ProjCodeTemplateVo> allTemplates = projCodeTemplateService.getAllTemplates();
        return MessageResult.successe(List.class).data(allTemplates).status(200).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/projtype")
    @ApiOperation(value = "查询工程模板", notes = "查询工程模板")
    public MessageResult<List> getProjTemplates() {
        List<ProjCodeTemplateVo> projTemplates = projCodeTemplateService.getProjTemplates();
        return MessageResult.successe(List.class).data(projTemplates).status(200).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/codetype")
    @ApiOperation(value = "查询代码模板", notes = "查询代码模板")
    public MessageResult<List> getCodeTemplates() {
        List<ProjCodeTemplateVo> codeTemplates = projCodeTemplateService.getCodeTemplates();
        return MessageResult.successe(List.class).data(codeTemplates).status(200).build();
    }
}
