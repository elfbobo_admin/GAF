package com.supermap.gaf.project.service;


import com.supermap.gaf.project.commontype.VmMetadata;

import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
public interface VmMetadataService {
    /**
     * 根据模板名称模板信息数据对象
     * @param vmName
     * @param vmRootPath
     * @return
     */
    VmMetadata select(String vmName, String vmRootPath);

    /**
     * 条件查询所有模板信息
     * @param vmRootPath
     * @param captionName
     * @return
     */
    List<VmMetadata> selectList(String vmRootPath, String captionName);
}
