package com.supermap.gaf.project.commontype;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("属性配置数据对象")
public class VmProperty {
    @ApiModelProperty("属性名")
    private String key;
    @ApiModelProperty("属性值")
    private String defaultValue;
    @JSONField(name="isRequired")
    @ApiModelProperty("是否必要")
    private boolean required;
    @ApiModelProperty("别名")
    private String caption;
    @JSONField(name="isList")
    @ApiModelProperty("是否是Array")
    private boolean list;
    /**value 是另一个对象时，描述信息*/
    @ApiModelProperty("属性配置数据对象数组")
    private List<VmProperty> elementMetadata;
    @JSONField(name="isString")
    public boolean isString(){
        return !this.isList() && CollectionUtils.isEmpty(this.getElementMetadata());
    }
    @JSONField(name="isStringList")
    public boolean isStringList(){
        return this.isList() && CollectionUtils.isEmpty(this.getElementMetadata());
    }
    @JSONField(name="isPropertiesList")
    public boolean isPropertiesList(){
        return this.isList() && !CollectionUtils.isEmpty(this.getElementMetadata());
    }
    @JSONField(name="isProperties")
    public boolean isProperties(){
        return !this.isList() && !CollectionUtils.isEmpty(this.getElementMetadata());
    }
}