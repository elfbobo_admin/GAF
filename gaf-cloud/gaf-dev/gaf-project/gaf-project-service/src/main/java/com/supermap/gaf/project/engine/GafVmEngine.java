package com.supermap.gaf.project.engine;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.project.commontype.VmMetadata;
import com.supermap.gaf.project.commontype.VmProperty;
import com.supermap.gaf.project.service.VmMetadataService;
import com.supermap.gaf.project.service.impl.XmlVmMetadataServiceImpl;
import com.supermap.gaf.utils.LogUtil;
import lombok.Getter;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Getter
public class GafVmEngine {
    private VelocityEngine ve;
    private String vmRootPath;
    private String templateResources;
    private VmMetadataService vmMetadataService;
    private static Logger logger = LogUtil.getLocLogger(GafVmEngine.class);

    /**
     *
     * @param vmRootPath  模板搜索根目录
     */
    public GafVmEngine(String vmRootPath){
        this(vmRootPath,new XmlVmMetadataServiceImpl());
    }
    /**
     *
     * @param vmRootPath 模板搜索根目录
     * @param vmMetadataService
     */
    public GafVmEngine(String vmRootPath, VmMetadataService vmMetadataService){
        this(vmRootPath,"resources",vmMetadataService);
    }
    public GafVmEngine(String vmRootPath, String templateResources, VmMetadataService vmMetadataService) {
        this.vmRootPath = vmRootPath;
        this.vmMetadataService = vmMetadataService;
        this.templateResources = templateResources;
        this.ve = new VelocityEngine();
        this.ve.setProperty("input.encoding", "utf-8");
        this.ve.setProperty("output.encoding", "utf-8");
        this.ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "file");
        this.ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, vmRootPath);
        this.ve.init();
    }

    @Deprecated
    /**
     * 模板生成 输出到目录
     * @param vmName
     * @param properties
     * @param outDir
     * @throws IOException
     */
    public void generatorVm(String vmName, Map<String,Object> properties,Path outDir) throws IOException {
        VmMetadata vmMetadata = this.vmMetadataService.select(vmName,vmRootPath);
        Map<String,String> vmFileMappings = null;
        if(vmMetadata!=null){
            vmFileMappings = vmMetadata.getFileMappings();
            List<VmProperty> vmProperties = vmMetadata.getRequiredProperties();
            checkPropertiesAndInjectDefaultProperties(vmProperties,properties);
        }
        VelocityContext ctx = new VelocityContext(properties);
        Path vmRoot = FileSystems.getDefault().getPath(vmRootPath + "/" + vmName+"/"+templateResources);
        if(!Files.exists(vmRoot)){
            throw new IllegalArgumentException("找不到resources目录,请确保模板文件在resources目录下 ");
        }

        if(!Files.exists(outDir)) {Files.createDirectories(outDir);}
        Files.walkFileTree(vmRoot, new OutDirFileVisitor(this,vmName,ctx,outDir,vmFileMappings));
    }

    /**
     * 模板生成 输出到zip
     * @param vmName
     * @param properties
     * @param zipOutputStream
     * @throws IOException
     */
    public void generatorVm(String vmName, Map<String,Object> properties,ZipOutputStream zipOutputStream) throws IOException {
        logger.info(vmName);
        logger.info(vmRootPath);
        VmMetadata vmMetadata = this.vmMetadataService.select(vmName,vmRootPath);
        Map<String,String> vmFileMappings = null;
        if(vmMetadata!=null){
            vmFileMappings = vmMetadata.getFileMappings();
            List<VmProperty> vmProperties = vmMetadata.getRequiredProperties();
            checkPropertiesAndInjectDefaultProperties(vmProperties,properties);
        }
        VelocityContext ctx = new VelocityContext(properties);
        Path vmRoot = FileSystems.getDefault().getPath(vmRootPath + "/" + vmName+"/"+templateResources);
        if(!Files.exists(vmRoot)){
            throw new IllegalArgumentException("找不到resources目录,请确保模板文件在resources目录下 ");
        }
        Files.walkFileTree(vmRoot, new ZipOutputStreamFileVisitor(this,vmName,ctx,zipOutputStream,vmFileMappings));
    }

    void checkPropertiesAndInjectDefaultProperties(List<VmProperty> vmProperties, Map<String,Object> properties){
        if(!CollectionUtils.isEmpty(vmProperties)){
            if(properties==null){
                properties = new HashMap<>(16);
            }
            for(VmProperty vmProperty:vmProperties){
                Object value = properties.get(vmProperty.getKey());
                if(value != null){
                    checkProperty(vmProperty,value);
                }else{
                    String defaultValue = vmProperty.getDefaultValue();
                    if(vmProperty.isRequired() && defaultValue==null){
                        throw new IllegalArgumentException("缺少必要参数："+vmProperty.getKey());
                    }
                    if(vmProperty.isString()){
                        properties.put(vmProperty.getKey(),defaultValue);
                    }
                    if(vmProperty.isStringList()){
                        if(defaultValue==null){
                            properties.put(vmProperty.getKey(),new ArrayList<>());
                        }else{
                            List<String> listValue = JSON.parseArray(defaultValue,String.class);
                            properties.put(vmProperty.getKey(),listValue);
                        }
                    }
                    if(vmProperty.isProperties()){
                        Map defaultValueMap = new HashMap(16);
                        if(defaultValue==null){
                            properties.put(vmProperty.getKey(),defaultValueMap);
                        }else{
                            try{
                                defaultValueMap = JSON.parseObject(defaultValue,Map.class);
                            }catch (Exception e){
                                throw new IllegalArgumentException("模板元数据解析失败："+e.getMessage());
                            }
                            properties.put(vmProperty.getKey(),defaultValueMap);
                        }
                        checkPropertiesAndInjectDefaultProperties(vmProperty.getElementMetadata(),defaultValueMap);
                    }

                    if(vmProperty.isPropertiesList()){
                        List<Map> valueList = new ArrayList();
                        valueList.add(new HashMap<>(16));
                        if(defaultValue==null){
                            properties.put(vmProperty.getKey(),valueList);
                        }else{
                            try{
                                valueList = JSON.parseArray(defaultValue,Map.class);
                            }catch (Exception e){
                                throw new RuntimeException("模板元数据解析失败："+e.getMessage());
                            }
                            properties.put(vmProperty.getKey(),valueList);
                        }
                        valueList.forEach(itProperties->checkPropertiesAndInjectDefaultProperties(vmProperty.getElementMetadata(),itProperties));
                    }
                }
            }
        }
    }
    void checkProperty(VmProperty vmProperty, Object value){
        if(value==null && vmProperty.isRequired()){
            throw new IllegalArgumentException("缺少必要参数："+vmProperty.getKey());
        }
        boolean existed = !(!vmProperty .isList() || vmProperty.isList() && value instanceof List);
        if(existed){
            throw new IllegalArgumentException("参数"+vmProperty.getKey()+"要求是数组");
        }
        List<VmProperty> vmProperties = vmProperty.getElementMetadata();
        if(vmProperty.isPropertiesList()) {
            List<Object> list = (List<Object>) value;
            for (Object o : list) {
                if (!(o instanceof Map)) {
                    throw new IllegalArgumentException("参数" + vmProperty.getKey() + "要求是数组并且数组元素要求是对象");
                }
                //....
                checkPropertiesAndInjectDefaultProperties(vmProperties, (Map<String, Object>) o);
            }
        }
        if(vmProperty.isProperties()){
            if(!(value instanceof Map)){
                throw new IllegalArgumentException("参数"+vmProperty.getKey()+"要求是字典对象");
            }
            //...
            checkPropertiesAndInjectDefaultProperties(vmProperties, (Map<String, Object>) value);
        }

    }
}
