package com.supermap.gaf.project.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.project.commontype.entity.BatchHandleParamsEntity;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.service.ProjCodeTemplateParamService;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Component
@Api(value = "模板参数 rest接口")
public class ProjCodeTemplateParamResource {
    @Autowired
    private ProjCodeTemplateParamService projCodeTemplateParamService;

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "删除模板参数", notes = "删除模板参数")
    @Path("/{projCodeTemplateParamId}")
    public MessageResult<Void> deleteProjCodeTemplateParam(@PathParam("projCodeTemplateParamId") String projCodeTemplateParamId) {
        projCodeTemplateParamService.removeById(projCodeTemplateParamId);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }


    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新模板参数", notes = "更新模板参数")
    @Path("/{projCodeTemplateParamId}")
    public MessageResult<Void> updateProjCodeTemplateParam(ProjCodeTemplateParamEntity projCodeTemplateParamEntity,@NotNull @PathParam("projCodeTemplateParamId") String projCodeTemplateParamId) {
        projCodeTemplateParamEntity.setProjCodeTemplateParamId(projCodeTemplateParamId);
        projCodeTemplateParamService.updateById(projCodeTemplateParamEntity);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "id查询模板参数", notes = "id查询模板参数")
    @Path("/{projCodeTemplateParamId}")
    public MessageResult<ProjCodeTemplateParamEntity> selectById(@PathParam("projCodeTemplateParamId") String projCodeTemplateParamId) {
        ProjCodeTemplateParamEntity param = projCodeTemplateParamService.getById(projCodeTemplateParamId);
        return MessageResult.successe(ProjCodeTemplateParamEntity.class).data(param).status(200).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据分组Id查询模板参数", notes = "根据分组Id查询模板参数")
    @Path("/group/{paramGroupId}")
    public MessageResult<List> selectByparamGroupId(@NotEmpty @PathParam("paramGroupId") String paramGroupId) {
        List<ProjCodeTemplateParamEntity> params = projCodeTemplateParamService.getByparamGroupId(paramGroupId);
        return MessageResult.successe(List.class).data(params).status(200).message("查询成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除模板参数", notes = "批量删除模板参数")
    public MessageResult<Void> batchDelete(@NotNull List<String> projCodeTemplateParamIds) {
        projCodeTemplateParamService.removeByIds(projCodeTemplateParamIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增模板参数", notes = "新增模板参数")
    public MessageResult<Void> insertProjCodeTemplateParam(ProjCodeTemplateParamEntity projCodeTemplateParam) {
        projCodeTemplateParam.setProjCodeTemplateParamId(null);
        projCodeTemplateParamService.save(projCodeTemplateParam);
        return MessageResult.successe(Void.class).status(200).message("新增模板参数操作成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "条件查询模板参数", notes = "条件查询模板参数")
    public MessageResult<PageUtils> pageList(@StringRange(entityClass = ProjCodeTemplateParamEntity.class) @QueryParam("searchFieldName") String searchFieldName,
                                       @QueryParam("searchFieldValue") String searchFieldValue,
                                       @StringRange(entityClass = ProjCodeTemplateParamEntity.class) @QueryParam("orderFieldName") String orderFieldName,
                                       @StringRange(value = {"asc", "desc"},ignoreCase = true) @QueryParam("orderMethod") String orderMethod,
                                       @QueryParam("pageNum") Long pageNum,
                                       @DefaultValue("50") @QueryParam("pageSize") Long pageSize) {
        PageUtils pageUtils = projCodeTemplateParamService.queryPageCondition(searchFieldName, searchFieldValue, orderFieldName, orderMethod, pageNum, pageSize);
        return MessageResult.successe(PageUtils.class).data(pageUtils).status(200).message("查询成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增模板参数", notes = "批量新增模板参数")
    public MessageResult<Void> batchInsert(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites) {
        projCodeTemplateParamService.insertBatch(projCodeTemplateParamEntites);
        return MessageResult.successe(Void.class).status(200).message("批量新增模板参数操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batchSaveOrUpdate")
    @ApiOperation(value = "批量新增或修改模板参数", notes = "批量新增或修改模板参数")
    public MessageResult<Void> batchInsertOrUpdate(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites) {
        projCodeTemplateParamService.insertOrUpdateBatch(projCodeTemplateParamEntites);
        return MessageResult.successe(Void.class).status(200).message("批量新增模板或修改参数操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batchHandlerParams")
    @ApiOperation(value = "批量处理模板参数", notes = "批量处理模板参数")
    public MessageResult<Boolean> batchHandlerParams(BatchHandleParamsEntity batchHandleParamsEntity) {
        Boolean res = projCodeTemplateParamService.batchHandleParams(batchHandleParamsEntity);
        String keyPre = "批量处理模板参数失败";
        if (!res) return MessageResult.failed(Boolean.class).data(res).message(keyPre).build();
        return MessageResult.successe(Boolean.class).data(true).message("批量处理模板参数成功").build();
    }

}
