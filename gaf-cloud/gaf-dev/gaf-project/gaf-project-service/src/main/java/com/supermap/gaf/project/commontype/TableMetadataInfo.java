package com.supermap.gaf.project.commontype;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TableMetadataInfo {
    private String tableName;
    private String tableLowCamelName;
    private String tableUpperCamelName;
    private String remarks;
    private String pkName;
    private String pkLowCamelName;
    private String pkUpperCamelName;
    private String pkJavaType;
    private String pkSqlType;
    private String pkJdbcType;
    @JSONField(name = "isGeneratedPrimaryKey")
    private Boolean isGeneratedPrimaryKey = false;
    private List<FieldMetadataInfo> fieldMetadataInfos;
}
