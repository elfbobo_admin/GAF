package com.supermap.gaf.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.supermap.gaf.project.commontype.entity.BatchHandleParamsEntity;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;

import java.util.List;

/**
 *
 *
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-04 14:46:24
 */
public interface ProjCodeTemplateParamService extends IService<ProjCodeTemplateParamEntity> {
    /**
     * 单字段条件 分页查询
     * @param searchFieldName 字段名
     * @param searchFieldValue 字段值
     * @param orderFieldName 排序字段名
     * @param orderMethod 排序字段值
     * @param pageNum 当前页
     * @param pageSize 页大小
     * @return
     */
    PageUtils queryPageCondition(String searchFieldName,
                                 String searchFieldValue,
                                 String orderFieldName,
                                 String orderMethod,
                                 Long pageNum,
                                 Long pageSize);

    /**
     * 根据id查询分组模板信息
     * @param codeTemplateParamGrpId
     * @return
     */
    List<ProjCodeTemplateParamEntity> getByGroupId(String codeTemplateParamGrpId);

    /**
     * 批量添加工程模板参数信息
     * @param projCodeTemplateParamEntites
     */
    void insertBatch(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites);

    /**
     * 批量新增或修改工程模板参数
     * @param projCodeTemplateParamEntites
     */
    void insertOrUpdateBatch(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites);

    /**
     * 查询是否含有工程模板参数信息
     * @param batchHandleParamsEntity
     * @return
     */
    Boolean batchHandleParams(BatchHandleParamsEntity batchHandleParamsEntity);



    /**
     * 根据参数分组ID删除参数
     * @param collect
     */
    void deleteByGroupIds(List<String> collect);

    /**
     * 根据分组Id查询模板参数
     * @param paramGroupId
     * @return
     */
    List<ProjCodeTemplateParamEntity> getByparamGroupId(String paramGroupId);
}

