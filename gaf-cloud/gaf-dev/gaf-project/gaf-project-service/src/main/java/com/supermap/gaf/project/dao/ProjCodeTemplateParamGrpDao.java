package com.supermap.gaf.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamGrpEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代码模板参数分组表
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:45
 */
@Mapper
public interface ProjCodeTemplateParamGrpDao extends BaseMapper<ProjCodeTemplateParamGrpEntity> {
	
}
