package com.supermap.gaf.project.engine;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;

/**
 * @author:yw
 * @Date 2021/3/17
 * 遍历模板文件将最终合并输出到目录
 **/
public class OutDirFileVisitor extends SimpleFileVisitor<Path> {

    private Map<String,String> vmFileMappings;

    private VelocityEngine ve;

    private String vmName;

    private Path vmRoot;

    private String templateResources;

    private VelocityContext ctx;

    private Path outDir;

    public OutDirFileVisitor(GafVmEngine vmEngine, String vmName, VelocityContext ctx, Path outDir, Map<String,String> vmFileMappings){
        super();
        this.ve = vmEngine.getVe();
        this.templateResources = vmEngine.getTemplateResources();
        this.vmName = vmName;
        this.vmRoot = FileSystems.getDefault().getPath(vmEngine.getVmRootPath()).resolve(vmName+"/"+templateResources);
        this.ctx = ctx;
        this.outDir = outDir;
        this.vmFileMappings = vmFileMappings;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        //动态文件名
        String relativePath = file.subpath(vmRoot.getNameCount(), file.getNameCount()).toString();
        relativePath = relativePath.replaceAll("[\\\\]+","/");
        Template template = ve.getTemplate("/"+vmName+"/"+templateResources+"/"+relativePath);
        if(!CollectionUtils.isEmpty(vmFileMappings)){
            String pattern = vmFileMappings.get(relativePath);
            StringWriter newFileName = new StringWriter();
            if(!StringUtils.isEmpty(pattern)){
                Velocity.evaluate( ctx, newFileName, "", pattern);
                if(vmRoot.getNameCount()==file.getNameCount()-1){
                    relativePath = newFileName.toString();
                }else{
                    relativePath = file.subpath(vmRoot.getNameCount(), file.getNameCount()-1)+"/"+newFileName.toString();
                }
            }
        }

        //合并模板文件
        File out = outDir.resolve(relativePath).toFile();
        Writer writer = new PrintWriter(new FileOutputStream(out));
        template.merge(ctx, writer);
        writer.flush();
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        //创建文件夹
        if(vmRoot.compareTo(dir)!=0){
            Path out = outDir.resolve(dir.subpath(vmRoot.getNameCount(), dir.getNameCount()));
            if (!Files.exists(out)){
                Files.createDirectories(out);
            }
        }
        return FileVisitResult.CONTINUE;
    }
}
