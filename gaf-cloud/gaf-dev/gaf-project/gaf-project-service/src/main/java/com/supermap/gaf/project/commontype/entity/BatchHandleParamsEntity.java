package com.supermap.gaf.project.commontype.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 *
 *
 * @author zhm
 */
@Data
@ApiModel("工程模板参数信息")
public class BatchHandleParamsEntity implements Serializable {

    /**
     * 删除参数id列表
     */
    @ApiModelProperty("删除参数id列表")
    private List<String> deleteIdsLst;


    /**
     * 新增或修改参数列表
     */
    @ApiModelProperty("新增或修改参数列表")
    private List<ProjCodeTemplateParamEntity> templateParamsLst;



    public List<String> getDeleteIdsLst() {
        return deleteIdsLst;
    }

    public void setDeleteIdsLst(List<String> deleteIdsLst) {
        this.deleteIdsLst = deleteIdsLst;
    }

    public List<ProjCodeTemplateParamEntity> getTemplateParamsLst() {
        return templateParamsLst;
    }

    public void setTemplateParamsLst(List<ProjCodeTemplateParamEntity> templateParamsLst) {
        this.templateParamsLst = templateParamsLst;
    }




}
