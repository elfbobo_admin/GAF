package com.supermap.gaf.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.project.commontype.constant.TempalteConstant;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateEntity;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamGrpEntity;
import com.supermap.gaf.project.commontype.enums.TemplateTypeEnum;
import com.supermap.gaf.project.commontype.utils.*;
import com.supermap.gaf.project.commontype.vo.ProjCodeTemplateEntityVo;
import com.supermap.gaf.project.commontype.vo.ProjCodeTemplateVo;
import com.supermap.gaf.project.commontype.vo.TemplateTypeVo;
import com.supermap.gaf.project.dao.ProjCodeTemplateDao;
import com.supermap.gaf.project.service.ProjCodeTemplateParamGrpService;
import com.supermap.gaf.project.service.ProjCodeTemplateParamService;
import com.supermap.gaf.project.service.ProjCodeTemplateService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Service("projCodeTemplateService")
public class ProjCodeTemplateServiceImpl extends ServiceImpl<ProjCodeTemplateDao, ProjCodeTemplateEntity> implements ProjCodeTemplateService {



    /**
     * 数据库模板表中的字段名，该字段名表示模板ID
     */
    public static final String PROJ_CODE_TEMPLATE_ID = "proj_code_template_id";
    /**
     * 数据库模板表中的字段名，该字段名表示模板名
     */
    public static final String TEMPLATE_NAME = "template_name";
    /**
     * 数据库模板表中的字段名，该字段名表示模板中文名
     */
    public static final String TEMPLATE_NAME_CN = "template_name_cn";

    @Autowired
    private ProjCodeTemplateParamGrpService projCodeTemplateParamGrpService;

    @Autowired
    private ProjCodeTemplateParamService projCodeTemplateParamService;


    @Autowired
    private StorageClient storageClient;

    /**
     * 将模板中参数类型数字编码转换文字说明
     *
     * @param projCodeTemplateEntity
     * @return
     */
    public static ProjCodeTemplateEntityVo convertTypeNumToDescriptionOne(ProjCodeTemplateEntity projCodeTemplateEntity) {
        ProjCodeTemplateEntityVo projCodeTemplateEntityVo = new ProjCodeTemplateEntityVo();
        BeanUtils.copyProperties(projCodeTemplateEntity, projCodeTemplateEntityVo);
        projCodeTemplateEntityVo.setTemplateTypeName(TemplateTypeEnum.typeNumToDescription(projCodeTemplateEntity.getTemplateType()));
        return projCodeTemplateEntityVo;
    }

    /**
     * 批量将模板中参数类型数字编码转换文字说明
     *
     * @param projCodeTemplateEntities
     * @return
     */
    public static List<ProjCodeTemplateEntityVo> convertTypeNumToDescription(List<ProjCodeTemplateEntity> projCodeTemplateEntities) {
        if (projCodeTemplateEntities != null) {
            List<ProjCodeTemplateEntityVo> collect = projCodeTemplateEntities.stream().map(projCodeTemplateEntity -> {
                return convertTypeNumToDescriptionOne(projCodeTemplateEntity);
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    @Override
    public PageUtils queryPageCondition(Integer templateType, String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Long pageNum, Long pageSize) {
        QueryWrapper<ProjCodeTemplateEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(!Objects.isNull(templateType) ,"template_type", templateType);
        if (!StringUtils.isEmpty(searchFieldName)) {
            queryWrapper.like(searchFieldName, searchFieldValue);
        }
        if (!StringUtils.isEmpty(orderFieldName)) {
            if (Constant.ASC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByAsc(orderFieldName);
            } else if (Constant.DESC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByDesc(orderFieldName);
            }
        }
        IPage<ProjCodeTemplateEntity> page = this.page(
                new Query<ProjCodeTemplateEntity>().getPage(orderFieldName, orderMethod, pageNum, pageSize),
                queryWrapper
        );
        List<ProjCodeTemplateEntity> records = page.getRecords();
        List<ProjCodeTemplateEntityVo> pageListVo = convertTypeNumToDescription(records);
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setPageList(pageListVo);
        return pageUtils;
    }

    @Override
    public List<TemplateTypeVo> getAllTemplateType() {
        TemplateTypeEnum[] values = TemplateTypeEnum.values();
        List<TemplateTypeVo> allTemplateType = new LinkedList<>();
        for (int i = 0; i < values.length; i++) {
            TemplateTypeVo templateTypeVo = new TemplateTypeVo();
            templateTypeVo.setType(values[i].getType());
            templateTypeVo.setDescription(values[i].getDescription());
            allTemplateType.add(templateTypeVo);
        }
        return allTemplateType;
    }

    @Override
    public void updateProjCodeTemplate(ProjCodeTemplateEntity projCodeTemplateEntity) {
        //UserDetail user = UserUtils.getUserAndCheck();
        projCodeTemplateEntity.setUpdatedBy(SecurityUtilsExt.getUserName());
        projCodeTemplateEntity.setUpdatedTime(new Date());
        //ProjCodeTemplateEntity oldTemplateInfo = this.getById(projCodeTemplateEntity.getProjCodeTemplateId());
        //String oldTemplatePath = oldTemplateInfo.getTemplatePath();
        //if (oldTemplatePath != null) {
        //    String newTemplatePath = projCodeTemplateEntity.getTemplatePath();
        //    if (!StringUtils.isEmpty(newTemplatePath)) {
        //        if (!oldTemplatePath.equalsIgnoreCase(newTemplatePath)) {
        //            try {
        //                // TODO 从文件服务器删除模板文件 更新时暂时不删除
        //            } catch (Exception e) {
        //
        //            }
        //        }
        //        JSONObject fileDownLoadPath = JSONObject.parseObject(newTemplatePath);
        //        // 解析路径
        //        String filePath = ParsePathUtils.parsePath(fileDownLoadPath);
        //        fileDownLoadPath.put(Constant.FILE_ABSOLUTE_PATH, rootPath + filePath);
        //        String s = JSONObject.toJSONString(fileDownLoadPath);
        //        projCodeTemplateEntity.setTemplatePath(s);
        //    }
        //}
        this.updateById(projCodeTemplateEntity);
    }

    @Transactional
    @Override
    public void delatebath(List<String> projCodeTemplateIds) {
        if (projCodeTemplateIds == null || projCodeTemplateIds.isEmpty()) return;
        for (String projCodeTemplateId : projCodeTemplateIds) {
            deleteTemplateAndGroupsAndParams(projCodeTemplateId);
        }
    }

    @Override
    public void insertProjCodeTemplate(ProjCodeTemplateEntity projCodeTemplateEntity) {
        Date date = new Date();
        projCodeTemplateEntity.setProjCodeTemplateId(null);
        projCodeTemplateEntity.setCreatedTime(date);
        projCodeTemplateEntity.setUpdatedTime(date);
        String userName = SecurityUtilsExt.getUserName();
        projCodeTemplateEntity.setCreatedBy(userName);
        projCodeTemplateEntity.setUpdatedBy(userName);
        this.save(projCodeTemplateEntity);
    }

    @Override
    public void inserBath(List<ProjCodeTemplateEntity> projCodeTemplateEntities) {
        if (projCodeTemplateEntities != null && projCodeTemplateEntities.size() > 0) {
            String userName = SecurityUtilsExt.getUserName();
            Date date = new Date();
            projCodeTemplateEntities.forEach(projCodeTemplateEntity -> {
                projCodeTemplateEntity.setCreatedTime(date);
                projCodeTemplateEntity.setUpdatedTime(date);
                projCodeTemplateEntity.setCreatedBy(userName);
                projCodeTemplateEntity.setUpdatedBy(userName);
                projCodeTemplateEntity.setProjCodeTemplateId(null);

            });
            this.saveBatch(projCodeTemplateEntities);
        }
    }

    @Override
    public ProjCodeTemplateEntityVo selectById(String projCodeTemplateId) {
        ProjCodeTemplateEntity projCodeTemplateEntity = this.getById(projCodeTemplateId);
        return convertTypeNumToDescriptionOne(projCodeTemplateEntity);
    }

    @Override
    public List<ProjCodeTemplateVo> getAllTemplates() {
        QueryWrapper<ProjCodeTemplateEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(PROJ_CODE_TEMPLATE_ID, TEMPLATE_NAME, TEMPLATE_NAME_CN);
        List<ProjCodeTemplateEntity> list = this.list(queryWrapper);
        if (list != null && list.size() > 0) {
            List<ProjCodeTemplateVo> collect = list.stream().map(projCodeTemplateEntity -> {
                ProjCodeTemplateVo projCodeTemplateVo = new ProjCodeTemplateVo();
                BeanUtils.copyProperties(projCodeTemplateEntity, projCodeTemplateVo);
                return projCodeTemplateVo;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteTemplateAndGroupsAndParams(String projCodeTemplateId) {
        try {
            // 删除模板文件
            ProjCodeTemplateEntity template = this.getById(projCodeTemplateId);
            if (template != null) {
                String templatePath = template.getTemplatePath();
                // 删除模板文件
                storageClient.delete(templatePath, SecurityUtilsExt.getTenantId());
            }
        } catch (Exception e) {
            // 删除失败也没有关系
        }
        List<ProjCodeTemplateParamGrpEntity> groups = projCodeTemplateParamGrpService.listGroupsByTemplateId(projCodeTemplateId);
        if (groups != null && groups.size() > 0) {
            List<String> collect = groups.stream().map(ProjCodeTemplateParamGrpEntity::getCodeTemplateParamGrpId).collect(Collectors.toList());
            projCodeTemplateParamService.deleteByGroupIds(collect);
            projCodeTemplateParamGrpService.removeByIds(collect);
        }
        this.removeById(projCodeTemplateId);
    }

    @Override
    public List<ProjCodeTemplateVo> getProjTemplates() {
        QueryWrapper<ProjCodeTemplateEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.in(TempalteConstant.TEMPLATE_TYPE,
                        TemplateTypeEnum.PROJECT_BACK.getType(),
                        TemplateTypeEnum.PROJECT_FRONT_BACK.getType(),
                        TemplateTypeEnum.PROJECT_FRONT.getType());
        List<ProjCodeTemplateEntity> list = this.list(queryWrapper);
        if (list != null && list.size() > 0) {
            List<ProjCodeTemplateVo> collect = list.stream().map(projCodeTemplateEntity -> {
                ProjCodeTemplateVo projCodeTemplateVo = new ProjCodeTemplateVo();
                BeanUtils.copyProperties(projCodeTemplateEntity, projCodeTemplateVo);
                return projCodeTemplateVo;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    @Override
    public List<ProjCodeTemplateVo> getCodeTemplates() {
        QueryWrapper<ProjCodeTemplateEntity> queryWrapper = new QueryWrapper<>();
        // 暂时修改为前后端 23
        List<ProjCodeTemplateEntity> list = this.list(queryWrapper
                .in(TempalteConstant.TEMPLATE_TYPE,
                        TemplateTypeEnum.CODE_FRONT_BACK.getType()));
        if (list != null && list.size() > 0) {
            List<ProjCodeTemplateVo> collect = list.stream().map(projCodeTemplateEntity -> {
                ProjCodeTemplateVo projCodeTemplateVo = new ProjCodeTemplateVo();
                BeanUtils.copyProperties(projCodeTemplateEntity, projCodeTemplateVo);
                return projCodeTemplateVo;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

}