package com.supermap.gaf.project.commontype;

import com.supermap.gaf.project.commontype.enums.DatasourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("数据库连接信息数据对象")
public class DatasourceConnParam {
    @ApiModelProperty(value = "数据源类型")
    private DatasourceTypeEnum datasourceType;
    @ApiModelProperty(value = "主机")
    private String host;
    @ApiModelProperty(value = "端口号")
    private Integer port;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "数据库服务名")
    private String databaseOrServiceName;
    @ApiModelProperty(value = "数据库的组织和结构")
    private String schema;
}
