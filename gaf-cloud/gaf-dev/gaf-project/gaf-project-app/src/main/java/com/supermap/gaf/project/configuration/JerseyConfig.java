package com.supermap.gaf.project.configuration;

import com.supermap.gaf.project.resources.root.ProjRootResource;
import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(ProjRootResource.class);
    }

    @PostConstruct
    public void initSwagger() {
        initBeanConfig();
    }

    public BeanConfig initBeanConfig() {
        BeanConfig config = new BeanConfig();
        config.setTitle("代码模板组件(gaf-project)");
        config.setVersion("v1");
        config.setContact("supermap gaf");
        config.setResourcePackage("com.supermap.gaf.project.resources.root");
        config.setPrettyPrint(true);
        config.setScan(true);
        return config;
    }
}