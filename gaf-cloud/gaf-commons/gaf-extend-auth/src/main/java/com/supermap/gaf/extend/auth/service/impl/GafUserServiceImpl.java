package com.supermap.gaf.extend.auth.service.impl;

import com.supermap.gaf.authority.client.AuthRoleCurrentClient;
import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.extend.auth.service.GafUserService;
import com.supermap.gaf.platform.client.AuthUserCurrentClient;
import com.supermap.gaf.security.RequestUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : duke
 * @since 2022/3/22 9:31 AM
 */
@Service
public class GafUserServiceImpl implements GafUserService {
    @Autowired
    private AuthUserCurrentClient authUserCurrentClient;
    @Autowired
    private AuthRoleCurrentClient authRoleCurrentClient;

    @Override
    public String getUserId() {
        return RequestUtil.getUserId();
    }

    @Override
    public String getUserName() {
        return RequestUtil.getUserName();
    }

    @Override
    public String getTenantId() {
        return RequestUtil.getTenantId();
    }

    @Override
    public AuthUser getUserInfo() {
        if (authUserCurrentClient != null){
            MessageResult<AuthUser> messageResult =  authUserCurrentClient.getUserInfo();
            if (messageResult != null && BooleanUtils.isTrue(messageResult.isSuccessed())){
                return messageResult.getData();
            }
        }
        return null;
    }

    @Override
    public List<AuthRole> getUserRoles() {
        if (authRoleCurrentClient != null){
            MessageResult<List<AuthRole>> messageResult =  authRoleCurrentClient.currentRoleList();
            if (messageResult != null && BooleanUtils.isTrue(messageResult.isSuccessed())){
                return messageResult.getData();
            }
        }
        return null;
    }
}
