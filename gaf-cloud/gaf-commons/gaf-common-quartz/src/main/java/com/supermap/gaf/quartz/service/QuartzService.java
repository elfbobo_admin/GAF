package com.supermap.gaf.quartz.service;

import com.supermap.gaf.quartz.entity.QuartzJobVo;
import org.quartz.*;

import java.util.Date;
import java.util.List;

/**
 * The interface Quartz service.
 *
 * @author : duke
 * @since 2021 /11/8 3:44 PM
 */
public interface QuartzService {

    /**
     * Schedule job date.
     *
     * @param jobDetail the job detail
     * @param trigger   the trigger
     * @return the date
     * @throws SchedulerException the scheduler exception
     */
    Date scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException;

    /**
     * 通过CRON表达式调度任务
     * MISFIRE_INSTRUCTION_DO_NOTHING
     *
     * @param jobBeanClass the job bean class
     * @param jobName      the job name
     * @param groupName    the group name
     * @param cron         the cron
     * @param data         the data
     * @return the string
     */
    String scheduleCronJob(Class<? extends Job> jobBeanClass, String jobName, String groupName, String cron, JobDataMap data) ;


    /**
     * 调度指定时间的任务
     * MISFIRE_INSTRUCTION_DO_NOTHING
     *
     * @param jobBeanClass the job bean class
     * @param jobName      the job name
     * @param groupName    the group name
     * @param startTime    the start time
     * @param data         the data
     * @return the string
     */
    String scheduleDateJob(Class<? extends Job> jobBeanClass, String jobName, String groupName, Date startTime, JobDataMap data) ;

    /**
     * 取消定时任务
     *
     * @param jobName   the job name
     * @param groupName the group name
     * @return true if the Job was found and deleted.
     */
    Boolean deleteScheduleJob(String jobName, String groupName) ;


    /**
     * 暂停定时任务
     *
     * @param jobName   the job name
     * @param groupName the group name
     * @return the boolean
     */
    Boolean pauseScheduleJob(String jobName, String groupName);

    /**
     * 恢复定时任务
     *
     * @param jobName   the job name
     * @param groupName the group name
     * @return the boolean
     */
    Boolean resumeScheduleJob(String jobName, String groupName);

    /**
     * 获取所有任务
     *
     * @param groupName the group name
     * @return jobs
     */
    List<QuartzJobVo> getJobs(String groupName);



}
