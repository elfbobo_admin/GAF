//package com.supermap.gaf.extend.spi.provider;
//
//import com.supermap.gaf.authentication.dao.TenantUserQueryMapper;
//import com.supermap.gaf.authentication.entity.entity.AuthenticationParam;
//import com.supermap.gaf.authentication.entity.entity.AuthenticationResult;
//import com.supermap.gaf.authentication.service.CustomLoginService;
//import com.supermap.gaf.authority.commontype.AuthUser;
//import com.supermap.gaf.extend.commontypes.UserInfo;
//import com.supermap.gaf.extend.spi.ValidateAuthenticationService;
//import com.supermap.gaf.platform.client.AuthUserClient;
//import com.supermap.gaf.platform.entity.TenantUser;
//import com.supermap.gaf.platform.entity.dto.UserTenantDTO;
//import com.supermap.gaf.platform.entity.vo.TenantUserSelectVo;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.util.StringUtils;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author wxl
// * @since 2022/4/13
// */
//public abstract class AbstractValidateAuthenticationServiceProvider implements ValidateAuthenticationService {
//
//    private static final Logger log = LoggerFactory.getLogger(AbstractValidateAuthenticationServiceProvider.class);
//
//
//    public static final String REDIS_LOGIN_SESSION_PREFIX = "login_session_to_user_info:";
//
//    @Autowired
//    private CustomLoginService customLoginService;
//
//    /**
//     * 普通的RestTemplate
//     */
//    @Autowired
//    protected RestTemplate restTemplate;
//
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//
//    @Autowired
//    private AuthUserClient authUserClient;
//
//    @Autowired
//    private TenantUserQueryMapper tenantUserQueryMapper;
//
//    @Override
//    public AuthenticationResult validate(AuthenticationParam authenticationParam) {
//
//        String shortCredential = authenticationParam.getShortCredential();
//        String jwtToken = authenticationParam.getJwtToken();
//
//        AuthenticationResult result = new AuthenticationResult();
//        if (!StringUtils.isEmpty(jwtToken)) {
//            try {
//                Map<String, ?> map = customLoginService.checkJwtToken(jwtToken);
//                if (null != map) {
//                    result.setUserId((String)map.get("user_id"));
//                    result.setUsername((String) map.get("user_name"));
//                    result.setJwtToken(jwtToken);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//
//        if (StringUtils.isEmpty(result.getUsername()) && !StringUtils.isEmpty(shortCredential)) {
//            try {
//                Map<String, String> oAuth2AccessToken = (Map<String, String>) redisTemplate.opsForHash().get(REDIS_LOGIN_SESSION_PREFIX + shortCredential, "token");
//                if (oAuth2AccessToken != null) {
//                    jwtToken = oAuth2AccessToken.get("access_token");
//                    Map<String, ?> map = null;
//                    try {
//                        map = customLoginService.checkJwtToken(jwtToken);
//                        if (null != map) {
//                            result.setUserId((String)map.get("user_id"));
//                            result.setUsername((String) map.get("user_name"));
//                            result.setJwtToken(jwtToken);
//                        }
//                    } catch (Exception e) {
//                        //检查到过期可以使用refreshtoken 刷新token
//                        String refreshToken = oAuth2AccessToken.get("refresh_token");
//                        OAuth2AccessToken refreshOauth2AccessToken = customLoginService.refreshOauth2AccessToken(refreshToken);
//                        //更新redis存储的认证信息
//                        customLoginService.refreshStoreLoginSession(shortCredential, refreshOauth2AccessToken);
//
//                        jwtToken = refreshOauth2AccessToken.getValue();
//                        map = customLoginService.checkJwtToken(jwtToken);
//                        if (null != map) {
//                            result.setUserId((String)map.get("user_id"));
//                            result.setUsername((String) map.get("user_name"));
//                            result.setJwtToken(jwtToken);
//                        }
//                    }
//                } else {
//                    // 根据短凭证获取用户信息
//                    UserInfo userInfo = getUserInfo(shortCredential);
//                    if (userInfo == null) throw new RuntimeException("获取用户信息userinfo失败");
//                    String userName = userInfo.getUserName();
//                    AuthUser authUser = authUserClient.getByUsername(userName).checkAndGetData();
//                    if (authUser == null) {
//                        AuthUser user = new AuthUser();
//                        user.setEmail(userInfo.getEmail());
//                        user.setName(userName);
//                        user.setRealName(userInfo.getAlias());
//                        user.setStatus(true);
//                        authUserClient.save(user).throwExIfFail();
//                    }
//                    OAuth2AccessToken token = customLoginService.createOauth2AccessTokenWithoutPassword(userName);
//                    customLoginService.storeLoginSession(shortCredential,userName,token,null,null,null);
//                    result.setUsername(userName);
//                    result.setJwtToken(token.getValue());
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                //refresh失败，删除
//                redisTemplate.delete(REDIS_LOGIN_SESSION_PREFIX + shortCredential);
//            }
//        }
//
//       if (result.getUserId() != null ) {
//            List<TenantUser> tenantUsers = tenantUserQueryMapper.selectList(TenantUserSelectVo.builder().userId(result.getUserId() ).build());
//            List<String> tenantStringList = new ArrayList<>();
//            for (TenantUser tenantUser : tenantUsers){
//                tenantStringList.add(tenantUser.getTenantId());
//            }
//            result.setTenantList(tenantStringList);
//        }
//
//        return result;
//
//    }
//
//    protected abstract UserInfo getUserInfo(String shortCredential);
//
//
//}
