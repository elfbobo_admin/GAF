package com.supermap.gaf.extend.commontypes;

/**
 * @author wxl
 * @since 2022/4/13
 */
public class UserInfo {

    private String userName;

    private String email;

    private String alias;

    /**
     * 一般设置为false
     */
    private boolean isAdmin;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
