package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authentication.entity.entity.AuthenticationParam;
import com.supermap.gaf.authentication.entity.entity.AuthenticationResult;
import com.supermap.gaf.authentication.service.CustomLoginService;
import com.supermap.gaf.extend.spi.ValidateAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.StringUtils;

import java.util.*;

import static com.supermap.gaf.authentication.entity.constant.LoginConstant.REDIS_LOGIN_SESSION_PREFIX;

/**
 * @author wxl
 * @since 2022/4/13
 */
public class DefaultValidateAuthenticationServiceProvider  implements ValidateAuthenticationService {

    private static final Logger log = LoggerFactory.getLogger(DefaultValidateAuthenticationServiceProvider.class);

    @Autowired
    private CustomLoginService customLoginService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    //@Autowired
    //private TenantUserQueryMapper tenantUserQueryMapper;;
    //
    //@Autowired
    //private TenantQueryMapper tenantQueryMapper;

    public static final List<String> TENANT_LIST = Collections.singletonList("system");

    @Override
    public AuthenticationResult validate(AuthenticationParam authenticationParam) {

        String customSessionId = authenticationParam.getShortCredential();
        String jwtToken = authenticationParam.getJwtToken();

        AuthenticationResult result = new AuthenticationResult();
        if (!StringUtils.isEmpty(jwtToken)) {
            try {
                Map<String, ?> map = customLoginService.checkJwtToken(jwtToken);
                if (null != map) {
                    result.setUserId((String)map.get("user_id"));
                    result.setUsername((String) map.get("user_name"));
                    result.setJwtToken(jwtToken);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (StringUtils.isEmpty(result.getUsername()) && !StringUtils.isEmpty(customSessionId)) {
            try {
                Map<String, String> oAuth2AccessToken = (Map<String, String>) redisTemplate.opsForHash().get(REDIS_LOGIN_SESSION_PREFIX + customSessionId, "token");
                if (oAuth2AccessToken != null) {
                    jwtToken = oAuth2AccessToken.get("access_token");
                    Map<String, ?> map = null;
                    try {
                        map = customLoginService.checkJwtToken(jwtToken);
                        if (null != map) {
                            result.setUserId((String)map.get("user_id"));
                            result.setUsername((String) map.get("user_name"));
                            result.setJwtToken(jwtToken);
                        }
                    } catch (Exception e) {
                        //检查到过期可以使用refreshtoken 刷新token
                        String refreshToken = oAuth2AccessToken.get("refresh_token");
                        OAuth2AccessToken refreshOauth2AccessToken = customLoginService.refreshOauth2AccessToken(refreshToken);
                        //更新redis存储的认证信息
                        customLoginService.refreshStoreLoginSession(customSessionId, refreshOauth2AccessToken);

                        jwtToken = refreshOauth2AccessToken.getValue();
                        map = customLoginService.checkJwtToken(jwtToken);
                        if (null != map) {
                            result.setUserId((String)map.get("user_id"));
                            result.setUsername((String) map.get("user_name"));
                            result.setJwtToken(jwtToken);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                //refresh失败，删除
                redisTemplate.delete(REDIS_LOGIN_SESSION_PREFIX + customSessionId);
            }
        }
        result.setTenantList(TENANT_LIST);
        return result;
    }
}
