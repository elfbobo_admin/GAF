/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.log.context;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import com.supermap.gaf.log.commontypes.FluentdConfig;
import org.fluentd.logger.FluentLogger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * @author:yj
 * @date:2021/3/25
 */
public class FluentdAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {
    public static FluentdConfig cfg;
    private FluentLogger fluentLogger;

    private boolean canUseFluentd = false;
    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public void start() {
        super.start();
        // 定时任务查询fluentd是否可用
        if (cfg != null && cfg.isEnable()) {
            if (fluentLogger == null) {
                fluentLogger = FluentLogger.getLogger(cfg.getTagPrefix(), cfg.getHost(), cfg.getPort());
            }
            startCheckFluentdStatusSchedule();
        }

    }

    /**
     * 周期性许可featureId对应状态验证
     */
    private void startCheckFluentdStatusSchedule() {
        scheduledExecutorService = Executors.newScheduledThreadPool(1, r -> {
                    Thread t = Executors.defaultThreadFactory().newThread(r);
                    t.setDaemon(true);
                    return t;
                }
        );

        scheduledExecutorService.scheduleAtFixedRate(() -> {
            if (fluentLogger != null) {
                fluentLogger.flush();
                if (fluentLogger.isConnected()) {
                    canUseFluentd = true;
                    scheduledExecutorService.shutdown();
                } else {
                    canUseFluentd = false;
                }
            }

        },10, 60, TimeUnit.SECONDS);
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        //if (cfg == null || !cfg.isEnable() || fluentLogger ==  null || !canUseFluentd) return;
        if (fluentLogger ==  null || !canUseFluentd) return;

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("level", eventObject.getLevel().toString());
        data.put("logger", eventObject.getLoggerName());
        data.put("message", eventObject.getMessage());
        data.put("application", cfg.getService());

        fluentLogger.log(cfg.getService(), data);
    }

    @Override
    public void stop() {
        try {
            super.stop();
        } finally {
            try {
                FluentLogger.closeAll();
            } catch (Exception e) {
                // pass
            }
            try {
                if (scheduledExecutorService != null && !scheduledExecutorService.isShutdown()) {
                    scheduledExecutorService.shutdown();
                }
            } catch (Exception e) {
            }
        }
    }
}
