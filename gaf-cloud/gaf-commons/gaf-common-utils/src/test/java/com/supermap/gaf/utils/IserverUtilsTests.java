package com.supermap.gaf.utils;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.data.mgt.entity.iserver.BaseResponse;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.util.IserverUtils;
import com.supermap.services.rest.management.ServiceType;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class IserverUtilsTests {



    @Test
    public void publishNullParamTest(){
        PublishParam publishParam = new PublishParam();
        BaseResponse<List<PublishResult>> re = IserverUtils.publishWorkspace(publishParam,"http://192.168.11.118:30712/iserver",
                true,"cookie","JSESSIONID=34B9B8A51D151395594C782132F6456B; hasplmlang=_int_");
        Assert.assertTrue(!re.getSucceed());
        Assert.assertTrue(re.getError().getCode()==400);
    }

    @Test
    public void publishTest(){
        PublishParam publishParam = new PublishParam();
        publishParam.setWorkspaceConnectionInfo("server=/data-s3fs/457c7508-30dc-4a62-bba9-30b79b7d6f58/datas/yctest.smwu;");
        publishParam.setServicesTypes(new ServiceType[]{ServiceType.RESTDATA});
        BaseResponse<List<PublishResult>> re = IserverUtils.publishWorkspace(publishParam,"http://192.168.11.118:30712/iserver",
                true,"cookie","JSESSIONID=34B9B8A51D151395594C782132F6456B; hasplmlang=_int_");
        System.out.println(JSON.toJSONString(re));
        Assert.assertTrue(re.getSucceed());
        Assert.assertTrue(re.getData().size()==1);
    }

    @Test
    public void publishTileTest(){
        PublishTileParam param = new PublishTileParam();
        param.setServerAdresses(new String[]{"192.168.192.100:30061"});
        param.setServiceTypes(new ServiceType[]{ServiceType.RESTMAP});
        param.setDatabase("test");
        param.setUsername("testadmin");
        param.setPassword("123456");
        BaseResponse<List<PublishResult>> re = IserverUtils.publishMapTile(param,
                "http://192.168.11.118:30712/iserver",
                true,"cookie","JSESSIONID=623A5EE6B832AD3FBF692DEA9FA7B552; hasplmlang=_int_; language=zh; pathType=normal; docRoot=%2Fusr;");
        System.out.println(re.getError().getErrorMsg());
        Assert.assertTrue(re.getSucceed());
    }
    @Test
    public void publish3DTileTest(){
        PublishTileParam param = new PublishTileParam();
        param.setServerAdresses(new String[]{"192.168.192.100:30061"});
        param.setServiceTypes(new ServiceType[]{ServiceType.RESTMAP});
        param.setDatabase("test");
        param.setUsername("testadmin");
        param.setPassword("123456");
        param.setTilesetNames(new String[]{"XZQsg1"});
        BaseResponse<List<PublishResult>> re = IserverUtils.publish3DTile(param,
                "http://192.168.11.118:30712/iserver",
                true,"cookie","JSESSIONID=1E4D5A1B9BD2C41BBB0A17F23C2F8231; hasplmlang=_int_; language=zh; pathType=normal; docRoot=%2Fusr;");
        System.out.println(re.getError().getErrorMsg());
        Assert.assertTrue(re.getSucceed());

    }
}
