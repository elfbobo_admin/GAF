package com.supermap.gaf.commontypes;

import java.util.HashMap;

public class Node extends HashMap {
    private static final String ID_KEY = "id";
    private static final String NAME_KEY = "name";
    public Node(String id, String name){
        super();
        setId(id);
        setName(name);
    }



    public String getId() {
        return (String) get(ID_KEY);
    }

    public void setId(String id) {
        put(ID_KEY,id);
    }

    public String getName() {
        return (String) get(NAME_KEY);
    }

    public void setName(String name) {
        put(NAME_KEY,name);
    }
}
