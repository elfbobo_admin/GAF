package com.supermap.gaf.data.mgt.entity.changeop.dataset.Field;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetVector;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.FieldChangeChain;
import org.apache.ibatis.type.Alias;

@Alias("delete2")
public class Delete extends FieldChangeChain.FieldChangeOp {
    private Delete(FieldChangeChain outer) {
        outer.super();
    }

    @Override
    public void update(Dataset dataset) {
        DatasetVector datasetVector = (DatasetVector) dataset;
        datasetVector.getFieldInfos().remove(getFieldName());
    }

    public static Builder builder(){
        return new Builder();
    }
    public static class Builder extends InnerChangeOpBuilder<Delete,FieldChangeChain> {

        @Override
        public Delete build(){
            return new Delete(parentChain);
        }
    }

}
