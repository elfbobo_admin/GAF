package com.supermap.gaf.data.mgt.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.supermap.data.DatasetType;
import com.supermap.data.Workspace;
import com.supermap.gaf.data.mgt.entity.iserver.*;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import com.supermap.services.rest.management.ServiceType;
import okhttp3.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.Asserts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.supermap.services.rest.management.ServiceType.*;


/**
 * The type Iobject utils.
 */
public class IserverUtils {
    private static final Logger log = LoggerFactory.getLogger(IserverUtils.class);
    private static final MediaType UTF8_JSON = MediaType.parse("application/json;charset=UTF-8");
    /**
     * The Workspace endpoint.
     */
    static final String WORKSPACE_ENDPOINT = "%s/manager/workspaces.json?returnContent=true";
    /**
     * The Providers endpoint.
     */
    static final String PROVIDERS_ENDPOINT = "%s/manager/providers.json";
    /**
     * The Components endpoint.
     */
    static final String COMPONENTS_ENDPOINT = "%s/manager/components.json";
    /**
     * The Client. no time out
     */
    static final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(0,TimeUnit.MINUTES)
            .readTimeout(0, TimeUnit.MINUTES)
            .writeTimeout(0, TimeUnit.MINUTES)
            .followRedirects(false).followSslRedirects(false)
            .build();

    /**
     * The Interface 2 service type.
     */
    static final HashBiMap<String, ServiceType> INTERFACE_2_SERVICE_TYPE = HashBiMap.create(9);

    static {
        INTERFACE_2_SERVICE_TYPE.put("rest", RESTMAP);
        INTERFACE_2_SERVICE_TYPE.put("restjsr", RESTVECTORTILE);
        INTERFACE_2_SERVICE_TYPE.put("wms111", WMS111);
        INTERFACE_2_SERVICE_TYPE.put("wms130", WMTS100);
        INTERFACE_2_SERVICE_TYPE.put("wmts-china", WMTSCHINA);
        INTERFACE_2_SERVICE_TYPE.put("arcgisrest", AGSRESTMAP);
        INTERFACE_2_SERVICE_TYPE.put("baidurest", BAIDUREST);
        INTERFACE_2_SERVICE_TYPE.put("googlerest", GOOGLEREST);
    }


    /**
     * Get enable publish service types set.
     * 暂不支持交通换乘分析服务
     *
     * @param workspace the workspace
     * @return the set
     */
    public static final Set<ServiceType> getPublishOptionalType(Workspace workspace) {

        Set<ServiceType> re = Sets.newHashSet(
                WMS111, WMS130, WFS100,
                WFS200, WMTS100, WMTSCHINA,
                WCS111, WCS112, WPS100);
        Set<DatasetType> datasetTypes = IobjectUtils.getContainDatasetTypes(workspace);
        if (workspace.getMaps().getCount() > 0) {
            re.add(ServiceType.AGSRESTMAP);
            re.add(ServiceType.BAIDUREST);
            re.add(ServiceType.GOOGLEREST);
            re.add(ServiceType.RESTMAP);
        }

        if (workspace.getScenes().getCount() > 0) {
            re.add(ServiceType.RESTREALSPACE);
        }
        if (!CollectionUtils.isEmpty(datasetTypes)) {
            re.add(ServiceType.RESTDATA);
            re.add(ServiceType.AGSRESTDATA);
            re.add(ServiceType.RESTADDRESSMATCH);
            re.add(RESTVECTORTILE);
            List<DatasetType> spatialAnalystNeed = Lists.newArrayList(
                    DatasetType.CAD, DatasetType.GRID, DatasetType.LINE,
                    DatasetType.LINEM, DatasetType.NETWORK, DatasetType.POINT,
                    DatasetType.REGION, DatasetType.TABULAR);

            if (spatialAnalystNeed.stream().anyMatch(datasetTypes::contains)) {
                re.add(ServiceType.RESTSPATIALANALYST);
            }
            if (datasetTypes.contains(DatasetType.NETWORK)) {
                re.add(ServiceType.RESTTRANSPORTATIONANALYST);
                re.add(ServiceType.AGSRESTNETWORKANALYST);
            }
            if (datasetTypes.contains(DatasetType.NETWORK3D)) {
                re.add(ServiceType.REST_NETWORKANALYST3D);
            }
        }
        return re;
    }

    /**
     * Publish workspace list.
     * 暂不支持交通换乘分析服务
     *
     * @param publishParam          the publishParam
     * @param iserverHost           the iserver host
     * @param authInHeader          是否使用header认证，否则使用url param认证
     * @param authInfoNameAndValues the auth info head name and values
     * @return the list
     */
    public static BaseResponse<List<PublishResult>> publishWorkspace(PublishParam publishParam, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        try {
            Asserts.notNull(publishParam.getWorkspaceConnectionInfo(),"workspaceConnectionInfo");
            Asserts.check(!ArrayUtils.isEmpty(publishParam.getServicesTypes()),"servicesTypes至少选择一项");
            // 设置默认delay参数
            if (isDelay(publishParam.getServicesTypes()) && MapUtils.isEmpty(publishParam.getDataProviderDelayCommitSetting())) {
                Map<String, Object> dataProviderDelayCommitSetting = Maps.newHashMap();
                dataProviderDelayCommitSetting.put("enable", false);
                publishParam.setDataProviderDelayCommitSetting(dataProviderDelayCommitSetting);
            }
            String param = GlobalJacksonObjectMapper.instance().writeValueAsString(publishParam);
            log.info("发布工作空间参数:{}",param);
            RequestBody body = RequestBody.create(UTF8_JSON, param);
            String url = String.format(WORKSPACE_ENDPOINT, iserverHost);
            Request request = buildRequestBuilderWithAuthInfo(url, authInHeader, authInfoNameAndValues)
                    .method("POST", body)
                    .build();
            Response response = client.newCall(request).execute();
            BaseResponse re = new BaseResponse();
            String bodyString = response.body().string();
            if (response.isSuccessful()) {
                re.setSucceed(true);
                log.info("发布成功：{}",bodyString);
                re.setData(GlobalJacksonObjectMapper.instance().readValue(bodyString, new TypeReference<List<PublishResult>>() {
                }));
            } else {
                re = GlobalJacksonObjectMapper.instance().readValue(bodyString, BaseResponse.class);
            }
            return re;
        } catch (Exception e) {
            throw new GafException(e);
        }
    }

    /**
     * Publish 3 d tile base response.
     * 不支持文件型
     *
     * @param param                 the param
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<List<PublishResult>> publish3DTile(MongoDBMVTTileProviderConfig param, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = param.getName();
        // 添加provider
        BaseResponse<AddProviderResult> providerResultBaseResponse = addMongoDBRealspaceProvider(param, iserverHost, authInHeader, authInfoNameAndValues);
        if (!providerResultBaseResponse.getSucceed()) {
            return BaseResponse.error(providerResultBaseResponse.getError());
        }
        String providerName = providerResultBaseResponse.getData().getName();
        // 添加对应component
        ComponentParam componentParam = ComponentParam.builder()
                .providers(providerName)
                .interfaceNames("rest")
                .type("com.supermap.services.components.impl.RealspaceImpl")
                .name(name)
                .build();
        BaseResponse<ChildResourceResult> childResourceResultBaseResponse = addComponent(componentParam, iserverHost, authInHeader, authInfoNameAndValues);
        if (!childResourceResultBaseResponse.getSucceed()) {
            log.error("删除服务提供者{}", componentParam.getProviders());
            deleteProvider(Arrays.asList(componentParam.getProviders().split(",")), iserverHost, authInHeader, authInfoNameAndValues);
            return BaseResponse.error(childResourceResultBaseResponse.getError());
        }
        // 解析成服务url返回
        ChildResourceResult childResourceResult = childResourceResultBaseResponse.getData();
        String componentsUrl = childResourceResult.getNewResourceLocation();
        String serviceUrl = convert2ServiceUrl(componentsUrl);
        List<PublishResult> publishResults = new ArrayList<>();
        PublishResult publishResult = new PublishResult();
        publishResult.setServiceAddress(serviceUrl + "/rest");
        log.info("发布成功：{}",publishResult.getServiceAddress());
        publishResult.setServiceType(RESTREALSPACE);
        publishResults.add(publishResult);
        BaseResponse<List<PublishResult>> re = new BaseResponse<>();
        re.setSucceed(true);
        re.setData(publishResults);
        return re;
    }

    /**
     * Publish map tile base response.
     *
     * @param param                 the param
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<List<PublishResult>> publishMapTile(PublishTileParam param, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        Asserts.check(!ArrayUtils.isEmpty(param.getServiceTypes()),"servicesTypes至少选择一项");
        String name = param.getName();
        String[] interfaceNames = getInterfaceNames(param.getServiceTypes());
        if (ArrayUtils.isEmpty(interfaceNames)) {
            throw new GafException("serviceTypes中不存在支持发布的服务类型");
        }
        // 添加provider
        String filePath = param.getFilePath();
        BaseResponse<AddProviderResult> providerResultBaseResponse = null;
        if (!StringUtils.isEmpty(filePath)) {
            if (!filePath.endsWith(".sci")) {
                throw new GafException("当前文件型瓦片不支持除.sci瓦片之外的其他类型");
            }
            Path path = Paths.get(filePath);
            boolean isVector = Files.exists(path.resolveSibling("styles")) && Files.exists(path.resolveSibling("tiles"));
            if (isVector) {
                MVTTileProviderConfig mvtTileProviderConfig = MVTTileProviderConfig.builder()
                        .configFilePath(FilenameUtils.separatorsToUnix(path.toString()))
                        .name(param.getName()).build();
                providerResultBaseResponse = addMVTTileProvider(mvtTileProviderConfig, iserverHost, authInHeader, authInfoNameAndValues);
            } else {
                UGCV5TileProviderConfig ugcv5TileProviderConfig = UGCV5TileProviderConfig.builder()
                        .configFile(FilenameUtils.separatorsToUnix(path.toString()))
                        .name(param.getName()).build();
                providerResultBaseResponse = addUGCV5TileProvider(ugcv5TileProviderConfig, iserverHost, authInHeader, authInfoNameAndValues);
            }
        }else{
            providerResultBaseResponse = addMongoDBMVTTileProvider(param, iserverHost, authInHeader, authInfoNameAndValues);

        }
        if (!providerResultBaseResponse.getSucceed()) {
            return BaseResponse.error(providerResultBaseResponse.getError());
        }

        // 添加对应component
        String providerName = providerResultBaseResponse.getData().getName();
        Map<String, Object> config = new HashMap<>();
        config.put("outputPath", "");
        config.put("outputSite", "");
        config.put("logLevel", "info");
        ComponentParam componentParam = ComponentParam.builder()
                .providers(providerName)
                .interfaceNames(String.join(",", interfaceNames))
                .type("com.supermap.services.components.impl.MapImpl")
                .name(name)
                .config(config)
                .build();
        BaseResponse<ChildResourceResult> childResourceResultBaseResponse = addComponent(componentParam, iserverHost, authInHeader, authInfoNameAndValues);
        if (!childResourceResultBaseResponse.getSucceed()) {
            log.error("删除服务提供者{}", componentParam.getProviders());
            deleteProvider(Arrays.asList(componentParam.getProviders().split(",")), iserverHost, authInHeader, authInfoNameAndValues);
            return BaseResponse.error(childResourceResultBaseResponse.getError());
        }

        // 解析成服务url返回
        ChildResourceResult childResourceResult = childResourceResultBaseResponse.getData();
        String componentsUrl = childResourceResult.getNewResourceLocation();
        String serviceUrl = convert2ServiceUrl(componentsUrl);
        List<PublishResult> publishResults = new ArrayList<>();
        for (String interfaceName : interfaceNames) {
            PublishResult publishResult = new PublishResult();
            publishResult.setServiceAddress(serviceUrl + "/" + interfaceName);
            log.info("发布成功：{}",publishResult.getServiceAddress());
            publishResult.setServiceType(INTERFACE_2_SERVICE_TYPE.get(interfaceName));
            publishResults.add(publishResult);
        }
        BaseResponse<List<PublishResult>> re = new BaseResponse<>();
        re.setSucceed(true);
        re.setData(publishResults);
        return re;
    }

    /**
     * Add ugcv 5 tile provider base response.
     *
     * @param config                the config
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<AddProviderResult> addUGCV5TileProvider(UGCV5TileProviderConfig config, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = config.getName();
        if (StringUtils.isEmpty(name)) {
            name = "gaf-" + UUID.randomUUID().toString().replace("-", "");
        }
        String jsonParam = buildProviderJsonParam(name, "com.supermap.services.providers.UGCV5TileProvider", config);
        BaseResponse<AddProviderResult> re = addProvider(jsonParam, iserverHost, authInHeader, authInfoNameAndValues);
        re.setData(new AddProviderResult(name));
        return re;
    }

    /**
     * Add mvt tile provider base response.
     *
     * @param config                the config
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<AddProviderResult> addMVTTileProvider(MVTTileProviderConfig config, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = config.getName();
        if (StringUtils.isEmpty(name)) {
            name = "gaf-mvttile" + UUID.randomUUID().toString().replace("-", "");
        }
        String jsonParam = buildProviderJsonParam(name, "com.supermap.services.providers.MVTTileProvider", config);
        BaseResponse<AddProviderResult> re = addProvider(jsonParam, iserverHost, authInHeader, authInfoNameAndValues);
        re.setData(new AddProviderResult(name));
        return re;
    }

    /**
     * Add mongo db realspace provider base response.
     *
     * @param config                the config
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<AddProviderResult> addMongoDBRealspaceProvider(MongoDBMVTTileProviderConfig config, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = config.getName();
        if (StringUtils.isEmpty(name)) {
            name = "gaf-3D-mongodb" + UUID.randomUUID().toString().replace("-", "");
        }
        String jsonParam = buildProviderJsonParam(name, "com.supermap.services.providers.MongoDBRealspaceProvider", config);
        BaseResponse<AddProviderResult> re = addProvider(jsonParam, iserverHost, authInHeader, authInfoNameAndValues);
        re.setData(new AddProviderResult(name));
        return re;
    }

    /**
     * Add mongo dbmvt tile provider base response.
     *
     * @param config                the config
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<AddProviderResult> addMongoDBMVTTileProvider(MongoDBMVTTileProviderConfig config, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        String name = config.getName();
        if (StringUtils.isEmpty(name)) {
            name = "gaf-mongodb" + UUID.randomUUID().toString().replace("-", "");
        }
        String jsonParam = buildProviderJsonParam(name, "com.supermap.services.providers.MongoDBMVTTileProvider", config);
        BaseResponse<AddProviderResult> re = addProvider(jsonParam, iserverHost, authInHeader, authInfoNameAndValues);
        re.setData(new AddProviderResult(name));
        return re;
    }

    /**
     * Add provider base response.
     *
     * @param jsonBody              the json body
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse addProvider(String jsonBody, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        try {
            log.info("向{}添加服务提供者。params:{}", iserverHost, jsonBody);
            RequestBody body = RequestBody.create(UTF8_JSON, jsonBody);
            String url = String.format(PROVIDERS_ENDPOINT, iserverHost);
            Request request = buildRequestBuilderWithAuthInfo(url, authInHeader, authInfoNameAndValues)
                    .method("POST", body)
                    .addHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8")
                    .build();
            Response response = client.newCall(request).execute();
            BaseResponse re = new BaseResponse();
            String bodyString = response.body().string();
            if (response.isSuccessful()) {
                log.info("向{}添加服务提供者成功",iserverHost);
                re.setSucceed(true);
            } else {
                re = GlobalJacksonObjectMapper.instance().readValue(bodyString, BaseResponse.class);
            }
            return re;
        } catch (Exception e) {
            e.printStackTrace();
            ErrorState errorState = new ErrorState();
            errorState.setCode(500);
            errorState.setErrorMsg(e.getMessage());
            return BaseResponse.error(errorState);
        }
    }

    /**
     * Add component base response.
     *
     * @param componentParam        the component param
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the base response
     */
    public static BaseResponse<ChildResourceResult> addComponent(ComponentParam componentParam, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        if (StringUtils.isEmpty(componentParam.getName())) {
            componentParam.setName(componentParam.getProviders() + "-" + System.currentTimeMillis());
        }
        String jsonBody = null;
        try {
            jsonBody = GlobalJacksonObjectMapper.instance().writeValueAsString(componentParam);
            log.info("向{}添加服务组件。params:{}", iserverHost, jsonBody);
            RequestBody body = RequestBody.create(UTF8_JSON, jsonBody);
            String url = String.format(COMPONENTS_ENDPOINT, iserverHost);
            Request request = buildRequestBuilderWithAuthInfo(url, authInHeader, authInfoNameAndValues)
                    .method("POST", body)
                    .addHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8")
                    .build();
            Response response = client.newCall(request).execute();
            BaseResponse re = new BaseResponse();
            String bodyString = response.body().string();
            if (response.isSuccessful()) {
                log.info("向{}添加服务组件成功", iserverHost);
                re.setSucceed(true);
                re.setData(GlobalJacksonObjectMapper.instance().readValue(bodyString, new TypeReference<ChildResourceResult>() {
                }));
            } else {
                re = GlobalJacksonObjectMapper.instance().readValue(bodyString, BaseResponse.class);
            }
            return re;
        }catch (Exception e) {
            e.printStackTrace();
            throw new GafException("添加服务组件失败:" + e.getMessage());
        }
    }

    /**
     * Is delay boolean.
     *
     * @param servicesTypes the services types
     * @return the boolean
     */
    static boolean isDelay(ServiceType[] servicesTypes) {
        if (servicesTypes == null) {
            return false;
        }
        Set<ServiceType> set = Sets.newHashSet(servicesTypes);
        List<ServiceType> delayTypes = Lists.newArrayList(RESTDATA, AGSRESTDATA, WFS100, WFS200, WCS111, WCS112);
        return delayTypes.stream().anyMatch(set::contains);
    }

    /**
     * Delete provider.
     *
     * @param names                 the names
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     */
    static void deleteProvider(Collection<String> names, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        try {
            RequestBody body = RequestBody.create(UTF8_JSON, GlobalJacksonObjectMapper.instance().writeValueAsString(names));
            String url = String.format(PROVIDERS_ENDPOINT, iserverHost);
            Request request = buildRequestBuilderWithAuthInfo(url, authInHeader, authInfoNameAndValues)
                    .method("PUT", body)
                    .build();
            client.newCall(request).execute();
        } catch (Exception e) {
            throw new GafException(e);
        }
    }

    /**
     * Delete components.
     *
     * @param names                 the names
     * @param iserverHost           the iserver host
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     */
    static void deleteComponents(Collection<String> names, String iserverHost, boolean authInHeader, String... authInfoNameAndValues) {
        try {
            RequestBody body = RequestBody.create(UTF8_JSON, GlobalJacksonObjectMapper.instance().writeValueAsString(names));
            String url = String.format(COMPONENTS_ENDPOINT, iserverHost);
            Request request = buildRequestBuilderWithAuthInfo(url, authInHeader, authInfoNameAndValues)
                    .method("PUT", body)
                    .build();
            client.newCall(request).execute();
        } catch (Exception e) {
            throw new GafException(e);
        }
    }

    /**
     * Convert 2 service url string.
     *
     * @param componentUrl the component url
     * @return the string
     */
    static String convert2ServiceUrl(String componentUrl) {
        String re = componentUrl.replace("/manager", "");
        re = re.replace("components", "services");
        re = re.replace(".json", "");
        return re;
    }

    /**
     * Get interface names string [ ].
     *
     * @param serviceTypes the service types
     * @return the string [ ]
     */
    static String[] getInterfaceNames(ServiceType[] serviceTypes) {
        Set<String> re = new HashSet<>();
        for (ServiceType item : serviceTypes) {
            String interfaceName = INTERFACE_2_SERVICE_TYPE.inverse().get(item);
            if (interfaceName != null) {
                re.add(interfaceName);
            }
        }
        return re.toArray(new String[re.size()]);
    }

    /**
     * Build request builder with auth info request . builder.
     *
     * @param url                   the url
     * @param authInHeader          the auth in header
     * @param authInfoNameAndValues the auth info name and values
     * @return the request . builder
     * @throws URISyntaxException the uri syntax exception
     */
    static Request.Builder buildRequestBuilderWithAuthInfo(String url, boolean authInHeader, String... authInfoNameAndValues) throws URISyntaxException {
        Request.Builder re = new Request.Builder()
                .url(url)
                .headers(Headers.of(authInfoNameAndValues));
        if (authInfoNameAndValues != null) {
            if (authInHeader) {
                re.headers(Headers.of(authInfoNameAndValues));
            } else {
                URIBuilder uriBuilder = new URIBuilder(url);
                for (int i = 0; i < authInfoNameAndValues.length; i += 2) {
                    uriBuilder.addParameter(authInfoNameAndValues[i], authInfoNameAndValues[i + 1]);
                }
                re.url(uriBuilder.build().toString());
            }
        }
        return re;
    }

    /**
     * Build provider json param string.
     *
     * @param name   the name
     * @param type   the type
     * @param config the config
     * @return the string
     */
    static String buildProviderJsonParam(String name, String type, Object config) {
        try {
            Map<String, Object> param = new HashMap<>(4);
            param.put("config", config);
            param.put("name", name);
            param.put("type", type);
            param.put("enable", true);
            return GlobalJacksonObjectMapper.instance().writeValueAsString(param);
        } catch (Exception e) {
            throw new GafException(e);
        }

    }



}