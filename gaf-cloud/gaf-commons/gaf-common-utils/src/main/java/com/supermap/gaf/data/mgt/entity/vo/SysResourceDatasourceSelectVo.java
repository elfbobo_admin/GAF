/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
*/
package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.util.Date;
import java.util.List;

/**
 * 数据源 条件查询实体
 *
 * @date:2021/3/25
 * @author wangxiaolong
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据源 条件查询实体")
@Data
public class SysResourceDatasourceSelectVo {
	@QueryParam("startTimeStamp")
	@ApiParam("开始时间戳")
	@ApiModelProperty("开始时间戳")
	private Long startTimeStamp;
	@ApiModelProperty("数据源id")
	private String datasourceId;
	@QueryParam("endTimeStamp")
	@ApiParam("结束时间戳")
	@ApiModelProperty("结束时间戳")
	private Long endTimeStamp;
	@QueryParam("isSdx")
	@ApiParam("是否是空间数据源")
	@ApiModelProperty("是否是空间数据源")
	private Boolean isSdx;
	@DefaultValue("false")
	@QueryParam("isTemplate")
	@ApiParam("是否是空间数据源模板")
	@ApiModelProperty("是否是空间数据源模板")
	private Boolean isTemplate = false;
	@DefaultValue("false")
	@QueryParam("isStandard")
	@ApiModelProperty("是否标准数据源。true:是，false:否。")
	private Boolean isStandard = false;
	@QueryParam("catalogCode")
	@ApiParam("数据源分类")
	@ApiModelProperty("数据源分类")
	private String catalogCode;
	@QueryParam("addr")
	@ApiParam("数据源地址")
	@ApiModelProperty("数据源地址")
	private String addr;
	@QueryParam("typeCodes")
	@ApiParam("数据源类型集合")
	@ApiModelProperty("数据源类型集合")
	private List<String> typeCodes;
	@QueryParam("metadata")
	@ApiParam("元数据文件路径")
	@ApiModelProperty("数据源类元数据文件路径")
	private String metadata;

	@QueryParam("searchFieldName")
	@ApiModelProperty("查询字段名")
	@StringRange(entityClass = SysResourceDatasource.class,message = "不在指定的字段名范围内")
    private String searchFieldName;
	@QueryParam("searchFieldValue")
	@ApiModelProperty("查询字段值")
	private String searchFieldValue;
	@QueryParam("orderFieldName")
	@ApiModelProperty("排序字段名")
	@StringRange(entityClass = SysResourceDatasource.class,message = "不在指定的字段名范围内")
    private String orderFieldName;
	@QueryParam("orderMethod")
	@ApiModelProperty("排序方法")
	@StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;

	/**
	 * 计算属性 将endTimeStamp时间戳转为Date
	 *
	 * @return
	 */
	public Date getEndTime() {
		return endTimeStamp == null ? null : new Date(this.endTimeStamp);
	}

	/**
	 * 计算属性 将startTimeStamp时间戳转为Date
	 *
	 * @return
	 */
	public Date getStartTime() {
		return startTimeStamp == null ? null : new Date(this.startTimeStamp);
	}
}
