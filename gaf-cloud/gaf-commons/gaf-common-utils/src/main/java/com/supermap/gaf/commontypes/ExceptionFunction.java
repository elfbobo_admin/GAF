package com.supermap.gaf.commontypes;


@FunctionalInterface
public interface ExceptionFunction<T, R> {
    R apply(T t) throws RuntimeException;
}
