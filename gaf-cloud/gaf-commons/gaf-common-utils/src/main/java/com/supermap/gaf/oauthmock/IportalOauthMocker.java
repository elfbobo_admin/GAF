package com.supermap.gaf.oauthmock;

import com.alibaba.fastjson.JSONObject;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;
import java.util.function.Function;

/**
 * The type Iportal oauth mocker.
 *
 * @author kb
 */
public class IportalOauthMocker {
    private static final Logger log = LoggerFactory.getLogger(IportalOauthMocker.class);
    /**
     * The Oauth login start endpoint.
     */
    static final String OAUTH_LOGIN_START_ENDPOINT = "%s/services/security/login/GAF?configID=%s";
    /**
     * The Client.
     */
    static final OkHttpClient client = new OkHttpClient().newBuilder().followRedirects(false).followSslRedirects(false)
            .build();
    /**
     * The Cas client host.
     */
    String casClientHostAndContextPath;
    /**
     * The App oauth config id.
     */
    String appOauthConfigId;

    /**
     * Instantiates a new Iportal oauth mocker.
     *
     * @param casClientHostAndContextPath    the cas client host
     * @param appOauthConfigId the app oauth config id
     */
    public IportalOauthMocker(String casClientHostAndContextPath, String appOauthConfigId) {
        this.casClientHostAndContextPath = casClientHostAndContextPath;
        this.appOauthConfigId = appOauthConfigId;
    }

  /*  public <T> T call(Headers casAuthHeader, Function<Headers,T> handler) throws IOException, UninitializedUserException {
        Headers appAuthHeader = login(casAuthHeader);
        return handler.apply(appAuthHeader);
    }
*/

    /**
     * Cas auth info valid boolean.
     *
     * @param authorizeEndpointRes the authorize endpoint res
     * @return the boolean
     */
    boolean casAuthInfoValid(Response authorizeEndpointRes){
        String location = authorizeEndpointRes.header("Location");
        if(location.contains("/viewer/#/authentication/login")){
            return false;
        }
        return true;
    }


    /**
     * 处理oauth2 callback响应，判断用户是否有效（注册）并解析出iportal登录认证信息头部返回。之后发起请求会携带这些头部信息
     *
     * @param callBackEndpointRes      the call back endpoint res
     * @param cookie                   the cookie
     * @param userUninitializedHandler the user uninitialized handler
     * @return headers
     * @throws UninitializedUserException 用户在iportal还没初始化
     * @throws IOException                the io exception
     */
    Headers handlerCallbackResponse(Response callBackEndpointRes, String cookie, Function<Headers,Boolean> userUninitializedHandler) throws UninitializedUserException, IOException {
        String location = callBackEndpointRes.header("Location");
        Headers re = Headers.of("Cookie",cookie);
        if(location.contains("/login/gaf/first")){
            if(userUninitializedHandler == null || !userUninitializedHandler.apply(re)){
                throw new UninitializedUserException("用户在iportal还没初始化,或初始化失败");
            }
        }
        return re;
    }


    /**
     * Login headers.
     *
     * @param casAuthHeader the cas auth header
     * @return the headers
     * @throws IOException                the io exception
     * @throws UninitializedUserException the uninitialized user exception
     */
    public Headers login(Headers casAuthHeader) throws IOException, UninitializedUserException {
        return login(casAuthHeader,null);
    }

    /**
     * 登录到cas client app获取app用户登录验证票据（cookie或其他头部）
     *
     * @param casAuthHeader            the cas auth header
     * @param userUninitializedHandler the user uninitialized handler
     * @return headers
     * @throws IOException                the io exception
     * @throws UninitializedUserException the uninitialized user exception
     */
    public Headers login(Headers casAuthHeader, Function<Headers,Boolean> userUninitializedHandler) throws IOException, UninitializedUserException {
        String oauthLoginStartUrl = String.format(OAUTH_LOGIN_START_ENDPOINT,casClientHostAndContextPath,appOauthConfigId);
        Request request = new Request.Builder()
                .url(oauthLoginStartUrl)
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();
        String location =  response.header("Location");
        if (!response.isRedirect() || location == null) {
            throw new AuthenticationException("未知错误，可能发生在 "+oauthLoginStartUrl+" endpoint");
        }
        String authorizeEndpoint = location;
        String iportalCookie = response.header("Set-Cookie");


        //  authorize request
        Request.Builder requestBuilder = new Request.Builder()
                .url(authorizeEndpoint)
                .method("GET", null);
        for(String name:casAuthHeader.names()){
            requestBuilder.addHeader(name,casAuthHeader.get(name));
        }
        Response authorizeEndpointRes = client.newCall(requestBuilder.build()).execute();
        location =  authorizeEndpointRes.header("Location");
        if (!response.isRedirect() || location==null) {
            throw new AuthenticationException("未知错误，可能发生在authorize endpoint");
        }
        if(!casAuthInfoValid(authorizeEndpointRes)){
            throw new AuthenticationException("当前认证信息无效");
        }

        // callback request
        requestBuilder = new Request.Builder().url(location)
                .header("Cookie",iportalCookie).method("GET",null);
        response = client.newCall(requestBuilder.build()).execute();
        if (!response.isRedirect()) {
            throw new AuthenticationException("未知错误，可能发生在callback endpoint");
        }
        String cookie2 = response.header("Set-Cookie");
        iportalCookie +=(cookie2==null?"":";"+cookie2);
        return handlerCallbackResponse(response,iportalCookie,userUninitializedHandler);
    }

    /**
     * The type Init iportal oauth user.
     */
    public class InitIportalOauthUser implements Function<Headers,Boolean> {
        private static final String OAUTH_ENDPOINT = "%s/services/security/oauth2.json";
        private static final String ACOUNT_BASIC_ENDPOINT = "%s/web/mycontent/account/basic.json";
        private String username;

        private String nickName;

        /**
         * Instantiates a new Init iportal oauth user.
         *
         * @param username the username
         */
        public InitIportalOauthUser(String username) {
            this.username = username;
        }

        /**
         * Instantiates a new Init iportal oauth user.
         *
         * @param username the username
         * @param nickName the nick name
         */
        public InitIportalOauthUser(String username, String nickName) {
            this.username = username;
            this.nickName = nickName;
        }

        @Override
        public Boolean apply(Headers headers){
            boolean re = false;
            try {
                autoRegister(headers);
                re = true;
                modifyNickName(headers);
            } catch (IOException e) {
                log.error("iportal注册GAF关联用户{}失败：{}",username,e.getMessage());
            }
            return re;
        }

        /**
         * Modify nick name.
         *
         * @param headers the headers
         * @throws IOException the io exception
         */
        void modifyNickName(Headers headers) throws IOException {
            if(StringUtils.isEmpty(this.nickName)){
                return;
            }
            MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
            JSONObject object = new JSONObject();
            object.put("nickname",this.nickName);
            RequestBody body = RequestBody.create(mediaType, object.toJSONString());
            Request.Builder requestBuilder = new Request.Builder()
                    .url(String.format(ACOUNT_BASIC_ENDPOINT,casClientHostAndContextPath))
                    .method("PUT", body);
            for(String name:headers.names()){
                requestBuilder.addHeader(name,headers.get(name));
            }
            Response response = client.newCall(requestBuilder.build()).execute();
            if(!response.isSuccessful()){
                log.error("iportal修改GAF关联用户{}的昵称失败",username);
            }
        }

        /**
         * Auto register.
         *
         * @param headers the headers
         * @throws IOException the io exception
         */
        void autoRegister(Headers headers) throws IOException {
            MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
            JSONObject object = new JSONObject();
            object.put("openID",this.username);
            object.put("loginType","GAF");
            RequestBody body = RequestBody.create(mediaType, object.toJSONString());
            Request.Builder requestBuilder = new Request.Builder()
                    .url(String.format(OAUTH_ENDPOINT,casClientHostAndContextPath))
                    .method("POST", body);
            for(String name:headers.names()){
                requestBuilder.addHeader(name,headers.get(name));
            }
            Response response = client.newCall(requestBuilder.build()).execute();
            if(!response.isSuccessful()){
                throw new AuthenticationException("用户初始化失败，发生在services/security/oauth2 endpoint");
            }
        }
    }


    public static void main(String[] args) throws IOException, UninitializedUserException {
        IportalOauthMocker mocker = new IportalOauthMocker("http://iportal.gaf-dev.chengdu/","222");

        Headers headers = mocker.login(Headers.of("Cookie","CUSTOM_SESSION_ID=360049ba463d443983be46a24a0f4bb6; JSESSIONID=167989C285DE8938D75C167CA3E6D19C"),mocker.new InitIportalOauthUser("SYS","平台基础数据管理者"));

    }



}
