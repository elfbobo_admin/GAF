package com.supermap.gaf.commontypes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KgGraph {
    private List<Node> nodes;
    private List<Link> links;
}
