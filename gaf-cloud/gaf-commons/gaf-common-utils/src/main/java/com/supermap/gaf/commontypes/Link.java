package com.supermap.gaf.commontypes;

import java.util.HashMap;

public class Link extends HashMap {
    private static final String SOURCE_KEY = "source";
    private static final String TARGET_KEY = "target";
    public Link(String source, String target){
        super();
        setSource(source);
        setTarget(target);
    }

    public String getSource() {
        return (String) get(SOURCE_KEY);
    }

    public void setSource(String source) {
        put(SOURCE_KEY,source);
    }

    public String getTarget() {
        return (String) get(TARGET_KEY);
    }

    public void setTarget(String target) {
        put(TARGET_KEY,target);
    }
}
