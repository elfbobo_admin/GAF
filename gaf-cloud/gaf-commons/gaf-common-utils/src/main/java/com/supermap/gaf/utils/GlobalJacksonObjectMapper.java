package com.supermap.gaf.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GlobalJacksonObjectMapper {

    private static ObjectMapper objectMapper = new ObjectMapper();
    static {
        //忽略 在json字符串中存在，但是在java对象中不存在对应属性的情况。防止错误
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    }
    public static ObjectMapper instance(){
        return objectMapper;
    }
}
