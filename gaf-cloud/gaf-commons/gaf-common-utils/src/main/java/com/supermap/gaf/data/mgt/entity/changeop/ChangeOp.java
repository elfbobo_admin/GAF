package com.supermap.gaf.data.mgt.entity.changeop;

import com.supermap.data.Datasource;

/**
 * The interface Change op.
 */
public interface ChangeOp {
    /**
     * Update.
     *
     * @param datasource the datasource
     */
    void update(Datasource datasource);

}
