package com.supermap.gaf.data.mgt.entity.iserver;

import lombok.Data;

@Data
public class ChildResourceResult {
    private String newResourceLocation;
}
