package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.Dataset;
import com.supermap.data.Datasource;
import com.supermap.data.EncodeType;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import org.apache.http.util.Asserts;


/**
 * The type Create new Dataset from template.
 */
public class CreateFromTemplate extends DatasetChangeChain.DatasetChangeOp {

    private Dataset templateDataset;

    private CreateFromTemplate(DatasetChangeChain outer, Dataset templateDataset){
        outer.super();
        this.templateDataset = templateDataset;
    }
    @Override
    public void update(Datasource datasource) {
        datasource.getDatasets().createFromTemplate(getDatasetName(),templateDataset);
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<CreateFromTemplate,DatasetChangeChain> {
        private Dataset templateDataset;

        /**
         * Template dataset builder.
         *
         * @param templateDataset the template dataset
         * @return the builder
         */
        public Builder templateDataset(Dataset templateDataset){
            this.templateDataset = templateDataset;
            return this;
        }

        @Override
        public CreateFromTemplate build(){
            Asserts.notNull(templateDataset,"Builder.templateDataset(xx) required");
            return new CreateFromTemplate(parentChain, templateDataset);
        }
    }
}
