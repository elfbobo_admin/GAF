package com.supermap.gaf.data.mgt.entity.iserver;

import com.supermap.services.rest.management.ServiceType;
import lombok.Data;

@Data
public class PublishTileParam extends MongoDBMVTTileProviderConfig {
    private ServiceType[] serviceTypes;
    private String filePath;
}
