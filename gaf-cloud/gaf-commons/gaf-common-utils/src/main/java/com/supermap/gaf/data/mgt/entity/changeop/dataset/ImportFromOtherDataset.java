package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.*;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.FieldMapping;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.http.util.Asserts;

import javax.validation.constraints.AssertTrue;
import javax.ws.rs.NotFoundException;
import java.nio.file.Files;
import java.util.List;


/**
 * The type Import from other dataset.
 */
public class ImportFromOtherDataset extends DatasetChangeChain.DatasetChangeOp {

    private DatasetVector srcDataset;

    private FieldMapping[] fieldMappings;

    private ImportFromOtherDataset(DatasetChangeChain outer, DatasetVector srcDataset,FieldMapping... fieldMappings){
        outer.super();
        this.srcDataset = srcDataset;
        this.fieldMappings = fieldMappings;
    }
    @Override
    public void update(Datasource datasource) {
        Recordset recordset = srcDataset.getRecordset(false, CursorType.STATIC);
        Dataset dataset = datasource.getDatasets().get(getDatasetName());
        if (!(dataset instanceof DatasetVector)) {
            throw new IllegalArgumentException(dataset.getName()+"数据集类型不支持Recordset Import操作,要求DatasetVector");
        }
        try{
            dataset.open();
            String[] srcFields = new String[fieldMappings.length];
            String[] destFields = new String[fieldMappings.length];
            for (int i = 0; i < fieldMappings.length; i++) {
                FieldMapping item = fieldMappings[i];
                srcFields[i] = item.getSrcName();
                destFields[i] = item.getDestName();
                existsField(srcDataset,srcFields[i]);
                existsField((DatasetVector) dataset,destFields[i]);
            }
            boolean re = ((DatasetVector) dataset).append(recordset,srcFields,destFields);
            if(!re){
                throw new RuntimeException("导入数据失败");
            }
        }finally {
            if(dataset.isOpen()){
                dataset.close();
            }
        }
    }

    /**
     *
     * @param datasetVector
     * @param fieldName
     * @throws RuntimeException 字段不存在抛出异常
     */
    void existsField(DatasetVector datasetVector,String fieldName){
       if(datasetVector.getFieldInfos().get(fieldName) == null){
           throw new RuntimeException(String.format("数据集%s不存在字段%s",datasetVector.getName(),fieldName));
       }
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<ImportFromOtherDataset,DatasetChangeChain> {
        private DatasetVector srcDataset;

        private FieldMapping[] fieldMappings;

        /**
         * Src dataset builder.
         *
         * @param srcDataset the src dataset
         * @return the builder
         */
        public Builder srcDataset(DatasetVector srcDataset){
            this.srcDataset = srcDataset;
            return this;
        }

        /**
         * Field mappings builder.
         *
         * @param fieldMappings the field mappings
         * @return the builder
         */
        public Builder fieldMappings(FieldMapping... fieldMappings){
            this.fieldMappings = fieldMappings;
            return this;
        }


        @Override
        public ImportFromOtherDataset build(){

            Asserts.notNull(srcDataset,"Builder.srcDataset(xx)");
            Asserts.check(fieldMappings!=null && fieldMappings.length>0,"Builder.fieldMappings(xx...) require not empty array");
            return new ImportFromOtherDataset(parentChain, srcDataset,fieldMappings);
        }
    }
}
