package com.supermap.gaf.data.mgt.entity.changeop;

import com.supermap.data.Datasource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Change op chain.
 */
public class ChangeOpChain implements ChangeOp{
    private static Logger log = LoggerFactory.getLogger(ChangeOpChain.class);

    private List<ChangeOp> ops = new ArrayList<>();

    @Override
    public void update(Datasource datasource) {
        for(ChangeOp op:ops){
            log.info("执行{}",op.getClass());
            op.update(datasource);
        }
    }

    /**
     * Add op change op chain.
     *
     * @param op the op
     * @return the change op chain
     */
    public ChangeOpChain addOp(ChangeOp op){
        ops.add(op);
        return this;
    }


    /**
     * Add op change op chain.
     *
     * @param builder the builder
     * @return the change op chain
     */
    public ChangeOpChain addOp(InnerChangeOpBuilder builder){
        builder.parentChain(this);
        addOp(builder.build());
        return this;
    }




}
