package com.supermap.gaf.security.spring;


import java.util.HashMap;
import java.util.Map;

/**
 * @author : duke
 * @since 2022/3/9 7:51 PM
 */
public class GafContextHolder {
    private static final ThreadLocal<Map<String,Object>> GAF_CONTEXT = new ThreadLocal<Map<String,Object>>();

    public static void put(String key, Object o) {
        Map<String,Object> context = GAF_CONTEXT.get();
        if (null == context){
            context = new HashMap<>(16);
        }
        context.put(key, o);
        GAF_CONTEXT.set(context);
    }
    public static Object get(String key) {
        Map<String,Object> context = GAF_CONTEXT.get();
        if (context == null){
            return null;
        }
        return context.get(key);
    }
    public static Map<String,Object> getGafContext() {
        return GAF_CONTEXT.get();
    }

    public static void remove() {
        GAF_CONTEXT.remove();
    }

}
