/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.service.impl;

import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.dao.AuthUserSupplementQueryMapper;
import com.supermap.gaf.authority.service.AuthUserSupplementQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author dqc
 * @date:2021/3/25 服务类
 */
@Service
public class AuthUserSupplementQueryServiceImpl implements AuthUserSupplementQueryService {
    @Autowired
    private AuthUserSupplementQueryMapper authUserSupplementQueryMapper;


    /**
     * id查询
     *
     * @return
     */
    @Override
    public AuthUserSupplement getById(String userId) {
        return authUserSupplementQueryMapper.select(userId);
    }

    @Override
    public AuthUserSupplement getByUserName(String username) {
        return authUserSupplementQueryMapper.getByUserName(username);
    }


}
