/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.extend.commontypes;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author : duke
 * @since 2020/11/23 11:01 AM
 */
@ApiModel("认证参数对象")
public class AuthenticationParam implements Serializable {
    private static final long serialVersionUID = -3977280069205979450L;
    //customSessionId
    @ApiModelProperty(value = "较短的凭证信息,能够通过该凭证获取对应关联的用户信息或者jwtToken,例如http请求头中包含的token或者cookie中包含的自定义sessionId")
    private String shortCredential;
    // authorization
    @ApiModelProperty("jwt,可直接验签解析的jwt,例如http请求头中包含的jwt")
    private String jwtToken;

    public AuthenticationParam() {
    }

    public String getShortCredential() {
        return shortCredential;
    }

    public void setShortCredential(String shortCredential) {
        this.shortCredential = shortCredential;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
