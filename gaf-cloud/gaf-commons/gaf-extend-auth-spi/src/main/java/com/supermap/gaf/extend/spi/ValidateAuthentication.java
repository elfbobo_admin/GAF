package com.supermap.gaf.extend.spi;


import com.supermap.gaf.extend.commontypes.AuthenticationParam;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.extend.commontypes.MessageResult;


/**
 * 校验认证状态,返回认证后的用户等信息
 */
public interface ValidateAuthentication {
    /**
     * 校验认证状态,返回认证信息
     *
     * @param param
     * @return 用户等信息 包括userId (用户id) username(用户名) jwtToken(jwt) tenantList(租户id列表)
     */
    MessageResult<AuthenticationResult> doValidateAuthentication(AuthenticationParam param);

}
