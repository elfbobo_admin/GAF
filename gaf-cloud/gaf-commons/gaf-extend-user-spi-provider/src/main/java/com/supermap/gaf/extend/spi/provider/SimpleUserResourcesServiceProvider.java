package com.supermap.gaf.extend.spi.provider;

import com.alibaba.fastjson.JSONObject;
import com.supermap.gaf.extend.commontypes.Menu;
import com.supermap.gaf.extend.spi.UserResourcesService;

import java.util.List;


/**
 * @author wxl
 * @since 2022/4/7
 */
public class SimpleUserResourcesServiceProvider implements UserResourcesService {

    @Override
    public List<Menu> listMenus() {
        String text = "[{\n" +
                "\t\t\t\"icon\": \"database\",\n" +
                "\t\t\t\"id\": \"bbfc6f9d-23dc-4d0b-99e4-982f48174a7e\",\n" +
                "\t\t\t\"name\": \"租户资源管理\",\n" +
                "\t\t\t\"order\": 1,\n" +
                "\t\t\t\"path\": \"/platform/TenantResource\",\n" +
                "\t\t\t\"pid\": \"c5ddae25-8235-413f-8292-66455c39df26\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}, {\n" +
                "\t\t\t\"icon\": \"user\",\n" +
                "\t\t\t\"id\": \"8e0afd96-ac5f-4706-af85-8fef7c6f864d\",\n" +
                "\t\t\t\"name\": \"用户管理\",\n" +
                "\t\t\t\"order\": 3,\n" +
                "\t\t\t\"path\": \"/platform/AuthUser\",\n" +
                "\t\t\t\"pid\": \"c5ddae25-8235-413f-8292-66455c39df26\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}, {\n" +
                "\t\t\t\"icon\": \"bank\",\n" +
                "\t\t\t\"id\": \"cf045e17-a929-470d-8f2a-876ba823cb18\",\n" +
                "\t\t\t\"name\": \"租户管理\",\n" +
                "\t\t\t\"order\": 2,\n" +
                "\t\t\t\"path\": \"/platform/MultiTenant\",\n" +
                "\t\t\t\"pid\": \"c5ddae25-8235-413f-8292-66455c39df26\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}, {\n" +
                "\t\t\t\"icon\": \"usergroup-add\",\n" +
                "\t\t\t\"id\": \"c5ddae25-8235-413f-8292-66455c39df26\",\n" +
                "\t\t\t\"name\": \"租户维护\",\n" +
                "\t\t\t\"order\": 1,\n" +
                "\t\t\t\"path\": \"\",\n" +
                "\t\t\t\"pid\": \"1524d080-90df-451e-b157-76ba973187c8\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}, {\n" +
                "\t\t\t\"icon\": \"deployment-unit\",\n" +
                "\t\t\t\"id\": \"1524d080-90df-451e-b157-76ba973187c8\",\n" +
                "\t\t\t\"name\": \"开发运维\",\n" +
                "\t\t\t\"order\": 5,\n" +
                "\t\t\t\"path\": \"\",\n" +
                "\t\t\t\"pid\": \"\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}, {\n" +
                "\t\t\t\"icon\": \"deployment-unit\",\n" +
                "\t\t\t\"id\": \"ca4faf63-2836-4e2e-baba-28ccacc5edd3\",\n" +
                "\t\t\t\"name\": \"系统管理\",\n" +
                "\t\t\t\"order\": 6,\n" +
                "\t\t\t\"path\": \"\",\n" +
                "\t\t\t\"pid\": \"\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"icon\": \"setting\",\n" +
                "\t\t\t\"id\": \"4fc3e149-cedb-11e9-8b31-223e840cdc84\",\n" +
                "\t\t\t\"name\": \"门户订制\",\n" +
                "\t\t\t\"order\": 1,\n" +
                "\t\t\t\"path\": \"/customization\",\n" +
                "\t\t\t\"pid\": \"ca4faf63-2836-4e2e-baba-28ccacc5edd3\",\n" +
                "\t\t\t\"target\": \"0\"\n" +
                "\t\t}\n" +
                "\n" +
                "\t]";
        return JSONObject.parseArray(text, Menu.class);
    }
}
