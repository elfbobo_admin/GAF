package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authority.client.AuthAuthorizationClient;
import com.supermap.gaf.authority.commontype.AuthResourceMenu;
import com.supermap.gaf.extend.commontypes.Menu;
import com.supermap.gaf.extend.spi.UserResourcesService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wxl
 * @since 2022/4/7
 */

public class DefaultUserResourcesServiceProvider implements UserResourcesService {

    @Autowired
    private AuthAuthorizationClient authAuthorizationClient;

    @Override
    public List<Menu> listMenus() {
        List<AuthResourceMenu> menus = authAuthorizationClient.listAuthResourceMenus().checkAndGetData();
        List<Menu> result = menus.stream().map(this::convertFrom).collect(Collectors.toList());
        return result;
    }

    private Menu convertFrom(AuthResourceMenu authResourceMenu) {
        Menu menu = new Menu();
        menu.setIcon(authResourceMenu.getIcon());
        menu.setName(authResourceMenu.getName());
        menu.setOrder(authResourceMenu.getSortSn());
        menu.setPath(authResourceMenu.getPath());
        menu.setPid(authResourceMenu.getParentId());
        menu.setTarget(authResourceMenu.getTarget());
        menu.setId(authResourceMenu.getResourceMenuId());
        menu.setUrl(authResourceMenu.getUrl());
        return menu;
    }
}
