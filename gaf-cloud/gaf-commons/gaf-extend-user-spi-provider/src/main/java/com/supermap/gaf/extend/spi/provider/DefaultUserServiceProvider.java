package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.extend.commontypes.User;
import com.supermap.gaf.extend.spi.UserService;
import com.supermap.gaf.platform.client.AuthUserClient;
import com.supermap.gaf.security.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * @author wxl
 * @since 2022/4/7
 */

public class DefaultUserServiceProvider implements UserService {

    @Autowired
    private AuthUserClient authUserClient;


    @Override
    public String getUserName() {
        return RequestUtil.getUserName();
    }

    @Override
    public String getTenantId() {
        return RequestUtil.getTenantId();
    }

    @Override
    public String getUserId() {
        return RequestUtil.getUserId();
    }

    @Override
    public User getUser() {
        String userName = getUserName();
        AuthUser authUser = authUserClient.getByUsername(userName).checkAndGetData();
        Objects.requireNonNull(authUser);
        User user = new User(authUser.getUserId(),userName,authUser.getRealName());
        user.setExtension(authUser);
        return user;
    }

    @Override
    public String getToken() {
        return RequestUtil.getToken();
    }
}
