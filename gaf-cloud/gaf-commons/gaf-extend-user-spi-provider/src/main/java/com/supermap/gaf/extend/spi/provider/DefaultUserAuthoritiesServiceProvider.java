package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authority.client.AuthRoleClient;
import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.extend.commontypes.Role;
import com.supermap.gaf.extend.spi.UserAuthoritiesService;
import com.supermap.gaf.security.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wxl
 * @since 2022/4/7
 */

public class DefaultUserAuthoritiesServiceProvider implements UserAuthoritiesService {

    @Autowired
    private AuthRoleClient authRoleClient;


    @Override
    public List<Role> listRoles() {
        String userId = RequestUtil.getUserId();
        List<AuthRole> authRoles = authRoleClient.roleList(userId).checkAndGetData();
        return authRoles.stream().map(authRole -> new Role(authRole.getRoleId(), authRole.getNameEn(), authRole.getRoleName())).collect(Collectors.toList());
    }

}
