//package com.supermap.gaf.extend.util;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//
//
///**
// * 获取请求头中的信息
// * @author wxl
// * @since 2022/3/4
// */
//public class RequestUtil {
//
//    public static final String TENANT = "tenant";
//    public static final String JWT_USER_ID_FIELD = "user_id";
//    public static final String JWT_USER_NAME_FIELD = "user_name";
//    public static final String JWT_HEADER = "Authorization";
//    public static final String JWT_PREFIX = "Bearer";
//
//    public static String  getTenantId() {
//        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (requestAttributes == null) return null;
//        HttpServletRequest request = requestAttributes.getRequest();
//
//        Cookie[] cookies = request.getCookies();
//        String cookieVal = null;
//        if (cookies != null) {
//            for (Cookie cookie : cookies) {
//                if (TENANT.equals(cookie.getName())) {
//                    cookieVal = cookie.getValue();
//                    break;
//                }
//            }
//        }
//        return cookieVal;
//    }
//
//    /**
//     * 请求头中获取token,并解析成userId
//     * @return
//     */
//    public static String getUserId() {
//        return JJWTUtils.getFieldValueFromJwsUntrusted(RequestUtil.getToken(), JWT_USER_ID_FIELD);
//    }
//
//    /**
//     * 请求头中获取token,并解析成username
//     * @return
//     */
//    public static String getUserName() {
//        return JJWTUtils.getFieldValueFromJwsUntrusted(RequestUtil.getToken(), JWT_USER_NAME_FIELD);
//    }
//
//    public static String getToken() {
//        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (requestAttributes == null) return null;
//        HttpServletRequest request = requestAttributes.getRequest();
//        String authHeader = request.getHeader(JWT_HEADER);
//        if (StringUtils.isNoneEmpty(authHeader)) {
//            String jwtToken = authHeader.trim();
//            if (StringUtils.isNotEmpty(JWT_PREFIX)) {
//                String[] split = authHeader.trim().split("\\s+");
//                if (split != null && split.length == 2 && split[0].equalsIgnoreCase(JWT_PREFIX)) {
//                    jwtToken = split[1];
//                }
//            }
//            return jwtToken;
//        }
//        return null;
//    }
//}