package com.supermap.gaf.gateway.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.supermap.iportal.web.proxy.GeoUtil;
import com.supermap.services.components.commontypes.Geometry;
import com.supermap.services.components.commontypes.Point2D;
import com.supermap.services.components.commontypes.PrjCoordSys;
import com.supermap.services.components.commontypes.Rectangle2D;
import com.supermap.services.util.CoordinateConversionTool;
import com.supermap.services.util.PrjCoordSysConversionTool;
import com.supermap.services.util.TileTool;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.geotools.geometry.jts.GeometryBuilder;
import org.locationtech.jts.geom.Polygon;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class IserverProxyUtils {

    private static final Cache<String, JSONObject> MAP_INFO_CACHE
            = Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).initialCapacity(10).maximumSize(100).build();

    public static boolean needReplace(String contentType) {
        if (StringUtils.isBlank(contentType)) {
            return false;
        } else {
            String var1 = contentType.toLowerCase();
            if (var1.contains("img/")) {
                return false;
            } else if (var1.contains("application/")) {
                if (var1.contains("application/data")) {
                    return false;
                } else if (var1.contains("application/wkt")) {
                    return false;
                } else if (var1.contains("application/scv")) {
                    return false;
                } else if (var1.contains("application/scvb")) {
                    return false;
                } else if (var1.contains("application/scvd")) {
                    return false;
                } else if (var1.contains("application/sci")) {
                    return false;
                } else if (var1.contains("application/sci3d")) {
                    return false;
                } else if (var1.contains("application/sct")) {
                    return false;
                } else if (var1.contains("application/cf")) {
                    return false;
                } else if (var1.contains("application/sgz")) {
                    return false;
                } else if (var1.contains("application/sgm")) {
                    return false;
                } else if (var1.contains("application/lsl")) {
                    return false;
                } else if (var1.contains("application/bru")) {
                    return false;
                } else if (var1.contains("application/geom")) {
                    return false;
                } else if (var1.contains("application/slm")) {
                    return false;
                } else if (var1.contains("application/postscript")) {
                    return false;
                } else if (var1.contains("application/json")) {
                    return true;
                } else if (var1.contains("application/rjson")) {
                    return true;
                } else if (var1.contains("application/jsonp")) {
                    return true;
                } else if (var1.contains("application/fastjson")) {
                    return true;
                } else if (var1.contains("application/xml")) {
                    return true;
                } else if (var1.contains("application/kml")) {
                    return true;
                } else {
                    return var1.contains("application/georss");
                }
            } else if (var1.contains("text/")) {
                return !var1.contains("text/css");
            } else {
                return false;
            }
        }
    }

    public static boolean needAuth(String path) {
        boolean isStatic = path.matches(".*/static/.+");
        return !isStatic && !StringUtils.containsAny(path, new CharSequence[]{"iserver/services/security/profile", "favicon.ico", "iserver/_setup.json","/login.json"});
    }

    public static PrjCoordSys static_prjCoordSys;
    private static Polygon originalTilePolygon;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class OriginalPolygon {
        private Polygon originalPolygon;
        private PrjCoordSys originalPrjCoordSys;
    }

    public static void main(String[] args) {
        String s = "[{\"bounds\":{\"bottom\":-22.284452107242203,\"center\":{\"x\":20.555657193016838,\"y\":21.09438243256087},\"height\":86.75766907960615,\"left\":-28.881704601876358,\"leftBottom\":{\"x\":-28.881704601876358,\"y\":-22.284452107242203},\"right\":69.99301898791003,\"rightTop\":{\"x\":69.99301898791003,\"y\":64.47321697236394},\"top\":64.47321697236394,\"valid\":true,\"width\":98.87472358978638},\"center\":{\"x\":22.170137937642767,\"y\":21.09438243256087},\"id\":0,\"parts\":[4],\"points\":[{\"x\":69.99301898791003,\"y\":47.99413548871908},{\"x\":-28.881704601876358,\"y\":64.47321697236394},{\"x\":17.64758053872778,\"y\":-22.284452107242203},{\"x\":69.99301898791003,\"y\":47.99413548871908}],\"prjCoordSys\":{\"epsgCode\":0,\"type\":\"PCS_USER_DEFINED\"},\"type\":\"REGION\"}]";
        System.out.println(s);
    }

    /*public static void main(String[] args) {
        String url = "https://iserver.supermap.io/iserver/services/map-china400/rest/maps/China/tileImage.png?width=256&height=256&redirect=false&transparent=true&cacheEnabled=true&origin=%7B%22x%22%3A-20037508.342789248%2C%22y%22%3A20037508.342789087%7D&overlapDisplayed=false&scale=3.3803271432053056e-9&x=0&y=0";
        url = URLDecoder.decode(url);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(false);
        List<String> geometrysJson = Arrays.asList("{\"bounds\":{\"bottom\":-22.284452107242203,\"center\":{\"x\":20.555657193016838,\"y\":21.09438243256087},\"height\":86.75766907960615,\"left\":-28.881704601876358,\"leftBottom\":{\"x\":-28.881704601876358,\"y\":-22.284452107242203},\"right\":69.99301898791003,\"rightTop\":{\"x\":69.99301898791003,\"y\":64.47321697236394},\"top\":64.47321697236394,\"valid\":true,\"width\":98.87472358978638},\"center\":{\"x\":22.170137937642767,\"y\":21.09438243256087},\"id\":0,\"parts\":[4],\"points\":[{\"x\":69.99301898791003,\"y\":47.99413548871908},{\"x\":-28.881704601876358,\"y\":64.47321697236394},{\"x\":17.64758053872778,\"y\":-22.284452107242203},{\"x\":69.99301898791003,\"y\":47.99413548871908}],\"prjCoordSys\":{\"epsgCode\":0,\"type\":\"PCS_USER_DEFINED\"},\"type\":\"REGION\"}");
        try(OutputStream out = new FileOutputStream(new File("C:\\Users\\kb\\Desktop\\tmp\\test.png"));){
            clipImageByGeometrys(uriComponents.toUri(),"https://iserver.supermap.io/iserver/services/map-china400/rest/maps/China.json",MediaType.valueOf("image/png"),geometrysJson,out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    public static byte[] clipImageByGeometrys(byte[] source, MultiValueMap<String, String> urlParams, String mapJsonUrl, MediaType contentType, List<String> geometrysJson, String format) throws IOException {
        OriginalPolygon originalPolygon = originalTilePolygon(mapJsonUrl, urlParams);
        org.locationtech.jts.geom.Geometry spatialRange = null;
        if (!CollectionUtils.isEmpty(geometrysJson)) {
            List<Geometry> spatialRanges = geometrysJson.stream().map(item -> JSON.parseObject(item, Geometry.class)).collect(Collectors.toList());
            spatialRange = GeoUtil.unionGeometries(spatialRanges);
            spatialRange = GeoUtil.validate(spatialRange);
        }
        if (originalPolygon != null && spatialRange != null) {
            Polygon polygon1984 = conver2WGS1984(originalPolygon.getOriginalPrjCoordSys(), originalPolygon.getOriginalPolygon());
            if (spatialRange.contains(polygon1984)) {
                return source;
            }
            if (spatialRange.intersects(polygon1984)) {

                org.locationtech.jts.geom.Geometry clipedPologon = spatialRange.intersection(polygon1984);
                if (contentType.toString().toLowerCase().contains("application/mvt")) {
                    return GeoUtil.clipMvtTile(source, originalPolygon.getOriginalPolygon(), originalPolygon.getOriginalPrjCoordSys(), clipedPologon);
                } else if (contentType.getType().equalsIgnoreCase("image")) {
                    try (ByteArrayInputStream in = new ByteArrayInputStream(source)) {
                        BufferedImage bufferedImage = ImageIO.read(in);
                        return GeoUtil.clipTileImage(bufferedImage, format, originalPolygon.getOriginalPolygon(), originalPolygon.getOriginalPrjCoordSys(), clipedPologon);
                    }
                }
            }
            if (contentType.getType().equalsIgnoreCase("image")) {
                try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                    returnEmptyImage(urlParams, format, out);
                    return out.toByteArray();
                }
            }
        }
        return new byte[]{};
    }


    static void returnEmptyImage(MultiValueMap<String, String> urlParams, String format, OutputStream out) throws IOException {
        int width = urlParams.get("width") == null ? 256 : Integer.parseInt(urlParams.getFirst("width"));
        int height = urlParams.get("height") == null ? 256 : Integer.parseInt(urlParams.getFirst("height"));
        boolean transparent = BooleanUtils.toBoolean(urlParams.getFirst("transparent"));
        BufferedImage image = TileTool.getBlankImage(width, height, transparent);
        ImageIO.write(image, format, out);
    }


    // 获取originalTilePolygon
    public static OriginalPolygon originalTilePolygon(String mapUrl, MultiValueMap<String, String> urlParams) {
        Rectangle2D rectangle2D = null;
        double scale = urlParams.get("scale") == null ? -1.0D : Double.parseDouble(urlParams.getFirst("scale"));
        Long x = urlParams.get("x") == null ? null : Long.parseLong(urlParams.getFirst("x"));
        Long y = urlParams.get("y") == null ? null : Long.parseLong(urlParams.getFirst("y"));
        int width = urlParams.get("width") == null ? 256 : Integer.parseInt(urlParams.getFirst("width"));
        int height = urlParams.get("height") == null ? 256 : Integer.parseInt(urlParams.getFirst("height"));
        Map<String, Object> someInf = parseSomeInfoFromMapUrl(mapUrl, scale);
        if (urlParams.containsKey("scale") && urlParams.containsKey("x") && urlParams.containsKey("y")) {
            Point2D origin = urlParams.get("origin") == null ? null : JSON.parseObject(urlParams.getFirst("origin"), Point2D.class);
            if (StringUtils.isNotBlank(mapUrl) && scale != -1.0D && x != null && y != null) {
                double resolution = (double) someInf.get("scale"); // 获取瓦片缩放
                if (origin == null) {
                    origin = (Point2D) someInf.get("bounds");
                }

                if (origin != null) {
                    rectangle2D = TileTool.getBounds(x, y, resolution, width, height, origin);
                }
            }
        } else if (urlParams.containsKey("viewBounds")) {
            String viewBounds = urlParams.getFirst("viewBounds");
            if (StringUtils.isNotBlank(viewBounds)) {
                String[] arr = viewBounds.split(",");
                if (arr.length == 4) {
                    rectangle2D = new Rectangle2D(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]), Double.parseDouble(arr[2]), Double.parseDouble(arr[3]));
                }
            }
        } else {
            return null;
        }
        PrjCoordSys prjCoordSys = urlParams.get("prjCoordSys") == null ? null : JSON.parseObject(urlParams.getFirst("prjCoordSys"), PrjCoordSys.class);
        if (prjCoordSys != null) {
            prjCoordSys = PrjCoordSysConversionTool.getPrjCoordSys(prjCoordSys.epsgCode);
        }
        prjCoordSys = prjCoordSys == null ? (PrjCoordSys) someInf.get("prjCoordSys") : prjCoordSys;
        Polygon originalPolygon = (new GeometryBuilder()).box(rectangle2D.getLeft(), rectangle2D.getTop(), rectangle2D.getRight(), rectangle2D.getBottom());
        return new OriginalPolygon(originalPolygon, prjCoordSys);
    }


    protected static Map<String, Object> parseSomeInfoFromMapUrl(String mapUrl, double scale) {
        Double re = null;
        Map<String, Object> map = new HashMap<>();
        JSONObject mapInfo = MAP_INFO_CACHE.get(mapUrl, url -> {
            String mapDetail = new RestTemplate().getForObject(url, String.class);
            JSONObject jsonObject =  JSON.parseObject(mapDetail);
            JSONObject value = new JSONObject();
            value.put("viewBounds",jsonObject.get("viewBounds"));
            value.put("scale",jsonObject.get("scale"));
            value.put("prjCoordSys",jsonObject.get("prjCoordSys"));
            value.put("bounds",jsonObject.get("bounds"));
            value.put("viewer",jsonObject.get("viewer"));
            return value;
        });
        Rectangle2D viewBounds = mapInfo.getObject("viewBounds", Rectangle2D.class);
        if (viewBounds != null) {
            com.supermap.services.components.commontypes.Rectangle viewer = mapInfo.getObject("viewer", com.supermap.services.components.commontypes.Rectangle.class);
            if (viewer != null) {
                double var9 = viewBounds.width() / (double) viewer.getWidth();
                Double var11 = mapInfo.getDouble("scale");
                if (var11 != null) {
                    re = var9 * var11;
                }
            }
        }
        PrjCoordSys var12 = mapInfo.getObject("prjCoordSys", PrjCoordSys.class);
        map.put("prjCoordSys", var12);
        static_prjCoordSys = var12;
        Rectangle2D var13 = mapInfo.getObject("bounds", Rectangle2D.class);
        if (var13 != null) {
            map.put("bounds", new Point2D(var13.getLeft(), var13.getTop()));
        }
        map.put("scale", re == null ? -1.0D / scale : re / scale);
        return map;
    }

    // 保存原始请求空间范围对象与保存原始请求坐标系对象到setAttribute，并且转换Polygon为WGS1984坐标系
    public static Polygon conver2WGS1984(PrjCoordSys prjCoordSys, Polygon polygon) {
        Polygon re = polygon;
        if (polygon == null) {
            return re;
        }
        if (prjCoordSys != null) {
            prjCoordSys.epsgCode = prjCoordSys.epsgCode < 0 && prjCoordSys.type != null ? prjCoordSys.type.value() : prjCoordSys.epsgCode;
            if (!Arrays.asList(4326, 4490, 4214).contains(prjCoordSys.epsgCode)) {
                PrjCoordSys prjCoordSys1984 = PrjCoordSysConversionTool.getWGS1984();
                re = (Polygon) CoordinateConversionTool.convert(polygon, prjCoordSys, prjCoordSys1984);
            }
        }
        return re;
    }
}
