package com.supermap.gaf.extend.spi;

import com.supermap.gaf.authentication.entity.entity.AuthenticationParam;
import com.supermap.gaf.authentication.entity.entity.AuthenticationResult;

/**
 * 根据参数 校验认证状态，并获取认证后的 用户信息
 */
public interface ValidateAuthenticationService {
    /**
     *
     * 根据参数 校验认证状态，并获取认证后的 用户信息
     * @param authenticationParam 认证校验参数 包括shortCredential 较短的凭证 能够通过该凭证获取对应关联的用户信息或者token,例如http请求头中包含的token或者cookie中包含的自定义sessionId
     *                            以及jwtToken  可直接验签解析的jwt,例如http请求头中包含的jwt
     * @return 用户等信息 包括userId (用户id) username(用户名) jwtToken(jwt) tenantList(租户id列表)
     */
    AuthenticationResult validate(AuthenticationParam authenticationParam);

}
