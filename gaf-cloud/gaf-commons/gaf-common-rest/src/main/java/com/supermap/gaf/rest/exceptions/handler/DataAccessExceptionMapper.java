/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.rest.exceptions.handler;

import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.dao.DataAccessException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 *
 *
 *
 * */
public class DataAccessExceptionMapper implements ExceptionMapper<DataAccessException> {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(DataAccessExceptionMapper.class);

    @Override
    public Response toResponse(DataAccessException dataAccessException) {
        log.error("数据访问错误",dataAccessException);
        MessageResult<String> result = MessageResult.failed(String.class).message("数据访问错误,请查看日志").build();
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }
}
