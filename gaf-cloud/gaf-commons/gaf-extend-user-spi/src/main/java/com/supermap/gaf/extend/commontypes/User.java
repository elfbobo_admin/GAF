/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.extend.commontypes;

import java.io.Serializable;

/**
 * 用户信息
 *
 */
public class User  implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     *
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;


    /**
     * 别名
     */
    private String userAlias;

    /**
     *
     * 手机号
     *
     */
    private String mobileNumber;
    /**
     * 邮箱
     */
    private String email;
    /**
     *
     * 地址
     */
    private String address;

    /**
     * 扩展
     * 可用于扩展用户信息
     * 默认为 @see com.supermap.gaf.authority.commontypeAuthUser 对象
     */
    private Object extension;

    /**
     * 用于反射调用
     */
    protected User() {
    }

    public User(String userId,String userName,String userAlias) {
        this.userId = userId;
        this.userName = userName;
        this.userAlias = userAlias;
    }

    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     * 必须提供
     * @return
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     * 必须提供
     * @return
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 用户别名 或者 用户的真实名字
     * @return
     */
    public String getUserAlias() {
        return userAlias;
    }

    /**
     * 设置用户别名
     * 必须提供
     * @return
     */
    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * 设置手机号
     * 可选
     * @return
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     * 可选
     * @return
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    /**
     * 设置住址
     * 可选
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    public Object getExtension() {
        return extension;
    }

    public void setExtension(Object extension) {
        this.extension = extension;
    }

}
