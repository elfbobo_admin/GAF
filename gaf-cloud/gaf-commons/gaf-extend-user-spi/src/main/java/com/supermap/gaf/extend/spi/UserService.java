package com.supermap.gaf.extend.spi;


import com.supermap.gaf.extend.commontypes.User;


public interface UserService {

    /**
     * 获取当前用户id
     * @return
     */
    String getUserId();

    /**
     * 获取当前用户id
     * @return
     */
    String getUserName();

    /**
     * 获取当前用户信息
     * @return
     */
    User getUser();

    /**
     * 当前的租户id
     * @return
     */
    String getTenantId();

    /**
     * 获取token
     * @return
     */
    String getToken();
}
