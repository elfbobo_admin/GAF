package com.supermap.gaf.extend.commontypes;

/**
 * 菜单
 * id  pid name order  target 必须设置
 * icon 可以为空
 * 当作为菜单分组时，path 和 url 应该为空
 * @author wxl
 * @since 2022/4/12
 */
public class Menu {
    /**
     * 菜单id
     */
    private String id;
    /**
     * 父菜单id
     */
    private String pid;
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单打开方式
     * 当值为0 表示从当前页面内部打开菜单
     * 当值为1 表示外部链接,浏览器新标签页打开
     * 当作为菜单分组时，应为0
     */
    private String target;

    /**
     * 菜单路由路径
     * 当target为0时使用
     * 当该菜单作为菜单组使用时应为空
     */
    private String path;
    /**
     * 外链地址
     * 当target为1时使用
     * 当该菜单作为菜单组时,应为空
     */
    private String url;

    /**
     * 菜单图标
     * 暂时为ant-design-vue 图标组件的名字
     */
    private String icon;

    /**
     * 父菜单下的同级排序序号
     */
    private Integer order;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
