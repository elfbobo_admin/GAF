-- liquibase formatted sql logicalFilePath:base_tenant/data_range
-- changeset SYS:20220303-0
CREATE TABLE "data_range" (
  "data_range_id" varchar(36) NOT NULL,
  "data_standard_id" varchar(36) NOT NULL,
  "min" numeric NOT NULL,
  "max" numeric NOT NULL,
  "range_type" char(2) NOT NULL,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "data_range_pkey" PRIMARY KEY ("data_range_id")
)
;



COMMENT ON COLUMN "data_range"."data_range_id" IS '主键。';

COMMENT ON COLUMN "data_range"."data_standard_id" IS '数据标准id。';

COMMENT ON COLUMN "data_range"."min" IS '最小值';

COMMENT ON COLUMN "data_range"."max" IS '最大值';

COMMENT ON COLUMN "data_range"."range_type" IS '区间类型。CC:左闭右闭，OO:左开右开...';

COMMENT ON COLUMN "data_range"."created_time" IS '创建时间。';

COMMENT ON COLUMN "data_range"."created_by" IS '创建人。';

COMMENT ON COLUMN "data_range"."updated_time" IS '更新时间。';

COMMENT ON COLUMN "data_range"."updated_by" IS '更新人。';

COMMENT ON TABLE "data_range" IS '数据标准值域范围';