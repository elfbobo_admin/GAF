-- liquibase formatted sql logicalFilePath:base_tenant/auth_user_role
-- changeset SYS:20220303-0
CREATE TABLE "auth_user_role" (
  "user_role_id" varchar(36) NOT NULL,
  "user_id" varchar(36) NOT NULL,
  "role_id" varchar(36) NOT NULL,
  "status" bool DEFAULT true,
  "sort_sn" int4,
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "auth_user_role_pkey" PRIMARY KEY ("user_role_id")
)
;



COMMENT ON COLUMN "auth_user_role"."user_role_id" IS '用户角色id。主键,uuid';

COMMENT ON COLUMN "auth_user_role"."user_id" IS '用户id。';

COMMENT ON COLUMN "auth_user_role"."role_id" IS '角色id。';

COMMENT ON COLUMN "auth_user_role"."status" IS '状态。true:有效，false:无效';

COMMENT ON COLUMN "auth_user_role"."sort_sn" IS '排序序号。同级中的序号';

COMMENT ON COLUMN "auth_user_role"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "auth_user_role"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "auth_user_role"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "auth_user_role"."updated_by" IS '修改人。修改人user_id';

COMMENT ON TABLE "auth_user_role" IS '用户角色表';

INSERT INTO "auth_user_role" ("user_role_id", "user_id", "role_id", "status", "sort_sn", "created_time", "created_by", "updated_time", "updated_by") VALUES ('a42dd080-ec91-4176-82e8-ab00ab019162', 'user_000000', 'af213268-7527-4a91-a3ac-d6c7d98860d1', 't', NULL, '2022-08-05 06:56:47.825826', 'sys_admin', '2022-08-05 06:56:47.825826', 'sys_admin');
