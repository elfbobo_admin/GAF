-- liquibase formatted sql logicalFilePath:base_tenant/cache_build_history
-- changeset SYS:20220608-0
create table cache_build_history
(
    id                 varchar(36) not null constraint cache_build_history_pkey primary key,
    datasource_id      varchar(36),
    dataset_name       varchar(100),
    cache_type         varchar(50),
    tile_type          varchar(50),
    color              varchar(100),
    query_parameter    varchar(255),
    status             integer,
    error_message      text,
    created_time       timestamp,
    publish_history_id varchar(36) not null,
    is_simp            boolean,
    simplifying_rate   numeric(2),
    storage_name       varchar(100)
);

comment on table cache_build_history is '缓存切片历史';

comment on column cache_build_history.id is '主键。uuid';

comment on column cache_build_history.datasource_id is '数据源id';

comment on column cache_build_history.dataset_name is '数据集名';

comment on column cache_build_history.cache_type is '切片类型。increment(增量),fullVolume(全量)';

comment on column cache_build_history.tile_type is '瓦片类型(数据集类型).暂时支持GRID,IMAGE,MODEL,POINT,LINE,REGION';

comment on column cache_build_history.color is '颜色。发布时的参数设置，例如{"r":249,"g":31,"b":31,"a":100}';

comment on column cache_build_history.query_parameter is '增量切片查询条件';

comment on column cache_build_history.status is '状态。0 失败 1成功 2进行中';

comment on column cache_build_history.error_message is '错误信息';

comment on column cache_build_history.created_time is '创建时间';

comment on column cache_build_history.publish_history_id is '发布历史id。当未进行服务发布时，发布历史可用作连接信息，可用于存储缓存产物';

comment on column cache_build_history.is_simp is '是否轻量化.专用于模型MODE';

comment on column cache_build_history.simplifying_rate is '简化率(轻量化专属)';

comment on column cache_build_history.storage_name is '存储名';