-- liquibase formatted sql logicalFilePath:base_tenant/data_sync_log
-- changeset SYS:20220303-0
CREATE TABLE "data_sync_log" (
  "data_sync_log_id" varchar(36) NOT NULL,
  "data_sync_task_id" varchar(36) NOT NULL,
  "content" text NOT NULL,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "cost_time" int8 NOT NULL,
  "total_amount" int8,
  "result_status" char(1) DEFAULT 0,
  CONSTRAINT "data_sync_log_pkey" PRIMARY KEY ("data_sync_log_id")
)
;



COMMENT ON COLUMN "data_sync_log"."data_sync_log_id" IS '主键';

COMMENT ON COLUMN "data_sync_log"."data_sync_task_id" IS '所属任务';

COMMENT ON COLUMN "data_sync_log"."content" IS '记录日志';

COMMENT ON COLUMN "data_sync_log"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_sync_log"."created_by" IS '创建人';

COMMENT ON COLUMN "data_sync_log"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_sync_log"."updated_by" IS '更新人';

COMMENT ON COLUMN "data_sync_log"."cost_time" IS '耗费时间-秒';

COMMENT ON COLUMN "data_sync_log"."total_amount" IS '数据总量';

COMMENT ON COLUMN "data_sync_log"."result_status" IS '执行状态。0代表成功，1代表失败。';

COMMENT ON TABLE "data_sync_log" IS '数据同步日志表';