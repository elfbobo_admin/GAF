-- liquibase formatted sql logicalFilePath:base_tenant/auth_department
-- changeset SYS:20220303-0
CREATE TABLE "auth_department" (
  "department_id" varchar(36) NOT NULL,
  "parent_id" varchar(36) ,
  "sort_sn" int4,
  "department_type" varchar ,
  "department_name" varchar(255) NOT NULL,
  "name_en" varchar(255) ,
  "brief_name" varchar(255) ,
  "code" varchar(100) ,
  "status" bool DEFAULT true,
  "description" varchar(500) ,
  "admin_id" varchar(36) ,
  "is_third_party" bool,
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "auth_department_pkey" PRIMARY KEY ("department_id")
)
;



COMMENT ON COLUMN "auth_department"."department_id" IS '部门id。主键,uuid';

COMMENT ON COLUMN "auth_department"."parent_id" IS '父级id。';

COMMENT ON COLUMN "auth_department"."sort_sn" IS '排序序号。同级中的序号';

COMMENT ON COLUMN "auth_department"."department_type" IS '部门类型。存字典code,定义：一级管理、二级研发…';

COMMENT ON COLUMN "auth_department"."department_name" IS '名称。中文名称';

COMMENT ON COLUMN "auth_department"."name_en" IS '英文名称。';

COMMENT ON COLUMN "auth_department"."brief_name" IS '简称。';

COMMENT ON COLUMN "auth_department"."code" IS '编码。每级4位数字，同级递增';

COMMENT ON COLUMN "auth_department"."status" IS '状态。true:有效，false:过期';

COMMENT ON COLUMN "auth_department"."description" IS '描述。';

COMMENT ON COLUMN "auth_department"."admin_id" IS '管理员id。部门管理员（上级管理员指定，此字段暂不使用）';

COMMENT ON COLUMN "auth_department"."is_third_party" IS '是否第三方。父级为true则子部门继承父部门该值';

COMMENT ON COLUMN "auth_department"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "auth_department"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "auth_department"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "auth_department"."updated_by" IS '修改人。修改人user_id';

COMMENT ON TABLE "auth_department" IS '部门表';


-- changeset SYS:20220510-0
INSERT INTO "auth_department"("department_id", "parent_id", "sort_sn", "department_type", "department_name", "name_en", "brief_name", "code", "status", "description", "admin_id", "is_third_party", "created_time", "created_by", "updated_time", "updated_by") VALUES ('department_000000', '0', 1, '1', '内置部门', 'gaf', 'gaf', NULL, 't', '内置部门，勿删', NULL, NULL, '2020-10-29 22:42:01', 'SYS', '2021-04-01 02:21:32.365886', 'SYS');