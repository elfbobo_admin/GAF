-- liquibase formatted sql logicalFilePath:base_tenant/themes
-- changeset SYS:20220303-0
CREATE TABLE "themes" (
  "id" varchar(255) NOT NULL,
  "color" varchar(255) NOT NULL DEFAULT 'red'::character varying,
  "multipleTabs" bool,
  "user" varchar(255) NOT NULL DEFAULT ''::character varying,
  CONSTRAINT "themes_pkey" PRIMARY KEY ("id")
)
;



COMMENT ON TABLE "themes" IS '布局设置数据信息';