-- liquibase formatted sql logicalFilePath:base_tenant/tile
-- changeset SYS:20220303-0
CREATE TABLE "tile" (
  "tile_id" varchar(36) NOT NULL,
  "tile_name" varchar(255) NOT NULL,
  "tile_alias" varchar(255) NOT NULL,
  "source_type" varchar(50) NOT NULL,
  "source_info" varchar(700) NOT NULL,
  "description" varchar(500) ,
  "created_time" timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" varchar(100) ,
  "updated_time" timestamp(0),
  "updated_by" varchar(100) ,
  CONSTRAINT "pk_tile" PRIMARY KEY ("tile_id")
)
;



CREATE UNIQUE INDEX "tile_pk" ON "tile" USING btree (
  "tile_id" "pg_catalog"."text_ops" ASC NULLS LAST
);

COMMENT ON COLUMN "tile"."tile_id" IS '瓦片id';

COMMENT ON COLUMN "tile"."tile_name" IS '瓦片名';

COMMENT ON COLUMN "tile"."tile_alias" IS '瓦片别名';

COMMENT ON COLUMN "tile"."source_type" IS '瓦片来源类型.包括mongodb(MongoDB) ugcv5(UGCV5瓦片)';

COMMENT ON COLUMN "tile"."source_info" IS '瓦片来源信息.json字符串格式, 文件型 包括 上传到存储后的文件路径，例如{"path": "tiles/myxxxtiles/xxxx.sci"}, mongodb或者web型的应该包含数据库连接信息，服务地址 数据库 用户名 密码 ,以及 瓦片名,例如：{"addr": "ip:port","userName":"用户名","password","dbName":"test"}';

COMMENT ON COLUMN "tile"."description" IS '描述';

COMMENT ON COLUMN "tile"."created_time" IS '创建时间';

COMMENT ON COLUMN "tile"."created_by" IS '创建人';

COMMENT ON COLUMN "tile"."updated_time" IS '修改时间';

COMMENT ON COLUMN "tile"."updated_by" IS '修改人';

COMMENT ON TABLE "tile" IS '瓦片';