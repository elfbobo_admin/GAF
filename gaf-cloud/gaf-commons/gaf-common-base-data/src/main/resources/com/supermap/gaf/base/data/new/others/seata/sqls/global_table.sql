-- liquibase formatted sql logicalFilePath:others/seata/global_table
-- changeset SYS:20220409-0
CREATE TABLE IF NOT EXISTS public.global_table
(
    xid                       VARCHAR(128) NOT NULL,
    transaction_id            BIGINT,
    status                    SMALLINT     NOT NULL,
    application_id            VARCHAR(32),
    transaction_service_group VARCHAR(32),
    transaction_name          VARCHAR(128),
    timeout                   INT,
    begin_time                BIGINT,
    application_data          VARCHAR(2000),
    gmt_create                TIMESTAMP(0),
    gmt_modified              TIMESTAMP(0),
    CONSTRAINT pk_global_table PRIMARY KEY (xid)
    );

CREATE INDEX IF NOT EXISTS idx_status_gmt_modified ON public.global_table (status, gmt_modified);
CREATE INDEX IF NOT EXISTS idx_transaction_id ON public.global_table (transaction_id);
