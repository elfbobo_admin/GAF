-- liquibase formatted sql logicalFilePath:base_tenant/data_quality_program
-- changeset SYS:20220303-0
CREATE TABLE "data_quality_program" (
  "data_quality_program_id" varchar(36) NOT NULL,
  "data_quality_program_name" varchar(1000) NOT NULL,
  "data_quality_model_id" varchar(36) NOT NULL,
  "data_quality_cron" varchar(1000) ,
  "sort_sn" int2 DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "data_quality_program_pkey" PRIMARY KEY ("data_quality_program_id")
)
;



COMMENT ON COLUMN "data_quality_program"."data_quality_program_id" IS '主键';

COMMENT ON COLUMN "data_quality_program"."data_quality_program_name" IS '方案名称';

COMMENT ON COLUMN "data_quality_program"."data_quality_model_id" IS '方案关联的模型id';

COMMENT ON COLUMN "data_quality_program"."data_quality_cron" IS '方案定时执行cron字符串';

COMMENT ON COLUMN "data_quality_program"."sort_sn" IS '排序';

COMMENT ON COLUMN "data_quality_program"."description" IS '描述';

COMMENT ON COLUMN "data_quality_program"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_quality_program"."created_by" IS '创建人';

COMMENT ON COLUMN "data_quality_program"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_quality_program"."updated_by" IS '更新人';

COMMENT ON TABLE "data_quality_program" IS '数据质量管理-方案表';