-- liquibase formatted sql logicalFilePath:base_tenant/mm_physics
-- changeset SYS:20220303-0
CREATE TABLE "mm_physics" (
  "physics_id" varchar(36) NOT NULL,
  "physics_name" varchar(255) ,
  "table_id" varchar(36) NOT NULL,
  "datasource_id" varchar(36) NOT NULL,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) NOT NULL,
  CONSTRAINT "mm_physics_pkey" PRIMARY KEY ("physics_id")
)
;



COMMENT ON COLUMN "mm_physics"."physics_id" IS '主键';

COMMENT ON COLUMN "mm_physics"."physics_name" IS '物理表名';

COMMENT ON COLUMN "mm_physics"."table_id" IS '逻辑表id';

COMMENT ON COLUMN "mm_physics"."datasource_id" IS '数据源id';

COMMENT ON COLUMN "mm_physics"."created_time" IS '创建时间';

COMMENT ON COLUMN "mm_physics"."created_by" IS '创建人';

COMMENT ON COLUMN "mm_physics"."updated_time" IS '更新时间';

COMMENT ON COLUMN "mm_physics"."updated_by" IS '更新人';

COMMENT ON TABLE "mm_physics" IS '数据模型管理-物理表';