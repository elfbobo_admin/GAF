-- liquibase formatted sql logicalFilePath:base_tenant/auth_user_supplement
-- changeset SYS:20220309-0
CREATE TABLE public.auth_user_supplement (
	user_id varchar(36) PRIMARY KEY NOT NULL,
	user_name varchar(255) NOT NULL  constraint unique_username unique,
	department_id varchar(36) NULL,
	sort_sn int2 NULL,
	post_id varchar(36) NULL,
	created_time timestamp NOT NULL DEFAULT now(),
	created_by varchar(255) NULL,
	updated_time timestamp NULL,
	updated_by varchar(255) NULL
);
COMMENT ON TABLE public.auth_user_supplement IS '用户信息补充表';
COMMENT ON COLUMN public.auth_user_supplement.user_id IS '主键';
COMMENT ON COLUMN public.auth_user_supplement.user_name IS '用户名';
COMMENT ON COLUMN public.auth_user_supplement.department_id IS '部门id';
COMMENT ON COLUMN public.auth_user_supplement.sort_sn IS '排序序号。同级中的序号';
COMMENT ON COLUMN public.auth_user_supplement.post_id IS '岗位id';

COMMENT ON COLUMN public.auth_user_supplement.created_time IS '创建时间';
COMMENT ON COLUMN public.auth_user_supplement.created_by IS '创建人';
COMMENT ON COLUMN public.auth_user_supplement.updated_time IS '更新时间';
COMMENT ON COLUMN public.auth_user_supplement.updated_by IS '更新人';



-- changeset SYS:20220510-1
INSERT INTO auth_user_supplement (user_id, user_name, department_id, sort_sn, post_id, created_time, created_by, updated_time, updated_by) VALUES ('user_000000', 'sys_admin', 'department_000000', 1, null, '2022-05-06 03:36:23.123661', 'sys_admin', '2022-05-06 03:36:23.123661', 'sys_admin');