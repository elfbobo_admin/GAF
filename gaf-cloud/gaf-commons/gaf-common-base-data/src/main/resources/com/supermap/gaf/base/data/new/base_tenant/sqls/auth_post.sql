-- liquibase formatted sql logicalFilePath:base_tenant/auth_post
-- changeset SYS:20220303-0
CREATE TABLE "auth_post" (
  "post_id" varchar(36) NOT NULL,
  "department_id" varchar(36) NOT NULL,
  "sort_sn" int4,
  "post_name" varchar(255) NOT NULL,
  "name_en" varchar(255) ,
  "code" varchar(100) ,
  "status" bool DEFAULT true,
  "description" varchar(500) ,
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "auth_post_pkey" PRIMARY KEY ("post_id")
)
;


COMMENT ON COLUMN "auth_post"."post_id" IS '岗位id。主键,uuid';

COMMENT ON COLUMN "auth_post"."department_id" IS '部门id。';

COMMENT ON COLUMN "auth_post"."sort_sn" IS '排序序号。同级中的序号';

COMMENT ON COLUMN "auth_post"."post_name" IS '名称。中文名称';

COMMENT ON COLUMN "auth_post"."name_en" IS '英文名称。';

COMMENT ON COLUMN "auth_post"."code" IS '编码。部门编码+4位编码';

COMMENT ON COLUMN "auth_post"."status" IS '状态。true:有效，false:过期';

COMMENT ON COLUMN "auth_post"."description" IS '描述。';

COMMENT ON COLUMN "auth_post"."created_time" IS '创建时间。';

COMMENT ON COLUMN "auth_post"."created_by" IS '创建人。';

COMMENT ON COLUMN "auth_post"."updated_time" IS '修改时间。';

COMMENT ON COLUMN "auth_post"."updated_by" IS '修改人。';

COMMENT ON TABLE "auth_post" IS '岗位表';