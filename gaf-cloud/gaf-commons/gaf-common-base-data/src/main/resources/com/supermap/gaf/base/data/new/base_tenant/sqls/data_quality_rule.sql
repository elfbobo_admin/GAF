-- liquibase formatted sql logicalFilePath:base_tenant/data_quality_rule
-- changeset SYS:20220303-0
CREATE TABLE "data_quality_rule" (
  "data_quality_rule_id" varchar(36) NOT NULL,
  "data_quality_rule_name" varchar(1000) NOT NULL,
  "data_quality_model_id" varchar(36) NOT NULL,
  "type_code" varchar(255) NOT NULL,
  "importance" varchar(255) NOT NULL,
  "params" varchar(500) NOT NULL,
  "sort_sn" int2 DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "data_quality_rule_pkey" PRIMARY KEY ("data_quality_rule_id")
)
;



COMMENT ON COLUMN "data_quality_rule"."data_quality_rule_id" IS '主键';

COMMENT ON COLUMN "data_quality_rule"."data_quality_rule_name" IS '数据质量管理规则名称';

COMMENT ON COLUMN "data_quality_rule"."data_quality_model_id" IS '所属模型id';

COMMENT ON COLUMN "data_quality_rule"."type_code" IS '规则种类code';

COMMENT ON COLUMN "data_quality_rule"."importance" IS '严重级别。A:严重;B:重要;C:一般';

COMMENT ON COLUMN "data_quality_rule"."params" IS '参数';

COMMENT ON COLUMN "data_quality_rule"."sort_sn" IS '排序';

COMMENT ON COLUMN "data_quality_rule"."description" IS '描述';

COMMENT ON COLUMN "data_quality_rule"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_quality_rule"."created_by" IS '创建人';

COMMENT ON COLUMN "data_quality_rule"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_quality_rule"."updated_by" IS '更新人';

COMMENT ON TABLE "data_quality_rule" IS '数据质量管理-规则表';