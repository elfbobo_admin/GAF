-- liquibase formatted sql logicalFilePath:base_tenant/data_code_content
-- changeset SYS:20220303-0
CREATE TABLE "data_code_content" (
  "name" varchar(255) NOT NULL,
  "en_name" varchar(255) NOT NULL,
  "description" text ,
  "data_code_id" varchar(36) NOT NULL,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "data_code_content_id" varchar(36) NOT NULL,
  "value" varchar(255) NOT NULL,
  CONSTRAINT "data_code_num_pkey" PRIMARY KEY ("data_code_content_id")
)
;



COMMENT ON COLUMN "data_code_content"."name" IS '名称。';

COMMENT ON COLUMN "data_code_content"."en_name" IS '英文名。';

COMMENT ON COLUMN "data_code_content"."description" IS '描述。';

COMMENT ON COLUMN "data_code_content"."data_code_id" IS '数据代码id';

COMMENT ON COLUMN "data_code_content"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_code_content"."created_by" IS '创建人';

COMMENT ON COLUMN "data_code_content"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_code_content"."updated_by" IS '更新人';

COMMENT ON COLUMN "data_code_content"."data_code_content_id" IS '主键。';

COMMENT ON COLUMN "data_code_content"."value" IS '编码取值';

COMMENT ON TABLE "data_code_content" IS '数据代码编码';