-- liquibase formatted sql logicalFilePath:base_tenant/customation
-- changeset SYS:20220303-0
CREATE TABLE "customation" (
  "loginAdress" varchar(255) NOT NULL,
  "logoutAdress" varchar(255) NOT NULL,
  "profileAdress" varchar(255) NOT NULL,
  "logo" varchar(255) NOT NULL,
  "title" varchar(255) NOT NULL,
  "color" varchar(255) NOT NULL,
  "multipleTabs" bool NOT NULL,
  "user" varchar(255) ,
  "layoutType" varchar(255) NOT NULL,
  "configInfo" text ,
  "defaultConfigInfo" text ,
  "id" varchar(36) NOT NULL,
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "customation_pkey" PRIMARY KEY ("id")
)
;



COMMENT ON COLUMN "customation"."id" IS '订制id。主键,uuid';

COMMENT ON COLUMN "customation"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "customation"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "customation"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "customation"."updated_by" IS '修改人。修改人user_id';

COMMENT ON TABLE "customation" IS '门户定制';

INSERT INTO "customation"("loginAdress", "logoutAdress", "profileAdress", "logo", "title", "color", "multipleTabs", "user", "layoutType", "configInfo", "defaultConfigInfo", "id", "created_time", "created_by", "updated_time", "updated_by") VALUES ('/api/authentication/login/control', '/logout', '/authority/authuserinfo', './img/logo.png', 'GAF', '#323944', 't', 'sys_admin', 'topLeft', '[{"id":0.02516052020167825,"icon":"table","title":"栅格布局","name":"grid","columns":[{"title":"col1","name":"栅格1","span":16,"list":[{"id":0.8526584733003963,"icon":"ordered-list","title":"GAF一站式企业协同研发云","name":"PlatStep","attr":{"direction":"horizontal","mode":"define","url":"","nodes":[{"title":"需求","desc":" ","icon":"question"},{"title":"编码","desc":" ","icon":"apartment"},{"title":"测试","desc":" ","icon":"safety-certificate"},{"title":"发布","desc":" ","icon":"upload"},{"title":"反馈","desc":" ","icon":"team"}]}}],"id":0.28614559864034916},{"title":"col2","name":"栅格2","span":8,"list":[{"id":0.0516170777617837,"icon":"bars","title":"通知公告","name":"PlatList","attr":{"mode":"define","url":"","data":["2020年12月31日，GAF2.0发布","2020年7月31日，GAF1.0发布","12月31日，GAF平台β版本发布","5月18日，GAF1.0大数据开发帮助文档","4月18日，GAF1.0开发规范文档发布","2019年3月18日，GAF1.0版本第一次交流会通知"]}}],"id":0.13957135963375333}]},{"id":0.819758058528611,"icon":"table","title":"栅格布局","name":"grid","columns":[{"title":"col1","name":"栅格1","span":6,"list":[{"id":0.04730618160569566,"icon":"link","title":"快速导航","name":"PlatQuickLink","attr":{"icon":"cloud-upload","link":"http://gaf.supermap.pub/iportal","embed":false,"size":200,"title":"GIS应用门户"}}],"id":0.7451927839755528},{"title":"col2","name":"栅格2","span":6,"list":[{"id":0.5423831604146665,"icon":"link","title":"快速导航","name":"PlatQuickLink","attr":{"icon":"cloud-download","link":"http://gaf.supermap.pub/iserver","embed":false,"size":200,"title":"GIS应用服务器"}}],"id":0.3808417387221108},{"title":"col2","name":"栅格2","span":6,"list":[{"id":0.5445573393617607,"icon":"link","title":"快速导航","name":"PlatQuickLink","attr":{"icon":"dot-chart","link":"http://gaf.supermap.pub/report/DesignCenter","embed":false,"size":200,"title":"报表设计中心"}}],"id":0.2435911753595854},{"title":"col2","name":"栅格2","span":6,"list":[{"id":0.35609869407343253,"icon":"link","title":"快速导航","name":"PlatQuickLink","attr":{"icon":"radar-chart","link":"http://igaf.supermap.pub/map/view/index.html#/","embed":false,"size":200,"title":"地图应用"}}],"id":0.1922175204674701}]}]', NULL, '7c94f0c0-d27f-4402-b01b-83d74e2a48f2', '2020-12-17 11:17:03.618182', 'SYS', '2021-03-31 01:43:38.224164', 'SYS');
