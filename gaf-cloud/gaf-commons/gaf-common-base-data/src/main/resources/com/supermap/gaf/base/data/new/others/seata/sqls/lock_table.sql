-- liquibase formatted sql logicalFilePath:others/seata/lock_table
-- changeset SYS:20220409-0
CREATE TABLE IF NOT EXISTS public.lock_table
(
    row_key        VARCHAR(128) NOT NULL,
    xid            VARCHAR(128),
    transaction_id BIGINT,
    branch_id      BIGINT       NOT NULL,
    resource_id    VARCHAR(256),
    table_name     VARCHAR(32),
    pk             VARCHAR(36),
    status         SMALLINT     NOT NULL DEFAULT 0,
    gmt_create     TIMESTAMP(0),
    gmt_modified   TIMESTAMP(0),
    CONSTRAINT pk_lock_table PRIMARY KEY (row_key)
    );

comment on column public.lock_table.status is '0:locked ,1:rollbacking';
CREATE INDEX IF NOT EXISTS idx_branch_id ON public.lock_table (branch_id);
CREATE INDEX IF NOT EXISTS idx_status ON public.lock_table (status);
