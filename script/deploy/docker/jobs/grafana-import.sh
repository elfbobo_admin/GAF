flag=0; echo "wait for grafana..."; while [ $flag -le 0 ]; do
if test $(curl --insecure -I -m 10 -o /dev/null -s -w %{http_code} http://gaf-grafana:3000/grafana) == 200; then
    echo "grafana available";
    flag=1;
fi
done; 

file=datasource.json
echo "importing $file" &&
curl --insecure --silent --fail --show-error \
--request POST http://admin:admin@gaf-grafana:3000/grafana/api/datasources \
--header "Content-Type: application/json" \
--data-binary "@$file" ;
echo "" ;


for file in *-dashboard.json ; do
if [ -e "$file" ] ; then
    echo "importing $file" &&
    curl --insecure --silent --fail --show-error \
    --request POST http://admin:admin@gaf-grafana:3000/grafana/api/dashboards/import \
    --header "Content-Type: application/json" \
    --data-binary "@$file" ;
    echo "" ;
fi
done; 
