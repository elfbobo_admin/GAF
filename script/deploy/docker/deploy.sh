#!/usr/bin/env bash

#	COMMANDS命令:
#	all                             部署GAF应用(包括监控组件)
#	base                            部署基础GAF应用
#	monitor                         部署GAF的监控相关应用
#	boot                            部署基础GAF-BOOT应用
#使用说明
usage()
{
    cat << USAGE >&2
Usage:
	GAF应用部署脚本用法: $0 [COMMANDS] [OPTIONS]

	COMMANDS命令:
	all                             部署GAF应用和监控相关应用
	base                            部署GAF应用
	monitor                         部署GAF监控相关应用

	iserver                         部署iserver并挂载GAF存储

	remove                          删除GAF应用
	create                          部署GAF单个GAF应用
	recreate                        重新线上拉取镜像并部署单个GAF应用

	stop                            停止GAF应用容器
	start                           启动GAF应用容器
	restart                         重启GAF应用容器

	OPTIONS选项:
	-v                              执行删除命令时,如果存在持久化数据即删除,volume

	Example示例:
	$0 all                          部署GAF应用
	$0 remove && $0 all             删除所有已部署的GAF应用,不删除数据,并重新以老数据部署
	$0 remove -v && $0 all          删除所有已部署的GAF应用并删除数据,完全重新部署
	$0 remove -v <service>          删除指定的已部署GAF应用,并删除对应数据
	$0 create <service>             部署指定的GAF应用
	$0 recreate <service>           重新线上拉取镜像并部署单个GAF应用
	$0 stop <service>               停止已部署的GAF应用容器
	$0 start <service>              启动指定的GAF应用容器
USAGE
    exit 1
}

workspace() {
    #设置工作目录
    Root_Current_Dir=$(cd `dirname $0`;pwd)
    cd $Root_Current_Dir
    #命令文件权限
    find $Root_Current_Dir -name "*.sh"|xargs chmod +x
    #加载GAF函数
    . $Root_Current_Dir/bin/gaf-func.sh
    #加载环境变量
    . $Root_Current_Dir/config
    . $Root_Current_Dir/conf/GAF_ENV_CONFIG.env
    #[[ "$HOSTIP" == "localhost" ]] && echo "HOSTIP不能为localhost，请重新修改内网IP地址" && exit 1
    if [ "${HOSTIP}" == "localhost" ]; then
      HOSTIPNEW=$(ip route get 1 | awk '{print $NF;exit}')
      echo "未修改HOSTIP(主机ip)地址localhost,自动修改为:${HOSTIPNEW}"
      sed -i 's/'${HOSTIP}'/'$HOSTIPNEW'/g' $Root_Current_Dir/config
      . $Root_Current_Dir/config
    fi
    GAF_BASE_DATA_PATH=`readlink -f $GAF_BASE_DATA_PATH`
    #创建数据目录,拷贝配置文件
    mkdir -p ${GAF_VOL_DIR} && cp -rf $Root_Current_Dir/conf/GAF_ENV_CONFIG.env ${GAF_VOL_DIR}
    #校验配置文件正确性
    [[ ! -d "$GAF_BASE_DATA_PATH" ]] && echo "GAF_BASE_DATA_PATH基础数据目录路径不存在，请重新输入路径" && exit 1
    #校验命令
    check_commands docker docker-compose
    #替换GAF_ENV_CONFIG.env里的变量
    sed_config_env
}

docker_compose() {
    docker-compose --env-file $Root_Current_Dir/.all_config.env $*
}
create() {
    docker_compose up -d $*
}
create_wait() {
    docker_compose up -d $*
    wait_container_health $*
}
remove() {
    [[ $* == "-v" ]] || [[ -z "$*" ]] && docker_compose down $* || docker_compose rm -sf $*
    [[ $1 == "-v" ]] && docker volume prune -f --filter label=gaf=true
}
run_job() {
    . $Root_Current_Dir/jobs/$1
}
recreate() {
    docker_compose pull $*
    docker_compose up --force-recreate -d $*
}

base() {
    #合并config和GAF_ENV_CONFIG.env文件为.all_config.env
    merge_config
    #启动GAF基础环境数据存储应用
    create "gaf-postgres gaf-redis-server gaf-mq gaf-minio"
    #初始化postgres数据库基础数据
    run_job initializing_base_data.sh
    run_job initializing_gaf_data_mgt_iserver_config_data.sh
    #为gaf-storage单独创建gaf-storage数据库和表
    check_database_storage_exist
    check_database_storage_exist_form
    # 为seata-server 初始化数据库与表
    check_database_seata_exist
    check_database_seata_exist_from
    #启动GAF优先启动应用
    create_wait "gaf-microservice-rigister gaf-microservice-conf"
    create "gaf-seata-server"
    #启动其他GAF基础应用
    create_wait "gaf-microservice-api gaf-microservice-gateway gaf-sys-mgt  gaf-platform gaf-authentication gaf-authority gaf-microservice-governance gaf-portal gaf-map gaf-data-mgt gaf-storage gaf-project"
    create "gaf-docker"
    create "gaf-web"
    #启动GAF 对象存储及挂载
    run_job initializing_gaf_storage_default_data.sh

    create_wait gaf-s3fs-mount
    #提示
    echo "启动GAF成功！！！GAF地址：http://${HOSTIP}"
}

monitor() {
    #初始化postgres数据库监控组件基础数据
    run_job initializing_monitor_data.sh
    #启动GAF基础环境监控应用
    create "gaf-elasticsearch gaf-fluentd-es gaf-zipkin"
    create "gaf-cadvisor gaf-node-exporter gaf-prometheus gaf-grafana"
    #导入grafana数据
    run_job initializing_grafana_data.sh
    #导入监控日志的菜单
    run_job initializing_log_monitor_menu.sh
}

boot() {
    #启动GAF基础环境数据存储应用
    create "gaf-postgres gaf-redis gaf-minio gaf-s3fs-mount-forboot"
    #向postgres数据库导入基础数据
    run_job initializing_boot_data.sh
    #启动GAF-BOOT应用
    create "gaf-boot"
    #提示
    echo "启动GAF成功！！！GAF地址：http://${HOSTIP}"
}

iserver() {
  # 部署iserver并挂载
  create "iserver"
  echo "iserver地址：http://${HOSTIP}:8090"
  echo "iserver初始账号密码设置,请参考GAF启动后的开发运维下的微服务配置中心菜单,"
  echo "gaf-data-mgt下的hostservers.hostserversetting[0].username"
  echo "和hostservers.hostserversetting[0].password"
}

workspace

#命令判断
case "$1" in
"help")
	usage
;;
"all")
	base
	monitor
;;
"base")
    base
;;
"monitor")
    monitor
;;
"iserver")
	iserver
;;
"remove")
    shift
    remove $*
;;
"create")
    shift
    create $*
;;
"stop")
    docker_compose $*
;;
"start")
    docker_compose $*
;;
"restart")
    docker_compose $*
;;
"recreate")
    shift
    recreate $*
;;
*)
	usage
;;
esac


