# SuperMap GAF
SuperMap GIS Application Framework 超图GIS应用框架


---

## 介绍

SuperMap GAF（SuperMap GIS Application Framework，超图GIS应用框架）基于SuperMap GIS基础软件进行研发，是连接GIS基础软件与行业应用的重要纽带。帮助GIS应用开发商构建大中型GIS应用平台，支撑大规模空间数据和服务管理，支持大数据、分布式空间分析等，提供丰富的二三维地图场景展示、应用和扩展开发的GIS应用框架。标准化，降低GIS技术门槛，减轻开发和维护的工作量，提高GIS应用开发效率，提升GIS应用能力。

GAF采用微服务、云原生、持续集成等先进成熟的IT技术，实现从GIS基础软件到行业应用的快速开发，极大地缩短行业应用开发时间，持续提升行业应用技术先进性。通过开源协同的研发模式建立统一开发规范和开发框架，集成各应用单位及个人的经验成果，提高应用程序研发和项目实施效率。

## 代码仓库

- [Gitee](https://gitee.com/supermapgaf/GAF)
- [Github(镜像库)](https://github.com/SuperMap/GAF)

## 特性

 **丰富的GIS应用组件** 

面向业务封装开箱即用的数据管理、服务管理等功能模块，向应用开发者提供事件处理、基础分析、地形分析、裁剪分析、空间查询等高复用性二三维组件。

 **个性化地图应用** 

内置多种个性化地图应用模板，用户可灵活地设计地图样式，在线调用自定义地图样式并发布，面向业务场景自由搭配二三维分析组件，支持自选风格完成专题地图创建。

 **专业的GIS应用搭建** 

根据业务抽象出多套标准简洁的GIS应用模板，支持GIS应用的快速配置，并提供丰富的二次开发接口，支持GIS应用的深度定制，赋能应用开发者快速搭建自然资源、智慧城市等场景的GIS行业应用。

 **精细的权限控制** 

提供灵活、易扩展的权限控制，支持对功能、数据、服务权限的细粒度控制，实现集中授权管理、权限的全生命周期管理。

 **云原生/微服务** 

采用云原生/微服务架构，便于敏捷开发、水平扩展，保证服务的高可用性。

 **灵活的部署方式** 

提供单体/微服务两种部署方式，通过模块化按需部署，既可对已有系统进行技术升级，也能实现从0到1的系统搭建，从而提高多场景适应能力，便于根据项目实际情况灵活调配资源。

 **持续优化改进** 

具备成熟的社区反馈渠道，以专业化的开发、支持团队为后盾，提供及时的bug修复、功能迭代，使您无后顾之忧。


## 快速开始



### Step 1:构建部署前准备

- Linux系统-系统要求CentOS7.5
- Docker[[帮助]](script/deploy/docker/README.md#docker)-容器运行环境
- docker-compose[[帮助]](script/deploy/docker/README.md#docker-compose)-容器启动命令
- Git[[帮助]](script/deploy/docker/README.md#git)-用与克隆代码

### Step 2:克隆代码

`git clone http://USER:PASSWORD@code.gaf.net.cn/aprdc/gaf/gaf-pro.git GAF`

特别提示：GAF有两个代码仓库，

主代码仓库地址：https://gitee.com/supermapgaf/GAF

镜像仓库地址：https://github.com/SuperMap/GAF

若您要拉取代码(pull)、提交问题(new issue)和贡献代码（pull request），请到主代码仓库中操作，谢谢。


### Step 3:源码构建部署

#### 3.1.进入脚本文件目录

`cd GAF/script/deploy/docker`

#### 3.2.编辑配置文件

- 配置文件名称：`.env`
- 对配置文件进行查看修改，文件内部有参数修改说明


#### 3.3.构建GAF应用镜像

`./build.sh`

#### 3.4.部署GAF基础应用

`./deploy.sh base`

- GAF监控相关应用部署（可选）[[帮助]](script/deploy/docker/README.md#GAF-MONITOR) 

### Step 4:进入GAF
- 使用`docker ps`查看各个容器服务的状态,status都为Health时表明各服务都已成功运行部署
- 当全部容器health时，就可以在客户端通过浏览器访问部署完成后提示的GAF地址


## 在线演示

- 演示地址: http://gaf.net.cn/

- 管理员账号：联系群管理员
- 普通账号(仅可查看): guest 密码: SuperMap123


## 技术文档

[开发者指南](https://gitee.com/supermapgaf/GAF/wikis/GAF%20V3.0%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3)

## 开源协议
Apache Licence 2.0。

## 用户权益

- 对未经过授权和不遵循 Apache 2.0 协议二次开源或者商业化我们将追究到底。
     
- 产品交流qq群：463398333

























