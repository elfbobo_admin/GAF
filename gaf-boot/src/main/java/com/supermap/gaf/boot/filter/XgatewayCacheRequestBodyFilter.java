package com.supermap.gaf.boot.filter;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class XgatewayCacheRequestBodyFilter implements Filter {
    public static final String MY_CACHED_REQUEST_DATA = "myCachedRequestData";


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest originalRequest = (HttpServletRequest) servletRequest;
        if(XgatewayIserverFilter.needRequestBody(originalRequest)){
            HttpServletRequestWrapper requestWrapper = new HttpServletRequestWrapper(originalRequest){
                @Override
                public ServletInputStream getInputStream() throws IOException {
                    byte[] body = (byte[]) super.getAttribute(MY_CACHED_REQUEST_DATA);
                    if(body==null){
                        body = IOUtils.toByteArray(super.getInputStream(),super.getContentLength());
                        super.setAttribute(MY_CACHED_REQUEST_DATA,body);
                    }
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body);
                    return new ServletInputStream() {
                        @Override
                        public int read() throws IOException {
                            return byteArrayInputStream.read();
                        }

                        @Override
                        public boolean isFinished() {
                            return false;
                        }

                        @Override
                        public boolean isReady() {
                            return false;
                        }

                        @Override
                        public void setReadListener(ReadListener listener) {

                        }
                    };
                }
            };
            requestWrapper.getInputStream();
            filterChain.doFilter(requestWrapper,servletResponse);
            return;
        }
       filterChain.doFilter(servletRequest,servletResponse);
    }
}
