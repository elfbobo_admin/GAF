package com.supermap.gaf.boot.filter;

import com.supermap.gaf.extend.commontypes.AuthenticationParam;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.extend.spi.ValidateAuthentication;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.CUSTOM_LOGIN_SESSION_NAME;
import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

/**
 * 注意： 该代码对应gaf-microservice-gateway中的同名的filter,功能逻辑等应该保持一致
 * @author : wxl
 * @date 2021/4/17
 */
public class XgatewayAuthenticationQueryFilter implements Filter {

    private ValidateAuthentication validateAuthentication;



    public XgatewayAuthenticationQueryFilter() {
    }

    public XgatewayAuthenticationQueryFilter(ValidateAuthentication validateAuthentication) {
        this.validateAuthentication = validateAuthentication;
    }



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        ExchangeAuthenticationAttribute attribute = ((ExchangeAuthenticationAttribute) servletRequest.getAttribute(EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME));
        if (attribute.getIsPublicUrl()) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        AuthenticationParam param = extra(((HttpServletRequest) servletRequest));
        com.supermap.gaf.extend.commontypes.MessageResult<AuthenticationResult> mr = this.validateAuthentication.doValidateAuthentication(param);
        if (mr != null && mr.isSuccessed()) {
            attribute.setAuthenticationResult(mr.getData());
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private AuthenticationParam extra( HttpServletRequest request) {
        AuthenticationParam param = null;

        String authorization = request.getHeader(AUTHORIZATION);

        String cookieVal = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            Optional<String> cookieValueOpt = Arrays.stream(cookies).filter(cookie -> cookie.getName().equalsIgnoreCase(CUSTOM_LOGIN_SESSION_NAME)).map(Cookie::getValue).findFirst();
            if (cookieValueOpt.isPresent()) {
                cookieVal = cookieValueOpt.get();
            }
        }

        if (!StringUtils.isEmpty(cookieVal) || !StringUtils.isEmpty(authorization)) {
            param = new AuthenticationParam();
            param.setShortCredential(cookieVal);
            param.setJwtToken(authorization);
            return  param;
        }
        return null;
    }


    @Override
    public void destroy() {

    }
}
