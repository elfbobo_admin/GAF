package com.supermap.gaf.boot.util;

import javax.servlet.ServletException;
import java.io.IOException;

public interface ThrowExceptionRunnable{
    void run() throws IOException, ServletException;
}