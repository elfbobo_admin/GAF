import http from '@gaf/axios'
import {devProxy} from './proxy'

const isDev = process.env.NODE_ENV === 'development'
export default http(isDev, devProxy)
