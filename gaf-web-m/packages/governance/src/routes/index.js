import Home from '../pages/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Login.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Table.vue')
  },
  {
    path: '/config',
    name: 'config',
    component: () => import(/* webpackChunkName: "about" */ '../pages/config')
  },
  {
    path: '/logs',
    name: 'logs',
    component: () => import(/* webpackChunkName: "about" */ '../pages/logs')
  },
  {
    path: '/metrics',
    name: 'metrics',
    component: () => import(/* webpackChunkName: "about" */ '../pages/metrics')
  },
  {
    path: '/routes',
    name: 'routes',
    component: () => import(/* webpackChunkName: "about" */ '../pages/routes')
  },
  {
    path: '/trace',
    name: 'trace',
    component: () => import(/* webpackChunkName: "about" */ '../pages/trace')
  }
]

export default routes
